Première instalation
====================

Cron
----
Pour une première instalation, ajouter la ligne suivante au cron de la machine hôte.
Cela permet de redémarrer le container hébergeant les cron et de purger 
les processus zombies restant sur la machine hôte. Pas de meilleur solution
existe pour le moment avec docker compose.

0 */3 * * * cd /var/www/hexapat && /usr/local/bin/docker-compose restart cron

Logrotate
---------
Mettre en place le fichier de configuration :
cp /var/www/hexapat/docker/logrotate/hpa /etc/logrotate.d/

Suivre les instruction à l'interieur de ce fichier.


Mise à jour d'une instance existante
====================================
Pour une mise à jour de HPA, lancer la commande suivante :
. install.sh {sous-domaine uniquement}

Par exemple : 
. install.sh demo
ou
. install.sh laboratoire-bioardaisne

Migration de la base de données
===============================
Pour une migration standard, se connecter au container et lancer la commande suivante :
php bin/console doctrine:migration:migrate

Install.sh
==========

    * magalie.chauvineau@agfa.com
        - IP: 185.113.160.36
        - Role: support
    * Unilabs
        - nicolas.lester@unilabs.com: 185.3.26.253
        - nicolas.lester@unilabs.fr: 185.3.26.251
    	