<?php
namespace Agfa\UserBundle\Entity;

use FOS\UserBundle\Model\User as BaseUser;
use Scheb\TwoFactorBundle\Model\Email\TwoFactorInterface;
use Scheb\TwoFactorBundle\Model\TrustedComputerInterface;
use Symfony\Component\HttpFoundation\HeaderBag;
use Symfony\Component\HttpFoundation\Request;

/**
 * User
 */
class User extends BaseUser implements TwoFactorInterface, TrustedComputerInterface
{
    const ROLE_DEBUG = 'ROLE_DEBUG';

    const ROLE_SUPER_ADMIN = 'ROLE_SUPER_ADMIN';

    const ROLE_ADMIN = 'ROLE_ADMIN';

    const ROLE_SUPPORT = 'ROLE_SUPPORT';

    const ROLE_LABO = 'ROLE_LABO';

    /**
     *
     * @var integer
     */
    protected $id;

    /**
     *
     * @var string
     */
    private $nbFailedLoginAttempt = 0;

    /**
     *
     * @var string
     */
    private $authCode;

    /**
     *
     * @var array
     */
    private $trusted;

    /**
     *
     * @var \Doctrine\Common\Collections\Collection
     */
    private $personnes;

    /**
     *
     * @var \Doctrine\Common\Collections\Collection
     */
    private $patients;

    /**
     *
     * @var boolean
     */
    private $hasUnassignedPatient = FALSE;

    /**
     *
     * @var boolean
     */
    private $paymentIsEnabled = FALSE;

    /**
     *
     * @var \Agfa\HpaBundle\Entity\Personne
     */
    private $currentPersonne;

    /**
     *
     * @var \Agfa\HpaBundle\Entity\Journal
     */
    private $journals;

    /**
     *
     * @var \Agfa\HpaBundle\Entity\Enrolement
     */
    private $enrolement;

    /**
     *
     * @var string
     */
    private $registrationId;

    /**
     *
     * @var integer
     */
    private $dob;

    /**
     *
     * @var array
     */
    private $firstEnrollmentDetails;

    /**
     *
     * @var \DateTime
     */
    private $createdAt;

    /**
     *
     * @var \DateTime
     */
    private $updatedAt;

    /**
     * Constructor
     */
    public function __construct()
    {
        parent::__construct();
        $this->journals = new \Doctrine\Common\Collections\ArrayCollection();
        $this->personnes = new \Doctrine\Common\Collections\ArrayCollection();
        $this->patients = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * on surcharge setEmail pour que l'utilisateur n'ait pas besoin de saisir un username
     */
    public function setEmail($email)
    {
        parent::setEmail($email);
        if (! empty($this->getEmail())) {
            $this->setUsername($email);
        }

        return $this;
    }

    /**
     *
     * {@inheritdoc}
     *
     * @see \Scheb\TwoFactorBundle\Model\Email\TwoFactorInterface::getEmail()
     */
    public function getEmail()
    {
        return $this->email;
    }

    public function isEmailAuthEnabled()
    {
        return true;
    }

    public function setEmailAuthCode($authCode)
    {
        $this->authCode = $authCode;

        return $this;
    }

    public function getEmailAuthCode()
    {
        return $this->authCode;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return User
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set authCode
     *
     * @param integer $authCode
     * @return User
     */
    public function setAuthCode($authCode)
    {
        $this->authCode = $authCode;

        return $this;
    }

    /**
     * Get authCode
     *
     * @return integer
     */
    public function getAuthCode()
    {
        return $this->authCode;
    }

    public function addTrustedComputer($token, \DateTime $validUntil, Request $request)
    {
        $now = new \DateTime();

        $headers = $request->headers;

        $this->trusted[$token] = [
            'valid-until' => $validUntil->format('r'),
            'last-login' => $now->format('r'),
            'user-agent' => $headers->get('User-Agent'),
            'ip' => $request->getClientIp()
        ];
    }

    public function removeTrustedComputer($token)
    {
        if (key_exists($token, $this->trusted)) {
            unset($this->trusted[$token]);
            return TRUE;
        }
        return FALSE;
    }

    public function isTrustedComputer($token)
    {
        if (isset($this->trusted[$token])) {
            $now = new \DateTime();
            $validUntil = new \DateTime($this->trusted[$token]['valid-until']);
            if ($now < $validUntil) {
                $this->trusted[$token]['last-login'] = $now->format('r');
                return true;
            }
        }
        return false;
    }

    public function getTrustedComputers()
    {
        return $this->trusted;
    }

    /**
     * Add personnes
     *
     * @param \Agfa\HpaBundle\Entity\Personne $personnes
     * @return User
     */
    public function addPersonne(\Agfa\HpaBundle\Entity\Personne $personnes)
    {
        $this->personnes[] = $personnes;

        return $this;
    }

    /**
     * Remove personnes
     *
     * @param \Agfa\HpaBundle\Entity\Personne $personnes
     */
    public function removePersonne(\Agfa\HpaBundle\Entity\Personne $personnes)
    {
        $this->personnes->removeElement($personnes);
    }

    /**
     * Get personnes
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPersonnes()
    {
        return $this->personnes;
    }

    /**
     * Add patients
     *
     * @param \Agfa\HpaBundle\Entity\Patient $patients
     * @return User
     */
    public function addPatient(\Agfa\HpaBundle\Entity\Patient $patients)
    {
        $this->patients[] = $patients;

        return $this;
    }

    /**
     * Remove patients
     *
     * @param \Agfa\HpaBundle\Entity\Patient $patients
     */
    public function removePatient(\Agfa\HpaBundle\Entity\Patient $patients)
    {
        $this->patients->removeElement($patients);
    }

    /**
     * Get patients
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPatients()
    {
        return $this->patients;
    }

    /**
     * Set trusted
     *
     * @param array $trusted
     * @return User
     */
    public function setTrusted($trusted)
    {
        $this->trusted = $trusted;

        return $this;
    }

    /**
     * Get trusted
     *
     * @return array
     */
    public function getTrusted()
    {
        return $this->trusted;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return User
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set currentPersonne
     *
     * @param \Agfa\HpaBundle\Entity\Personne $currentPersonne
     *
     * @return User
     */
    public function setCurrentPersonne(\Agfa\HpaBundle\Entity\Personne $currentPersonne = null)
    {
        $this->currentPersonne = $currentPersonne;

        return $this;
    }

    /**
     * Get currentPersonne
     *
     * @return \Agfa\HpaBundle\Entity\Personne
     */
    public function getCurrentPersonne()
    {
        return $this->currentPersonne;
    }

    /**
     * Set hasUnassignedPatient
     *
     * @param boolean $hasUnassignedPatient
     *
     * @return User
     */
    public function setHasUnassignedPatient($hasUnassignedPatient)
    {
        $this->hasUnassignedPatient = $hasUnassignedPatient;

        return $this;
    }

    /**
     * Get hasUnassignedPatient
     *
     * @return boolean
     */
    public function getHasUnassignedPatient()
    {
        return $this->hasUnassignedPatient;
    }

    /**
     * Set paymentIsEnabled
     *
     * @param boolean $paymentIsEnabled
     *
     * @return User
     */
    public function setPaymentIsEnabled($paymentIsEnabled)
    {
        $this->paymentIsEnabled = $paymentIsEnabled;

        return $this;
    }

    /**
     * Get paymentIsEnabled
     *
     * @return boolean
     */
    public function getPaymentIsEnabled()
    {
        return $this->paymentIsEnabled;
    }

    /**
     * Add journal
     *
     * @param \Agfa\HpaBundle\Entity\Journal $journal
     *
     * @return User
     */
    public function addJournal(\Agfa\HpaBundle\Entity\Journal $journal)
    {
        $this->journals[] = $journal;

        return $this;
    }

    /**
     * Remove journal
     *
     * @param \Agfa\HpaBundle\Entity\Journal $journal
     */
    public function removeJournal(\Agfa\HpaBundle\Entity\Journal $journal)
    {
        $this->journals->removeElement($journal);
    }

    /**
     * Get journals
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getJournals()
    {
        return $this->journals;
    }

    /**
     * Add enrolement
     *
     * @param \Agfa\HpaBundle\Entity\Enrolement $enrolement
     *
     * @return User
     */
    public function addEnrolement(\Agfa\HpaBundle\Entity\Enrolement $enrolement)
    {
        $this->enrolement[] = $enrolement;

        return $this;
    }

    /**
     * Remove enrolement
     *
     * @param \Agfa\HpaBundle\Entity\Enrolement $enrolement
     */
    public function removeEnrolement(\Agfa\HpaBundle\Entity\Enrolement $enrolement)
    {
        $this->enrolement->removeElement($enrolement);
    }

    /**
     * Get enrolement
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getEnrolement()
    {
        return $this->enrolement;
    }

    /**
     * Set nbFailedLoginAttempt
     *
     * @param integer $nbFailedLoginAttempt
     *
     * @return User
     */
    public function setNbFailedLoginAttempt($nbFailedLoginAttempt)
    {
        $this->nbFailedLoginAttempt = $nbFailedLoginAttempt;

        return $this;
    }

    /**
     * Get nbFailedLoginAttempt
     *
     * @return integer
     */
    public function getNbFailedLoginAttempt()
    {
        return $this->nbFailedLoginAttempt;
    }

    /**
     *
     * @return mixed
     */
    public function getRegistrationId()
    {
        return $this->registrationId;
    }

    /**
     *
     * @return mixed
     */
    public function getDob()
    {
        return $this->dob;
    }

    /**
     *
     * @param mixed $registrationId
     */
    public function setRegistrationId($registrationId)
    {
        $this->registrationId = $registrationId;
    }

    /**
     *
     * @param mixed $dob
     */
    public function setDob($dob)
    {
        $this->dob = $dob;
    }

    /**
     * Set firstEnrollmentDetails
     *
     * @param array $firstEnrollmentDetails
     *
     * @return User
     */
    public function setFirstEnrollmentDetails($firstEnrollmentDetails)
    {
        $this->firstEnrollmentDetails = $firstEnrollmentDetails;

        return $this;
    }

    /**
     * Get firstEnrollmentDetails
     *
     * @return array
     */
    public function getFirstEnrollmentDetails()
    {
        return $this->firstEnrollmentDetails;
    }
}
