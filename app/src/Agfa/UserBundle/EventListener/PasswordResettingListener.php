<?php

namespace Agfa\UserBundle\EventListener;

use FOS\UserBundle\FOSUserEvents;
use FOS\UserBundle\Event\FormEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

/**
 * Service pour gérer la modification du mot de passe
 * @author awopm
 * @property \Doctrine\ORM\EntityManager em
 * @property \Symfony\Component\Security\Core\SecurityContext $securityContext
 */
class PasswordResettingListener implements EventSubscriberInterface
{
    private $router;

    public function __construct(UrlGeneratorInterface $router)
    {
        $this->router = $router;
    }

    /**
     * {@inheritDoc}
     */
    public static function getSubscribedEvents()
    {
        return [
                FOSUserEvents::RESETTING_RESET_SUCCESS => 'onPasswordResettingSuccess'
        ];
    }

    public function onPasswordResettingSuccess(FormEvent $event)
    {
        $url = $this->router->generate('default_index');
        $event->setResponse(new RedirectResponse($url));
    }
}