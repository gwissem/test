<?php
namespace Agfa\UserBundle\EventListener;

use FOS\UserBundle\FOSUserEvents;
use FOS\UserBundle\Event\FilterUserResponseEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Agfa\HpaBundle\Entity\Enrolement;
use Agfa\HpaBundle\Services\Demandes;

/**
 * Listener responsible to change the redirection at the end of the password resetting
 */
class RegistrationConfirmedListener implements EventSubscriberInterface
{

    public function __construct(Demandes $demandesService)
    {
        $this->demandesService = $demandesService;
    }

    /**
     *
     * {@inheritdoc}
     */
    public static function getSubscribedEvents()
    {
        return array(
            FOSUserEvents::REGISTRATION_CONFIRMED => 'onRegistrationConfirmedSuccess'
        );
    }

    public function onRegistrationConfirmedSuccess(FilterUserResponseEvent $event)
    {
        $user = $event->getUser();

        $enrollmentDetails = $user->getFirstEnrollmentDetails();

        if(!empty($enrollmentDetails)){
            // Enrol the user
            $enrolement = new Enrolement();
            $enrolement->setLogin($enrollmentDetails['registrationId']);
            $enrolement->setPassword($enrollmentDetails['dob']);

            // Retrieve the patient record or display the reason why it could not be retrieved
            $this->demandesService->retrieveDemande($user, $enrolement);

            $user->setFirstEnrollmentDetails(null);
        }

        return;
    }
}