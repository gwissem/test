<?php
namespace Agfa\UserBundle\EventListener;

use FOS\UserBundle\Model\UserInterface;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Agfa\HpaBundle\Services\Journalist;
use Symfony\Component\HttpFoundation\Session\Session;
use Agfa\HpaBundle\Entity\Journal;
use Agfa\HpaBundle\Manager\PatientManager;
use UAParser\Parser;
use Symfony\Component\HttpFoundation\RequestStack;
use Doctrine\ORM\EntityManager;
use Agfa\HpaBundle\Manager\JournalManager;

/**
 *
 * @author charles
 */
class InteractiveLoginListener implements EventSubscriberInterface
{

    public function __construct(Journalist $journalist, Session $session, PatientManager $patientMgr, RequestStack $requestStack, EntityManager $em, JournalManager $jouralMgr, $journalNbEntriesToKeep, $journalNbMonthToKeep)
    {
        $this->journal = $journalist;
        $this->session = $session;
        $this->patientMgr = $patientMgr;
        $this->request = $requestStack->getCurrentRequest();
        $this->em = $em;
        $this->journalMgr = $jouralMgr;
        $this->nbEntriesToKeep = $journalNbEntriesToKeep;
        $this->nbMonthToKeep = $journalNbMonthToKeep;
    }

    /**
     *
     * {@inheritdoc}
     *
     */
    public static function getSubscribedEvents()
    {
        return [
            'security.interactive_login' => 'onSecurityInteractiveLogin'
        ];
    }

    public function onSecurityInteractiveLogin(InteractiveLoginEvent $event)
    {
        $user = $event->getAuthenticationToken()->getUser();

        if ($user instanceof UserInterface) {

            $events = $this->journal->getEntry($user, Journal::EVENT_USER_LOGIN, 2);

            // Only keep x entries in the journal
            $this->journalMgr->getAllButLastEntries($user, $this->nbEntriesToKeep, $this->nbMonthToKeep);

            /* @var $last_login Journal */
            $last_login = array_shift($events);

            // TODO Check if the following line is really used. I doubt it.
            if ($last_login) {
                $this->session->set('last_login', $last_login->getCreatedAt());
            }

            // Clear expired devices
            $count = 0;
            $now = new \DateTime();
            $trustedComputers = $user->getTrustedComputers();

            foreach ($trustedComputers as $token => $trustedComputer) {
                $validUntil = new \DateTime($trustedComputer['valid-until']);
                if ($validUntil < $now) {
                    $user->removeTrustedComputer($token);
                    $count ++;
                }
            }
            $this->em->flush();

            // Add more informations about the user who logs in. For security reasons.
            $uaParser = Parser::create();
            $userAgent = $uaParser->parse($this->request->headers->get('User-Agent'));

            $this->journal->addSuccess(\Agfa\HpaBundle\Entity\Journal::EVENT_USER_LOGIN, $user, $user, "Connexion au compte utilisateur", array(
                'user_agent' => array(
                    'hardware' => $userAgent->ua->toString(),
                    'software' => $userAgent->os->toString()
                )
            ));
        }
    }
}
