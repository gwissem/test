<?php

namespace Agfa\UserBundle\EventListener;

use FOS\UserBundle\FOSUserEvents;
use FOS\UserBundle\Event\FormEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

/**
 *
 * @author awopm
 * @property \Doctrine\ORM\EntityManager em
 * @property \Symfony\Component\Security\Core\SecurityContext $securityContext
 */
class ChangePasswordListener implements EventSubscriberInterface
{
    private $em;

    public function __construct(UrlGeneratorInterface $router)
    {
        $this->router = $router;
    }

    /**
     * {@inheritDoc}
     */
    public static function getSubscribedEvents()
    {
        return [
        		FOSUserEvents::CHANGE_PASSWORD_SUCCESS => 'onChangePasswordSuccess'
        ];
    }

    public function onChangePasswordSuccess(FormEvent $event)
    {
    	$url = $this->router->generate('configuration_securite');

    	$event->setResponse(new RedirectResponse($url));
    }
}