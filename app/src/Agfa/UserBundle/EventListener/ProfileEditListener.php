<?php

namespace Agfa\UserBundle\EventListener;

use FOS\UserBundle\FOSUserEvents;
use FOS\UserBundle\Event\FormEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

/**
 *
 * @author awopm
 * @property \Doctrine\ORM\EntityManager em
 * @property \Symfony\Component\Security\Core\SecurityContext $securityContext
 */
class ProfileEditListener implements EventSubscriberInterface
{
    private $em;

    public function __construct(UrlGeneratorInterface $router)
    {
        $this->router = $router;
    }

    /**
     * {@inheritDoc}
     */
    public static function getSubscribedEvents()
    {
        return [
        		FOSUserEvents::PROFILE_EDIT_SUCCESS => 'onProfileEditSuccess'
        ];
    }

    public function onProfileEditSuccess(FormEvent $event)
    {
    	$url = $this->router->generate('configuration_securite');

    	$event->setResponse(new RedirectResponse($url));
    }
}