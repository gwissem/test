<?php
namespace Agfa\UserBundle\EventListener;

use Symfony\Component\Security\Core\AuthenticationEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Security\Core\Event\AuthenticationFailureEvent;
use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Bundle\FrameworkBundle\Routing\Router;
use FOS\UserBundle\Util\TokenGenerator;
use Symfony\Bundle\TwigBundle\TwigEngine;
use Agfa\HpaBundle\Services\Journalist;
use Agfa\HpaBundle\Entity\Journal;
use Symfony\Bundle\FrameworkBundle\Translation\Translator;

/**
 * Add 1 to failed_login_attempt if the authentication was unsuccessful.
 *
 * @author charles
 *
 */
class AuthenticationFailureListener implements EventSubscriberInterface
{

    public function __construct(Translator $translator, Session $session, EntityManager $em, Router $router, TokenGenerator $tokenGenerator, \Swift_Mailer $mailer, TwigEngine $templating, Journalist $journal, $maxLoginAttempts, $clientEmail)
    {
        $this->translator = $translator;
        $this->session = $session;
        $this->em = $em;
        $this->router = $router;
        $this->tokenGenerator = $tokenGenerator;
        $this->mailer = $mailer;
        $this->templating = $templating;
        $this->journal = $journal;

        $this->maxLoginAttempts = $maxLoginAttempts;
        $this->clientEmail = $clientEmail;
    }

    public static function getSubscribedEvents()
    {
        return array(
            AuthenticationEvents::AUTHENTICATION_FAILURE => 'onAuthenticationFailure'
        );
    }

    public function onAuthenticationFailure(AuthenticationFailureEvent $event)
    {
        $token = $event->getAuthenticationToken();

        $username = $token->getUsername();

        // Be carreful: if the username of the user is not the same as its email, one can try using the
        // email to brute force the account without this listener being triggered.
        $user = $this->em->getRepository('AgfaUserBundle:User')->findOneBy(array(
            'username' => $username
        ));

        if ($user && $user->isEnabled()) {
            $nbFailedLoginAttempt = $user->getNbFailedLoginAttempt();
            $nbLoginAttemptsLeft = $this->maxLoginAttempts-1 - $nbFailedLoginAttempt;

            if ($nbLoginAttemptsLeft > 0) {
                // Add 1 to failed number attempts to log in
                $user->setNbFailedLoginAttempt($nbFailedLoginAttempt + 1);

                // Let the user know of what is happening

                $this->session->getFlashBag()->add('danger', sprintf('%s %s', $this->translator->transChoice('agfa_hpa.authentication_listener', $nbLoginAttemptsLeft), $this->translator->trans('agfa_hpa.authentication_listener.unlock')));

            } elseif ($nbLoginAttemptsLeft == 0) {
                // Disable the account
                $user->setEnabled(FALSE);

                // Make the email with the token in order to unlock the account
                $user->setConfirmationToken($this->tokenGenerator->generateToken());

                $url = $this->router->generate('fos_user_registration_confirm', array('token' => $user->getConfirmationToken()), Router::ABSOLUTE_URL);

                // Create the mail mssg
                $message = \Swift_Message::newInstance()
                    ->setSubject('Protection de votre compte - Verrouillage')
                    ->setFrom($this->clientEmail)
                    ->setTo($user->getEmail())
                    ->setContentType('text/html')
                    ->setBody(
                        $this->templating->render(
                            ":email:locked-account.html.twig", array(
                                'user' => $user,
                                'maxLoginAttempts' => $this->maxLoginAttempts,
                                'confirmationUrl' => $url))
                        );
                $this->mailer->send($message);

                // Add an entry to journal
                $this->journal->addWarning(Journal::EVENT_USER_LOCKED_ACCOUNT, $user, $user, sprintf("Plus de %s tentatives d'accès.", $this->maxLoginAttempts));

                $this->session->getFlashBag()->add('danger', "Ce compte est bloqué. Veuillez confirmer votre identité en validant le lien que vous avez reçu par courriel.");
            }

            $this->em->flush($user);
        }
    }
}