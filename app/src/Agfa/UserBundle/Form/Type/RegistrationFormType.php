<?php
namespace Agfa\UserBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Agfa\HpaBundle\Form\DataTransformer\BsrvrPasswordTransformer;
use Symfony\Component\Validator\Constraints as Assert;

class RegistrationFormType extends AbstractType
{

    private $transformer;

    public function __construct(BsrvrPasswordTransformer $transformer)
    {
        $this->transformer = $transformer;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);
        $builder->remove('username');

        // Add form fields to retrieve the CR
        $builder->add('registrationId', TextType::class, array(
            'constraints' => new Assert\Regex(array('pattern' => '/^(\d{4}[a-z0-9]\d{9,11})$|^((19|20)\d{2})(0?[1-9]|1[012])(0[1-9]|[123][0-9])([0-9]*5)$|^[0-9]{9}\-[0-9][a-z][0-9]{10,12}$|^([0-9]{4}[a-z][0-9]{5}[a-z0-9][0-9]{4})$|^([0-9]{9}\-[0-9][a-z][0-9]{5}[a-z0-9][0-9]{4})$|^([a-z][0-9]{10})$|^(test)$/i', 'message' => 'L\'identifiant indiqué n\'a pas le bon format. Merci d\'indiquer l\'identifiant fourni par votre laboratoire. Exemple : 4693B7040300001')),
            'label' => 'agfa.fosuser_bundle.registration.form.registration_id'
        ));
        $builder->add('dob', TextType::class, array(
            'label' => 'agfa.fosuser_bundle.registration.form.dob'
        ));

        $builder->get('dob')->addModelTransformer($this->transformer);
    }

    public function getParent()
    {
        return 'FOS\UserBundle\Form\Type\RegistrationFormType';
    }

    public function getBlockPrefix()
    {
        return 'agfa_user_registration';
    }
}