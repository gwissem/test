<?php

namespace Agfa\UserBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class AgfaUserBundle extends Bundle
{
	public function getParent()
	{
		return 'FOSUserBundle';
	}
}
