<?php
namespace Agfa\UserBundle\Mailer;

use Scheb\TwoFactorBundle\Model\Email\TwoFactorInterface;
use Scheb\TwoFactorBundle\Mailer\AuthCodeMailerInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class AuthCodeMailer implements AuthCodeMailerInterface
{

    public function __construct($sender_email, $sender_name, $mailer, $router, \Twig_Environment $twig, $logger)
    {
        $this->sender_email = $sender_email;
        $this->sender_name = $sender_name;
        $this->mailer = $mailer;
        $this->router = $router;
        $this->twig = $twig;
        $this->logger = $logger;
    }

    public function sendAuthCode(TwoFactorInterface $user)
    {
        $authCode = $user->getEmailAuthCode();
        $authLink = $this->router->generate('default_index', [
            '_auth_code' => $authCode
        ], UrlGeneratorInterface::ABSOLUTE_URL);

        $email_plain = $this->twig->render(':Authentication:auth_code_plain.txt.twig', [
            'email' => $user->getEmail(),
            'auth_code' => $authCode,
            'auth_link' => $authLink
        ]);

        $email_html = $this->twig->render(':Authentication:auth_code_email.html.twig', [
            'email' => $user->getEmail(),
            'auth_code' => $authCode,
            'auth_link' => $authLink
        ]);

        $message = new \Swift_Message();
        $message->setTo($user->getEmail())
            ->setFrom([
            $this->sender_email => $this->sender_name
        ])
            ->setSubject("Code de sécurité")
            ->setBody($email_plain)
            ->addPart($email_html, 'text/html');
        $this->mailer->send($message);
    }
}