<?php
namespace Agfa\UserBundle\Manager;

use Doctrine\ORM\EntityManager;
use FOS\UserBundle\Util\CanonicalizerInterface;
use FOS\UserBundle\Doctrine\UserManager as FosUserManager;

class UserManager extends FosUserManager
{

    protected $em;

    public function __construct(EntityManager $em, CanonicalizerInterface $canonicalizer)
    {
        $this->em = $em;
        $this->canonicalizer = $canonicalizer;
    }

    public function getRepository()
    {
        return $this->em->getRepository('AgfaUserBundle:User');
    }

    public function getUsers($limit = null)
    {
        return $this->getRepository()
            ->getUsers($limit)
            ->getQuery()
            ->useQueryCache(true)
            ->getResult();
    }

    public function countUsers($searchTerms = null)
    {
        // Pour garder le signe '+' contenu dans certaines adresse email
        $searchTerms = rawurldecode($searchTerms);
        $searchTerms = $this->canonicalizer->canonicalize($searchTerms);

        return $this->getRepository()
            ->countUsers($searchTerms)
            ->getQuery()
            ->useQueryCache(true)
            ->getSingleScalarResult();
    }

    public function getUsersByEmail($searchTerms = null)
    {
        // Pour garder le signe '+' contenu dans certaines adresse email
        $searchTerms = rawurldecode($searchTerms);
        $searchTerms = $this->canonicalizer->canonicalize($searchTerms);

        return $this->getRepository()
            ->getUsersByEmail($searchTerms)
            ->getQuery()
            ->useQueryCache(true)
            ->getResult();
    }
}