<?php

namespace Agfa\UserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

/**
 * @Route("/trusted-computer")
 * @author charles
 *
 */
class TrustedComputerController extends Controller
{
    /**
     * @Route("/remove", name="remove_trusted_computer")
     * @Template(":TrustedComputer:remove")
     * @Method("POST")
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
	public function removeAction(Request $request)
	{
		/* @var $user \Agfa\UserBundle\Entity\User */
		$user = $this->getUser();

		$token = $request->request->get('token');

		$removed = $user->removeTrustedComputer($token);

		$this->getDoctrine()->getManager()->flush();

		return new Response(json_encode($removed));
	}
}