<?php
namespace Agfa\UserBundle\Controller;

use FOS\UserBundle\FOSUserEvents;
use FOS\UserBundle\Event\GetResponseUserEvent;
use FOS\UserBundle\Event\FilterUserResponseEvent;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use FOS\UserBundle\Controller\RegistrationController as BaseController;
use FOS\UserBundle\Event\FormEvent;
use Agfa\HpaBundle\Entity\Enrolement;

class RegistrationController extends BaseController
{

    /**
     *
     * @param Request $request
     *
     * @return Response
     */
    public function registerAction(Request $request)
    {
        $data = array();
        $em = $this->getDoctrine()->getManager();
        $comBsrvrService = $this->get('communication_bioserveur');

        /** @var $formFactory FactoryInterface */
        $formFactory = $this->get('fos_user.registration.form.factory');
        /** @var $userManager UserManagerInterface */
        $userManager = $this->get('fos_user.user_manager');
        /** @var $dispatcher EventDispatcherInterface */
        $dispatcher = $this->get('event_dispatcher');

        $user = $userManager->createUser();
        $user->setEnabled(true);

        $event = new GetResponseUserEvent($user, $request);
        $dispatcher->dispatch(FOSUserEvents::REGISTRATION_INITIALIZE, $event);

        if (null !== $event->getResponse()) {
            return $event->getResponse();
        }

        $form = $formFactory->createForm();
        $form->setData($user);

        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                // BOC - Retrieve the CR from BioServeur, stop all upon error
                $translator = $this->get('translator.default');
                $enrolement = new Enrolement();

                $data['registrationId'] = $form->get('registrationId')->getData();
                $data['dob'] = $form->get('dob')->getData();

                $enrolement->setLogin($data['registrationId']);
                $enrolement->setPassword($data['dob']);

                // Check that the enrollment is possible before creating the user account and adding the patient record
                $result = $comBsrvrService->getPatientRecord($enrolement);

                // No need to check if it is formated as an editCr: with 'creer_enrolement' set to false, it will be formated as pdf.
                if(FALSE === $comBsrvrService->prIsAvailableFromBsrvr()){
                    $this->addFlash('warning', "Votre compte rendu n'a pas été trouvé. Peut-être n'est il pas encore disponible ? Si vous avez été averti de sa disponibilité par votre laboratoire, merci de corriger une éventuelle erreur de saisie (identifiant ou date de naissance); sinon merci de prendre contact avec ce dernier.");

                    $bsrvrConstLogin = substr($result->compte_rendu->identifiant_structure, 0, 4) . $result->compte_rendu->identifiant_patient;
                    $this->get('logger')->error(sprintf("Patient record not found (login) : %s", $data['registrationId']));

                    return $this->render('@FOSUser/Registration/register.html.twig', array(
                        'form' => $form->createView()
                    ));
                }

                // Prevent accessing a patient record that has already been downloaded by an other user.
                if(FALSE === $comBsrvrService->prIsNew($result->compte_rendu->identifiant_structure, $result->compte_rendu->identifiant_patient, $result->compte_rendu->numero_demande)){
                    $this->addFlash('danger', $translator->trans('agfa.hpa_bundle.enrolment.consultation_direct.already_owned'));

                    $bsrvrConstLogin = substr($result->compte_rendu->identifiant_structure, 0, 4) . $result->compte_rendu->identifiant_patient;
                    $this->get('logger')->error(sprintf("Patient record already linked to an account (const. login / nodemx) : %s / %s", $bsrvrConstLogin, $result->compte_rendu->numero_demande));

                    return $this->render('@FOSUser/Registration/register.html.twig', array(
                        'form' => $form->createView()
                    ));
                }

                $em->persist($enrolement);

                $registrationDetails = array('registrationId' => $data['registrationId'], 'dob' => $data['dob']);
                $user->setFirstEnrollmentDetails($registrationDetails);
                // EOC - Retrieve the CR from BioServeur, stop all upon error

                $event = new FormEvent($form, $request);
                $dispatcher->dispatch(FOSUserEvents::REGISTRATION_SUCCESS, $event);

                $userManager->updateUser($user);

                if (null === $response = $event->getResponse()) {
                    $url = $this->generateUrl('fos_user_registration_confirmed');
                    $response = new RedirectResponse($url);
                }

                $dispatcher->dispatch(FOSUserEvents::REGISTRATION_COMPLETED, new FilterUserResponseEvent($user, $request, $response));

                return $response;
            }

            $event = new FormEvent($form, $request);
            $dispatcher->dispatch(FOSUserEvents::REGISTRATION_FAILURE, $event);

            if (null !== $response = $event->getResponse()) {
                return $response;
            }
        }

        return $this->render('@FOSUser/Registration/register.html.twig', array(
            'form' => $form->createView()
        ));
    }

    /**
     * Quand on ne trouve pas l'utilisateur, c'est que ce dernier a déjà cliqué sur le lien de confirmation pour son adresse email.
     * On lui indique et le redirigige vers l'accueil.
     *
     * @author Cédric Bertolini
     */
    public function confirmAction(Request $request, $token)
    {
        /** @var $userManager \FOS\UserBundle\Model\UserManagerInterface */
        $userManager = $this->get('fos_user.user_manager');

        $user = $userManager->findUserByConfirmationToken($token);

        if (null === $user) {
            $this->addFlash('warning', 'Ce lien a déjà été utilisé.');
            return new RedirectResponse($this->container->get('router')->generate('default_index'));
        }

        /** @var $dispatcher \Symfony\Component\EventDispatcher\EventDispatcherInterface */
        $dispatcher = $this->get('event_dispatcher');

        $user->setConfirmationToken(null);
        $user->setEnabled(true);

        $event = new GetResponseUserEvent($user, $request);
        $dispatcher->dispatch(FOSUserEvents::REGISTRATION_CONFIRM, $event);

        $userManager->updateUser($user);

        if (null === $response = $event->getResponse()) {
            $url = $this->generateUrl('fos_user_registration_confirmed');
            $response = new RedirectResponse($url);
        }

        $dispatcher->dispatch(FOSUserEvents::REGISTRATION_CONFIRMED, new FilterUserResponseEvent($user, $request, $response));

        return $response;
    }
}
