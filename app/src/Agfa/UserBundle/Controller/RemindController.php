<?php

namespace Agfa\UserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class RemindController extends Controller
{
    /**
     * @Route("/remind/{email}", name="remind_email")
     * @Template(":Remind:remind")
     *
     * @param string $email
     * @throws NotFoundHttpException
     * @return \Symfony\Component\HttpFoundation\Response|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function remindAction($email)
    {
        $user = $this->get('fos_user.user_manager')->findUserByEmail($email);
        if(!$user) {
        	throw new NotFoundHttpException("Cannot find user from email [$email]");
        }
        if(!$user->getConfirmationToken()) {
        	return $this->render(':Remind:no-token.html.twig');
        }

        $this->get('fos_user.mailer')->sendConfirmationEmailMessage($user);

        $this->get('session')->set('fos_user_send_confirmation_email/email', $user->getEmail());

        $url = $this->get('router')->generate('fos_user_registration_check_email');
        return $this->redirect($url);
    }
}