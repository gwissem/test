<?php

namespace Agfa\UserBundle\Security;

use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserChecker as UserCheckerParent;
use Symfony\Component\HttpFoundation\Session\Session;
use Doctrine\ORM\EntityManager;

class UserChecker extends UserCheckerParent
{
    public function __construct(Session $session, EntityManager $em){
        $this->session = $session;
        $this->em = $em;
    }


    public function checkPreAuth(UserInterface $user)
    {
        parent::checkPreAuth($user);

//         if($user->getNbFailedLoginAttempt() > 0){
//             $this->session->getFlashBag()->add('alert', 'Nope!');
//         }

    }

    public function checkPostAuth(UserInterface $user)
    {
        parent::checkPostAuth($user);

        $user->setNbFailedLoginAttempt(0);
        $this->em->flush($user);
    }
}