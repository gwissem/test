<?php
namespace Agfa\UserBundle\Security;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Routing\Router;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Security\Http\Logout\LogoutSuccessHandlerInterface;
use Agfa\HpaBundle\Services\Configuration;

class LogoutHandler implements LogoutSuccessHandlerInterface
{

    public function __construct(Router $router, Configuration $configuration)
    {
        $this->router = $router;
        $this->urlRedirection = $configuration->getLogoutRedirection();
    }

    public function onLogoutSuccess(Request $request)
    {
        $response = new RedirectResponse($this->router->generate('default_index'));

        if ($this->urlRedirection) {
            $response = new RedirectResponse($this->urlRedirection);
        }

        return $response;
    }
}

