<?php
namespace Agfa\HpaBundle\Controller;

use Agfa\HpaBundle\Entity\Personne;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use Agfa\HpaBundle\Entity\Journal;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

/**
 * Personne controller.
 *
 * @Route("personne")
 */
class PersonneController extends BaseController
{

    /**
     * Lists all personne entities.
     *
     * @Route("/", name="personne_index")
     *
     * @Method ("GET")
     */
    public function indexAction()
    {
        $personnes = $this->get('agfa_hpa.personne_manager')->getDemandes($this->getUser());
        $patientsResult = array();
        $totalDemandes = array();

        foreach($personnes as $newpersonne){
            foreach($newpersonne->getPersonnePatients() as $PersonnePatients){
                $patient = $PersonnePatients->getPatient();
                $patientsResult[$newpersonne->getId()][$patient->getId()]['nom_complet'] = $patient->getNomComplet();
                $patientsResult[$newpersonne->getId()][$patient->getId()]['patient_id'] = $patient->getId();

                // FIXME The $patient->getDemandes() creates lazy loading. Probably because of the leftJoin on demandes...?
                foreach($patient->getDemandes() as $demande){
                    $labo = $demande->getEtablissement();
                    $totalDemandes[$newpersonne->getId()][$patient->getId()][$labo->getId()]['nb_demandes'] = isset($totalDemandes[$newpersonne->getId()][$patient->getId()][$labo->getId()]['nb_demandes']) ? $totalDemandes[$newpersonne->getId()][$patient->getId()][$labo->getId()]['nb_demandes']+1 : 1;
                    $totalDemandes[$newpersonne->getId()][$patient->getId()][$labo->getId()]['name'] = $labo->getNom();
                    $totalDemandes[$newpersonne->getId()][$patient->getId()][$labo->getId()]['ville'] = $labo->getVille();
                }
            }
        }

        return $this->render('personne/index.html.twig', array(
            'personnes' => $personnes,
            'newPersonnes' => $patientsResult,
            'labos' => $totalDemandes
        ));
    }

    /**
     * Creates a new personne entity.
     *
     * @Route("/new", name="personne_new")
     *
     * @Method ({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        // A user that impersonate an other user can't perform this
        if($this->isGranted('ROLE_PREVIOUS_ADMIN')){
            $this->addFlash('danger', "Vous ne pouvez pas <strong>modifier</strong> les données d'un patient.");
            $this->get('logger')->addInfo(sprintf("Tentative de modification d'une personne d'un autre utilisateur par un admin (%s)", $this->getUser()->getEmail()));
            return $this->redirectToRoute('personne_index');
        }

        $personne = new Personne();
        $form = $this->createForm('Agfa\HpaBundle\Form\PersonneType', $personne);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($personne);
            $em->flush();

            return $this->redirectToRoute('personne_index');
        }

        return $this->render('personne/new.html.twig', array(
            'personne' => $personne,
            'form' => $form->createView()
        ));
    }

    // /**
    // * Finds and displays a personne entity.
    // *
    // * @Route("/{personneId}", name="personne_show")
    // * @Method("GET")
    // */
    // public function showAction($personneId)
    // {
    // $personne = $this->get('agfa_hpa.personne_manager')->getPersonne($this->getUser(), $personneId);
    // $deleteForm = $this->createDeleteForm($personne);

    // return $this->render('personne/show.html.twig', array(
    // 'personne' => $personne,
    // 'delete_form' => $deleteForm->createView(),
    // ));
    // }

    /**
     * Displays a form to edit an existing personne entity.
     *
     * @Route("/{personneId}/edit", name="personne_edit")
     *
     * @Method ({"GET", "POST"})
     */
    public function editAction(Request $request, $personneId)
    {
        // A user that impersonate an other user can't perform this
        if($this->isGranted('ROLE_PREVIOUS_ADMIN')){
            $this->addFlash('danger', "Vous ne pouvez pas <strong>modifier</strong> les données d'un patient.");
            $this->get('logger')->addInfo(sprintf("Tentative de modification d'une personne d'un autre utilisateur par un admin (%s)", $this->getUser()->getEmail()));
            return $this->redirectToRoute('personne_index');
        }

        $personne = $this->get('agfa_hpa.personne_manager')->getPersonne($this->getUser(), $personneId);
        $personneBeforeUpdate = clone ($personne);

        $deleteForm = $this->createDeleteForm($personne);

        $editForm = $this->createForm('Agfa\HpaBundle\Form\PersonneType', $personne);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {

            $this->addFlash('success', 'Le nom ou/et prénom de la personne ont bien été mis à jour.');
            $this->get('journalist')->addWarning(Journal::EVENT_PERSONNE_MODIFICATION, $personne, $this->getUser(), sprintf('%s devient %s', $personneBeforeUpdate->getNomComplet(), $personne->getNomComplet()));

            $this->getDoctrine()
                ->getManager()
                ->flush();

            return $this->redirectToRoute('personne_index');
        }

        return $this->render('personne/edit.html.twig', array(
            'personne' => $personne,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView()
        ));
    }

    /**
     * Deletes a personne entity.
     *
     * @Route("/{personneId}", name="personne_delete")
     *
     * @Method ("DELETE")
     */
    public function deleteAction(Request $request, $personneId)
    {
        // A user that impersonate an other user can't perform this
        if($this->isGranted('ROLE_PREVIOUS_ADMIN')){
            $this->addFlash('danger', "Vous ne pouvez pas <strong>supprimer</strong> les données d'un patient.");
            $this->get('logger')->addInfo(sprintf("Tentative de suppression d'une personne d'un autre utilisateur par un admin (%s)", $this->getUser()->getEmail()));
            return $this->redirectToRoute('personne_index');
        }

        $personne = $this->get('agfa_hpa.personne_manager')->getPersonne($this->getUser(), $personneId);

        $form = $this->createDeleteForm($personne);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            // Removes the personne.
            // Checks if the personne has patient. If this is the case, a message is shown and the personne is not deleted until all patient are deleted before.
            if (($nbPatient = count($this->get('agfa_hpa.personne_patient_manager')->getPersonneIsUsed($this->getUser(), $personneId))) == 0) {
                $this->get('journalist')->addWarning(Journal::EVENT_PERSONNE_SUPPRESSION, $personne, $this->getUser(), sprintf('%s', $personne->getNomComplet()));

                $em->remove($personne);
                $em->flush();

                $this->addFlash('success', $this->trans('agfa.hpa_bundle.personne.delete.success', array('%personneName%' => $personne->getNomComplet())));

            } else {
                $this->addFlash('danger', $this->transChoice('agfa.hpa_bundle.personne.delete.fail', $nbPatient, array('%personneName%' => $personne->getNomComplet())));
                return $this->redirectToRoute('personne_edit', array('personneId' => $personneId));
            }
        }

        return $this->redirectToRoute('personne_index');
    }

    /**
     * Creates a form to delete a personne entity.
     *
     * @param Personne $personne
     *            The personne entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Personne $personne)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('personne_delete', array(
            'personneId' => $personne->getId()
        )))
            ->setMethod('DELETE')
            ->add('submit', SubmitType::class, array(
                'label' => ' Supprimer',
                'button_class' => 'danger'
            ))
            ->getForm();
    }
}
