<?php
namespace Agfa\HpaBundle\Controller\Admin;

use Agfa\HpaBundle\Entity\Journal;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Agfa\UserBundle\Entity\User;

/**
 * Journal controller.
 *
 * @Route("admin/user/{user}/journal/", requirements={"user":"\d+"})
 */
class JournalController extends Controller
{

    /**
     * Lists all journal entities.
     *
     * @Route("", name="admin_journal")
     *
     * @Method ("GET")
     */
    public function indexAction(User $user)
    {
        $journals = $this->get('agfa_hpa.journal_manager')->getUserEntries($user);

        return $this->render('Admin/journal/index.html.twig', array(
            'journals' => $journals,
            'user' => $user
        ));
    }

    /**
     * Finds and displays a journal entity.
     *
     * @Route("{id}/", name="admin_journal_show")
     *
     * @Method ("GET")
     */
    public function showAction(User $user, Journal $journal)
    {
        $journal= $this->get('agfa_hpa.journal_manager')->getUserEntry($user, $journal);

        return $this->render('Admin/journal/show.html.twig', array(
            'journal' => $journal,
            'user' => $user
        ));
    }
}
