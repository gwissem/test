<?php
namespace Agfa\HpaBundle\Controller\Admin;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;
use Agfa\UserBundle\Entity\User;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Agfa\HpaBundle\Entity\Demande;
use Agfa\HpaBundle\Entity\Journal;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Form\Extension\Core\Type\TextType;

/**
 * Demande controller.
 *
 * @Route("/admin/demandes/")
 */
class DemandeController extends Controller
{

    /**
     * Lists all demandes in the admin area with options to mark a demande as paid.
     *
     * @Route("user/{user}/", name="admin_demande", requirements={"user":"\d+"})
     *
     * @Method ({"GET","PUT"})
     */
    public function indexAction(Request $request, User $user)
    {
        $demandes = $this->get('agfa_hpa.demande_manager')->getUserDemandes($user);
        $markAsPaidForms = array();

        // Build form for 'mark as paid' buttons
        foreach ($demandes as $demande) {
            $markAsPaidForms[$demande->getId()] = $this->createMarkAsPaidForm($user, $demande->getId())
                ->createView();
        }

        //
        $form = $this->createMarkAsPaidForm($user, $request->request->get('demandeId'));
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $data = $form->getData();
            $demandeId = $data['demandeId'];
            $userDemande = $this->get('agfa_hpa.demande_manager')->getDemande($user, $demandeId);

            // If the restriction to access a CR is not enabled, there is no need to update the amount as to unlock the access to the CR.
            if ($userDemande->getEtablissement()
                ->getPaiement()
                ->getActif() === FALSE || $userDemande->getEtablissement()
                ->getPaiement()
                ->getAffichageCr() === TRUE || ($userDemande->getEtablissement()
                ->getPaiement()
                ->getAffichageCr() === FALSE && $userDemande->getEtablissement()
                ->getPaiement()
                ->getAffichageCr() === TRUE)) {
                throw new NotFoundHttpException(
                    sprintf('User %s attempted to reset paieSolde of demande %s when not needed.', $this->getUser(),
                        $userDemande->getNodem()));
            }

            // Do not update if paieSolde is already 0
            if ($userDemande->getPaieSolde() > 0) {
                // Update payment data for the demande
                $userDemande->setPaieSolde(0)->setPaieSource(Demande::PAID_ADMIN);
                $em->persist($userDemande);
                $em->flush();

                // Log an entry in the journal
                $journal = $this->get('journalist');
                $journal->addWarning(Journal::EVENT_PAYMENT_UPDATED, $userDemande, $user,
                    sprintf('Nodemx %s. Marquée comme payé par %s', $userDemande->getNodemx(), $this->getUser()));

                // Add flash message
                $this->addFlash('success',
                    sprintf(
                        'Le compte rendus %s est maintenant marqué comme étant payé et est accessible par le patient.',
                        $userDemande->getNodem()));
            } else {
                // Add flash message
                $this->addFlash('warning',
                    sprintf('Le compte rendus %s est déjà réglé et donc accessible par le patient.',
                        $userDemande->getNodem()));
            }
            // Redirect to prevent problems when reloading the page
            return $this->redirectToRoute('admin_demande',
                array(
                    'user' => $user->getId()
                ));
        }

        return $this->render('Admin/demande/index.html.twig',
            array(
                'demandes' => $demandes,
                'markAsPaidForms' => $markAsPaidForms,
                'user' => $user
            ));
    }

    /**
     * @Route("search/", name="admin_demande_search")
     *
     * @Method ({"GET", "POST"})
     */
    public function searchAction(Request $request)
    {
        $demande = null;
        $relatedDemandes = array();
        $relatedDemandesForms = array();

        $form = $this->createSearchDemandeForm();
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $demandeMgr = $this->get('agfa_hpa.demande_manager');
            $data = $form->getData();
            $nodemxData = $data['nodemx'];
            $nodemx = preg_replace("/^([0-9]{4})([0-9a-z]\d+)$/i", "$2", $nodemxData) ?? $nodemxData;

            // If there is more than one demande, go to select page
            $demandes = $demandeMgr->getDemandesAnonymously($nodemx);
            if (count($demandes) > 1) {
                return $this->redirectToRoute('admin_demande_select', array(
                    'nodemx' => $nodemx
                ));
            }

            if (! $demande = $demandeMgr->getDemandeAnonymously($nodemx)) {
                $this->addFlash('warning', "La demande <strong>$nodemx</strong> n'a pas été trouvée");
            } else {
                $relatedDemandes = $demandeMgr->getAllDemandes($demande->getPatient()
                    ->getUser());

                foreach ($relatedDemandes as $relatedDemande) {
                    $relatedDemandesForms[$relatedDemande->getId()] = $this->createSearchDemandeForm(
                        $relatedDemande->getNodemx())
                        ->createView();
                }
            }
        }

        return $this->render('Admin/demande/search.html.twig',
            array(
                'form' => $form->createView(),
                'demande' => $demande,
                'relatedDemandes' => $relatedDemandes,
                'relatedDemandesForms' => $relatedDemandesForms
            ));
    }

    /**
     * In case there is more than one CR with the same nodemx, as the user which one to choose.
     *
     * @Route("search/{nodemx}/", name="admin_demande_select")
     * @Route("search/{nodemx}/{laboCode}", name="admin_demande_selected", requirements={"laboCode"="\d+"})
     */
    public function selectAccount(Request $request, $nodemx, $laboCode = null)
    {
        $demande = null;
        $relatedDemandes = array();
        $relatedDemandesForms = array();

        $demandeMgr = $this->get('agfa_hpa.demande_manager');

        if (! $demandes = $demandeMgr->getDemandesAnonymously($nodemx)) {
            $this->addFlash('warning', "La demande <strong>$nodemx</strong> n'a pas été trouvée");
            return $this->redirectToRoute('admin_demande_search');
        } elseif (count($demandes) > 1 && $laboCode) {

            if ($laboCode) {
                $demande = $demandeMgr->getDemandesAnonymously($nodemx, $laboCode);
                $demande = $demande[0];
            }

            $relatedDemandes = $demandeMgr->getAllDemandes($demande->getPatient()
                ->getUser());
            foreach ($relatedDemandes as $relatedDemande) {
                $relatedDemandesForms[$relatedDemande->getId()] = $this->createSearchDemandeForm(
                    $relatedDemande->getNodemx())
                    ->createView();
            }
        }

        return $this->render('Admin/demande/select.html.twig',
            array(
                'demandes' => $demandes,
                'demande' => $demande,
                'relatedDemandes' => $relatedDemandes,
                'relatedDemandesForms' => $relatedDemandesForms
            ));
    }

    /**
     * Create a form to mark a Demande as paid.
     *
     * @param User $user
     * @param integer $demandeId
     * @return \Symfony\Component\Form\FormInterface
     */
    private function createMarkAsPaidForm(User $user, $demandeId)
    {
        $form = $this->createFormBuilder()
            ->setMethod('PUT')
            ->setAction($this->generateUrl('admin_demande', array(
            'user' => $user->getId()
        )))
            ->add('demandeId', HiddenType::class, array(
            'data' => $demandeId
        ))
            ->getForm();

        return $form;
    }

    /**
     * Create a search form for demandes
     *
     * @param User $user
     * @param string $nodemx
     * @return \Symfony\Component\Form\FormInterface
     */
    private function createSearchDemandeForm($nodemx = null)
    {
        $form = $this->createFormBuilder()
            ->setMethod('POST')
            ->setAction($this->generateUrl('admin_demande_search'))
            ->add('nodemx', TextType::class, array(
            'label' => 'Identifiant Hexalis Patient'
        ));

        if ($nodemx) {
            $form->add('nodemx', HiddenType::class, array(
                'data' => $nodemx
            ));
        }

        return $form->getForm();
    }
}
