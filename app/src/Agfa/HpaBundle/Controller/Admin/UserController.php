<?php
namespace Agfa\HpaBundle\Controller\Admin;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Agfa\UserBundle\Entity\User;
use Symfony\Component\HttpFoundation\Request;
use Agfa\HpaBundle\Controller\BaseController;

/**
 * User management panel.
 *
 * @Route("/admin/user/")
 */
class UserController extends BaseController
{

    /**
     * Max number of users to display on one page.
     *
     * @var integer
     */
    const NUMBER_OF_USERS = 15;

    /**
     * Lists all User entities.
     *
     * @Route("", name="admin_user")
     *
     * @Method ("GET")
     * @Template(":Admin/User:index.html.twig")
     * @param Request $request
     * @return array
     */
    public function indexAction(Request $request)
    {
        $userMgr = $this->get('agfa_user.user_manager');

        $formsEnableUser = array();
        $formsEnableDebug = array();
        $users = array();
        $nbUsers = array();
        $searchTerms = $request->query->get('searchTerms');

        $users = $userMgr->getUsersByEmail($searchTerms);
        $nbUsers = $userMgr->countUsers($searchTerms);

        foreach ($users as $user) {
            $formsEnableUser[$user->getId()] = $this->createEnableForm($searchTerms, $user)->createView();
            $formsEnableDebug[$user->getId()] = $this->createDebugForm($user)->createView();
        }

        return array(
            'users' => $users,
            'nbUsers' => $nbUsers,
            'formsEnableUser' => $formsEnableUser,
            'formsEnableDebug' => $formsEnableDebug,
            'searchTerms' => rawurldecode($searchTerms)
        );
    }

    /**
     * Enable / disable a user account.
     *
     * @Route("enable-account/{userId}/", name="admin_user_enable_account", requirements={"userId":"\d+"})
     *
     * @Method ({"GET", "POST"})
     * @param Request $request
     * @param integer $userId
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function enableAccountAction(Request $request, $userId)
    {
        $em = $this->getDoctrine()->getManager();
        $userMgr = $this->get('fos_user.user_manager');

        $user = $userMgr->findUserBy(array('id' => $userId));

        if ($user->isEnabled()) {
            $user->setEnabled(false);
            $resultMessage = sprintf('Le compte de %s a été désactivé.', $user->getEmail());
        } else {
            $user->setEnabled(true);

            $resultMessage = sprintf('Le compte de %s a été activé.', $user->getEmail());
        }
        $em->flush();

        $this->addFlash('success', $resultMessage);

        return $this->redirect($this->generateUrl('admin_user', array(
            'searchTerms' => $request->query->get("searchTerms")
        )));
    }

    /**
     * @Route("enable-debug/{userId}/", name="admin_user_enable_debug", requirements={"userId":"\d+"})
     *
     * @Method ({"GET", "POST"})
     * @param Request $request
     * @param integer $userId
     */
    public function enableDebugAction(Request $request, $userId)
    {
        $em = $this->getDoctrine()->getManager();
        $userMgr = $this->get('fos_user.user_manager');


        $user = $userMgr->findUserBy(array('id' => $userId));

        if (!in_array(User::ROLE_DEBUG, $user->getRoles())) {
            $user->addRole(User::ROLE_DEBUG);
            $resultMessage = sprintf("Le mode debug a été <strong>activé</strong> pour l'utilisateur <strong>%s</strong>.", $user->getEmail());
        } else {
            $user->removeRole(User::ROLE_DEBUG);
            $resultMessage = sprintf("Le mode debug a été <strong>désactivé</strong> pour l'utilisateur <strong>%s</strong>.", $user->getEmail());
        }
        $em->flush();

        $this->addFlash('success', $resultMessage);

        return $this->redirect($this->generateUrl('admin_user', array('searchTerms' => rawurlencode($user->getEmail()))));
    }

    /**
     * Display a list of all users of the system.
     *
     * @Route("list-accounts/", name="admin_user_list_users")
     * @Template(":User:ajax/list-users.html.twig")
     *
     * @Method ("GET")
     * @param Request $request
     * @return array
     */
    public function listUsersAction(Request $request)
    {
        // FIXME Search terms cleans up + and @ signs resulting in empty results
        $userMgr = $this->get('agfa_user.user_manager');
        $forms = array();
        $users = array();
        $nbUsers = array();
        $searchTerms = $request->query->get('searchTerms');

        if (! empty($searchTerms) && strlen($searchTerms) > 2) {
            $users = $userMgr->getUsersByEmail($searchTerms);
            $nbUsers = $userMgr->countUsers($searchTerms);

            foreach ($users as $user) {
                $forms[$user->getId()] = $this->createEnableForm($searchTerms, $user)->createView();
            }
        }

        return array(
            'users' => $users,
            'nbUsers' => $nbUsers,
            'forms' => $forms
        );
    }

    /**
     * Create a form with a sublit button depending if the user account is disabled or enabled.
     *
     * @param \Agfa\UserBundle\Entity\User $user
     * @return \Symfony\Component\Form\Form
     * @param string $searchTerms
     * @param User $user
     * @return \Symfony\Component\Form\FormInterface
     */
    private function createEnableForm($searchTerms, User $user)
    {
        $form = $this->createFormBuilder()
            ->setAction($this->generateUrl('admin_user_enable_account', array(
            'userId' => $user->getId(),
            'searchTerms' => $searchTerms
        )))
            ->setMethod('POST')
            ->getForm();

        return $form;
    }

    /**
     * Create a form to switch debug mode for an user account.
     *
     * @param \Agfa\UserBundle\Entity\User $user
     * @return \Symfony\Component\Form\Form
     * @param string $searchTerms
     * @param User $user
     * @return \Symfony\Component\Form\FormInterface
     */
    private function createDebugForm(User $user)
    {
        $form = $this->createFormBuilder()
            ->setAction($this->generateUrl('admin_user_enable_debug', array(
            'userId' => $user->getId()
        )))
            ->setMethod('POST')
            ->getForm();

        return $form;
    }
}
