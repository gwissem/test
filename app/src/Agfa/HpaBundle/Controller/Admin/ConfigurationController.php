<?php
namespace Agfa\HpaBundle\Controller\Admin;

use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Agfa\HpaBundle\Form\Admin\ConfigurationType;
use Agfa\HpaBundle\Controller\BaseController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

/**
 * Configuration controller.
 *
 * @Route("/admin/configuration")
 */
class ConfigurationController extends BaseController
{

    /**
     * Finds and displays a Configuration entity.
     *
     * @Route("/", name="admin_configuration_show")
     *
     * @Method ("GET")
     * @Template(":Admin:Configuration/show.html.twig")
     */
    public function showAction()
    {
        $themeName = $this->container->getParameter('client_theme');

        $entity = $this->get('agfa_hpa.configuration_manager')->getConfiguration($themeName);

        if (! $entity) {
            throw $this->createNotFoundException('Unable to find Configuration entity.');
        }

        return array(
            'entity' => $entity
        );
    }

    /**
     * Displays a form to edit an existing Configuration entity.
     *
     * @Route("/edit", name="admin_configuration_edit")
     * @Method ({"GET", "POST"})
     * @Template(":Admin:Configuration/edit.html.twig")
     */
    public function editAction(Request $request)
    {
        $themeName = $this->container->getParameter('client_theme');

        $entity = $this->get('agfa_hpa.configuration_manager')->getConfiguration($themeName);

        if (! $entity) {
            throw $this->createNotFoundException('Unable to find Configuration entity.');
        }

        $editForm = $this->createForm(ConfigurationType::class, $entity);

        $editForm->add('submit', SubmitType::class, array(
            'label' => 'Mettre à jour'
        ));

        $editForm->handleRequest($request);

        if ($editForm->isSubmitted()) {
            if ($editForm->isValid()) {
                $this->getDoctrine()
                    ->getManager()
                    ->flush();

                $this->addFlash('success', 'Les modifications ont bien été prises en compte.');

                return $this->redirectToRoute('admin_configuration_show');
            }

            $this->addFlash('danger', 'Une erreur est survenue. Merci de bien vouloir vérifier vos données.');
        }

        return array(
            'entity' => $entity,
            'edit_form' => $editForm->createView()
        );
    }

}
