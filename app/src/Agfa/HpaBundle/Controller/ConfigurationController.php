<?php
namespace Agfa\HpaBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use UAParser\Parser;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Agfa\HpaBundle\Entity\Enrolement;
use Agfa\HpaBundle\Form\Type\EnrolementType;
use Agfa\HpaBundle\Entity\BsrvrCom;

/**
 * Account details and configuration accessible by all normal user of the app.
 * @Route("/configuration")
 */
class ConfigurationController extends Controller
{
    /**
     * Display a page with all the devices that have been authorized.
     * @Route("/securite", name="configuration_securite")
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function securiteAction(Request $request)
    {
        $user = $this->getUser();

        // Get previous to last login entry in hpa_journal for the user currently authenticated
        $authentications = $this->get('agfa_hpa.journal_manager')->getUserAuthentication($user);

        $trustedComputers = $user->getTrustedComputers();

        foreach ($trustedComputers as $token => $trusted) {
            $trustedComputers[$token]['last-login'] = new \DateTime($trusted['last-login']);
            $trustedComputers[$token]['valid-until'] = new \DateTime($trusted['valid-until']);
            $trustedComputers[$token]['token'] = $token;

            // If the current user is the one in the log, set to true
            $trustedComputers[$token]['current-user'] = FALSE;
            if(isset($trustedComputers[$token]['ip']) && $trustedComputers[$token]['ip'] == $request->getClientIp() && $request->headers->get('User-Agent') == $trusted['user-agent']){
                $trustedComputers[$token]['current-user'] = TRUE;
            }

//             // Cannot be used as it is: pattern is for hardware string + software string once parsed while user agent is full string with a lot more data info.
//             $pattern = '/(.*\,\D+)([0-9]{1,2}\.[0-9]{1,2}\.\d+)/i';
//             if(isset($trustedComputers[$token]['ip']) &&
//                 $trustedComputers[$token]['ip'] == $request->getClientIp() &&
//                 preg_replace($pattern, '$1', $request->headers->get('User-Agent')) == preg_replace($pattern, '$1', $trusted['user-agent'])){
//                     $trustedComputers[$token]['current-user'] = TRUE;
//             }

            $uaParser = Parser::create();
            $userAgent = $uaParser->parse($trusted['user-agent']);

            $trustedComputers[$token]['hardware'] = $userAgent->os->toString();
            $trustedComputers[$token]['software'] = $userAgent->ua->toString();
        }

        return $this->render(':Configuration:securite.html.twig', [
            'trusted_computers' => $trustedComputers,
            'authentications' => $authentications
        ]);
    }

    /**
     * Display the currently logged in user account informations: email and password.
     *
     * @Route("/compte", name="configuration_account")
     * @Template(":Configuration:account")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function accountAction(Request $request)
    {
        // Create the 'enrolement' form
        $enrolement = new Enrolement();
        $enrolement->setUser($this->getUser());
        $form = $this->createForm(EnrolementType::class, $enrolement, [
            'action' => $this->generateUrl('enrolement')
        ]);

        return $this->render(':Configuration:account.html.twig', array('form' => $form->createView()));
    }

    /**
     * Delete the currently logged in user account with all its related data.
     * Can only be done after the user password has been submited.
     *
     * @Route("/compte/delete", name="configuration_account_delete")
     * @Template(":Configuration:account")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function accountDeleteAction(Request $request)
    {
        $deleteForm = $this->createDeleteUserForm();
        $deleteForm->handleRequest($request);

        if ($request->getMethod() == 'DELETE' && $deleteForm->isSubmitted()) {
            $user = $this->getUser();

            $factory = $this->get('security.encoder_factory');
            $encoder = $factory->getEncoder($user);

            if ($encoder->isPasswordValid($user->getPassword(), $deleteForm->getData()['password'], $user->getSalt())) {
                $em = $this->getDoctrine()->getManager();

                // Add notification of unenrollment to BsrvrCom
                foreach($user->getPatients() as $patient){
                    $data = array('nopat' => $patient->getBsrvrNopat(), 'bsrvr_ref_etablissement' => $patient->getEtablissement()->getBsrvrRefEtablissement());

                    $notificationUnenrollment = new BsrvrCom();
                    $notificationUnenrollment->setAction(BsrvrCom::ACTION_UPDATE_UNENROLL);
                    $notificationUnenrollment->setData($data);

                    $em->persist($notificationUnenrollment);
                }

                // Delete the user
                $em->remove($user);
                $em->flush();

                // Add entry to Piwik
                if ($this->getParameter('piwik_api_status') === TRUE) {
                    \PiwikTracker::$URL = 'https://' . $this->container->getParameter('host_piwik');
                    $tracker = new \PiwikTracker($this->container->getParameter('piwik_site_id'));
                    $tracker->doTrackEvent('Comptes', 'Suppression', 'Supprimé');
                }

                $this->addFlash('success', 'Votre compte et toutes les données qui lui étaient liées ont bien été supprimés.');

                return $this->redirect($this->generateUrl('fos_user_security_logout'));
            }

            $this->addFlash('danger', "Votre mot de passe n'a pas été reconnu.");
        }

        return $this->render(':Configuration:account-delete-confirmation.html.twig', array(
            'deleteForm' => $deleteForm->createView()
        ));
    }

    /**
     * Creates the delete user form.
     *
     * @return \Symfony\Component\Form\FormInterface
     */
    private function createDeleteUserForm()
    {
        $deleteForm = $this->createFormBuilder()
            ->setAction($this->generateUrl('configuration_account_delete'))
            ->setMethod('DELETE')
            ->add('password', PasswordType::class, array(
            'label' => "Merci d'indiquer votre mot de passe afin de confirmer la suppression de votre compte et de toutes vos données",
            'attr' => array(
                'autocomplete' => 'new-password'
            )
        ))
            ->getForm();

        return $deleteForm;
    }
}