<?php
namespace Agfa\HpaBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Agfa\HpaBundle\Entity\Demande;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use Symfony\Component\HttpFoundation\Response;
use Agfa\HpaBundle\Entity\Enrolement;
use Agfa\HpaBundle\Entity\Journal;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Agfa\HpaBundle\Form\Type\EnrolementType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Agfa\HpaBundle\Entity\BsrvrCom;

/**
 * @Route("/compte-rendus/")
 *
 * @author charles
 *
 */
class DemandeController extends BaseController
{

    /**
     * Hides the patient report in the view.
     *
     * @var string
     */
    const HIDE_CR = 'hide';

    /**
     * Show a patient depending on criterias
     *
     * @var string
     */
    const SHOW_CONDITIONAL = 'show_conditional';

    /**
     * Show a patient record with no condition
     *
     * @var string
     */
    const SHOW_UNCONDITIONAL = 'show_unconditional';

    /**
     * @Route("list/", name="demande_index_default")
     * @Route("list/{year}/{month}/", name="demande_index", requirements={"year":"\d+", "month":"\d+"})
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * TODO If all result have the same month, hide the filter
     */
    public function indexAction(Request $request, $month = null, $year = null)
    {
        $user = $this->getUser();

        /* @var $personne Personne */
        $personne = $this->get('contexte')->getPersonneContexte();

        if (! $personne) {
            return $this->redirectToRoute('default_index');
        }

        /* on charge toutes les demandes du personne */
        $demandes = $this->get('agfa_hpa.demande_manager')->getDemandes($user, $personne);

        /* on fait la liste des mois ayant des demandes, avec pour chaque mois l'id de la première demande affichée du mois */
        $datesList = array();
        $countDemands = array();
        $hasUnreadArray = array();
        $yearHasUnread = array();
        $oldestYear = 0;
        $oldestMonth = 0;
        $listOfDates = array();

//         if(count($demandes) > 0)
//         {
//             $cacheKeyHasUnreadAray = ('hasUnreadArray-u'.$user->getId().'-p'.$demandes[0]->getPatient()->getId());
//             $cachedHasUnreadArray = $this->get('cache.app')->getItem($cacheKeyHasUnreadAray);


//             $cachedKeyYearHasUnread = ('hasUnreadArray-u'.$user->getId().'-p'.$demandes[0]->getPatient()->getId());
//             $cachedYearHasUnread = $this->get('cache.app')->getItem($cachedKeyYearHasUnread);

//             if (false === $cachedHasUnreadArray->isHit()) {

                // Filter data by year and/or month
                foreach ($demandes as $demande) {
                    $demandeYear = (int) $demande->getDateAccueil()->format('Y');
                    $demandeMonth = (int) $demande->getDateAccueil()->format('m');
                    array_push($listOfDates, $demande->getDateAccueil());

                    // If there is not yet any data in the array, set it to false
                    if (! isset($hasUnreadArray[$demandeYear][$demandeMonth])) {
                        $hasUnreadArray[$demandeYear][$demandeMonth] = FALSE;
                        $yearHasUnread[$demandeYear] = FALSE;
                    }

                    if (! empty($countDemands[$demandeYear][$demandeMonth])) {
                        $countDemands[$demandeYear][$demandeMonth] = $countDemands[$demandeYear][$demandeMonth] + 1;
                        if ($demande->getIsNew() == TRUE) {
                            $hasUnreadArray[$demandeYear][$demandeMonth] = TRUE;
                            $yearHasUnread[$demandeYear] = TRUE;
                        }
                    } else {
                        $countDemands[$demandeYear][$demandeMonth] = 1;
                        if ($demande->getIsNew() == TRUE) {
                            $hasUnreadArray[$demandeYear][$demandeMonth] = TRUE;
                            $yearHasUnread[$demandeYear] = TRUE;
                        }
                    }
                }

//                 // Set hasUnreadArray
//                 $cachedHasUnreadArray->set($cacheKeyHasUnreadAray, $hasUnreadArray);
//                 $this->get('cache.app')->save($cachedHasUnreadArray);

//                 // Set yearHasUnread
//                 $cachedYearHasUnread->set($cachedKeyYearHasUnread, $yearHasUnread);
//                 $this->get('cache.app')->save($cachedYearHasUnread);
//             }else{
//                 $hasUnreadArray = $cachedHasUnreadArray;
//                 $yearHasUnread = $cachedYearHasUnread;
//             }
//         }

//             // Flush cache
//             $key = 'Demande'.$user->getId();
// //             $this->get('cache.app')->deleteItem('test');

//             // to delete cache
//             $em = $this->getDoctrine()->getManager();
//             $cacheDriver = $em->getConfiguration()->getResultCacheImpl();
//             $cacheDriver->delete('my_item');
//             // to delete all cache entries
//             //         $cacheDriver->deleteAll();


        // Get oldest date
        asort($listOfDates);
        $oldestDate = array_pop($listOfDates);
        if ($oldestDate) {
            $oldestMonth = ! isset($month) ? $oldestDate->format('m') : $month;
            $oldestYear = ! isset($year) ? $oldestDate->format('Y') : $year;
        } else {
            $now = new \DateTime('NOW');
            $oldestMonth = $now->format('m');
            $oldestYear = $now->format('Y');
        }

        foreach ($countDemands as $demandeYear2 => $demandeMonths) {
            foreach ($demandeMonths as $demandeMonth2 => $countDemands2) {
                $datesList[$demandeYear2][$demandeMonth2] = array(
                    'count' => $countDemands2,
                    'hasUnread' => $hasUnreadArray[$demandeYear2][$demandeMonth2],
                    'date' => new \DateTime("$demandeYear2-$demandeMonth2-01")
                );
            }
        }

        // Get the list of demands based on the date selected
        $filteredDemandes = $this->get('agfa_hpa.demande_manager')->getDemandesByMonth($user, $personne, $oldestYear, $oldestMonth);

        // Extract data related to results and add them to an array for ease of use
        $resultats = array();

        // Define how CR should be displayed: hidden until paid, visible but can pay, visible and no payment available
        $showCr = array();

        foreach ($demandes as $demande) {
            if ($demande->getEtablissement()->getPaiement() && $demande->getPaieSolde() > 0 && $demande->getEtablissement()
                ->getPaiement()
                ->getActif()) {

                $affichageCrPaiement = $demande->getEtablissement()
                    ->getPaiement()
                    ->getAffichageCr();
                $affichageCrDemande = $demande->getAffichageCr();

                if ($affichageCrPaiement == FALSE && $affichageCrDemande == FALSE) {
                    $showCr[$demande->getId()] = self::HIDE_CR;
                } elseif (($affichageCrPaiement == TRUE || $affichageCrDemande == TRUE)) {
                    $showCr[$demande->getId()] = self::SHOW_CONDITIONAL;
                } else {
                    $showCr[$demande->getId()] = self::SHOW_UNCONDITIONAL;
                }
            } else {
                // Just in case no data is defined in Paiement
                $showCr[$demande->getId()] = self::SHOW_UNCONDITIONAL;
            }

            // THE FOLLOWING GOES TOO HEAVY ON THE SERVER. lEAVE COMMENTED OUT FOR NOW UNTIL A FIX IS FOUND.
            // THE QUERY HAS BEEN SIMPLIFIED TOO.
//             // Only keep the result of table "examen" to avoid making a 'group by' on the query.
//             foreach ($demande->getResultats() as $resultatPerDemande) {
//                 $resultats[$demande->getId()][$resultatPerDemande->getElement()
//                     ->getExamen()
//                     ->getId()] = $resultatPerDemande->getElement()->getExamen();
//             }

        }

        return $this->render(':Demande:index.html.twig', [
            'personne' => $personne,
            'demandes' => $demandes,
            'showCr' => $showCr,
            'resultats' => $resultats,
            'filtered_demandes' => $filteredDemandes,
            'dates_list' => $datesList,
            'year_has_unread' => $yearHasUnread
        ]);
    }

    /**
     * @Route("delete/{demandeId}/", name="demande_delete", requirements={"demandeId"="\d+"})
     *
     * @param Request $request
     * @param integer $demandId
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteAction(Request $request, $demandeId)
    {
        // A user that impersonate an other user can't perform this
        if($this->isGranted('ROLE_PREVIOUS_ADMIN')){
            $this->addFlash('danger', "Vous ne pouvez pas <strong>supprimer</strong> les données d'un patient.");
            $this->get('logger')->addInfo(sprintf("Tentative de suppression d'une demande d'un autre utilisateur par un admin (%s)", $this->getUser()->getEmail()));
            return $this->redirectToRoute('default_index');
        }

        $user = $this->getUser();
        $demande = $this->get('agfa_hpa.demande_manager')->getDemande($user, $demandeId);

        $form = $this->createDeleteForm($demande);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // Adding entry to journal has to be BEFORE the entity is removed else it will create a not found exception
            $this->get('journalist')->addWarning(Journal::EVENT_CR_SUPPRESSION, $demande, $user, sprintf("Nodemx %s, constant login %s", $demande->getNodemx(), $demande->getBsrvrConstantLogin()));

            if ($this->getParameter('piwik_api_status') === TRUE) {
                \PiwikTracker::$URL = 'https://' . $this->container->getParameter('host_piwik');
                $tracker = new \PiwikTracker($this->container->getParameter('piwik_site_id'));
                $tracker->doTrackEvent('Comptes rendus', 'Suppression', 'Supprimé');
            }

            $em = $this->getDoctrine()->getManager();
            $em->remove($demande);
            $em->flush();

            $this->addFlash('success', 'Le compte rendu à bien été supprimé');
        }

        return $this->redirectToRoute('default_index');
    }

    // /**
    // * Off until we get a proper reason to use this page
    // * @Route("/{demandeId}", name="demande_detail", requirements={"demandeId":"\d+"})
    // *
    // * @param integer $demandeId
    // * @param Request $request
    // * @throws UnauthorizedHttpException
    // * @return \Symfony\Component\HttpFoundation\Response
    // */
    // public function showAction(Request $request, $demandeId)
    // {
    // $demande = $this->get('agfa_hpa.demande_manager')->getDemande($this->getUser(), $demandeId);

    // // FIXME Why is this here? => it redirects to the person the demande has just been assigned to
    // $personne = $demande->getPatient()->getPersonne();
    // $this->get('contexte')->setPersonneContexte($personne);

    // return $this->render(':Demande:show.html.twig', [
    // 'demande' => $demande
    // ]);
    // }

    /**
     * @Route("{demandeId}/", name="demande_post_enrolement", requirements={"demandeId":"\d+"})
     */
    public function postEnrolementAction(Request $request, $demandeId)
    {
        // FIXME If the demand has been assigned to a user account already, it crashes :
        // the system update the existing demande (attached to someone else) and then
        // redirect here but the demande is not accessible to the user.
        $demande = $this->get('agfa_hpa.demande_manager')->getDemande($this->getUser(), $demandeId);

        // Redirects to the person the demande has just been assigned to
        $personne = $demande->getPatient()->getPatientPersonnes()[0]->getPersonne();

        // Add an entry into user to set the current personne for the user
        $em = $this->getDoctrine()->getManager();
        $this->get('contexte')->setPersonneContexte($personne);
        $em->flush();

        return $this->render(':Demande:post_enrolement.html.twig', [
            'demande' => $demande
        ]);
    }

    /**
     * @Route("{demandeId}/telecharger/", name="demande_download", requirements={"demandeId":"\d+"})
     *
     * @param integer $demande_id
     * @param Request $request
     * @throws UnauthorizedHttpException
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function downloadAction(Request $request, $demandeId)
    {
        // A user that impersonate an other user can't perform this
        if($this->isGranted('ROLE_PREVIOUS_ADMIN')){
            $this->addFlash('danger', "Vous ne pouvez pas <strong>télécharger</strong> les données d'un patient.");
            $this->get('logger')->addInfo(sprintf("Tentative de téléchargement d'une demande d'un autre utilisateur par un admin (%s)", $this->getUser()->getEmail()));
            return $this->redirectToRoute('default_index');
        }

        /* @var $user User */
        $user = $this->getUser();

        /* @var $em \Doctrine\ORM\EntityManager */
        $em = $this->getDoctrine()->getManager();

        $demande = $this->get('agfa_hpa.demande_manager')->getDemandeToDownload($user, $demandeId);
        $personne = $demande->getPatient()->getPatientPersonnes()[0]->getPersonne();

        // Check that a demande can be downloaded: check if balance is 0 and if not, that the option to hide reports before the payment is made is not enabled
        // If the option to block viewing reports is enabled and the balance owned for that report is more than 0, block download
        if ($demande->getEtablissement()->getPaiement() && $demande->getEtablissement()
            ->getPaiement()
            ->getAffichageCr() == FALSE && $demande->getAffichageCr() == FALSE && $demande->getPaieSolde() > 0 && $demande->getEtablissement()
            ->getPaiement()
            ->getActif() == TRUE && $demande->getIsNew() == TRUE) {
            throw new NotFoundHttpException('The report could not be downloaded as it has not been paid in full yet.');
        }

        if ($personne->getUser()->getId() !== $user->getId()) {
            throw new UnauthorizedHttpException("You do not have access to this report.");
        }

        /*
         * première lecture du compte rendu
         */
        if ($demande->getIsNew() == TRUE) {
            $lecture = new BsrvrCom();
            $lecture->setDemande($demande);
            $lecture->setAction(BsrvrCom::ACTION_UPDATE_READING);
            $em->persist($lecture);

            $demande->setIsNew(FALSE);

            $this->get('journalist')->addSuccess(Journal::EVENT_CR_DOWNLOAD, $demande, $demande->getPatient()
                ->getUser(), sprintf('Nodemx %s, constant login: %s', $demande->getNodemx(), $demande->getBsrvrConstantLogin()));

            $em->flush();
        }

        if ($this->getParameter('piwik_api_status') === TRUE) {
            \PiwikTracker::$URL = 'https://' . $this->container->getParameter('host_piwik');
            $tracker = new \PiwikTracker($this->container->getParameter('piwik_site_id'));
            $tracker->doTrackEvent('Comptes rendus', 'Téléchargement', 'Avec compte');
        }

        $content = stream_get_contents($demande->getCompteRendu());
        $content_length = strlen($content);

        $response = new Response();
        $response->setPrivate();
        $response->headers->set('Cache-Control', 'must-revalidate, post-check=0, pre-check=0');
        $response->headers->set('Content-Type', 'application/pdf');
        $response->headers->set('Content-Disposition', $response->headers->makeDisposition(ResponseHeaderBag::DISPOSITION_ATTACHMENT, $demande->getCode() . ".pdf"));
        $response->headers->set('Content-Length', $content_length);
        $response->setContent($content);

        // FIXME Try the following when you will have some proper binary content in the db for test
        // $response = new BinaryFileResponse($content);
        // $response->setContentDisposition(ResponseHeaderBag::DISPOSITION_ATTACHMENT);

        return $response;
    }

    /**
     * @Route("{demandeId}/verifier-suppression/", name="demande_delete_confirmation", requirements={"demandeId":"\d+"})
     *
     * @param integer $demandeId
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function deleteConfirmationAction($demandeId, Request $request)
    {
        // A user that impersonate an other user can't perform this
        if($this->isGranted('ROLE_PREVIOUS_ADMIN')){
            $this->addFlash('danger', "Vous ne pouvez pas <strong>supprimer</strong> les données d'un patient.");
            $this->get('logger')->addInfo(sprintf("Tentative de suppression d'une demande d'un autre utilisateur par un admin (%s)", $this->getUser()->getEmail()));
            return $this->redirectToRoute('default_index');
        }

        /* @var $user User */
        $user = $this->getUser();

        $demande = $this->get('agfa_hpa.demande_manager')->getDemande($user, $demandeId);

        // Create delete form
        $deleteForm = $this->createDeleteForm($demande)->createView();

        return $this->render(':Demande:delete_confirmation.html.twig', [
            'demande' => $demande,
            'deleteForm' => $deleteForm
        ]);
    }

    /**
     * Creates delete form
     *
     * @param Demande $demande
     * @return \Symfony\Component\Form\FormInterface
     */
    private function createDeleteForm(Demande $demande)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('demande_delete', array(
            'demandeId' => $demande->getId()
        )))
            ->setMethod('DELETE')
            ->add('submit', SubmitType::class, array(
            'label' => $this->trans('agfa.hpa_bundle.demande.supprimer_verification.form.submit'),
            'button_class' => 'danger'
        ))
            ->getForm();
    }
}
