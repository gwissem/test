<?php
namespace Agfa\HpaBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Agfa\HpaBundle\Entity\RechercheDemande;
use Agfa\HpaBundle\Form\Type\RechercheDemandeType;
use Agfa\HpaBundle\Entity\Personne;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

/**
 * @Route("/recherche")
 *
 * @author charles
 */
class RechercheController extends Controller
{
    /**
     * Hides the patient report in the view.
     * @var string
     */
    const HIDE_CR = 'hide';

    /**
     * Show a patient depending on criterias
     * @var string
     */
    const SHOW_CONDITIONAL = 'show_conditional';

    /**
     * Show a patient record with no condition
     * @var string
     */
    const SHOW_UNCONDITIONAL = 'show_unconditional';

    /**
     * @Route("/", name="recherche_demande")
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function rechercheDemandeAction(Request $request)
    {
        $user = $this->getUser();
        $resultats = array();
        $showCr = array();
        $demandes = '';

        if(!$personne = $this->get('contexte')->getPersonneContexte()){
            $this->addFlash('warning', "La recherche est possible que sur un compte contenant des résultats.");
            return $this->redirect($this->generateUrl('default_index'));
        }

        $recherche = new RechercheDemande();

        $form = $this->getRechercheDemandeForm($recherche, $personne);

        if ($request->getMethod() == 'POST') {
            $form->handleRequest($request);
            if ($form->isValid()) {

                $formVariables = $request->request->get('recherche_demande');

                // Get the demandes
                $demandes = $this->get('agfa_hpa.demande_manager')->searchDemandes($user, $personne, $formVariables);

                // Track with Piwik
                if ($this->getParameter('piwik_api_status') === true) {
                    $this->get('agfa_hpa.piwik.search_form_service')->compteRendusForm($formVariables);
                }

                if (! $demandes) {
                    $this->addFlash('info', "Aucune demande n'a été trouvée avec les critères indiqués.");
                }else{
                    foreach($demandes as $demande){
                        foreach($demande->getResultats() as $resultatPerDemande){
                            $resultats[$demande->getId()][$resultatPerDemande->getElement()->getExamen()->getId()] = $resultatPerDemande->getElement()->getExamen();
                        }

                        if($demande->getEtablissement()->getPaiement() && $demande->getPaieSolde() > 0 && $demande->getEtablissement()->getPaiement()->getActif()){
                            $affichageCrPaiement = $demande->getEtablissement()->getPaiement()->getAffichageCr();
                            $affichageCrDemande = $demande->getAffichageCr();

                            if($affichageCrPaiement == FALSE && $affichageCrDemande == FALSE){
                                $showCr[$demande->getId()] = self::HIDE_CR;
                            }elseif(($affichageCrPaiement == TRUE || $affichageCrDemande == TRUE )){
                                $showCr[$demande->getId()] = self::SHOW_CONDITIONAL;
                            }else{
                                $showCr[$demande->getId()] = self::SHOW_UNCONDITIONAL;
                            }
                        }else{
                            // Just in case no data is defined in Paiement
                            $showCr[$demande->getId()] = self::SHOW_UNCONDITIONAL;
                        }
                    }
                }
            }
        }

        return $this->render(':Recherche:recherche_demande.html.twig', [
            'form' => $form->createView(),
            'demandes' => $demandes,
            'resultats' => $resultats,
            'showCr' => $showCr
        ]);
    }

    private function getRechercheDemandeForm(RechercheDemande $recherche, Personne $personne)
    {
        $form = $this->createForm(RechercheDemandeType::class, $recherche, array(
            'action' => $this->generateUrl('recherche_demande'),
            'method' => 'POST',
            'trait_choices' => array(
                'personne' => $personne
            )
        ));

        $form->add('submit', SubmitType::class, array(
            'label' => 'Rechercher'
        ));

        return $form;
    }
}
