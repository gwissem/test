<?php
namespace Agfa\HpaBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Agfa\HpaBundle\Entity\Journal;
use Agfa\HpaBundle\Form\Type\AffectationPatientType;
use Agfa\HpaBundle\Entity\Personne;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Agfa\HpaBundle\Entity\PersonnePatient;
use Agfa\HpaBundle\Entity\Patient;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Agfa\HpaBundle\Entity\BsrvrCom;

/**
 * @Route("/patient")
 *
 * @author charles
 */
class PatientController extends BaseController
{

    /**
     * @Route("/affecter-personne/{patientId}", name="patient_assign", requirements={"patientId":"\d+|"})
     * @Template(":Patient:affecter")
     *
     * Affecte un patient à une personne
     */
    public function assignAction(Request $request, $patientId = null)
    {
        $user = $this->getUser();
        $em = $this->getDoctrine()->getManager();
        $this->patientMgr = $this->get('agfa_hpa.patient_manager');

        // If no patientId is set, get the first unassigned patient
        if (! $patientId) {
            $unassignedPatients = $this->patientMgr->findPatientSansPersonne($user);
            if (count($unassignedPatients) > 0) {
                $patientId = $unassignedPatients[0]->getId();
            }else {
                // If there is no unassigned patient and no patient id, redirect to homepage
                $this->addFlash('info', 'Tous les patient sont affectés correctement');
                $user->setHasUnassignedPatient(FALSE);
                $em->persist($user);
                $em->flush();
                return $this->redirectToRoute('default_index');
            }
        }

        // Check that the patient is not already assigned to another personne as the current user will become the owner of the patient. Redirect to 'reassign' if this is the case
        if ($patient = $this->get('agfa_hpa.personne_patient_manager')->getPatientIsUsed($patientId)) {
            throw new NotFoundHttpException("Le patient $patientId est déjà affecté à une personne.");
        }

        $patient = $this->patientMgr->getPatient($user, $patientId);
        if ($patient == null) {
            throw new NotFoundHttpException("Aucune donnée trouvée pour le patient ($patientId).");
        }

        $personnePatient = new PersonnePatient();
        $personnePatient->setPatient($patient);

        $form = $this->createForm(AffectationPatientType::class, $personnePatient, array(
            'trait_choices' => array(
                'user' => $user,
                'patient' => $patient
            )
        ));

        $form->handleRequest($request);

        if ($form->isValid()) {
            $formData = $form->getData();

            if ($formData->getPersonne() == null) {
                $personne = new Personne();
                $personne->setNom($patient->getNom());
                $personne->setPrenom($patient->getPrenom());
                $personne->setSexe($patient->getSexe());
                $personne->setDateNaissance($patient->getDateNaissance());
                $personne->setUser($patient->getUser());
                $em->persist($personne);

                // The following flush is necessary so that we can get the entity id for the journal entry.
                $em->flush();

                $this->get('journalist')->addInfo(Journal::EVENT_PERSONNE_CREATION, $personne, $personne->getUser(), $personne->getNomComplet());

                $personnePatient->setPersonne($personne);
            } else {
                $personne = $this->get('agfa_hpa.personne_manager')->getPersonne($user, $formData->getPersonne());
                $personnePatient->setPersonne($personne);
            }

            $this->get('journalist')->addWarning(Journal::EVENT_PATIENT_EDITION, $patient, $patient->getUser(), sprintf("Rattachement du patient %s à la personne : %s", $patient->getNomComplet(), $personne->getNomComplet()));

            // If the following flush is taken out, no report can be re-affected
            $em->persist($personnePatient);
            $em->flush();

            // Check if there is more patient to assign, set to hasUnassignedPatient to false if not, reload the same page otherwise
            if (! $this->patientMgr->findPatientSansPersonne($user)) {
                $user->setHasUnassignedPatient(FALSE);
                $em->persist($user);
                $em->flush();
            }else{
                return $this->redirectToRoute('patient_assign');
            }

            return $this->redirectToRoute('default_switch_personne', array(
                'personneId' => $personnePatient->getPersonne()
                    ->getId()
            ));
        }

        return $this->render(':Patient:affectation.html.twig', [
            'assign' => true,
            'patient' => $patient,
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/reaffecter-personne/{personneId}/{patientId}/", name="patient_reassign", requirements={"personneId":"\d+", "patientId":"\d+"})
     *
     * Réaffecte un patient à une autre personne
     */
    public function reassignAction(Request $request, $personneId, $patientId)
    {
        // A user that impersonate an other user can't perform this
        if($this->isGranted('ROLE_PREVIOUS_ADMIN')){
            $this->addFlash('danger', "Vous ne pouvez pas <strong>modifier</strong> les données d'un patient.");
            $this->get('logger')->addInfo(sprintf("Tentative de modification d'une personne d'un autre utilisateur par un admin (%s)", $this->getUser()->getEmail()));
            return $this->redirectToRoute('personne_index');
        }

        $user = $this->getUser();
        $em = $this->getDoctrine()->getManager();

        $personnePatient = $this->get('agfa_hpa.personne_patient_manager')->getPersonnePatient($user, $personneId, $patientId);
        if ($personnePatient == null) {
            throw new NotFoundHttpException("Aucune donnée trouvée pour le patient ($patientId) et la personne ($personneId) donnée.");
        }

        $personne = $personnePatient->getPersonne();
        $patient = $personnePatient->getPatient();

        $form = $this->createForm(AffectationPatientType::class, $personnePatient, array(
            'trait_choices' => array(
                'user' => $user,
                'patient' => $patient
            )
        ));

        $form->handleRequest($request);

        if ($form->isValid()) {
            $formData = $form->getData();

            if ($formData->getPersonne() == null) {
                $personne = new Personne();
                $personne->setNom($patient->getNom());
                $personne->setPrenom($patient->getPrenom());
                $personne->setSexe($patient->getSexe());
                $personne->setDateNaissance($patient->getDateNaissance());
                $personne->setUser($patient->getUser());
                $em->persist($personne);

                // The following flush is necessary so that we can get the entity id for the journal entry.
                $em->flush();

                $this->get('journalist')->addInfo(Journal::EVENT_PERSONNE_CREATION, $personne, $personne->getUser(), $personne->getNomComplet());

                $personnePatient->setPersonne($personne);
                // Only update if there is something to update
            } elseif ($formData->getPersonne() != $personne) {
                $this->get('journalist')->addWarning(Journal::EVENT_PATIENT_EDITION, $patient, $patient->getUser(), sprintf("Rattachement du patient %s à la personne %s", $patient->getNomComplet(), $formData->getPersonne()
                    ->getNomComplet()));
            }

            // If the following flush is taken out, no report can be re-affected
            $em->flush();

            return $this->redirectToRoute('personne_index');
        }

        return $this->render(':Patient:affectation.html.twig', [
            'assign' => false,
            'patient' => $patient,
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/{patientId}/verifier-suppression", name="patient_delete_confirmation", requirements={"patientId":"\d+"})
     *
     * @param integer $patientId
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function deleteConfirmationAction($patientId, Request $request)
    {
        // A user that impersonate an other user can't perform this
        if($this->isGranted('ROLE_PREVIOUS_ADMIN')){
            $this->addFlash('danger', "Vous ne pouvez pas <strong>supprimer</strong> les données d'un patient.");
            $this->get('logger')->addInfo(sprintf("Tentative de suppression d'un patient d'un autre utilisateur par un admin (%s)", $this->getUser()->getEmail()));
            return $this->redirectToRoute('personne_index');
        }

        /* @var $user User */
        $user = $this->getUser();

        $patient = $this->get('agfa_hpa.patient_manager')->getPatient($user, $patientId);

        // Create delete form
        $deleteForm = $this->createDeleteForm($patient)->createView();

        return $this->render(':Patient:delete_confirmation.html.twig', [
            'patient' => $patient,
            'deleteForm' => $deleteForm
        ]);
    }

    /**
     * @Route("delete/{patientId}/", name="patient_delete", requirements={"patientId"="\d+"})
     *
     * @param Request $request
     * @param integer $demandId
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteAction(Request $request, $patientId)
    {
        // A user that impersonate an other user can't perform this
        if($this->isGranted('ROLE_PREVIOUS_ADMIN')){
            $this->addFlash('danger', "Vous ne pouvez pas <strong>supprimer</strong> les données d'un patient.");
            $this->get('logger')->addInfo(sprintf("Tentative de suppression d'un patient d'un autre utilisateur par un admin (%s)", $this->getUser()->getEmail()));
            return $this->redirectToRoute('personne_index');
        }

        $user = $this->getUser();
        $patient = $this->get('agfa_hpa.patient_manager')->getPatient($user, $patientId);
        $latePatient = clone $patient;

        $form = $this->createDeleteForm($patient);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            // Let BioServeur know about the unenrollment of the patient
            $updateBsrvrUnenroll = new BsrvrCom();
            $updateBsrvrUnenroll->setAction(BsrvrCom::ACTION_UPDATE_UNENROLL);
            $updateBsrvrUnenroll->setData(array('nopat' => $patient->getBsrvrNopat(), 'bsrvr_ref_etablissement' => $patient->getEtablissement()->getBsrvrRefEtablissement()));
            $em->persist($updateBsrvrUnenroll);

            $em->remove($patient);
            $em->flush();

            $this->get('journalist')->addWarning(Journal::EVENT_PATIENT_SUPPRESSION, $latePatient, $user, $this->transChoice("agfa.hpa_bundle.patient.delete.journal", count($latePatient->getDemandes()), array(
                '%patientName%' => $latePatient
            )));

            $this->addFlash('success', $this->transChoice("agfa.hpa_bundle.patient.delete.journal", count($latePatient->getDemandes()), array(
                '%patientName%' => $latePatient
            )));

            unset($latePatient);
        }

        return $this->redirectToRoute('personne_index');
    }

    /**
     * Creates delete form
     *
     * @param Patient $patient
     * @return \Symfony\Component\Form\FormInterface
     */
    private function createDeleteForm(Patient $patient)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('patient_delete', array(
            'patientId' => $patient->getId()
        )))
            ->setMethod('DELETE')
            ->add('submit', SubmitType::class, array(
            'label' => $this->trans('agfa.hpa_bundle.patient.supprimer_verification.form.submit'),
            'button_class' => 'danger'
        ))
            ->getForm();
    }

    // Will be used to repare the sharing of reports between users
    // public function linkReportToPatient(){}
    // public function linkPatientToPerson(){}
}
