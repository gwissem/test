<?php
namespace Agfa\HpaBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * List of shortcuts used in controllers.
 */
class BaseController extends Controller
{

    /**
     * Shortcut to the Symfony's translator.
     *
     * @return \Symfony\Bundle\FrameworkBundle\Translation\Translator
     */
    public function trans()
    {
        return call_user_func_array(array(
            $this->get('translator'),
            "trans"
        ), func_get_args());
    }

    /**
     * Shortcut to the Symfony's translator with choices.
     *
     * @return \Symfony\Bundle\FrameworkBundle\Translation\Translator
     */
    public function transChoice()
    {
        return call_user_func_array(array(
            $this->get('translator'),
            "transChoice"
        ), func_get_args());
    }

//     /**
//      * Shortcut to the flashbag
//      *
//      * @return mixed
//      */
//     public function getFlashBag()
//     {
//         return call_user_func_array(array(
//             $this->get('session'),
//             "getFlashBag"
//         ), func_get_args());
//     }

//     /**
//      * Sortcut to get application settings from the database
//      *
//      * @param unknown $key
//      */
//     public function getSettings($key)
//     {
//         return $this->getDoctrine()
//             ->getRepository('BecchaBookerBundle:Setting')
//             ->findOneBy(array(
//             'uniqueKey' => $key
//         ));
//     }

//     /**
//      * Get the current locale
//      */
//     public function getLocale()
//     {
//         return $this->get('request')->getLocale();
//     }
}