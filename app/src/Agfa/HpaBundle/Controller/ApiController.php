<?php
namespace Agfa\HpaBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Agfa\HpaBundle\Entity\Journal;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Agfa\HpaBundle\Entity\Paiement;

/**
 * API to communicate with BioServeur.
 *
 * @Route("/biologie/api")
 */
class ApiController extends BaseController
{

    /**
     * Request received from BioServeur in order to add a new CR.
     *
     * @Route("/publication_compte_rendu", name="api_publication_compte_rendu")
     *
     * @Method ("POST")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function createCrAction(Request $request)
    {
        $parameters = $this->container->getParameter('agfa_hpa');
        $bioserveurEnv = $parameters['bioserveur_env'];
        $typeLog = 'ERROR';

        $requete = json_decode($request->getContent(), TRUE);

        // Compare the structure of the answer with what it should be
        $bien_formee = $this->checkRequestFormat($requete, [
            "authentification" => [
                "passphrase" => NULL
            ],
            "compte_rendu" => [
                "affichage_cr" => NULL,
                "identifiant_structure" => NULL,
                "identifiant_patient" => NULL,
                "numero_demande" => NULL,
                "solde_patient" => NULL,
                "source_paiement" => NULL,
                "contenu" => NULL,
                "entite_juridique" => NULL,
                "laboratoire" => NULL
            ]
        ]);

        if (! $bien_formee) {
            $reponse = [
                'statut' => "BAD_REQUEST",
                'message_erreur' => "Requête mal formée.",
                'supprimer_enrolement' => FALSE
            ];
        } else {
            try {
                $reponse = $this->createCr($requete, $request);
                $typeLog = $reponse['statut'];
            } catch (\Exception $e) {
                $typeLog = 'ERROR';
                $error = "Erreur lors de l'intégration d'un compte rendu reçu de BioServeur [" . $e->getMessage() . "]";
                $this->get('monolog.logger.api')->critical($error);
                $reponse = [
                    'statut' => 'UNKNOWN_ERROR',
                    'message_erreur' => $error,
                    'supprimer_enrolement' => FALSE
                ];
            }
        }

        if ($bioserveurEnv['debug'] == TRUE) {
            $this->get('communication_bioserveur')->loguerEchange($requete, $reponse, 'Create CR, '.$typeLog);
        }

        $json_reponse = json_encode($reponse);

        // ecriture de l'échange dans un fichier de log
        // $logfile = $this->get('kernel')->getLogDir() . '/bioserveur.log';
        // $stream = fopen($logfile, 'a');
        // $this->get('communication_bioserveur')->loguerEchange($stream, $requete, $reponse, 'Reception CR');
        // fclose($stream);

        $response = new Response();
        $response->setContent($json_reponse);
        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }

    /**
     * Request received from BioServeur in order to delete an existing CR.
     *
     * @Route("/notification_suppression", name="api_notification_suppression")
     *
     * @Method ("POST")
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function deleteCrAction(Request $request)
    {
        $parameters = $this->container->getParameter('agfa_hpa');
        $bioserveurEnv = $parameters['bioserveur_env'];
        $typeLog = 'ERROR';

        $json_requete = $request->getContent();
        $requete = json_decode($json_requete, TRUE);

        $bien_formee = $this->checkRequestFormat($requete, [
            "authentification" => [
                "passphrase" => NULL
            ],
            "notification_suppression" => [
                "timestamp" => NULL,
                "compte_rendu" => [
                    "identifiant_structure" => NULL,
                    "identifiant_patient" => NULL,
                    "numero_demande" => NULL
                ]
            ]
        ]);

        if (! $bien_formee) {
            $reponse = [
                'statut' => "BAD_REQUEST",
                'message_erreur' => "Requête mal formée.",
                'supprimer_enrolement' => FALSE
            ];
        } else {
            try {
                $reponse = $this->deleteCr($requete, $request);
                $typeLog = 'SUCCESS';
            } catch (\Exception $e) {
                $typeLog = 'ERROR';
                $error = "Erreur lors de l'intégration d'une notification de suppression reçue de BioServeur [" . $e->getMessage() . "]";
                $this->get('monolog.logger.api')->critical($error);
                $reponse = [
                    'statut' => 'UNKNOWN_ERROR',
                    'message_erreur' => $error,
                    'supprimer_enrolement' => FALSE
                ];
            }
        }

        if ($bioserveurEnv['debug'] == TRUE) {
            $this->get('communication_bioserveur')->loguerEchange($requete, $reponse, 'Delete CR, '.$typeLog);
        }

        $json_reponse = json_encode($reponse);

        $response = new Response();
        $response->setContent($json_reponse);
        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }

    // /**
    // * Request received from BioServeur in order to update a CR payment's status.
    // *
    // * @Route("/cr/set-paid", name="api_set_cr_paid")
    // *
    // * @Method ("POST")
    // * @param Request $request
    // * @return \Symfony\Component\HttpFoundation\Response $response
    // */
    // public function setCrsAsPaidAction(Request $request)
    // {
    // $json_requete = $request->getContent();
    // $requete = json_decode($json_requete, TRUE);

    // // Check format of request
    // // "constant_login" is an array
    // $bien_formee = $this->checkRequestFormat($requete, [
    // "authentification" => [
    // "passphrase" => NULL
    // ],
    // "paiement" => [
    // "constant_logins" => NULL
    // ]
    // ]);

    // // If request is valid, update db
    // if (! $bien_formee) {
    // $reponse = [
    // 'statut' => "BAD_REQUEST",
    // 'message_erreur' => "Requête mal formée."
    // ];
    // } else {
    // try {
    // $reponse = $this->setCrsAsPaid($request, $requete);
    // } catch (\Exception $e) {
    // $error = "Erreur lors de la mise à jour de l'état du paiement [" . $e->getMessage() . "]";
    // $this->get('monolog.logger.api')->error($error);
    // $reponse = [
    // 'statut' => 'UNKNOWN_ERROR',
    // 'message_erreur' => $error
    // ];
    // }
    // }

    // // Prepare and return response
    // $json_reponse = json_encode($reponse);

    // $response = new Response();
    // $response->setContent($json_reponse);
    // $response->headers->set('Content-Type', 'application/json');

    // return $response;
    // }

    /**
     * Request received from BioServeur in order to update all data related to the Entité Juridique: Paiement,
     * list of linked laboratories and details of the entité juridique
     * @Route("/ej/update", name="api_ej_update")
     *
     * @Method ("POST")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function updateEntiteJuridiqueAction(Request $request)
    {
        $parameters = $this->container->getParameter('agfa_hpa');
        $bioserveurEnv = $parameters['bioserveur_env'];
        $typeLog = 'ERROR';

        $jsonRequest = $request->getContent();
        $entiteJuridiqueDetails = json_decode($jsonRequest, TRUE);

        // Check format of request
        $bien_formee = $this->checkRequestFormat($entiteJuridiqueDetails, [
            "authentification" => [
                "passphrase" => NULL
            ],
            "statut" => NULL,
            "message_erreur" => NULL,
            "entite_juridique" => [
                "finess" => NULL,
                "liste_labo_login" => NULL,
                "code_ej" => NULL,
                "code_postal" => NULL,
                "raison_sociale" => NULL,
                "adresse" => NULL,
                "libelle" => NULL,
                "paiement" => [
                    "nom" => NULL,
                    "prenom" => NULL,
                    "telephone" => NULL,
                    "email" => NULL,
                    "tpe" => NULL,
                    "cle" => NULL,
                    "societe" => NULL,
                    "affichage_cr" => NULL,
                    "actif" => NULL,
                    "liste_labo_login" => NULL
                ]
            ]
        ]);

        // If request is valid, update db
        if (! $bien_formee) {
            $reponse = [
                'statut' => "BAD_REQUEST",
                'message_erreur' => "Requête mal formée."
            ];
        } else {
            try {
                $reponse = $this->updateEntiteJuridique($entiteJuridiqueDetails, $request);
                $typeLog = 'SUCCESS';
            } catch (\Exception $e) {
                $typeLog = 'ERROR';
                $error = "Erreur lors de la mise à jour ou de la création d'une entrée de paiement [" . $e->getMessage() . "]";
                $this->get('monolog.logger.api')->error($error);
                $reponse = [
                    'statut' => 'UNKNOWN_ERROR',
                    'message_erreur' => $error
                ];
            }
        }

        if ($bioserveurEnv['debug'] == TRUE) {
            $this->get('communication_bioserveur')->loguerEchange($entiteJuridiqueDetails, $reponse, 'Update EJ, '.$typeLog);
        }

        // Prepare and return response
        $json_reponse = json_encode($reponse);

        $response = new Response();
        $response->setContent($json_reponse);
        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }

    /**
     * @Route("/cr/update/partial", name="api_cr_update_partial")
     *
     * @Method ("POST")
     */
    public function partialUpdateCrAction(Request $request)
    {
        $parameters = $this->container->getParameter('agfa_hpa');
        $bioserveurEnv = $parameters['bioserveur_env'];
        $typeLog = "ERROR";

        $jsonRequest = $request->getContent();
        $details = json_decode($jsonRequest, TRUE);

        $bien_formee = $this->checkRequestFormat($details, [
            "authentification" => [
                "passphrase" => NULL
            ],
            "compte_rendu" => array(
                "numero_demande" => NULL,
                "identifiant_structure" => NULL,
                "identifiant_patient" => NULL,
                "affichage_cr" => NULL,
                "solde_patient" => NULL, // Has to be a float with 2 decimal. If 10, then 10.00
                "source_paiement" => NULL // Can be false or 'not_paid' for unpaid, 'labo' or 'online'
            )
        ]);

        // If request is valid, update db
        if (! $bien_formee) {
            $reponse = [
                'statut' => "BAD_REQUEST",
                'message_erreur' => "Requête mal formée."
            ];
        } else {
            try {
                $reponse = $this->partialUpdateCr($details, $request);
                $typeLog = 'SUCCESS';
            } catch (\Exception $e) {
                $typeLog = 'ERROR';
                $error = "Erreur lors de la mise à jour du solde ou du statut de l'affichage d'un CR [" . $e->getMessage() . "]";
                $this->get('monolog.logger.api')->error($error);
                $reponse = [
                    'statut' => 'UNKNOWN_ERROR',
                    'message_erreur' => $error
                ];
            }
        }

        if ($bioserveurEnv['debug'] == TRUE) {
            $this->get('communication_bioserveur')->loguerEchange($details, $reponse, 'Partial update CR, '.$typeLog);
        }

        // Prepare and return response
        $json_reponse = json_encode($reponse);

        $response = new Response();
        $response->setContent($json_reponse);
        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }

    /**
     * Update only some specific details about a CR.
     * This is done because BioServeur cannot resend an editCr unless it has been updated.
     *
     * @param array $details
     * @param Request $request
     * @return string[]|boolean[]|\Symfony\Bundle\FrameworkBundle\Translation\Translator[]
     */
    private function partialUpdateCr($details, $request)
    {
        $em = $this->getDoctrine()->getManager();
        $demandeMgr = $this->get('agfa_hpa.demande_manager');
        $compteRendu = $details['compte_rendu'];

        // Check passphrase
        $authentification = $this->checkPassphrase($details['authentification'], $request);

        if ($authentification !== TRUE) {
            return [
                'statut' => 'UNAUTHORIZED',
                'message_erreur' => $this->trans('agfa.hpa_bundle.api.error.passphrase'),
                'supprimer_enrolement' => FALSE
            ];
        }

        // Get a demande
        $bsrvrDemande = $compteRendu['numero_demande'];
        $bsrvrNopat = $compteRendu['identifiant_patient'];
        $bsrvrLaboId = $compteRendu['identifiant_structure'];

        $bsrvrLaboId = substr($bsrvrLaboId, 0, 4);
        $bsrvrConstantLogin = $bsrvrLaboId . $bsrvrNopat;

        $demande = $demandeMgr->getRepository()->findOneBy(array(
            'bsrvrConstantLogin' => $bsrvrConstantLogin,
            'nodemx' => $bsrvrDemande
        ));

        if(!$demande){
            return [
                'statut' => 'DEMANDE_INCONNUE',
                'message_erreur' => "La demande n'existe pas"
            ];
        }

        $affichageCr = $compteRendu['affichage_cr'];
        if (isset($affichageCr)) {
            $previousAffichageCr = $demande->getAffichageCr();
            $demande->setAffichageCr($affichageCr);
        }

        $soldePatient = $compteRendu['solde_patient'];
        if (isset($soldePatient)) {
            $previousSoldePatient = $demande->getPaieSolde();
            $demande->setPaieSolde($soldePatient);
        }

        $sourcePaiement = $compteRendu['source_paiement'];
        if (isset($sourcePaiement)) {
            $previousSourcePaiement = $demande->getPaieSource();
            $demande->setPaieSource($sourcePaiement);
        }

        $em->flush();

        // Log an entry in the journal
        $tplJournalLog = 'Modifié de <strong>%s</strong> vers <strong>%s</strong>';
        $modifiedData = array();

        if(isset($previousAffichageCr) && $previousAffichageCr != $affichageCr){
            $previousAffichageCr = $previousAffichageCr ? 'visible même avant paiement' : 'bloqué jusqu\'à paiement';
            $affichageCr = $affichageCr ? 'visible même avant paiement' : 'bloqué jusqu\'à paiement';
            $modifiedData['affichage_cr'] = sprintf($tplJournalLog, $previousAffichageCr, $affichageCr);
        }

        if(isset($previousSoldePatient) && $previousSoldePatient != $soldePatient){
            $modifiedData['solde_patient'] = sprintf($tplJournalLog, $previousSoldePatient, $soldePatient);
        }

        if(isset($previousSourcePaiement) && $previousSourcePaiement != $sourcePaiement){
            $modifiedData['source_paiement'] = sprintf($tplJournalLog, $previousSourcePaiement, $sourcePaiement);
        }

        // Only save if there is any modification
        if(count($modifiedData) > 0){
            $em->flush();

            $this->get('journalist')->addInfo(Journal::EVENT_CR_REMPLACEMENT_PARTIEL, $demande, $demande->getPatient()
                ->getUser(), sprintf('Nodemx %s, constant login %s', $demande->getNodemx(), $demande->getBsrvrConstantLogin()), array(
                    'demande' => $modifiedData
                ));
        }

        return [
            'statut' => 'OK',
            'message_erreur' => ""
        ];
    }

    /**
     * Update an existing payment details.
     *
     * @param array $requete
     *            Data received from BioServeur.
     * @param Request $request
     * @return array
     */
    private function updateEntiteJuridique(array $entiteJuridiqueDetails, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $paiementMgr = $this->get('agfa_hpa.paiement_manager');
        $etablissementMgr = $this->get('agfa_hpa.etablissement_manager');
        $paiementRepository = $em->getRepository('AgfaHpaBundle:Paiement');

        /* traitement authentification */
        $authentification = $this->checkPassphrase($entiteJuridiqueDetails['authentification'], $request);

        if ($authentification !== TRUE) {
            return [
                'statut' => 'UNAUTHORIZED',
                'message_erreur' => $this->trans('agfa.hpa_bundle.api.error.passphrase'),
                'supprimer_enrolement' => FALSE
            ];
        }

        $ejDetails = $entiteJuridiqueDetails['entite_juridique'];
        $paymentDetails = $ejDetails['paiement'];

        // Check compulsory payment details given
        $compulsoryDataCheck = $this->checkCompulsoryPaymentDetails($paymentDetails);
        if ($paymentDetails['actif'] == true && is_array($compulsoryDataCheck) && ! empty($compulsoryDataCheck)) {
            // Add error to log and stop here
            return [
                'statut' => 'BAD_REQUEST',
                'message_erreur' => 'Une ou plusieurs des données suivantes sont manquantes : ' . implode(', ', $compulsoryDataCheck)
            ];
        }

        // Update EJ details
        if ($entiteJuridique = $etablissementMgr->getEtablissement($ejDetails['finess'])) {
            $etablissementMgr->updateEtablissement($entiteJuridique, $ejDetails);
        }

        // Get Paiement details through TPE, if any
        $payment = $paiementRepository->findOneBy(array(
            'tpe' => $paymentDetails['tpe']
        ));

        // Reset links to payment
        // NOTE Unlinks have to be perfomed before getting data for Etablissement, otherwise the entity manager does noes react very well.
        // if ($payment) {
        // $etablissementRepository->unlinkEtablissementPaiement($payment);
        // }else{
        // // Create a new Payment entry if none is found
        // $payment = new Paiement();
        // }

        if (! $payment) {
            // Create a new Payment entry if none is found
            $payment = new Paiement();
        }

        // Reset labo link to ET
        // NOTE Disabled as a CR without lab would not be displayed and would create problems in the interface. Also: integrity problem for past patient record!!! Deploy history feature to fix.
        // $etablissementRepository->unlinkLabsFromEntiteJuridique($entiteJuridique);

        // Make one array of all labo logins so that only one loop is used to update the data
        $laboLogins = array_merge($paymentDetails['liste_labo_login'], $ejDetails['liste_labo_login']);
        $laboLogins = array_unique($laboLogins);

        foreach ($laboLogins as $laboLogin) {
            if ($establishment = $etablissementMgr->getEtablissement($laboLogin)) {
                // Update payment if it exists
                if ($payment) {
                    $paiementMgr->updatePayment($payment, $paymentDetails);

                    // Update payment details
                    if (in_array($laboLogin, $paymentDetails['liste_labo_login'])) {
                        $establishment->setPaiement($payment);
                    }
                }

                // Update link betwwen lab and entité juridique
                if (in_array($laboLogin, $ejDetails['liste_labo_login'])) {
                    $establishment->setParent($entiteJuridique);
                }
                $em->persist($establishment);
            }
        }
        $em->flush();

        return [
            'statut' => 'OK',
            'message_erreur' => ""
        ];
    }

    /**
     * Create a new CR.
     *
     * @param array $requete
     *            Data received from BioServeur.
     * @param Request $request
     * @return array
     */
    private function createCr(array $requete, Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        // Check authorization
        $authentification = $this->checkPassphrase($requete['authentification'], $request);

        if ($authentification !== TRUE) {
            return [
                'statut' => 'UNAUTHORIZED',
                'message_erreur' => $this->trans('agfa.hpa_bundle.api.error.passphrase'),
                'supprimer_enrolement' => FALSE
            ];
        }

        /* traitement données */
        $parametersCompteRendu = $requete['compte_rendu'];
        $contenu = $parametersCompteRendu['contenu'];

        $entiteJuridiqueArray = $parametersCompteRendu['entite_juridique'];
        $identifiantPatient = $parametersCompteRendu['identifiant_patient'];

        $entiteJuridique = $em->getRepository('AgfaHpaBundle:Etablissement')->findOneBy([
            'bsrvrRefEtablissement' => $entiteJuridiqueArray['finess']
        ]);

        if (! $entiteJuridique) {
            return [
                'statut' => 'ENTITE_JURIDIQUE_INCONNU',
                'message_erreur' => sprintf("L'entité juridique %s n'existe pas", $entiteJuridiqueArray['finess']),
                'supprimer_enrolement' => TRUE
            ];
        }

        $patient = $em->getRepository('AgfaHpaBundle:Patient')->findOneBy([
            'bsrvrNopat' => $identifiantPatient,
            'etablissement' => $entiteJuridique
        ]);

        if (! $patient) {
            return [
                'statut' => 'PATIENT_INCONNU',
                'message_erreur' => "Ce patient n'existe pas",
                'supprimer_enrolement' => TRUE
            ];
        }

        $data = $contenu['data'];
        if ($contenu['base64']) {
            $xmlEditcr = base64_decode($data);
        }

        /* @var $demande Demande  */
        $demande = $this->get('editcr_connector')->creerDemande($xmlEditcr, null, $parametersCompteRendu);

        $em->flush();

        $this->get('journalist')->addInfo(Journal::EVENT_BSRVRCOM_CR, $demande, $demande->getPatient()
            ->getUser(), sprintf('Nodemx %s, constant login %s', $demande->getNodemx(), $demande->getBsrvrConstantLogin()), array(
            'demande' => array(
                'bsrvr_constant_login' => $demande->getBsrvrConstantLogin(),
                'patient' => $demande->getPatient()
            )
        ));

        $em->flush();

        return [
            'statut' => 'OK',
            'message_erreur' => "",
            'supprimer_enrolement' => FALSE
        ];
    }

    /**
     * Delete an existing CR.
     *
     * @param array $requete
     *            Data received from BioServeur.
     * @param Request $request
     * @return array
     */
    private function deleteCr(array $requete, Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        /* traitement authentification */
        $authentification = $this->checkPassphrase($requete['authentification'], $request);

        if ($authentification !== TRUE) {
            return [
                'statut' => 'UNAUTHORIZED',
                'message_erreur' => $this->trans('agfa.hpa_bundle.api.error.passphrase'),
                'supprimer_enrolement' => FALSE
            ];
        }

        $compteRendu = $requete['notification_suppression']['compte_rendu'];
        $bsrvrConstLogin = $compteRendu['identifiant_structure'] . $compteRendu['identifiant_patient'];
        $nodemx = $compteRendu['numero_demande'];

        // Find a demande with the data given by Bioserveur
        $demande = $this->get('agfa_hpa.demande_manager')->findDemande($bsrvrConstLogin, $nodemx);

        if (is_null($demande)) {
            return [
                'statut' => 'DEMANDE_INCONNUE',
                'message_erreur' => "La demande n'existe pas"
            ];
        }

        $this->get('journalist')->addInfo(Journal::EVENT_CR_SUPPRESSION, $demande, $demande->getPatient()
            ->getUser(), "Par Bioserveur. Nodemx $nodemx, constant login $bsrvrConstLogin");

        $em->remove($demande);
        $em->flush();

        return [
            'statut' => 'OK',
            'message_erreur' => ""
        ];
    }

    // /**
    // * NOTE Was done as a mean to mark a list of CR as paid. Has been replaced by a single request sent by BioServeur when a CR is being updated.
    // * Set 'paieSource' (entity demande) to 'paid' and solde to 0.
    // *
    // * @param Request $request
    // * @param array $requete
    // * Data received from BioServeur.
    // * @return array
    // */
    // private function setCrsAsPaid(Request $request, array $requete)
    // {
    // $em = $this->getDoctrine()->getManager();
    // $nonUpdatedConstantLogins = array();

    // /* traitement authentification */
    // $authentification = $this->checkPassphrase($requete['authentification'], $request);

    // if ($authentification !== TRUE) {
    // return [
    // 'statut' => 'UNAUTHORIZED',
    // 'message_erreur' => $this->trans('agfa.hpa_bundle.api.error.passphrase'),
    // 'supprimer_enrolement' => FALSE
    // ];
    // }

    // // Get demande id if it exists
    // $constantLogins = $requete['paiement']['constant_logins'];
    // foreach ($constantLogins as $constantLogin) {
    // if ($demande = $em->getRepository('AgfaHpaBundle:Demande')->findOneBy(array(
    // 'bsrvrConstantLogin' => $constantLogin
    // ))) {
    // $demande->setPaieSource(Demande::PAID_LABORATORY)->setPaieSolde(0);
    // $em->flush();
    // } else {
    // $nonUpdatedConstantLogins[] = $constantLogin;
    // }
    // }

    // $gluedNonUpdatedDemandes = implode(',', $nonUpdatedConstantLogins);

    // if (! $demande) {
    // return [
    // 'statut' => 'DEMANDES_INCONNUS',
    // 'message_erreur' => "Ce ou ces demandes n'ont pas été trouvés.",
    // 'demandes' => $gluedNonUpdatedDemandes
    // ];
    // } else {
    // return [
    // 'statut' => 'OK',
    // 'message_erreur' => ""
    // ];
    // }
    // }

    /**
     * Check that the passphrase provided matches the one set in configurations.
     *
     * @param array $authentification
     *            Data received from BioServeur.
     * @param Request $request
     * @return boolean TRUE if the passphrase matches the one expected, FALSE otherwise.
     */
    private function checkPassphrase(array $authentification, Request $request)
    {
        $ip = $request->getClientIp();
        $passphrase = $authentification['passphrase'];

        $this->get('monolog.logger.api')->debug("Echange API avec la machine [$ip] authentifiee par [$passphrase]");

        $parametersAgfaHpa = $this->container->getParameter('agfa_hpa');
        $passphraseBioserveur = $parametersAgfaHpa['bioserveur']['pass_bioserveur'];

        $success = FALSE;
        if ($passphraseBioserveur == $passphrase) {
            $success = TRUE;
        } else {
            $this->get('monolog.logger.api')->critical("Access to Bioserveur denied with actual passphrase. Check HPA's configurations.");
        }

        return $success;
    }

    /**
     * Check that the request received from BioServeur is formated as required.
     * The model has to be an array with all values set to NULL.
     *
     * @param array $requete
     *            Data received from BioServeur.
     * @param array $format
     *            The expected format for the request.
     * @return boolean TRUE if the request is properly formated, FALSE otherwise.
     */
    private function checkRequestFormat(array $requete, array $format)
    {
        if (! is_array($requete) || ! is_array($format) || count($requete) !== count($format)) {
            return FALSE;
        }

        foreach ($format as $key => $value) {
            if (! key_exists($key, $requete)) {
                return FALSE;
            }
            if ($value !== NULL && ! $this->checkRequestFormat($requete[$key], $value)) {
                return FALSE;
            }
        }

        return TRUE;
    }

    /**
     * Check that compulsory values for Payment are all given by BioServeur.
     *
     * @param array $paymentDetails
     */
    private function checkCompulsoryPaymentDetails($paymentDetails)
    {
        $errors = array();

        $compulsoryDetails = array(
            'tpe',
            'cle',
            'email',
            'societe'
        );

        foreach ($compulsoryDetails as $compulsoryDetail) {
            $field = $paymentDetails[$compulsoryDetail];

            if (! isset($field) || empty($field)) {
                $this->get('monolog.logger.api')->addError(sprintf('Compulsory data is missing for Paiement entity: %s', $compulsoryDetail));
                array_push($errors, $compulsoryDetail);
            }
        }

        return $errors;
    }
}
