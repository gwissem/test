<?php
namespace Agfa\HpaBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Cache;
use Symfony\Component\HttpFoundation\Request;
use Agfa\HpaBundle\Entity\Enrolement;
use Agfa\HpaBundle\Form\Type\EnrolementType;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * @Route("/")
 *
 * @author charles
 *
 */
class DefaultController extends Controller
{

    /**
     * @Route("", name="default_index")
     * @Route("accueil/")
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request)
    {
        $user = $this->getUser();
        $securityContext = $this->container->get('security.authorization_checker');

        if ($securityContext->isGranted('IS_AUTHENTICATED_FULLY')) {

            if ($this->get('contexte')->getPersonneContexte()) {
                return $this->redirect($this->generateUrl('demande_index_default'));
            } else {
                $personnes = $this->get('agfa_hpa.personne_manager')->getPersonnes($user);
                $nb = count($personnes);
                if ($nb === 0) {
                    $form = $this->createForm(EnrolementType::class, new Enrolement(), [
                        'action' => $this->generateUrl('enrolement')
                    ]);
                    // Welcome page with 'enrolement' form
                    return $this->render(':Default:nouveau-compte.html.twig', [
                        'form' => $form->createView()
                    ]);
                } else {
                    // Select a personne page
                    return $this->redirect($this->generateUrl('selection_personne'));
                }
            }
        }

        return $this->forward('FOSUserBundle:Security:login');
    }

    /**
     * @Route("switch-personne/{personneId}/", name="default_switch_personne", requirements={"personneId"="\d+"})
     */
    public function switchPersonne(Request $request, $personneId)
    {
        $user = $this->getUser();
        $personnes = $this->get('agfa_hpa.personne_manager')->getPersonnes($user);

        // NOTE This is to switch personne. Why is it here? Reponse: because there is no PersonneControler for now.
        // si on recoit un personne_id et qu'il correspond à une personne
        // ou bien si on n'a pas de personne en session mais qu'il n'y a qu'une personne possible
        // dans ces 2 cas, on définit cette personne comme la personne en session
        $nb = count($personnes);
        if (($personneId && $personne = $this->get('agfa_hpa.personne_manager')->getPersonne($user, $personneId)) || (! $this->get('contexte')->getPersonneContexte() && ($nb === 1) && ($personne = array_shift($personnes)))) {

            $this->get('contexte')->setPersonneContexte($personne);

            // Update the current personne for the user
            $em = $this->getDoctrine()->getManager();
            $em->flush();

            if ($request->headers->get('referer') == $this->generateUrl('recherche_demande', array(), UrlGeneratorInterface::ABSOLUTE_URL)) {
                return $this->redirect($request->headers->get('referer'));
            } else {
                return $this->redirect($this->generateUrl('default_index'));
            }
        }
    }

    /**
     * Page displaying the "personnes" a user has within his/her account
     * in order to choose one before going into his/her account.
     *
     * @Route("selection-personne/", name="selection_personne")
     * @Template(":Default:selection-personne.html.twig")
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function selectionPersonneAction(Request $request)
    {
        $user = $this->getUser();

        $this->get('session')->remove('personne_id');

        $personnes = $this->get('agfa_hpa.personne_manager')->getPersonnes($user);

        // If the user only has one personne in its account, select it
        if (count($personnes) == 1) {
            return $this->redirectToRoute('default_switch_personne', array(
                'personneId' => $personnes[0]->getId()
            ));
        }

        return array(
            'personnes' => $personnes
        );
    }

    /**
     * @Route("conditions-utilisation/", name="default_cgu")
     * @Template(":Default:cgu.html.twig")
     * @Cache(smaxage="60")
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function cguAction()
    {
        return array();
    }

    /**
     * @Route("aide/", name="default_faq")
     * @Template(":Default:faq.html.twig")
     * @Cache(smaxage="60")
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function faqAction()
    {
        return array();
    }

    /**
     * Crée un fichier contenant les URL publiques de l'application pour les moteurs de recherche.
     *
     * @Route("sitemap.txt", name="default_sitemap")
     * @Template(":Default:sitemap.html.twig")
     * @Cache(smaxage="60")
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function sitemapAction()
    {
        $routes = array(
            'default_index',
            'default_faq',
            'default_cgu',
            'consultation_directe',
            'fos_user_security_login',
            'fos_user_resetting_request',
            'fos_user_registration_register'
        );

        return array(
            'routes' => $routes
        );
    }

    /**
     * Creates a manifest for progressive web app.
     *
     * @Route("manifest.json", name="default_manifest")
     * @Cache(smaxage="60")
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function manifestAction()
    {
        $themeName = $this->getParameter('client_theme');

        $manifest = array(
            'theme_color' => $this->getParameter('client_theme_color'),
            'background_color' => $this->getParameter('client_theme_color'),
            'name' => $this->getParameter('client_brand') . " - Résultats d'analyses médicales.",
            'short_name' => $this->getParameter('client_brand'),
            'start_url' => '.'.$this->generateUrl('default_index'),
            'description' => "Sauvegarder et gérer vos résultats d'analyses médicales.",
            'display' => 'standalone',
            'orientation' => 'portrait',
            'icons' => array(
                array(
                    "src" => '/themes/' . $themeName . "/images/android-icon-36x36.png",
                    "sizes" => "36x36",
                    "type" => "image\/png",
                    "density" => "0.75"
                ),
                array(
                    "src" => '/themes/' . $themeName . "/images/android-icon-48x48.png",
                    "sizes" => "48x48",
                    "type" => "image/png",
                    "density" => "1.0"
                ),
                array(
                    "src" => '/themes/' . $themeName . "/images/android-icon-72x72.png",
                    "sizes" => "72x72",
                    "type" => "image/png",
                    "density" => "1.5"
                ),
                array(
                    "src" => '/themes/' . $themeName . "/images/android-icon-96x96.png",
                    "sizes" => "96x96",
                    "type" => "image/png",
                    "density" => "2.0"
                ),
                array(
                    "src" => '/themes/' . $themeName . "/images/android-icon-144x144.png",
                    "sizes" => "144x144",
                    "type" => "image/png",
                    "density" => "3.0"
                ),
                array(
                    "src" => '/themes/' . $themeName . "/images/android-icon-192x192.png",
                    "sizes" => "192x192",
                    "type" => "image/png",
                    "density" => "4.0"
                )
            )
        );

        $response = new JsonResponse($manifest);

        return $response;
    }

    /**
     * @Route("guide-utilisateur/", name="default_user_guide")
     * @Template(":Default:user-guide.html.twig")
     * @Cache(smaxage="60")
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    function userGuideAction()
    {
        return array();
    }

    /**
     * @Route("creer-un-compte/", name="default_create_account_guide")
     * @Template(":Default:create-account-guide.html.twig")
     * @Cache(smaxage="60")
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    function createAccountGuideAction()
    {
        return array();
    }
}
