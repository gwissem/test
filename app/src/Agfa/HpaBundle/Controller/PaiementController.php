<?php
namespace Agfa\HpaBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Agfa\HpaBundle\Entity\Demande;
use Agfa\HpaBundle\Entity\Journal;
use Agfa\MoneticoPaiementBundle\Entity\Response as MoneticoEntityResponse;
use Symfony\Component\HttpFoundation\Response;
use Agfa\HpaBundle\Entity\BsrvrCom;

/**
 * @Route("/paiement/")
 *
 * @author charles
 *
 */
class PaiementController extends Controller
{

    /**
     * List all demandes that have not been paid yet.
     *
     * @Route("attente/", name="paiement_attente")
     */
    public function attenteAction(Request $request)
    {
        // Check that the user has access to this module
        $this->checkPaymentModuleAccess();

        // Get demandes with no payement made and for which the labo allow online payment
        $demandes = $this->get('agfa_hpa.demande_manager')->getUnpaidDemandes($this->getUser());

        return $this->render(':paiement:attente.html.twig', array(
            'demandes' => $demandes
        ));
    }

    /**
     * @Route("confirmer/{demandeId}", name="paiement_confirm", requirements={"demandeId":"\d+"})
     */
    public function confirmAction(Request $request, $demandeId)
    {
        $data = array();
        $requestForm = NULL;

        $user = $this->getUser();
        $em = $this->getDoctrine()->getManager();
        $this->demandeMgr = $this->get('agfa_hpa.demande_manager');
        $this->requestMgr = $this->get('agfa_hpa.request_manager');

        // Check that the user has access to this module
        $this->checkPaymentModuleAccess();

        // Get demande with no payment made and for which the labo allow online payment
        $demande = $this->demandeMgr->getUnpaidDemande($user, $demandeId);
        $etablissement = $demande->getEtablissement();

        /**
         * Build payment request form
         *
         * @var \Agfa\MoneticoPaiementBundle\Library\Request $payment
         */
        $payment = $this->get('agfa_monetico_paiement.request_service');
        $moneticoApi = $this->get('agfa_monetico_paiement.api_service');
        $journal = $this->get('journalist');

        $payment->setMontant($demande->getPaieSolde())
            ->setTexteLibre(sprintf('Nodemx %s, constantlogin %s', $demande->getNodemx(), $demande->getBsrvrConstantLogin()))
            ->setTpe($etablissement->getPaiement()
            ->getTpe())
            ->setCle($etablissement->getPaiement()
            ->getCle())
            ->setSociete($etablissement->getPaiement()
            ->getSociete())
            ->setEmail($user->getEmail());

        // Build form
        $requestForm = $moneticoApi->createRequestForm($payment)->createView();

        $request = $moneticoApi->getRequest();

        // We need to pick the same data from request but from the child class so that I can link it to demande
        $req = $em->getRepository('AgfaHpaBundle:Request')->find($request);

        // Check that request is not already linked to a demande
        if (! count($req->getDemandes()) > 0) {
            $demande->addRequest($req);
            $em->persist($demande);
        }

        // Create a form to remove request if user cancels request for payment
        $cancelForm = $this->createFormBuilder()
            ->setMethod('DELETE')
            ->setAction($this->generateUrl('paiement_confirm_cancel', array(
            'requestId' => $req->getId()
        )))
            ->getForm();

        // The following is specific to HPA
        $paymentArray = $journal->convertPaymentRequest($payment);
        $journal->addInfo(Journal::EVENT_PAYMENT_REQUEST, NULL, $this->getUser(), sprintf('Nodemx %s, constant login %s', $demande->getNodemx(), $demande->getBsrvrConstantLogin()), $paymentArray);

        return $this->render(':paiement:confirm.html.twig', array(
            'demande' => $demande,
            'data' => $data,
            'requestForm' => $requestForm,
            'cancelForm' => $cancelForm->createView()
        ));
    }

    /**
     * @Route("confirmer/annuler/{requestId}", name="paiement_confirm_cancel", requirements={"requestId":"\d+"})
     */
    public function cancelConfirmAction(Request $request, $requestId)
    {
        $request = $this->get('agfa_hpa.request_manager')->getUserRequest($this->getUser(), $requestId);

        $requestMgr = $this->get('agfa_hpa.request_manager');
        $requestMgr->deleteRequest($request);

        return $this->redirect($this->generateUrl('default_index'));
    }

    /**
     * Remove an entry in Monetico request table when the 'cancel' button is used.
     * Access to this controller is done through the security.yml file. It is restricted by Monetico servers' IP address.
     *
     * @Route("response/", name="paiement_response")
     */
    public function responseAction(Request $request)
    {
        $journal = $this->get('journalist');
        $ackReceipt = new Response();

        // Get Monetico API service
        $mntcApi = $this->get('agfa_monetico_paiement.api_service');

        // Get Monetico response service.
        $responseObject = $this->get('agfa_monetico_paiement.response_service');

        // Get data from response
        // NOTE The commented if is a debug aternative to set the response to GET instead of POST. Other part is in Agfa\MoneticoPaiementBundle\Service::createResponse()
        $responseData = $request->request->all();
        // Debug only
        // $responseData = $request->getQueryString();

        $reference = $request->get('reference');

        // Retrive the 'cle' for the payment from the the etablissement table
        $requestEntity = $this->get('agfa_hpa.request_manager')
            ->getRepository()
            ->findOneBy(array(
            'reference' => $reference
        ));

        // Even though a request can be for more than one demande, the payment can only be for one etablissement, thus the cle will be unique.
        // It it therefore ok to use [0] on demandes.
        $cle = $requestEntity->getDemandes()[0]->getEtablissement()
            ->getPaiement()
            ->getCle();

        if (! $response = $mntcApi->createResponse($responseObject, $responseData, $cle)) {
            // Set response to 'not good'
            $ackReceipt->setContent("version=2\ncdr=1");
            $this->get('logger')->addCritical('The response received from Monetico Paiement server did not match expectations.');
        } else {
            $demande = $this->get('agfa_hpa.demande_manager')->getDemandeByReference($response->getReference());

            switch ($response->getCodeRetour()) {
                case MoneticoEntityResponse::CODE_RETOUR_PAIEMENT:
                case MoneticoEntityResponse::CODE_RETOUR_PAYETEST:
                    // Work out the new balance
                    $previousSolde = $demande->getPaieSolde();
                    $montant = $mntcApi->parseMontant($response->getMontant());
                    $paidMontant = $montant['montant'];

                    $newBalance = $previousSolde - $paidMontant;

                    $demande->setPaieSolde($newBalance)->setPaieSource(Demande::PAID_ONLINE);

                    $em = $this->getDoctrine()->getManager();
                    $em->persist($demande);

                    // Send update to Bioserveur API: add an entry to hpa_bioserveur_update
                    $bsrvrCom = new BsrvrCom();
                    $bsrvrCom->setDemande($demande);
                    $bsrvrCom->setAction(BsrvrCom::ACTION_UPDATE_PAYMENT);
                    $em->persist($bsrvrCom);

                    $em->flush();

                    $paymentArray = $journal->convertPaymentResponse($response);
                    $journal->addInfo(Journal::EVENT_PAYMENT_RESPONSE, NULL, $demande->getPatient()
                        ->getUser(), sprintf('Nodemx %s, constant login %s', $demande->getNodemx(), $demande->getBsrvrConstantLogin()), $paymentArray);
                    break;
                case MoneticoEntityResponse::CODE_RETOUR_ANNULATION:
                    $paymentArray = $journal->convertPaymentResponse($response);
                    $journal->addDanger(Journal::EVENT_PAYMENT_RESPONSE, NULL, $demande->getPatient()
                        ->getUser(), sprintf('Nodemx %s, constant login %s', $demande->getNodemx(), $demande->getBsrvrConstantLogin()), $paymentArray);
                    break;
                default:
                    $this->get('logger')->addCritical('Code retour for Monetico response is null and should not be.');
                    break;
            }

            // Set response to 'all good'
            $ackReceipt->setContent("version=2\ncdr=0");
        }

        $ackReceipt->headers->set('Content-Type', 'text/plain');

        return $ackReceipt;
    }

    /**
     * @Route("historique/", name="paiement_historique")
     */
    public function historiqueAction(Request $request)
    {
        // Check that the user has access to this module
        $this->checkPaymentModuleAccess();

        // Get all paid entry for a given user
        $demandes = $this->get('agfa_hpa.demande_manager')->getPaidDemandes($this->getUser());

        return $this->render(':paiement:historique.html.twig', array(
            'demandes' => $demandes
        ));
    }

    /**
     * Route to which the client comes back to the site when ??? To search for this information
     *
     * @Route("retour/", name="paiement_retour")
     */
    public function retourAction(Request $request)
    {
        return $this->render(':paiement:retour.html.twig', array());
    }

    /**
     * Route to which the client comes back to the site when the process went fine.
     *
     * @Route("retour-ok/", name="paiement_retour_ok")
     * Test : http://hexapat-reloaded.loc/app_dev.php/paiement/retour-ok/?TPE=1234567&date=05%2f12%2f2006_a_11%3a55%3a23&montant=62%2e75EUR&reference=ABERTYP00145&MAC=e4359a2c18d86cf2e4b0e646016c202e89947b04&texte-libre=LeTexteLibre&code-retour=paiement&cvx=oui&vld=1208&brand=VI&status3ds=1&numauto=010101&originecb=FRA&bincb=010101&hpancb=74E94B03C22D786E0F2C2CADBFC1C00B004B7C45&ipclient=127%2e0%2e0%2e1&originetr=FRA&veres=Y&pares=Y
     */
    public function retourOkAction(Request $request)
    {
        return $this->render(':paiement:retour-ok.html.twig');
    }

    /**
     * Route to which the client comes back to the site when an error occurred.
     *
     * @Route("retour-err/", name="paiement_retour_err")
     */
    public function retourErrAction(Request $request)
    {
        return $this->render(':paiement:retour-err.html.twig', array(
            'request' => $request
        ));
    }

    /**
     * Check if a user has payment module access.
     *
     * @throws NotFoundHttpException
     */
    private function checkPaymentModuleAccess()
    {
        if ($this->getUser()->getPaymentIsEnabled() === FALSE) {
            throw new NotFoundHttpException("Le module de paiement n'est pas accessible à cet utilisateur.");
        }

        return;
    }
}
