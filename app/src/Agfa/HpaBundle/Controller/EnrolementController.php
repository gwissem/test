<?php
namespace Agfa\HpaBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Agfa\HpaBundle\Entity\Enrolement;
use Agfa\HpaBundle\Form\Type\EnrolementType;
use Symfony\Component\Form\FormError;
use Agfa\HpaBundle\Exception\EnrolementException;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use PiwikTracker;
use Agfa\HpaBundle\Entity\Journal;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Enrollment controller
 */
class EnrolementController extends Controller
{

    /**
     * Link a patient to the currently used account and retrieve the patient record.
     *
     * @Route("/enrolement/nouveau-compte-rendu", name="enrolement")
     * @Template(":Enrolement:consultationEnrolee")
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function consultationEnroleeAction(Request $request)
    {
        $user = $this->getUser();
        $enrolement = new Enrolement();

        $form = $this->createForm(EnrolementType::class, $enrolement);

        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($enrolement);

            try {
                /* @var $demande Demande */
                $demande = $this->get('communication_bioserveur')->demandeEnrolement($user, $enrolement);
            } catch (EnrolementException $e) {
                $form->addError(new FormError($e->getMessage()));
            }

            if (isset($demande)) {
                $rapprochement = $this->get('patients')->rapprocherPatient($demande);

                $em->flush();

                // Check that the demande that has just been retrieved belongs to the user that is currently logged in
                // else the redirection will not work
                if ($this->get('agfa_hpa.demande_manager')->getDemandeOnOtherUserAccount($user, $demande->getId())) {
                    $this->addFlash('warning', 'Ce compte rendu a déjà été téléchargé.');
                    $this->get('journalist')->addDanger(Journal::EVENT_CR_CREATION, $demande, $user, 'Ce compte rendu est déjà présent sur un compte utilisateur', array(
                        'demande' => array(
                            'patient' => $demande->getPatient(),
                            'bsrvr_constant_login' => $demande->getBsrvrConstantLogin()
                        )
                    ));
                }

                // If the CR does not belong to another user: SUCCESS
                if ($rapprochement) {
                    /* si on a réussi à rapprocher le patient, on affiche le CR */
                    if ($this->getParameter('piwik_api_status') === TRUE) {
                        PiwikTracker::$URL = 'https://' . $this->container->getParameter('host_piwik');
                        $tracker = new PiwikTracker($this->container->getParameter('piwik_site_id'));
                        $tracker->doTrackEvent('Comptes', 'Enrolement', 'Utilisateur inscrit');
                    }

                    return $this->redirect($this->generateUrl('demande_post_enrolement', [
                        'demandeId' => $demande->getId()
                    ]));
                } else {
                    /* sinon on affiche les infos qu'on a et on demande au user de choisir la bonne personne */
                    if ($this->getParameter('piwik_api_status') === TRUE) {
                        PiwikTracker::$URL = 'https://' . $this->container->getParameter('host_piwik');
                        $tracker = new PiwikTracker($this->container->getParameter('piwik_site_id'));
                        $tracker->doTrackEvent('Comptes', 'Enrolement', 'Utilisateur inscrit');
                    }

                    // Set hasUnassignedPatient to false to make sure that if the user does not pass the next step, there is a reminder to go back and do so
                    $user->setHasUnassignedPatient(TRUE);
                    $em->persist($user);
                    $em->flush($user);

                    return $this->redirectToRoute('patient_assign', [
                        'patientId' => $demande->getPatient()
                            ->getId()
                    ]);
                }
            }
            $em->flush();
        }

        return $this->render(':Enrolement:consultation-enrolee.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * Direct access to patient record without account as a pdf file.
     *
     * @Route("/direct", name="consultation_directe")
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function consultationDirecteAction(Request $request)
    {
        // Returns 404 if direct access to patient record is disabled.
        if($this->get('agfa_hpa.configuration_service')->getPatientRecordDirectAccess() == false){
            throw new NotFoundHttpException('Direct access to patient record is disabled.');
        }

        $enrolement = new Enrolement();

        $form = $this->createForm(EnrolementType::class, $enrolement);

        // Get data from the URL to pre-fill the form
        if ($enrolementLogin = $request->query->get('logincode')) {
            $form->getData()->setLogin($enrolementLogin);
            $form->setData($form->getData());
        }

        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($enrolement);

            try {
                $compteRenduPdf = $this->get('communication_bioserveur')->recupererCompteRendu($enrolement);
            } catch (EnrolementException $e) {
                // examiner $statut pour savoir quoi faire
                // $statut = $e->getStatut();
                // pour l'instant on ne sait pas quoi faire d'autre que réafficher le formulaire
                $form->addError(new FormError($e->getMessage()));
            }

            if (isset($compteRenduPdf)) {

                if ($this->getParameter('piwik_api_status') === TRUE) {
                    PiwikTracker::$URL = 'https://' . $this->container->getParameter('host_piwik');
                    $tracker = new PiwikTracker($this->container->getParameter('piwik_site_id'));
                    $tracker->doTrackEvent('Comptes rendus', 'Téléchargement', 'Sans compte');
                }

                $content_length = strlen($compteRenduPdf);

                $response = new Response();
                $response->setPrivate();
                $response->headers->set('Cache-Control', 'must-revalidate, post-check=0, pre-check=0');
                $response->headers->set('Content-Type', 'application/pdf');
                $response->headers->set('Content-Disposition', $response->headers->makeDisposition(ResponseHeaderBag::DISPOSITION_INLINE, "COMPTE_RENDU.pdf"));
                $response->headers->set('Content-Length', $content_length);
                $response->setContent($compteRenduPdf);

                return $response;
            }
        }

        return $this->render(':Enrolement:consultation-directe.html.twig', [
            'form' => $form->createView()
        ]);
    }
}
