<?php
namespace Agfa\HpaBundle\Manager;

use Doctrine\ORM\EntityManager;
// use Doctrine\ORM\Mapping\ClassMetadata;

class ConfigurationManager
{

    const CACHE_LIFETIME = 7200;

    const CACHE_KEY_GETCONFIGURATION = 'getConfiguration';

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    public function getRepository()
    {
        return $this->em->getRepository('AgfaHpaBundle:Configuration');
    }

    public function getConfiguration($themeName)
    {
        return $this->getRepository()
            ->getConfiguration($themeName)
            ->getQuery()
            ->useQueryCache(true)
//             ->useResultCache(true, self::CACHE_LIFETIME, self::CACHE_KEY_GETCONFIGURATION)
//             ->setCacheMode(ClassMetadata::CACHE_USAGE_NONSTRICT_READ_WRITE)
            ->setCacheable(true)
            ->setCacheRegion('slc_configuration')
            ->getSingleResult();
    }
}