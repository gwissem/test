<?php
namespace Agfa\HpaBundle\Manager;

use Doctrine\ORM\EntityManager;
use Agfa\HpaBundle\Manager\BaseManager;
use Agfa\UserBundle\Entity\User;

class PersonnePatientManager extends BaseManager
{

    protected $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    public function getRepository()
    {
        return $this->em->getRepository('AgfaHpaBundle:PersonnePatient');
    }

    public function getPersonnePatient(User $user, $personneId, $patientId)
    {
        return $this->getRepository()
            ->getPersonnePatient($user, $personneId, $patientId)
            ->getQuery()
            ->useQueryCache(true)
            ->getOneOrNullResult();
    }

    public function getPersonneIsUsed(User $user, $personneId)
    {
        return $this->getRepository()
            ->getPersonneIsUsed($user, $personneId)
            ->getQuery()
            ->useQueryCache(true)
            ->getResult();
    }

    public function getPatientIsUsed($personneId)
    {
        return $this->getRepository()
            ->getPatientIsUsed($personneId)
            ->getQuery()
            ->useQueryCache(true)
            ->getOneOrNullResult();
    }
}