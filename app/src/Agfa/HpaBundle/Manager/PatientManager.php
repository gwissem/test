<?php
namespace Agfa\HpaBundle\Manager;

use Doctrine\ORM\EntityManager;
use Agfa\HpaBundle\Manager\BaseManager;
use Agfa\UserBundle\Entity\User;

class PatientManager extends BaseManager
{

    protected $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    public function getRepository()
    {
        return $this->em->getRepository('AgfaHpaBundle:Patient');
    }

    public function getPatient(User $user, $patientId)
    {
        return $this->getRepository()
            ->getPatient($user, $patientId)
            ->getQuery()
            ->useQueryCache(true)
            ->getSingleResult();
    }

    public function getPatientByInsc(User $user, $patientInsc)
    {
        return $this->getRepository()
            ->getPatientByInsc($user, $patientInsc)
            ->getQuery()
            ->useQueryCache(true)
            ->getOneOrNullResult();
    }

    public function findPatientSansPersonne(User $user)
    {
        return $this->getRepository()
            ->findPatientSansPersonne($user)
            ->getQuery()
            ->useQueryCache(true)
            ->getResult();
    }

    public function findPatientFromPersonalDetails(User $user, $nom, $prenom, $dateNaissance)
    {
        return $this->getRepository()
            ->findPatientFromPersonalDetails($user, $nom, $prenom, $dateNaissance)
            ->getQuery()
            ->useQueryCache(true)
            ->getOneOrNullResult();
    }
}