<?php
namespace Agfa\HpaBundle\Manager;

use Doctrine\ORM\EntityManager;
use Agfa\HpaBundle\Entity\Request;
use Agfa\UserBundle\Entity\User;

class RequestManager
{

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    public function getRepository()
    {
        return $this->em->getRepository('AgfaHpaBundle:Request');
    }

    public function getUserRequest(User $user, $requestId)
    {
        return $this->getRepository()
            ->getUserRequest($user, $requestId)
            ->getQuery()
            ->useQueryCache(true)
            ->getSingleResult();
    }

    public function deleteRequest(Request $request)
    {
        if (! $request) {
            throw new \Symfony\Component\HttpKernel\Exception\NotFoundHttpException('Monetico request not found');
        }

        $this->em->remove($request);
        $this->em->flush();

        return;
    }
}