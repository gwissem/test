<?php
namespace Agfa\HpaBundle\Manager;

use Doctrine\ORM\EntityManager;
use Agfa\HpaBundle\Entity\Etablissement;

class EtablissementManager extends BaseManager
{

    protected $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    public function getRepository()
    {
        return $this->em->getRepository('AgfaHpaBundle:Etablissement');
    }

    /**
     * Create data in Etablissement or update if an entry is found using BioServeur etablissement reference number.
     *
     * @param array $etablissementDetails
     * @param \Agfa\HpaBundle\Entity\Etablissement $etablissement
     */
    public function updateEtablissement(Etablissement $etablissement, array $etablissementDetails)
    {
        $query = $this->getRepository()->updateEtablissement($etablissement, $etablissementDetails);

        $this->persistAndFlush($query);

        return;
    }

    /**
     * Get an Etablissement by its BioServeur etablissement reference number.
     * 'parent is null' is necessary because it is a self referencing table.
     *
     * @param integer $bsrvrRefEtablissement
     * @return \Doctrine\ORM\QueryBuilder
     */
    public function getEtablissement($bsrvrRefEtablissement)
    {
        $query = $this->getRepository()
            ->getEtablissement($bsrvrRefEtablissement)
            ->getQuery()
            ->useQueryCache(true)
            ->getOneOrNullResult();

        return $query;
    }
}