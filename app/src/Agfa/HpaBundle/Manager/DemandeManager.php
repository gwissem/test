<?php
namespace Agfa\HpaBundle\Manager;

use Doctrine\ORM\EntityManager;
use Agfa\UserBundle\Entity\User;
use Agfa\HpaBundle\Entity\Personne;
use Agfa\HpaBundle\Entity\Patient;
use Agfa\HpaBundle\Entity\Demande;

class DemandeManager
{

    const cacheLifetime = 60;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    /**
     *
     * @return \Doctrine\ORM\EntityRepository
     */
    public function getRepository()
    {
        return $this->em->getRepository('AgfaHpaBundle:Demande');
    }

    /**
     * Get all demandes on a user account.
     * Used in :
     * - admin/search
     *
     * @param User $user
     * @return unknown
     */
    public function getAllDemandes($user)
    {
        return $this->getRepository()
            ->getAllDemandes($user)
            ->getQuery()
            ->useQueryCache(true)
            ->getResult();
    }

    public function getDemandeAnonymously($nodemx)
    {
        return $this->getRepository()
            ->getDemandesAnonymously($nodemx)
            ->getQuery()
            ->useQueryCache(true)
            ->getOneOrNullResult();
    }

    public function getDemandesAnonymously($nodemx, $laboCode = null)
    {
        return $this->getRepository()
            ->getDemandesAnonymously($nodemx, $laboCode)
            ->getQuery()
            ->useQueryCache(true)
            ->getResult();
    }

    public function getPatientDemandes(Patient $patient)
    {
        return $this->getRepository()
            ->getPatientDemandes($patient)
            ->getQuery()
            ->useQueryCache(true)
            ->getResult();
    }

    public function getDemandes(User $user, Personne $personne, $limit = NULL)
    {
        // $cacheId = sprintf('getDemandes-%d_pid-%d', $user->getId(), $personne->getId());

        // return $this->getRepository()
        // ->getDemandes($user, $personne, $limit)
//         ->getQuery()
//         ->useQueryCache(true)
        // ->useResultCache(true, self::cacheLifetime, $cacheId)
        // ->getResult();
        $query = $this->getRepository()
            ->getDemandes($user, $personne, $limit)
            ->getQuery()
            ->useQueryCache(true)
            ->getResult();

        return $query;
    }

    public function getDemandesByMonth(User $user, Personne $personne, $year, $month)
    {
        // $cacheId = sprintf('getDemandesByMonth_uid-%d_pid-%d', $user->getId(), $personne->getId());

        // return $this->getRepository()
        // ->getDemandesByMonth($user, $personne, $year, $month)
        // ->getQuery()
        // ->useQueryCache(true)
        // ->useResultCache(true, self::cacheLifetime, $cacheId)
        // ->getResult();
        $query = $this->getRepository()
            ->getDemandesByMonth($user, $personne, $year, $month)
            ->getQuery()
            ->useQueryCache(true)
            ->getResult();

        return $query;
    }

    public function getDemande(User $user, $demandeId)
    {
        return $this->getRepository()
            ->getDemande($user, $demandeId)
            ->getQuery()
            ->useQueryCache(true)
            ->getSingleResult();
    }

    public function getDemandeToDownload(User $user, $demandeId)
    {
        return $this->getRepository()
            ->getDemandeToDownload($user, $demandeId)
            ->getQuery()
            ->useQueryCache(true)
            ->getSingleResult();
    }

    public function getDemandeOnOtherUserAccount(User $user, $demandeId)
    {
        return $this->getRepository()
            ->getDemandeOnOtherUserAccount($user, $demandeId)
            ->getQuery()
            ->useQueryCache(true)
            ->getOneOrNullResult();
    }

    public function getDemandesResultats(User $user, Personne $personne)
    {
        return $this->getRepository()
            ->getDemandesResultats($user, $personne)
            ->getQuery()
            ->useQueryCache(true)
            ->getResult();
    }

    public function searchDemandes(User $user, Personne $personne, $criterias)
    {
        return $this->getRepository()
            ->searchDemandes($user, $personne, $criterias)
            ->getQuery()
            ->useQueryCache(true)
            ->getResult();
    }

    public function getDemandeForRapprochement(User $user, $demandeId)
    {
        return $this->getRepository()
            ->getDemandeForRapprochement($user, $demandeId)
            ->getQuery()
            ->useQueryCache(true)
            ->getOneOrNullResult();
    }

    public function getUnpaidDemandes(User $user)
    {
        return $this->getRepository()
            ->getUnpaidDemandes($user)
            ->getQuery()
            ->useQueryCache(true)
            ->getResult();
    }

    public function getUnpaidDemande(User $user, $demandeId)
    {
        return $this->getRepository()
            ->getUnpaidDemande($user, $demandeId)
            ->getQuery()
            ->useQueryCache(true)
            ->getSingleResult();
    }

    public function getPaidDemandes(User $user)
    {
        return $this->getRepository()
            ->getPaidDemandes($user)
            ->getQuery()
            ->useQueryCache(true)
            ->getResult();
    }

    public function getDemandeByReference($moneticoReference)
    {
        return $this->getRepository()
            ->getDemandeByReference($moneticoReference)
            ->getQuery()
            ->useQueryCache(true)
            ->getSingleResult();
    }

    public function findDemande($bsrvrConstLogin, $nodemx)
    {
        return $this->getRepository()
            ->findDemande($bsrvrConstLogin, $nodemx)
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function getUserDemandes(User $user)
    {
        return $this->getRepository()
            ->getUserDemandes($user)
            ->getQuery()
            ->useQueryCache(true)
            ->getResult();
    }

    public function getDemandsForBsrvrUpdateReading($limit = NULL)
    {
        return $this->getRepository()
            ->getDemandsForBsrvrUpdateReading($limit)
            ->getQuery()
            ->useQueryCache(true)
            ->getResult();
    }

    public function getDemandsForBsrvrUpdatePayment()
    {
        return $this->getRepository()
            ->getDemandsForBsrvrUpdatePayment()
            ->getQuery()
            ->useQueryCache(true)
            ->getResult();
    }
}