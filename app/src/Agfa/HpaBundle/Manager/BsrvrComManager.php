<?php
namespace Agfa\HpaBundle\Manager;

use Doctrine\ORM\EntityManager;

class BsrvrComManager
{

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    public function getRepository()
    {
        return $this->em->getRepository('AgfaHpaBundle:BsrvrCom');
    }

    public function getUnenrollment()
    {
        return $this->getRepository()
            ->getUnenrollment()
            ->getQuery()
            ->getOneOrNullResult();
    }
}