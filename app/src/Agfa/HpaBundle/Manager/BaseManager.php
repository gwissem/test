<?php

namespace Agfa\HpaBundle\Manager;

/**
 * Methods that can be used by in repository.
 */
abstract class BaseManager
{
    protected function persistAndFlush($entity)
    {
        $this->em->persist($entity);
        $this->em->flush();

        return;
    }

}