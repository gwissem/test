<?php
namespace Agfa\HpaBundle\Manager;

use Doctrine\ORM\EntityManager;
use Agfa\HpaBundle\Manager\BaseManager;
use Agfa\UserBundle\Entity\User;

class PersonneManager extends BaseManager
{

    protected $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    public function getRepository()
    {
        return $this->em->getRepository('AgfaHpaBundle:Personne');
    }

    public function getPersonnes(User $user)
    {
        return $this->getRepository()
            ->getPersonnes($user)
            ->getQuery()
            ->useQueryCache(true)
            ->getResult();
    }

    public function getMenuPersonnesData(User $user)
    {
        return $this->getRepository()
            ->getMenuPersonnesData($user)
            ->getQuery()
            ->useQueryCache(true)
            ->getResult();
    }

    public function getPersonne(User $user, $personneId)
    {
        return $this->getRepository()
            ->getPersonne($user, $personneId)
            ->getQuery()
            ->useQueryCache(true)
            ->getOneOrNullResult();
    }

    public function getDemandes(User $user)
    {
        return $this->getRepository()
            ->getDemandes($user)
            ->getQuery()
            ->useQueryCache(true)
            ->getResult();
    }
}