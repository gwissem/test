<?php
namespace Agfa\HpaBundle\Manager;

use Doctrine\ORM\EntityManager;
use Agfa\HpaBundle\Entity\Paiement;

class PaiementManager extends BaseManager
{

    protected $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    public function getRepository()
    {
        return $this->em->getRepository('AgfaHpaBundle:Paiement');
    }

    /**
     * Create data in Paiement or update if an entry is found using the TPE code.
     *
     * @param array $paymentDetails
     * @param \Agfa\HpaBundle\Entity\Paiement $paiement
     */
    public function updatePayment(Paiement $paiement, array $paymentDetails)
    {
        $query = $this->getRepository()->updatePayment($paiement, $paymentDetails);

        $this->persistAndFlush($query);

        return;
    }
}