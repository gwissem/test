<?php
namespace Agfa\HpaBundle\Manager;

use Doctrine\ORM\EntityManager;

class ResultatManager extends BaseManager
{

    protected $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    public function getRepository()
    {
        return $this->em->getRepository('AgfaHpaBundle:Resultat');
    }

}