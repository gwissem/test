<?php
namespace Agfa\HpaBundle\Manager;

use Doctrine\ORM\EntityManager;
use Agfa\UserBundle\Entity\User;
use Agfa\HpaBundle\Entity\Journal;

class JournalManager
{

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    public function getRepository()
    {
        return $this->em->getRepository('AgfaHpaBundle:Journal');
    }

    public function getUserEntries(User $user)
    {
        return $this->getRepository()
            ->getUserEntries($user)
            ->getQuery()
            ->useQueryCache(true)
            ->getResult();
    }

    public function getUserEntry(User $user, Journal $journal)
    {
        return $this->getRepository()
            ->getUserEntry($user, $journal)
            ->getQuery()
            ->useQueryCache(true)
            ->getOneOrNullResult();
    }

    public function getUserAuthentication(User $user)
    {
        return $this->getRepository()
            ->getUserAuthentication($user)
            ->getQuery()
            ->useQueryCache(true)
            ->getResult();
    }

    public function getAllButLastEntries(User $user, $nbEntriesToKeep, $nbMonthToKeep)
    {
        $nots = $this->getRepository()
            ->getLastEntries($user, $nbEntriesToKeep, $nbMonthToKeep)
            ->getQuery()
            ->useQueryCache(true)
            ->getArrayResult();

        $entries = $this->getRepository()
            ->getAllButLastEntries($user, $nots)
            ->getQuery()
            ->useQueryCache(true)
            ->getResult();

        foreach ($entries as $entry) {
            $this->em->remove($entry);
        }

        $this->em->flush();
    }
}