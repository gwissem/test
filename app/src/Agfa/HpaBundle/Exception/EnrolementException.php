<?php

namespace Agfa\HpaBundle\Exception;

use \Exception;

class EnrolementException extends Exception
{
	protected $statut = '';
	
	public function __construct($message, $statut, $code = 0, Exception $previous = null) {
		$this->setStatut($statut);
		parent::__construct($message, $code, $previous);
	}
	
	public function getStatut()
	{
		return $this->statut;
	}
	
	public function setStatut($statut)
	{
		$this->statut = $statut;
		return $this;
	}
}