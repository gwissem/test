<?php
namespace Agfa\HpaBundle\Services;

use Symfony\Component\HttpFoundation\Session\Session;
use Agfa\HpaBundle\Entity\Personne;
use Agfa\HpaBundle\Manager\PersonneManager;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;

class Contexte
{

    private $user, $session, $personneMgr, $tokenStorage;

    public function __construct(Session $session, PersonneManager $personneMgr, TokenStorage $tokenStorage)
    {
        $this->session = $session;
        $this->personneMgr = $personneMgr;

        if (null === $this->tokenStorage = $tokenStorage->getToken()) {
            return;
        }

        if (! is_object($this->user = $this->tokenStorage->getUser())) {
            return;
        }
    }

    public function getPersonneContexte()
    {
        // For whatever reason, the object is sometime not re-instanciated and thus, the constructor not called.
        if (! is_object($this->user)) {
            return;
        }

        return $this->user->getCurrentPersonne();
    }

    public function setPersonneContexte(Personne $personne)
    {
        $this->user->setCurrentPersonne($personne);
    }
}