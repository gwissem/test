<?php
namespace Agfa\HpaBundle\Services;

use Agfa\HpaBundle\Manager\DemandeManager;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;

class Paiement
{

    private $user, $demandeMgr, $tokenStorage;

    public function __construct(DemandeManager $demandeMgr, TokenStorage $tokenStorage)
    {
        $this->demandeMgr = $demandeMgr;

        if (null === $this->tokenStorage = $tokenStorage->getToken()) {
            return;
        }

        if (! is_object($this->user = $this->tokenStorage->getUser())) {
            return;
        }
    }

    public function getUnpaidDemandes()
    {
        if (! is_object($this->user)) {
            return;
        }

        $nbUnpaidDemandes = count($this->demandeMgr->getUnpaidDemandes($this->user));

        return $nbUnpaidDemandes;
    }
}