<?php
namespace Agfa\HpaBundle\Services;

use Symfony\Bridge\Monolog\Logger;
use Agfa\HpaBundle\Entity\Patient;
use Agfa\HpaBundle\Entity\Demande;
use Agfa\HpaBundle\Entity\Examen;
use Agfa\HpaBundle\Entity\Chapitre;
use Agfa\HpaBundle\Entity\Element;
use Agfa\HpaBundle\Entity\Resultat;
use Doctrine\Bundle\DoctrineBundle\Registry as Doctrine;
use Symfony\Component\HttpKernel\Kernel;
use Agfa\HpaBundle\Entity\Journal;
use Agfa\HpaBundle\Entity\Etablissement;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;
use Agfa\HpaBundle\Manager\PaiementManager;
use Agfa\HpaBundle\Entity\Paiement;

/**
 * Extract data from the editCR and add them to the database.
 */
class EditcrConnector
{

    public function __construct(Doctrine $doctrine, Logger $logger, Kernel $kernel, Journalist $journalist, PaiementManager $paiementMgr)
    {
        $this->em = $doctrine->getManager();
        $this->logger = $logger;
        $this->kernel = $kernel;
        $this->journalist = $journalist;
        $this->paiementMgr = $paiementMgr;
    }

    /**
     * Parcourt un flux editCR pour extraire et renvoyer le compte rendu joint
     *
     * @param string $content
     *            le flux editCR
     * @return string
     */
    public function extraireCompteRenduPdf($content)
    {
        /* parsing xml du editcr */
        libxml_use_internal_errors(true);

        $editCR = simplexml_load_string($content);

        if ($editCR === false) {
            $this->logger->error("Erreur lors du chargement du XML", libxml_get_errors());
            $this->logger->error("Contenu du fichier reçu", [
                "content" => $content
            ]);
            throw new \Exception("Erreur lors du chargement du XML");
        }

        /* decodage du compte rendu pdf ; on l'enleve du editcr */
        $compteRenduPdf = base64_decode(trim($editCR->documentCompteRendu));

        return $compteRenduPdf;
    }

    /**
     * Consomme un flux editCR et crée l'entité Demande et les entités Résultat correspondantes
     *
     * @param string $xmlEditcr
     *            le flux editCR
     * @param \Agfa\UserBundle\Entity\User $user
     *            en cas d'enrolement, l'utilisateur qui enrole
     * @param array $parameters
     * @throws UnexpectedTypeException
     * @throws \Exception
     * @return \Agfa\HpaBundle\Entity\Demande
     */
    // TODO function extractData($xmlEditcr) Separate extract data and insert data into two methods
    public function creerDemande($xmlEditcr, \Agfa\UserBundle\Entity\User $user = NULL, array $parameters = array())
    {
        $entiteJuridique = isset($parameters['entite_juridique']) ? $parameters['entite_juridique'] : NULL;
        $laboratoire = isset($parameters['laboratoire']) ? $parameters['laboratoire'] : NULL;
        $bsrvrLaboId = isset($parameters['bsrvr_labo_id']) ? $parameters['bsrvr_labo_id'] : NULL;
        $bsrvrNopat = isset($parameters['bsrvr_nopat']) ? $parameters['bsrvr_nopat'] : NULL;

        // Get infos about the laboratory
        if ((array) $laboratoire) {
            $loginLabo = isset($laboratoire['login']) ? $laboratoire['login'] : NULL;
            $libelleLabo = isset($laboratoire['libelle']) ? $laboratoire['libelle'] : NULL;
        }

        // Lab FINESS (lab login code) can never be '999999999' because it was used for the migration of the database.
        if($loginLabo == '999999999'){
            throw new \Exception("Lab FINESS (lab login code) '999999999' is not allowed.");
        }

        // Get infos about the entité juridique
        if ((array) $entiteJuridique) {
            $finessEntiteJuridique = isset($entiteJuridique['finess']) ? $entiteJuridique['finess'] : NULL;
            $libelleEntiteJuridique = isset($entiteJuridique['libelle']) ? $entiteJuridique['libelle'] : NULL;
            $bsrvrCodeEntiteJuridique = isset($entiteJuridique['code_ej']) ? $entiteJuridique['code_ej'] : NULL;
            $codePostalEntiteJuridique = isset($entiteJuridique['code_postal']) ? $entiteJuridique['code_postal'] : NULL;
            $raisonSocialeEntiteJuridique = isset($entiteJuridique['raison_sociale']) ? $entiteJuridique['raison_sociale'] : NULL;
            $adresseEntiteJuridique = isset($entiteJuridique['adresse']) ? $entiteJuridique['adresse'] : NULL;
        }

        /* Payment data */
        // INFO The balance can not be retrieved from the editCr has there is no backward communication between
        // Bioserveur and Hexalis (balance paid is not automatically updated from Bioserveur to Hexalis).
        if ($labPaiement = (array) $laboratoire['paiement']) {
            $laboMntcTpe = isset($labPaiement['tpe']) ? $labPaiement['tpe'] : NULL;
            $laboMntcCle = isset($labPaiement['cle']) ? $labPaiement['cle'] : NULL;
            $laboMntcSociete = isset($labPaiement['societe']) ? $labPaiement['societe'] : NULL;
            $laboPaieEmail = isset($labPaiement['email']) ? $labPaiement['email'] : NULL;
            $laboPaieNom = isset($labPaiement['nom']) ? $labPaiement['nom'] : NULL;
            $laboPaiePrenom = isset($labPaiement['prenom']) ? $labPaiement['prenom'] : NULL;
            $laboPaieTelephone = isset($labPaiement['telephone']) ? $labPaiement['telephone'] : NULL;
            $laboPaieAffichageCr = $labPaiement['paiement_affichage_cr'];
            $laboPaieActif = $labPaiement['paiement_actif'];
        }

        // Get balance to be paid for the 'CR'
        // INFO The balance can not be retrieved from the editCr has there is no backward communication between
        // Bioserveur and Hexalis (balance paid is not automatically updated from Bioserveur to Hexalis).
        if ($paieSolde = isset($parameters['solde_patient']) ? $parameters['solde_patient'] : NULL) {
            // $paieSolde has to be a float or an integer (as per the api doc.). Log an error otherwise.
            if ($paieSolde && (! is_float($paieSolde) && ! is_int($paieSolde))) {
                $this->logger->addCritical("Le solde patient doit obligatoirement être un chiffre à deux décimales.");
                throw new UnexpectedTypeException($paieSolde, 'float');
            }
        }

        // Get source of payement
        $paieSource = isset($parameters['source_paiement']) && $parameters['source_paiement'] !== FALSE ? $parameters['source_paiement'] : Demande::PAID_NEW;

        /* parsing xml du editcr */
        libxml_use_internal_errors(true);
        $editCR = simplexml_load_string($xmlEditcr);
        if ($editCR === false) {
            $this->logger->error("Erreur lors du chargement du XML", libxml_get_errors());
            $this->logger->error("Contenu du fichier reçu", [
                "content" => $xmlEditcr
            ]);
            throw new \Exception("Erreur lors du chargement du XML");
        }

        /* lecture des infos et création des entites */
        // beccha: Si le $bsrvrNopat est fourni, il provient de la requète API vers Bioserveur, utile pour le téléchargmement de compte rendu en direct.
        if (empty($bsrvrNopat)) {
            $bsrvrNopat = trim($editCR->CompteRendu->Utilisation->CodeDestinataire);
        }
        $codeLabo = trim($editCR->CompteRendu->Laboratoire->clabo);

        // $bsrvrLaboId : is given as bioserver's answer from the API request
        if (empty($bsrvrLaboId)) {
            // If the information does not ecists in the request, just get the data from the EditCr
            $bsrvrLaboId = trim($editCR->CompteRendu->Laboratoire->identifiantStructure);
        }
        $bsrvrLaboId = substr($bsrvrLaboId, 0, 4);
        $bsrvrConstantLogin = $bsrvrLaboId . $bsrvrNopat;

        $versionDicoTech = trim($editCR->CompteRendu->Demande->versionDico);
        $numeroDemande = trim($editCR->CompteRendu->Demande->nodem);
        $numeroDemandeExtended = trim($editCR->CompteRendu->Demande->nodemx);

        $datenr = trim($editCR->CompteRendu->Demande->datenr);
        $heurenreg = trim($editCR->CompteRendu->Demande->heurenreg);
        $heurenreg = preg_match('/^([01]?[0-9]|2[0-3])[H][0-5][0-9]/', mb_strtoupper($heurenreg)) ? mb_strtoupper($heurenreg) : '00H00';
        $dateAccueil = \DateTime::createFromFormat('d/m/y H\Hi', $datenr . ' ' . $heurenreg);

        $datprel = trim($editCR->CompteRendu->Demande->datprel);
        $heureprel = trim($editCR->CompteRendu->Demande->heureprel);
        $heureprel = preg_match('/^([01]?[0-9]|2[0-3])[H][0-5][0-9]/', mb_strtoupper($heureprel)) ? mb_strtoupper($heureprel) : '00H00';
        $datePrelevement = \DateTime::createFromFormat('d/m/y H\Hi', $datprel . ' ' . $heureprel);
        if (! $datePrelevement)
            $datePrelevement = NULL;

        $nomPatient = trim($editCR->CompteRendu->Patient->nom);
        $prenomPatient = trim($editCR->CompteRendu->Patient->prenom);
        $dateNaissancePatient = \DateTime::createFromFormat('d/m/Y', trim($editCR->CompteRendu->Patient->datnais));
        // hexalis encode le sexe en M/F, on réencode en H/F
        $sexePatient = $this->encodeGender(trim($editCR->CompteRendu->Patient->cgenre));
        $inscPatient = trim($editCR->CompteRendu->Patient->INS);

        $nomPrescripteur = trim($editCR->CompteRendu->Prescripteurs->UnPrescripteur[0]->ipresc);

        /* decodage du compte rendu pdf ; on l'enleve du editcr */
        $compteRenduPdf = base64_decode(trim($editCR->documentCompteRendu));
        unset($editCR->documentCompteRendu);

        /* creation d'un code de hashage et vérification de l'unicité */
        $hash = hash('sha256', $compteRenduPdf, FALSE);
        /*
         * en commentaire tant que ce n'est pas absolument utile
         * if($this->em->getRepository('AgfaHpaBundle:Demande')->findOneBy(['hash' => $hash])) {
         * throw new \Exception("Ce compte rendu a déjà été intégré.");
         * }
         */

        /* Get the Entite Juridique */
        $ejIsNew = FALSE;
        $entiteJuridique = $this->em->getRepository('AgfaHpaBundle:Etablissement')->findOneBy([
            'bsrvrRefEtablissement' => $finessEntiteJuridique
        ]);
        if (! $entiteJuridique) // && $user)
        {
            $entiteJuridique = new Etablissement();
            $entiteJuridique->setNom($raisonSocialeEntiteJuridique)
                ->setAdresse($adresseEntiteJuridique)
                ->setCodePostal($codePostalEntiteJuridique)
                ->setBsrvrRefEtablissement($finessEntiteJuridique)
                ->setLibelle($libelleEntiteJuridique)
                ->setBsrvrCodeEntiteJuridique($bsrvrCodeEntiteJuridique);

            $this->em->persist($entiteJuridique);
            $ejIsNew = TRUE;
        }

        // Labo
        $labo = $this->em->getRepository('AgfaHpaBundle:Etablissement')->findOneBy([
            'bsrvrRefEtablissement' => $loginLabo
        ]);
        if (! $labo) {
            $nom = trim($editCR->CompteRendu->Laboratoire->ilabo);
            $adresse = trim($editCR->CompteRendu->Laboratoire->adr1 . " " . $editCR->CompteRendu->Laboratoire->adr2);
            $codePostal = trim($editCR->CompteRendu->Laboratoire->cpost);
            $ville = trim($editCR->CompteRendu->Laboratoire->lpost);
            $telephone = trim($editCR->CompteRendu->Laboratoire->tele);

            $labo = new Etablissement();
            $labo->setParent($entiteJuridique)
                ->setNom($nom)
                ->setAdresse($adresse)
                ->setCodePostal($codePostal)
                ->setVille($ville)
                ->setHxlCodeStructure($codeLabo)
                ->setTelephone($telephone)
                ->setBsrvrCodeLabo($bsrvrLaboId)
                ->setBsrvrRefEtablissement($loginLabo)
                ->setLibelle($libelleLabo)
                ->setBsrvrCodeEntiteJuridique($bsrvrCodeEntiteJuridique);
        }elseif($ejIsNew === TRUE && NULL !== $labo->getParent()){
            /**
             * If the EJ has just been created, there is a problem: an existing lab cannot be linked to a new EJ
             * as it would make the existing data wrong.
             * NOTE: An existing lab with no linked EJ yet can be linked to the newly created EJ as to allow prior version of HPA to update existing data.
             */
            $error = sprintf("Un laboratoire (ID: %s) déjà lié à une entité juridique (ID: %s) ne peut pas être ré-assigné à une nouvelle entité juridique.", $labo->getId(), $labo->getParent()->getId());
            throw new \InvalidArgumentException($error);
        }else{
            $labo->setParent($entiteJuridique);
        }

        // Paiement
        // Only create an entry if there is some data about the payement
        if($laboMntcTpe && $laboMntcCle && $laboMntcSociete && $laboPaieEmail){
            $paymentDetails = array(
                'tpe' => $laboMntcTpe,
                'cle' => $laboMntcCle,
                'telephone' => $laboPaieTelephone,
                'societe' => $laboMntcSociete,
                'prenom' => $laboPaiePrenom,
                'nom' => $laboPaieNom,
                'email' => $laboPaieEmail,
                'affichage_cr' => $laboPaieAffichageCr,
                'actif' => $laboPaieActif
            );

            // Only create new entry if no existing entry exists.
            if (! $paiement = $this->em->getRepository('AgfaHpaBundle:Paiement')->findOneBy([
                'tpe' => $laboMntcTpe
            ])) {
                $paiement = new Paiement();
            }

            // If Paiement is disabled, only update the status ...otherwise update the status and data too
            $this->paiementMgr->updatePayment($paiement, $paymentDetails);

            // Always update the payment that is linked to the lab
            $labo->setPaiement($paiement);
        }

        $this->em->persist($labo);

        // Patient ; si on ne le trouve pas on le crée car un patient est surtout la donnée de son identifiant bioserveur (nopat)
        $patient = $this->em->getRepository('AgfaHpaBundle:Patient')->findOneBy([
            'etablissement' => $entiteJuridique,
            'bsrvrNopat' => $bsrvrNopat
        ]);
        if (! $patient && $user) {
            $patient = new Patient();
            $patient->setEtablissement($entiteJuridique);
            $patient->setBsrvrNopat($bsrvrNopat);
            $patient->setInsc($inscPatient);
            $patient->setNom($nomPatient);
            $patient->setPrenom($prenomPatient);
            $patient->setSexe($sexePatient);
            $patient->setDateNaissance($dateNaissancePatient);
            $patient->setUser($user);

            $this->em->persist($patient);
            $this->em->flush();

            $this->journalist->addInfo(Journal::EVENT_PATIENT_CREATION, $patient, $patient->getUser(), sprintf("%s. Nopat : %s", $patient, $patient->getBsrvrNopat()));
        }
        if (! $patient) {
            throw new \Exception("Le patient [$bsrvrNopat] de la structure [$bsrvrLaboId] est inconnu");
        }

        /**
         * If paiement is activated, enabled the access to it for the owner of the CR
         * INFO Once the payment is enabled, it is never disabled in order to allow the patient to access its past payments
         * INFO We have to use the patient to get the user as the user might not be authenticated (automatic receiving of CR)
         */
        if ($laboPaieActif === TRUE && $patient->getUser()) {
            $patient->getUser()->setPaymentIsEnabled(TRUE);
        }

        /* on regarde si c'est une création de nouvelle demande ou un annule-et-remplace d'une ancienne */
        $creation = FALSE;
        // WARNING Do not use nodemx instead of nodem because data prior to HPA 1.1 had no nodemx.
        $demande = $this->em->getRepository('AgfaHpaBundle:Demande')->findOneBy([
            'patient' => $patient,
            'nodem' => $numeroDemande
        ]);

        // If the 'demande' does not already exists, create it
        if (! $demande) {
            $creation = TRUE;
            $demande = new Demande();

            // The balance is added at the demande's creation but never updated thereafter except if the paieSource is set to PAID_NEW.
            $demande->setPaieSolde($paieSolde);

            // Source of payment. If nothing, then 'demande' is available for payment
            // A new Demand can still have a Paie source other than 'new'
            $demande->setPaieSource($paieSource);
        } else {
            // If the demande already exists and has no payment source already, we can update it
            switch ($demande->getPaieSource()) {
                case Demande::PAID_NEW:
                case Demande::PAID_LABORATORY:
                    $demande->setPaieSource($paieSource);
                    $demande->setIsUpdated(TRUE);
                    $demande->setPaieSolde($paieSolde);
                    $this->journalist->addWarning(Journal::EVENT_PAYMENT_UPDATED, $demande, $demande->getPatient()
                        ->getUser(), sprintf('Nodemx %s. Rest dû MAJ par Bioserveur', $demande->getNodemx()));
                    break;
                default:
                    // Log an error is the paieSolde is different and an update has been tried
                    if ($paieSolde !== $demande->getPaieSolde()) {
                        $this->logger->addError(sprintf('Attempt to update balance on CR %s for patient %s while it has already been paid.', $demande->getNodemx(), $demande->getBsrvrConstantLogin()));
                    }
            }
        }

        // Data for the 'CR' is created or always updated
        $demande->setBsrvrConstantLogin($bsrvrConstantLogin);
        $demande->setCompteRendu($compteRenduPdf);
        $demande->setNodem($numeroDemande);
        $demande->setNodemx($numeroDemandeExtended);
        $demande->setDateAccueil($dateAccueil);
        $demande->setDatePrelevement($datePrelevement);
        $demande->setEditcr(gzcompress($editCR->asXML()));
        $demande->setNomPatient($nomPatient);
        $demande->setPrenomPatient($prenomPatient);
        $demande->setDateNaissancePatient($dateNaissancePatient);
        $demande->setSexePatient($sexePatient);
        $demande->setInscPatient($inscPatient);
        $demande->setHash($hash);
        $demande->setNomPrescripteur($nomPrescripteur);
        $demande->setPatient($patient);
        $demande->setEtablissement($labo);
        $demande->setAffichageCr($parameters['affichage_cr']);
        $demande->setCode(sprintf("CR_%s_%s_%s", $demande->getDateAccueil()
            ->format('YmdHi'), $demande->getEtablissement()
            ->getBsrvrCodeLabo(), $demande->getPatient()
            ->getBsrvrNopat()));

        $this->em->persist($demande);
        $this->em->flush();

        // Records an entry in the journal
        if ($creation === TRUE) {
            $this->journalist->addInfo(Journal::EVENT_CR_CREATION, $demande, $demande->getPatient()
                ->getUser(), sprintf("Nodemx %s, constant login %s", $demande->getNodemx(), $demande->getBsrvrConstantLogin()));
        } else {
            $this->journalist->addInfo(Journal::EVENT_CR_REMPLACEMENT, $demande, $demande->getPatient()
                ->getUser(), sprintf("Nodemx %s, constant login %s", $demande->getNodemx(), $demande->getBsrvrConstantLogin()));
        }

        // The content of the patient record is gathered and recorded into rb.
        // The following data is never being updated.
        foreach ($editCR->CompteRendu->Resultats->Chapitre as $chapitre_xml) {
            $codeLoinc = trim($chapitre_xml->code_loinc);
            $codeUniqueLabo = $versionDicoTech . trim($chapitre_xml->code);

            if ($codeLoinc) {
                $chapitre_entity = $this->em->getRepository('AgfaHpaBundle:Chapitre')->findOneBy([
                    'codeLoinc' => $codeLoinc
                ]);
            } else {
                $chapitre_entity = $this->em->getRepository('AgfaHpaBundle:Chapitre')->findOneBy([
                    'etablissement' => $entiteJuridique,
                    'code' => $codeUniqueLabo
                ]);
            }

            if (! $chapitre_entity) {
                $chapitre_entity = new Chapitre();
                $chapitre_entity->setEtablissement($entiteJuridique);
                $chapitre_entity->setCode($codeUniqueLabo);
                $chapitre_entity->setCodeLoinc($codeLoinc);
                $chapitre_entity->setLibelle(trim($chapitre_xml->libelle));
                $chapitre_entity->setLibelleLoinc(trim($chapitre_xml->libelle_loinc));
                $this->em->persist($chapitre_entity);
            }

            // on met ensemble tous les examens descendants de <Chapitre> et tous les examens descendants d'un <SousChapitre> dans <Chapitre>
            $liste_examens = [];
            foreach ($chapitre_xml->Examen as $examen_xml) {
                $liste_examens[] = $examen_xml;
            }

            foreach ($chapitre_xml->SousChapitre as $souschapitre_xml) {
                foreach ($souschapitre_xml->Examen as $examen_xml) {
                    $liste_examens[] = $examen_xml;
                }
            }

            foreach ($liste_examens as $examen_xml) {
                $codeLoinc = trim($examen_xml->code_loinc);
                $codeUniqueLabo = $versionDicoTech . trim($examen_xml->code);
                if ($codeLoinc) {
                    $examen_entity = $this->em->getRepository('AgfaHpaBundle:Examen')->findOneBy([
                        'chapitre' => $chapitre_entity,
                        'codeLoinc' => $codeLoinc
                    ]);
                } else {
                    $examen_entity = $this->em->getRepository('AgfaHpaBundle:Examen')->findOneBy([
                        'chapitre' => $chapitre_entity,
                        'code' => $codeUniqueLabo
                    ]);
                }
                if (! $examen_entity) {
                    $examen_entity = new Examen();
                    $examen_entity->setChapitre($chapitre_entity);
                    $examen_entity->setCode($codeUniqueLabo);
                    $examen_entity->setCodeLoinc($codeLoinc);
                    $examen_entity->setLibelle(trim($examen_xml->libelle));
                    $examen_entity->setLibelleLoinc(trim($examen_xml->libelle_loinc));
                    $this->em->persist($examen_entity);
                }
                foreach ($examen_xml->Element as $element_xml) {
                    $codeLoinc = trim($element_xml->code_loinc);
                    $codeUniqueLabo = $versionDicoTech . trim($element_xml->code);
                    if ($codeLoinc) {
                        $element_entity = $this->em->getRepository('AgfaHpaBundle:Element')->findOneBy([
                            'examen' => $examen_entity,
                            'codeLoinc' => $codeLoinc
                        ]);
                    } else {
                        $element_entity = $this->em->getRepository('AgfaHpaBundle:Element')->findOneBy([
                            'examen' => $examen_entity,
                            'code' => $codeUniqueLabo
                        ]);
                    }
                    if (! $element_entity) {
                        $element_entity = new Element();
                        $element_entity->setExamen($examen_entity);
                        $element_entity->setCode($codeUniqueLabo);
                        $element_entity->setCodeLoinc($codeLoinc);
                        $element_entity->setLibelle(trim($element_xml->libelle));
                        $element_entity->setLibelleLoinc(trim($element_xml->libelle_loinc));
                        $this->em->persist($element_entity);
                    }
                    foreach ($element_xml->Resultat as $resultat_xml) {
                        $valeur = $this->cleanUpValeur($resultat_xml->valeur);
                        /* on n'enregistre pas les résultats vides dans résultat ; ces examens sont donc absents de la demande */
                        if (empty($valeur))
                            continue;

                        $resultat_entity = new Resultat();
                        $resultat_entity->setDemande($demande);
                        $resultat_entity->setElement($element_entity);
                        $resultat_entity->setNormInf(floatval($resultat_xml->normInf));
                        $resultat_entity->setNormSup(floatval($resultat_xml->normSup));
                        $resultat_entity->setUnite(trim($resultat_xml->unite));
                        $resultat_entity->setValeurAlpha($valeur);
                        if (trim($element_xml['nature']) === 'N') {
                            $valeur = str_replace(",", ".", $valeur);
                            $resultat_entity->setValeurNum(floatval($valeur));
                        }
                        $this->em->persist($resultat_entity);
                    }
                }
            }
        }

        return $demande;
    }

    /**
     * Normalise data for gender
     *
     * @param string $cgenre
     * @return string
     */
    private function encodeGender($cgenre)
    {
        switch ($cgenre) {
            case 'F':
                return 'F';
                break;

            default:
                return 'H';
                break;
        }
    }

    /**
     * Removes dirts from the data contained in field 'valeur' as in Hexalis this field can contain anything (integer, float, text with html tags...).
     *
     * @param string $valeur
     * @return string
     */
    private function cleanUpValeur($valeur)
    {
        $valeur1 = stripcslashes($valeur);
        $valeur2 = strip_tags($valeur1);
        $valeur3 = preg_replace('/u([\da-fA-F]{4})/', '&#x\1;', $valeur2);
        $valeur4 = html_entity_decode($valeur3);
        $valeur5 = preg_replace('/\s+/', ' ', $valeur4);
        $valeur6 = trim($valeur5);

        return $valeur6;
    }
}