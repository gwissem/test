<?php
namespace Agfa\HpaBundle\Services;

use Agfa\HpaBundle\Manager\PersonneManager;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;

class MenuData
{

    private $user, $personneMgr;

    public function __construct(PersonneManager $personneMgr, TokenStorage $tokenStorage)
    {
        $this->personneMgr = $personneMgr;

        if (null === $this->tokenStorage = $tokenStorage->getToken()) {
            return;
        }

        if (! is_object($this->user = $this->tokenStorage->getUser())) {
            return;
        }
    }

    public function getPersonneMenuData()
    {
        if (! is_object($this->user)) {
            return;
        }

        $personnes = $this->personneMgr->getMenuPersonnesData($this->user);

        $menuPersonnesData = array();
        $hasUnread = FALSE;

        foreach ($personnes as $personne) {
            $menuPersonneData = array();
            $menuPersonneData['personneId'] = $personne->getId();
            $menuPersonneData['fullname'] = $personne->getNomComplet();
            $nbUnread = 0;

            foreach ($personne->getPersonnePatients() as $personnePatient) {
                $patient = $personnePatient->getPatient();

                foreach ($patient->getDemandes() as $demande) {
                    if ($demande->getIsNew() == TRUE) {
                        $nbUnread = $nbUnread + 1;
                        $menuPersonneData['total_unread'] = $nbUnread;
                    }
                }

                if ($nbUnread > 0) {
                    $hasUnread = TRUE;
                }
            }
            $menuPersonnesData[] = $menuPersonneData;
        }

        return array(
            'menu_personnes_data' => $menuPersonnesData,
            'has_unread' => $hasUnread
        );
    }
}