<?php
namespace Agfa\HpaBundle\Services;

use Doctrine\Bundle\DoctrineBundle\Registry as Doctrine;
use \PiwikTracker;
use Symfony\Component\Routing\Router;
use Symfony\Component\HttpFoundation\Session\Session;
use Agfa\HpaBundle\Manager\DemandeManager;
use Agfa\HpaBundle\Exception\EnrolementException;
use Doctrine\ORM\QueryBuilder;
use Agfa\HpaBundle\Entity\Personne;
use Agfa\UserBundle\Entity\User;
use Symfony\Component\HttpFoundation\RedirectResponse;

class Demandes
{

    public function __construct(Doctrine $doctrine, Journalist $journalist, Router $router, Session $session, DemandeManager $demandeMgr, Patients $patientsService, CommunicationBioserveur $comBsrvrService, $hostPiwik, $piwikSiteId, $piwikApiStatus)
    {
        $this->em = $doctrine->getManager();

        $this->journalist = $journalist;
        $this->router = $router;
        $this->flashBag = $session->getFlashBag();
        $this->demandeMgr = $demandeMgr;
        $this->patientsService = $patientsService;
        $this->comBsrvrService = $comBsrvrService;
        $this->hostPiwik = $hostPiwik;
        $this->piwikSiteId = $piwikSiteId;
        $this->piwikApiStatus = $piwikApiStatus;
    }

    public function getNonLusParUser(User $user)
    {
        return $this->em->getRepository('AgfaHpaBundle:Demande')->findBy([
            'user' => $user,
            'isNew' => TRUE
        ], [
            'dateAccueil' => 'DESC'
        ]);
    }

    /**
     * Used in Twig templates to select a personne after login
     * TODO We should not be querying for each user.
     * One query should return the total for each personn.
     */
    public function getNonLusParPersonne(Personne $personne)
    {
        $qb = new QueryBuilder($this->em);
        $query = $qb->select('de')
            ->from('AgfaHpaBundle:Demande', 'de')
            ->join('de.patient', 'pa')
            ->join('pa.patientPersonnes', 'pp')
            ->join('pp.personne', 'pe')
            ->where('pe = :personne')
            ->andWhere('de.isNew = :isNew')
            ->orderBy('de.dateAccueil', 'ASC')
            ->setParameter('personne', $personne)
            ->setParameter('isNew', TRUE)
            ->getQuery();
        return $query->getResult();
    }

    /**
     * Retrieves a demande after the user has registered.
     *
     * @param User $user
     * @param array $data
     * @return void|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function retrieveDemande($user, $enrolement)
    {
        try {
            $demande = $this->comBsrvrService->demandeEnrolement($user, $enrolement);
        } catch (EnrolementException $e) {
            $this->flashBag->add('danger', $e->getMessage());
        }

        if (isset($demande)) {
            $rapprochement = $this->patientsService->rapprocherPatient($demande);

            $this->em->flush();

            // Let the user knwo that he is now enroled and has some results available.
            $ejName = $demande->getEtablissement()->getParent()->getLibelle();
            $this->flashBag->add('success', sprintf('Votre compte rendu à été ajouté à votre compte. Tous les comptes rendus suivants provenant des laboratoires <strong>%s</strong> seront maintenant automatiquement intégrés à votre espace patient.', $ejName));

            if ($rapprochement) {
                /* si on a réussi à rapprocher le patient, on affiche le CR */
                if ($this->piwikApiStatus === TRUE) {
                    PiwikTracker::$URL = 'https://' . $this->hostPiwik;
                    $tracker = new PiwikTracker($this->piwikSiteId);
                    $tracker->doTrackEvent('Comptes', 'Enrolement', 'Utilisateur inscrit');
                }

                return new RedirectResponse($this->router->generate('demande_post_enrolement', [
                    'demandeId' => $demande->getId()
                ]));
            } else {
                /* sinon on affiche les infos qu'on a et on demande au user de choisir la bonne personne */
                if ($this->piwikApiStatus === TRUE) {
                    PiwikTracker::$URL = 'https://' . $this->hostPiwik;
                    $tracker = new PiwikTracker($this->piwikSiteId);
                    $tracker->doTrackEvent('Comptes', 'Enrolement', 'Utilisateur inscrit');
                }

                // Set hasUnassignedPatient to false to make sure that if the user does not pass the next step, there is a reminder to go back and do so
                $user->setHasUnassignedPatient(TRUE);
                $this->em->persist($user);
                $this->em->flush();

                return new RedirectResponse($this->router->generate('patient_assign', [
                    'patientId' => $demande->getPatient()
                        ->getId()
                ]));
            }
        }

        return;
    }
}