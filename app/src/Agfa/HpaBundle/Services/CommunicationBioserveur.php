<?php
namespace Agfa\HpaBundle\Services;

use Doctrine\Bundle\DoctrineBundle\Registry as Doctrine;
use Symfony\Bridge\Monolog\Logger;
use Agfa\HpaBundle\Entity\Enrolement;
use Symfony\Component\HttpKernel\Kernel;
use Agfa\HpaBundle\Entity\Journal;
use Agfa\HpaBundle\Exception\EnrolementException;
use Agfa\UserBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Translation\Translator;
use Agfa\HpaBundle\Entity\Demande;
use Symfony\Component\Routing\Router;
use Symfony\Component\HttpFoundation\Session\Session;
use Agfa\HpaBundle\Manager\DemandeManager;
use Agfa\HpaBundle\Entity\BsrvrCom;

/**
 * Used by the API controller in order to communicate with BioServeur.
 * TODO Regoup checks on direct and enroled access.
 *
 * @property string hostBioserveur
 */
class CommunicationBioserveur
{

    const API_STATUS_OK = 'OK';

    const API_STATUS_BAD_IDENTIFIERS = 'MAUVAIS_IDENTIFIANTS';

    const API_STATUS_BAD_PORTAL = 'MAUVAIS_PORTAIL';

    const API_STATUS_DEMANDE_INCONNU = 'DEMANDE_INCONNU';

    const API_STATUS_BAD_REQUEST = 'BAD_REQUEST';

    const API_STATUS_UNAUTHORIZED = 'UNAUTHORIZED';

    const API_STATUS_UNKNOWN_ERROR = 'UNKNOWN_ERROR';

    const API_SOURCE_PAYMENT_NOT_PAID = 'NOT_PAID';

    const API_SOURCE_PAYMENT_LABO = 'LABO';

    const API_SOURCE_PAYMENT_ONLINE = 'ONLINE';

    const API_SOURCE_PAYMENT_ADMIN = 'ADMIN';

    /**
     * When a problem is detected.
     *
     * @var integer
     */
    const STATUS_NOT_OK = 0;

    /**
     * When everything is ok.
     *
     * @var integer
     */
    const STATUS_OK = 1;

    private $patientRecord;

    public function __construct(Doctrine $doctrine, Logger $logger, Kernel $kernel, EditcrConnector $editcrConnector, Journalist $journal, Translator $translator, $bioserveur, Router $router, Session $session, DemandeManager $demandeMgr)
    {
        $this->em = $doctrine->getManager();
        $this->logger = $logger;
        $this->kernel = $kernel;
        $this->bsrvr = $bioserveur['bioserveur'];
        $this->bsrvrEnv = $bioserveur['bioserveur_env'];
        $this->connector = $editcrConnector;
        $this->journal = $journal;
        $this->translator = $translator;
        $this->router = $router;
        $this->session = $session;
        $this->demandeMgr = $demandeMgr;
    }

    /**
     * Ask Bioserveur for a CR.
     * Overwritten by mock during functional testing (phpUnit).
     *
     * @param Enrolement $enrolement
     * @return string JSON formated string.
     */
    public function getPatientRecord(Enrolement $enrolement, $createEnrollment = FALSE, $user = FALSE)
    {
        $userEmail = $user !== FALSE ? $user->getEmail() : $user;

        $requete = [
            'authentification' => [
                'passphrase' => $this->bsrvr['pass_hexapat']
            ],
            'patient_login' => $enrolement->getLogin(),
            'patient_password' => $enrolement->getPassword(),
            'creer_enrolement' => $createEnrollment,
            'utilisateur_email' => $userEmail
        ];

        $this->patientRecord = $this->envoyerRequete($this->bsrvrEnv['uri_enrolement'], $requete);

        return $this->patientRecord;
    }

    /**
     * Send a request to BioServeur to enrol a patient and get its CR
     *
     * @param \Agfa\HpaBundle\Entity\Enrolement $enrolement
     *            Data about the enrollment
     * @return \Agfa\HpaBundle\Entity\Demande Data regarding the demand, eventually without patient.
     */
    public function demandeEnrolement(User $user, Enrolement $enrolement)
    {
        $result = $this->getPatientRecord($enrolement, TRUE, $user);
        $statut = $result->statut;

        $enrolement->setUser($user)->setStatut($statut);

        $this->em->persist($enrolement);
        $this->em->flush();

        /* traitement données */
        $compteRendu = $result->compte_rendu;
        $laboratoire = (array) $compteRendu->laboratoire;
        $entiteJuridique = (array) $compteRendu->entite_juridique;
        $soldePatient = $compteRendu->solde_patient;
        $identifiantStructure = $compteRendu->identifiant_structure;
        $identifiantPatient = $compteRendu->identifiant_patient;
        $numeroDemande = $compteRendu->numero_demande;
        $contenu = $compteRendu->contenu;
        $bsrvrConstLogin = substr($identifiantStructure, 0, 4) . $identifiantPatient;
        $messagePatient = isset($result->message_patient) ? $result->message_patient : null;
        $portailUrl = isset($result->portail_url) ? $result->portail_url : null;

        // If BioServeur does not respond with a 'ok' status, throw an exception
        if (FALSE === $this->prIsAvailableFromBsrvr($enrolement)) {

            if ($statut == self::API_STATUS_BAD_PORTAL) {
                $mssg = $this->translator->trans("agfa.hpa_bundle.enrolment.consultation_direct.bad_portal");

                if(isset($portailUrl)){
                    $mssg .= ' ' . $this->translator->trans("agfa.hpa_bundle.enrolment.consultation_direct.bad_portal_go_to", array(
                        '%domainPortal%' => $portailUrl
                    ));
                }
                $this->session->getFlashBag()->add('danger', $mssg);

                $this->logger->error(sprintf("Patient is on wrong portal (login) : %s", $enrolement->getLogin()));

                $this->journal->addDanger(Journal::EVENT_ENROLEMENT_BAD_PORTAL, $enrolement, $enrolement->getUser(), sprintf("%s (%s) : %s / %s", $this->translator->trans('bioserveur.api_constants.' . $statut), $result->message_erreur, $enrolement->getLogin(), $enrolement->getPassword()), array(
                    'api_query' => array(
                        'patient_login' => $enrolement->getLogin(),
                        'patient_password' => $enrolement->getPassword()
                    ),
                    'api_response' => array(
                        'identifiant_structure' => $identifiantStructure,
                        'identifiant_patient' => $identifiantPatient,
                        'numero_demande' => $numeroDemande
                    )
                ));
            } elseif ($statut == self::API_STATUS_BAD_IDENTIFIERS) {
                $messagePatient = isset($messagePatient) ? $messagePatient : $this->translator->trans("agfa.hpa_bundle.enrolment.consultation_direct.bad_identifier_generic");
                $this->session->getFlashBag()->add('danger', $messagePatient);
                $this->logger->error(sprintf("Patient has wrong identifiers (login) : %s", $enrolement->getLogin()));

                $this->journal->addDanger(Journal::EVENT_ENROLEMENT_BAD_IDENTIFIERS, $enrolement, $enrolement->getUser(), sprintf("%s (%s) : %s / %s", $this->translator->trans('bioserveur.api_constants.' . $statut), $result->message_erreur, $enrolement->getLogin(), $enrolement->getPassword()), array(
                    'api_query' => array(
                        'patient_login' => $enrolement->getLogin(),
                        'patient_password' => $enrolement->getPassword()
                    ),
                    'api_response' => array(
                        'identifiant_structure' => $identifiantStructure,
                        'identifiant_patient' => $identifiantPatient,
                        'numero_demande' => $numeroDemande
                    )
                ));
            } else {
                // Same error message as BAD_IDENTIFIER. Can BioServeur do the difference between 'IDs are wrong' and cannot find the PR?
                $this->session->getFlashBag()->add('danger', $this->translator->trans("agfa.hpa_bundle.enrolment.consultation_direct.bad_identifier"));
                $this->logger->error(sprintf("Patient record not found (login) : %s", $enrolement->getLogin()));

                $this->journal->addDanger(Journal::EVENT_ENROLEMENT_FAILURE, $enrolement, $enrolement->getUser(), sprintf("%s (%s) : %s / %s", $this->translator->trans('bioserveur.api_constants.' . $statut), $result->message_erreur, $enrolement->getLogin(), $enrolement->getPassword()), array(
                    'api_query' => array(
                        'patient_login' => $enrolement->getLogin(),
                        'patient_password' => $enrolement->getPassword()
                    ),
                    'api_response' => array(
                        'identifiant_structure' => $identifiantStructure,
                        'identifiant_patient' => $identifiantPatient,
                        'numero_demande' => $numeroDemande
                    )
                ));
            }

            return;
        }

        // If the patient record is formated as a pdf file, throw an exception
        if (TRUE === $this->prIsFormatedAsPdf()) {
            $this->journal->addDanger(Journal::EVENT_ENROLEMENT_FAILURE, $enrolement, $enrolement->getUser(), sprintf("%s - %s (%s)", $statut, $result->message_erreur, "Reception d'un pdf au lieu d'un editCR."), array(
                'api_query' => array(
                    'patient_login' => $enrolement->getLogin(),
                    'patient_password' => $enrolement->getPassword()
                ),
                'api_response' => array(
                    'identifiant_structure' => $identifiantStructure,
                    'identifiant_patient' => $identifiantPatient,
                    'numero_demande' => $numeroDemande
                )
            ));

            $this->logger->error(sprintf("Pdf format given, editcr expected (login) : %s", $enrolement->getLogin()));
            throw new EnrolementException("Une erreur est survenue lors de la réception du compte rendu. Merci de contacter votre laboratoire.", 'ERREUR_RECEPTION_PDF');
        }

        // Prevent accessing a patient record that has already been downloaded by a user.
        // NOTE: preventing more than one user to access a report will not work for reports previous to HPA 2.0 because we use nodemx and this data did not exists in version <= HPA 1.0.2
        if (FALSE === $this->prIsNew($result->compte_rendu->identifiant_structure, $result->compte_rendu->identifiant_patient, $result->compte_rendu->numero_demande)) {
            $this->logger->error(sprintf("Patient record already linked to an account (const. login / nodemx) : %s / %s", $bsrvrConstLogin, $numeroDemande));
            throw new EnrolementException($this->translator->trans('agfa.hpa_bundle.enrolment.consultation_direct.already_owned'), 'ERREUR_RECEPTION_ALREADY_OWNED');
        }

        // If the patient record is formated as a editCr file creates the enrollment else throw an exception
        if (TRUE === $this->prIsFormatedAsEditCr()) {
            $editcr = $contenu->data;
            if ($contenu->base64) {
                $editcr = base64_decode($editcr);
            }

            if ($demande = $this->connector->creerDemande($editcr, $user, array(
                'affichage_cr' => $compteRendu->affichage_cr,
                'laboratoire' => $laboratoire,
                'entite_juridique' => $entiteJuridique,
                'bsrvr_labo_id' => $identifiantStructure,
                'bsrvr_nopat' => $identifiantPatient,
                'solde_patient' => $soldePatient
            ))) {
                $this->em->flush();

                // Add an entry in the journal
                $this->journal->addSuccess(Journal::EVENT_ENROLEMENT_SUCCESS, $enrolement, $enrolement->getUser(), null, array(
                    'api_query' => array(
                        'patient_login' => $enrolement->getLogin(),
                        'patient_password' => $enrolement->getPassword()
                    ),
                    'api_response' => array(
                        'identifiant_structure' => $identifiantStructure,
                        'identifiant_patient' => $identifiantPatient,
                        'numero_demande' => $numeroDemande
                    )
                ));

                return $demande;
            }
        }

        $this->logger->error(sprintf("Unknown error (login) : %s", $enrolement->getLogin()));
        throw new EnrolementException("Une erreur est survenue lors de la réception du compte rendu. Merci de contacter votre laboratoire.", 'ERREUR_RECEPTION_API');
    }

    /**
     * Send a request to BioServeur to get a CR without user account.
     *
     * @param \Agfa\HpaBundle\Entity\Enrolement $enrolement
     *            Les données de l'enrôlement à réaliser
     * @return string A Pdf file.
     */
    public function recupererCompteRendu(Enrolement $enrolement)
    {
        $result = $this->getPatientRecord($enrolement);
        $statut = strtoupper($result->statut);
        $messagePatient = isset($result->message_patient) ? $result->message_patient : $this->translator->trans("agfa.hpa_bundle.undefined_error");
        $portailUrl = isset($result->portail_url) ? $result->portail_url : null;

        $compteRendu = $result->compte_rendu;
        $contenu = $compteRendu->contenu;
        $identifiantStructure = $compteRendu->identifiant_structure;
        $identifiantPatient = $compteRendu->identifiant_patient;
        $numeroDemande = $compteRendu->numero_demande;
        $bsrvrConstLogin = substr($identifiantStructure, 0, 4) . $identifiantPatient;

        $enrolement->setStatut($statut);
        $this->em->flush();

        if ($this->prCanBeDisplayed() === FALSE) {
            $this->session->getFlashBag()->add('danger', $this->translator->trans("agfa.hpa_bundle.enrolment.consultation_enrolee.no_access", array(
                '%urlLogin%' => $this->router->generate('fos_user_security_login'),
                '%urlRegister%' => $this->router->generate('fos_user_registration_register')
            )));
            return;
        } elseif ($statut == self::API_STATUS_BAD_PORTAL) {
            $mssg = $this->translator->trans("agfa.hpa_bundle.enrolment.consultation_direct.bad_portal");

            if(isset($portailUrl)){
                $mssg .= ' ' . $this->translator->trans("agfa.hpa_bundle.enrolment.consultation_direct.bad_portal_go_to", array(
                    '%domainPortal%' => $portailUrl
                ));
            }
            $this->session->getFlashBag()->add('danger', $mssg);
            return;
        } elseif ($statut == self::API_STATUS_BAD_IDENTIFIERS) {
            // Disabled as it is BioServeur who manages error messages for now. To correct for version 2.1.0.
            // $messagePatient = $this->translator->trans("agfa.hpa_bundle.enrolment.consultation_direct.bad_identifier");
            $messagePatient = isset($messagePatient) ? $messagePatient : $this->translator->trans("agfa.hpa_bundle.enrolment.consultation_direct.bad_identifier_generic");

            $this->session->getFlashBag()->add('danger', $messagePatient);
            return;
        } elseif ($statut !== self::API_STATUS_OK) {
            // If there is an error other than the one defined above
            throw new EnrolementException(isset($result->message_patient) ? $result->message_patient : $result->message_erreur, $statut);
        }

        // Prevent accessing a patient record that has already been downloaded by a user.
        // NOTE: preventing more than one user to access a report will not work for reports previous to HPA 2.0 because we use nodemx and this data did not exists in version <= HPA 1.0.2
        if (FALSE === $this->prIsNew($compteRendu->identifiant_structure, $compteRendu->identifiant_patient, $compteRendu->numero_demande)) {
            $this->logger->error(sprintf("Patient record already linked to an account (const. login / nodemx) : %s / %s", $bsrvrConstLogin, $numeroDemande));
            $this->session->getFlashBag()->add('danger', $this->translator->trans("agfa.hpa_bundle.enrolment.consultation_direct.already_owned"));
            return;
        }

        // If data is received with OK status
        switch ($contenu->format) {
            case 'pdf':
                $compteRenduPdf = $contenu->data;
                if ($contenu->base64) {
                    $compteRenduPdf = base64_decode($compteRenduPdf);
                }
                break;

            case 'editcr':
                try {
                    $compteRenduPdf = $this->connector->extraireCompteRenduPdf($contenu);
                } catch (\Exception $e) {
                    $this->logger->critical("Exception triggered while trying to access patient with no account (enrolment id : " . $enrolement->getId() . ", " . $e->getMessage() . ").");
                    throw new EnrolementException("Erreur lors de la réception du compte rendu. Veuillez contacter votre laboratoire.", 'ERREUR_RECEPTION');
                }
                break;
        }
        $this->em->flush();

        return $compteRenduPdf;
    }

    /**
     * Sends a notification to BioServeur with all CRs that were read.
     *
     * @param array $lectures
     * @return boolean TRUE if update is ok, FALSE if an error occurred.
     */
    public function notificationLectures($demandes)
    {
        $notificationLectures = [];

        foreach ($demandes as $demande) {
            foreach ($demande->getBsrvrComs() as $action) {
                $notificationLectures[] = [
                    'timestamp' => $action->getCreatedAt()->format('YmdHis'),
                    'compte_rendu' => [
                        'identifiant_structure' => $demande->getEtablissement()->getBsrvrCodeLabo(),
                        'identifiant_patient' => $demande->getPatient()->getBsrvrNopat(),
                        'numero_demande' => $demande->getNodemx()
                    ]
                ];
            }
        }

        $requete = [
            'authentification' => [
                'passphrase' => $this->bsrvr['pass_hexapat']
            ],
            'notification_lectures' => $notificationLectures
        ];

        $response = $this->envoyerRequete($this->bsrvrEnv['uri_notification_reading'], $requete);

        // Log error if any
        if (strtoupper($response->statut) == self::API_STATUS_OK) {
            return TRUE;
        } else {
            $this->logger->critical("Problem occurred with the update of 'demandes' on reading status [" . json_encode($response) . "]");

            return FALSE;
        }
    }

    /**
     * Sends a notification to BioServeur with all patients that were deleted.
     *
     * @param array $lectures
     * @return boolean TRUE if update is ok, FALSE if an error occurred.
     */
    public function notificationDesenrolement(BsrvrCom $unenrollment)
    {
        $data = $unenrollment->getData();
        $bsrvrRefEtablissement = $data['bsrvr_ref_etablissement'];
        $nopat = $data['nopat'];

        $requete = [
            'authentification' => [
                'passphrase' => $this->bsrvr['pass_hexapat']
            ],
            'notification_unenrollment' => [
                'timestamp' => $unenrollment->getCreatedAt()->format('YmdHis'),
                'patient' => [
                    'finess' => $bsrvrRefEtablissement,
                    'nopat' => $nopat
                ]
            ]
        ];

        $response = $this->envoyerRequete($this->bsrvrEnv['uri_notification_unenrollment'], $requete);

        // Log error if any
        if (strtoupper($response->statut) == self::API_STATUS_OK) {
            return TRUE;
        } else {
            // Increment attempts
            $attempts = $unenrollment->getAttempt();
            $unenrollment->setAttempt($attempts+1);
            $this->em->persist($unenrollment);
            $this->em->flush();

            $this->logger->critical("Problem occurred with the update of 'unenrollment' on reading status [" . json_encode($response) . "]");

            return FALSE;
        }
    }

    /**
     * Sends an updates of the payment details to Bioserveur
     *
     * @param Demande $demande
     * @return array
     */
    public function updatePaiement($demandes)
    {
        $notificationPayments = [];

        /* @var $lecture Lecture */
        foreach ($demandes as $demande) {
            $numerosTransactions = array();

            // Because it is possible that one day there is more than one transaction for a CR
            foreach ($demande->getRequests() as $request) {
                $numerosTransactions[] = $request->getReference();
            }

            foreach ($demande->getBsrvrComs() as $action) {
                $notificationPayments[] = [
                    'timestamp' => $action->getCreatedAt()->format('YmdHis'),
                    'compte_rendu' => [
                        'identifiant_structure' => $demande->getEtablissement()->getBsrvrCodeLabo(),
                        'identifiant_patient' => $demande->getPatient()->getBsrvrNopat(),
                        'numero_demande' => $demande->getNodemx(),
                        'solde' => $demande->getPaieSolde(),
                        'numeros_transactions' => $numerosTransactions
                    ]
                ];
            }
        }

        $requete = [
            'authentification' => [
                'passphrase' => $this->bsrvr['pass_hexapat']
            ],
            'notification_paiement' => $notificationPayments
        ];

        $response = $this->envoyerRequete($this->bsrvrEnv['uri_notification_payment'], $requete);

        // Update demands only if the nodemx is not in the ignore_nodemx part of the response
        $nbSuccess = array();
        $nbFailure = array();

        foreach ($demandes as $demande) {
            foreach ($demande->getBsrvrComs() as $action) {
                $whatToDo = FALSE;
                $responseStatus = '';

                if (isset($response->resultat_maj_paiement) && count($response->resultat_maj_paiement) > 0) {
                    foreach ($response->resultat_maj_paiement as $responseUpdatePayment) {
                        if ($whatToDo === FALSE && $demande->getNodemx() == $responseUpdatePayment->nodemx && strtoupper($responseUpdatePayment->statut) == self::API_STATUS_OK) {
                            $whatToDo = 'remove';
                            $responseStatus = $responseUpdatePayment->statut;
                        } elseif ($whatToDo === FALSE && $demande->getNodemx() == $responseUpdatePayment->nodemx) {
                            // Three attempts are made before the Doctrine query does not fetch the demande anymore from the db
                            $whatToDo = 'update';
                            $responseStatus = $responseUpdatePayment->statut;
                        }
                    }
                } else {
                    if (strtoupper($response->statut) == self::API_STATUS_OK) {
                        $whatToDo = 'remove';
                    } else {
                        $whatToDo = 'update';
                    }
                }

                if ($whatToDo == 'remove') {
                    $this->em->remove($action);
                    $nbSuccess[$demande->getNodemx()] = isset($nbSuccess[$demande->getNodemx()]) ? $nbSuccess[$demande->getNodemx()] ++ : 1;
                }

                if ($whatToDo == 'update') {
                    $action->setAttempt($action->getAttempt() + 1);
                    $action->setResponseStatus($responseStatus);
                    $nbFailure[$demande->getNodemx()] = isset($nbFailure[$demande->getNodemx()]) ? $nbFailure[$demande->getNodemx()] ++ : 1;
                }
            }
        }

        $this->em->flush();

        // Return result, log if any error
        switch (strtoupper($response->statut)) {
            case self::API_STATUS_OK:
                $result = array(
                    'status' => self::STATUS_OK,
                    'message' => 'La demande des mises à jour de paiement s\'est bien passée.'
                );
                break;
            case self::API_STATUS_UNKNOWN_ERROR:
            case self::API_STATUS_DEMANDE_INCONNU:
                $this->logger->critical(sprintf("hpa:bsrvr:update-payment - Problem occurred with the update of 'demandes' on payment status: %s success, %s failure [%s]", count($nbSuccess), count($nbFailure), json_encode($response)));
                $result = array(
                    'status' => self::STATUS_NOT_OK,
                    'message' => 'La demande des mises à jour de lecture n\'a pas été exécutée ou incomplètement.'
                );
                ;
                break;
            case self::API_STATUS_BAD_REQUEST:
                $this->logger->critical("Bad request [" . json_encode($response) . "]");
                $result = array(
                    'status' => self::STATUS_NOT_OK,
                    'message' => 'Problème avec le format JSON de la demande.'
                );
                break;
            case self::API_STATUS_UNAUTHORIZED:
                $this->logger->critical("Access to Bioserveur API restricted [" . json_encode($response) . "]");
                $result = array(
                    'status' => self::STATUS_NOT_OK,
                    'message' => 'Accès à Bioserveur API refusé.'
                );
                break;
        }

        return $result;
    }

    /**
     * Logs the exchange without the content of the CR.
     *
     * @param resource $stream
     * @param array $requete
     * @param array $reponse
     * @param string $nomOperation
     */
    public function loguerEchange($requete, $reponse, $type = null)
    {
        if (! is_array($reponse)) {
            $reponse = json_decode($reponse->getBody());

            array_walk_recursive($reponse, function ($element) {
                if (isset($element->contenu)) {
                    $element->contenu = "...";
                }
            });
        }

        if(isset($requete['compte_rendu']['contenu'])){
            $requete['compte_rendu']['contenu'] = "...";
        }

        $now = new \DateTime();

        $logfile = $this->kernel->getLogDir() . '/bioserveur.log';
        $stream = fopen($logfile, 'a');
        fwrite($stream, "\n");
        fwrite($stream, sprintf("%s [%s]\n", $now->format('c'), $type));
        fwrite($stream, "\n");
        fwrite($stream, "Request body\n");
        fwrite($stream, json_encode($requete, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES));
        fwrite($stream, "\n");
        fwrite($stream, "\n");
        fwrite($stream, "Response body\n");
        fwrite($stream, $reponse = json_encode($reponse, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES));
        fwrite($stream, "\n");
        fwrite($stream, "******************************************************************");
        fwrite($stream, "\n");
        fclose($stream);

        return;
    }

    /**
     * Check that a CR is available to retrieve.
     *
     * @param Enrolement $enrolement
     * @return boolean
     */
    public function prIsAvailableFromBsrvr()
    {
        if (strtoupper($this->patientRecord->statut) == self::API_STATUS_OK) {
            return TRUE;
        }

        return FALSE;
    }

    /**
     * Return true if the patient record is formated as a pdf file.
     *
     * @return boolean
     */
    public function prIsFormatedAsPdf()
    {
        if (strtolower($this->patientRecord->compte_rendu->contenu->format) == 'pdf') {
            return TRUE;
        }

        return FALSE;
    }

    /**
     * Return true if the patient record is formated as a editcr file.
     *
     * @return boolean
     */
    public function prIsFormatedAsEditCr()
    {
        if (strtolower($this->patientRecord->compte_rendu->contenu->format) == 'editcr') {
            return TRUE;
        }

        return FALSE;
    }

    /**
     * Find the patient record in the db and return FALSE if it is already found.
     *
     * @return boolean
     */
    public function prIsNew($idStructure, $idPatient, $nodemx)
    {
        $bsrvrConstLogin = substr($idStructure, 0, 4) . $idPatient;

        if (is_null($this->demandeMgr->findDemande($bsrvrConstLogin, $nodemx))) {
            return TRUE;
        }

        return FALSE;
    }

    /**
     * Returns TRUE if a patient record can be displayed.
     *
     * @return boolean
     */
    private function prCanBeDisplayed()
    {
        if ($this->paiementIsActivated() === TRUE && $this->paiementAffichageCr() === FALSE && $this->prIsPaid() === FALSE) {
            return FALSE;
        }

        return TRUE;
    }

    /**
     * Returns TRUE when the patient record has been paid.
     *
     * @return boolean
     */
    private function prIsPaid()
    {
        $sourcePaiement = strtoupper($this->patientRecord->compte_rendu->source_paiement);
        $soldePatient = $this->patientRecord->compte_rendu->solde_patient;

        if ($sourcePaiement == self::API_SOURCE_PAYMENT_NOT_PAID || ! is_null($soldePatient)) {
            return FALSE;
        }

        return TRUE;
    }

    /**
     * Returns TRUE when the CR is available before payment.
     *
     * @return boolean
     */
    private function paiementAffichageCr()
    {
        if (strtolower($this->patientRecord->compte_rendu->laboratoire->paiement->paiement_affichage_cr) == TRUE) {
            return TRUE;
        }

        if (strtolower($this->patientRecord->compte_rendu->affichage_cr) == FALSE) {
            return FALSE;
        }

        return TRUE;
    }

    /**
     * Returns TRUE if the payment is activated.
     *
     * @return boolean
     */
    private function paiementIsActivated()
    {
        if (strtolower($this->patientRecord->compte_rendu->laboratoire->paiement->paiement_actif) == TRUE) {
            return TRUE;
        }

        return FALSE;
    }

    /**
     * Sends a https request
     *
     * @param string $path
     * @param array $requete
     * @throws EnrolementException
     * @return string JSON formated string.
     */
    private function envoyerRequete($path, $requete)
    {
        if (substr($path, 0, 1) !== '/') {
            $path = '/' . $path;
        }

        $url = $this->bsrvrEnv['host'] . $path;

        $client = new \GuzzleHttp\Client();

        try {
            $response = $client->request('POST', $url, array(
                'json' => $requete,
                'exceptions' => TRUE
            ));
        } catch (\Exception $e) {
            $this->logger->critical("Exception lors de l'envoi d'une requete d'API Bioserveur [" . $e->getMessage() . "]");
        }

        $result = (string) $response->getBody();

        // Log exchange if debug is on
        if ($this->bsrvrEnv['debug'] === TRUE) {

            $status = isset($response->status) ? $response->status : '';

            $this->loguerEchange($requete, $response, $status);
        }

        return json_decode($result);
    }
}
