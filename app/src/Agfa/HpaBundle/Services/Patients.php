<?php
namespace Agfa\HpaBundle\Services;

use Doctrine\Bundle\DoctrineBundle\Registry as Doctrine;
use Symfony\Bridge\Monolog\Logger;
use Agfa\HpaBundle\Entity\Personne;
use Agfa\HpaBundle\Entity\Demande;
use Agfa\HpaBundle\Entity\Journal;
use Agfa\HpaBundle\Manager\DemandeManager;
use Agfa\HpaBundle\Entity\PersonnePatient;
use Agfa\HpaBundle\Manager\PatientManager;
use Agfa\UserBundle\Entity\User;

class Patients
{

    /* @var Doctrine */
    public $doctrine;

    public function __construct(Doctrine $doctrine, Logger $logger, Journalist $journalist, DemandeManager $demandeMgr, PatientManager $patientMgr)
    {
        $this->doctrine =   $doctrine;
        $this->em =         $doctrine->getManager();
        $this->logger =     $logger;
        $this->journalist = $journalist;
        $this->demandeMgr = $demandeMgr;
        $this->patientMgr = $patientMgr;
    }

    /**
     * Essaie de trouver une personne à qui rattacher le patient de la demande
     *
     * @param Demande $demande
     * @return boolean
     */
    public function rapprocherPatient(Demande $demande)
    {
        $patient = $demande->getPatient();
        $user = $patient->getUser();

        // FIXME Vérifier les requète du service car il y en a trop (33).
        // Il va peut être falloir stopper les redirection ou trouver une autre solution afin de vérifer cela
        // Travailler avec les fichiers EditCrConnector, DemandeRepository, DemandeManager


        /* CAS 1 - si le patient est déjà rattaché à une personne, on affiche directement le détail du CR */
        // The following query will only return a result if the demande is already linked to a personne
        if($this->demandeMgr->getDemandeForRapprochement($user, $demande->getId())){
            return true;
        }

        /* CAS 2 - s'il n'existe pas encore de personne, on la crée */
        if (count($user->getPersonnes()) === 0) {
            $personne = new Personne();
            $personne->setNom($patient->getNom());
            $personne->setPrenom($patient->getPrenom());
            $personne->setSexe($patient->getSexe());
            $personne->setDateNaissance($patient->getDateNaissance());
            $personne->setUser($patient->getUser());
            $this->em->persist($personne);

            $personnePatient = new PersonnePatient();
            $personnePatient->setPersonne($personne)
                ->setPatient($patient);
            $this->em->persist($personnePatient);

            $this->em->flush();

            $this->journalist->addInfo(Journal::EVENT_PATIENT_EDITION, $patient, $patient->getUser(), sprintf("Rattachement du patient %s à la personne %s", $patient->getNomComplet(), $personne->getNomComplet()));
            $this->journalist->addInfo(Journal::EVENT_PERSONNE_CREATION, $personne, $personne->getUser(), sprintf('Création de la personne %s ', $personne->getNomComplet()));

            return true;
        }


        /*
         * CAS 3 - si aucun des patients du compte n'a le meme nom et que aucun n'a le meme prenom et que
         * aucun n'a la meme date de naissance, on crée une personne sans demander l'avis de l'utilisateur
         * FIXME beccha - Est-ce que cela ne devrait pas être avant l'étape de vérification au moins une personne existe ? Serait un problème si un flush était lancé...
         */
//         $criteria = new \Doctrine\Common\Collections\Criteria();

//         $criteria->where($criteria->expr()
//             ->orX($criteria->expr()
//             ->eq('nom', $patient->getNom()), $criteria->expr()
//             ->eq('prenom', $patient->getPrenom()), $criteria->expr()
//             ->eq('dateNaissance', $patient->getDateNaissance())));

//         $criteria->andWhere($criteria->expr()
//             ->eq('user', $patient->getUser()));

//         $patients_point_commun = $this->em->getRepository('AgfaHpaBundle:Patient')->matching($criteria);

//         if (count($patients_point_commun) === 0) {
//             $personne = new Personne();
//             $personne->setNom($patient->getNom());
//             $personne->setPrenom($patient->getPrenom());
//             $personne->setSexe($patient->getSexe());
//             $personne->setDateNaissance($patient->getDateNaissance());
//             $personne->setUser($demande->getUser());
//             $this->em->persist($personne);

//             $personnePatient = new PersonnePatient();
//             $personnePatient->setPersonne($personne)
//                 ->setPatient($patient);
//             $this->em->persist($personnePatient);

//             $this->em->flush();

//             $this->journalist->addInfo(Journal::EVENT_PERSONNE_CREATION, $personne, $personne->getUser());
//             $this->journalist->addInfo(Journal::EVENT_PATIENT_EDITION, $patient, $patient->getUser());

//             return true;
//         }


        /* CAS 4 - on recherche un patient avec le même INS-C et le même user */

        // FIXME URGENT Comment cela fonctionne dans le cas d'un patient partagé ?


        if($patientMemeInsc = $this->patientMgr->getPatientByInsc($user, $patient->getInsc())){
            $personne = $patientMemeInsc->getPatientPersonnes()[0]->getPersonne();

            $personnePatient = new PersonnePatient();
            $personnePatient->setPersonne($personne)
                ->setPatient($patient);
            $this->em->persist($personnePatient);
            $this->em->flush();

            $this->journalist->addInfo(Journal::EVENT_PATIENT_EDITION, $patient, $user, sprintf("Rattachement du patient %s à la personne %s", $patient->getNomComplet(), $personne->getNomComplet()));

            return true;
        }


        /* CAS 5 - on recherche un patient avec la même identité */
        $patientSamePersonalDetails = $this->patientMgr->findPatientFromPersonalDetails($user, $patient->getNom(), $patient->getPrenom(), $patient->getDateNaissance());

        /* si on le trouve, on l'associe à la demande et on valide */
        if ($patientSamePersonalDetails) {
            $personne = $patientSamePersonalDetails->getPatientPersonnes()[0]->getPersonne();

            $personnePatient = new PersonnePatient();
            $personnePatient->setPersonne($personne)
                ->setPatient($patient);
            $this->em->persist($personnePatient);
            $this->em->flush();

            $this->journalist->addInfo(Journal::EVENT_PATIENT_EDITION, $patient, $user, sprintf("Rattachement du patient %s à la personne %s", $patient->getNomComplet(), $personne->getNomComplet()));

            return true;
        }


        /* CAS 6 - sinon on affiche les infos qu'on a et on demande au user de choisir la bonne personne */
        return false;
    }
}