<?php
namespace Agfa\HpaBundle\Services;

use Doctrine\Bundle\DoctrineBundle\Registry as Doctrine;
use Agfa\HpaBundle\Entity\Journal;
use Agfa\UserBundle\Entity\User;
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\HttpFoundation\RequestStack;
use Agfa\MoneticoPaiementBundle\Library\Request;
use Agfa\MoneticoPaiementBundle\Library\Response;

class Journalist
{

    public function __construct(Doctrine $doctrine, RequestStack $requestStack)
    {
        $this->em = $doctrine->getManager();
        $this->requestStack = $requestStack;
    }

    /**
     * For user triggered events that went successfully
     *
     * @param integer $type
     * @param object $entity
     * @param User $user
     * @param string $shortDescription
     * @param array $longDescription
     */
    public function addSuccess($type, $entity = NULL, User $user, $shortDescription = NULL, array $longDescription = NULL)
    {
        $this->createEntry($type, $entity, $user, $shortDescription, Journal::EVENT_LEVEL_SUCCESS, $longDescription);
        return;
    }

    /**
     * For system triggered events that went successfully
     *
     * @param integer $type
     * @param object $entity
     * @param User $user
     * @param string $shortDescription
     * @param array $longDescription
     */
    public function addInfo($type, $entity = NULL, User $user, $shortDescription = NULL, array $longDescription = NULL)
    {
        $this->createEntry($type, $entity, $user, $shortDescription, Journal::EVENT_LEVEL_INFO, $longDescription);
        return;
    }

    /**
     * For user triggered events of importance (updates or deletion of data) that went successfully
     *
     * @param integer $type
     * @param object $entity
     * @param User $user
     * @param string $shortDescription
     * @param array $longDescription
     */
    public function addWarning($type, $entity = NULL, User $user, $shortDescription = NULL, array $longDescription = NULL)
    {
        $this->createEntry($type, $entity, $user, $shortDescription, Journal::EVENT_LEVEL_WARNING, $longDescription);
        return;
    }

    /**
     * For user or system triggered events that went wrong
     *
     * @param integer $type
     * @param object $entity
     * @param User $user
     * @param string $shortDescription
     * @param array $longDescription
     */
    public function addDanger($type, $entity = NULL, User $user, $shortDescription = NULL, array $longDescription = NULL)
    {
        $this->createEntry($type, $entity, $user, $shortDescription, Journal::EVENT_LEVEL_DANGER, $longDescription);
        return;
    }

    /**
     * Ecrit dans Journal une ligne de trace
     *
     * @param integer $type
     *            cf \Agfa\HpaBundle\Entity\Journal::EVENT_*
     * @param object $entity
     *            entité doctrine
     * @param \Agfa\UserBundle\Entity\User $user
     *            qui doit voir la trace
     * @param string $extra
     *            informations complémentaires
     */
    private function createEntry($type, $entity = NULL, User $user, $shortDescription = NULL, $level = NULL, array $longDescription = NULL)
    {
        $journal = new Journal();
        $journal->setType($type)
            ->setIpAddress($this->requestStack->getMasterRequest()
            ->getClientIp())
            ->setShortDescription($shortDescription)
            ->setLongDescription($longDescription)
            ->setUser($user)
            ->setLevel($level);

        if ($entity) {
            $journal->setEntityId($entity->getId());
        }

        $this->em->persist($journal);
        $this->em->flush();

        return;
    }

    /**
     * Retourne les dernières entrées pour un user donné
     *
     * @param User $user
     * @param string $type
     * @param integer $limit
     * @return string
     */
    public function getEntry($user, $type = NULL, $limit = NULL)
    {
        $qb = new QueryBuilder($this->em);

        $query = $qb->select('j')
            ->from('AgfaHpaBundle:Journal', 'j')
            ->where('j.user = :user')
            ->setParameter('user', $user)
            ->orderBy('j.createdAt', 'DESC');

        if (isset($type)) {
            $qb->andWhere('j.type = :type')->setParameter('type', $type);
        }

        if ($limit) {
            $qb->setMaxResults($limit);
        }

        $query = $qb->getQuery();

        return $query->getResult();
    }

    /**
     * Converts paiement object into usable array for the longDescription field.
     *
     * @param Request $payment
     * @return array
     */
    public function convertPaymentRequest(Request $payment)
    {
        $paymentArray = array();
        $paymentArray['payment_request'] = array(
            'tpe' => $payment->getTpe(),
            'date' => $payment->getDate(),
            'montant' => $payment->getMontant(),
            'reference' => $payment->getReference(),
            'texte_libre' => $payment->getTexteLibre(),
            'version' => $payment->getVersion(),
            'lgue' => $payment->getLgue(),
            'societe' => $payment->getSociete(),
            'email' => $payment->getEmail()
        );

        return $paymentArray;
    }




    public function convertPaymentResponse(Response $response)
    {
        $paymentArray = array();
        $paymentArray['payment_response'] = array(
            'motif_refus' => $response->getMotifRefus(),
            'ip_client' =>$response->getIpClient(),
            'code_retour' => $response->getCodeRetour(),
            'montant' => $response->getMontant(),
            'date' => $response->getDate(),
            'reference' => $response->getReference(),
            'texte_libre' => $response->getTexteLibre(),
            'cvx' => $response->getCvx(),
            'vld' => $response->getVld(),
            'brand' => $response->getBrand(),
            'status3ds' => $response->getStatus3ds(),
            'num_auto' => $response->getNumAuto(),
            'origine_cb' => $response->getOrigineCb(),
            'origine_tr' => $response->getOrigineTr(),
            'veres' => $response->getVeres(),
            'pares' => $response->getPares(),
            'filtrage_cause' => $response->getFiltrageCause(),
            'filtrage_valeur' => $response->getFiltrageValeur(),
            'mode_paiement' => $response->getModePaiement()
        );

        return $paymentArray;
    }
}
