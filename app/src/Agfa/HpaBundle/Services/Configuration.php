<?php
namespace Agfa\HpaBundle\Services;

use Agfa\HpaBundle\Manager\ConfigurationManager;

/**
 * Get settings parameters for the app (website) from the database.
 */
class Configuration
{

    public function __construct(ConfigurationManager $configurationManager, $themeName)
    {
        $this->configuration = $configurationManager->getConfiguration($themeName);
    }

    /**
     * Get the value for the logo alignment.
     *
     * @return string left or center
     */
    public function getLogoAlignement()
    {
        return $this->configuration->getLogoAlignement();
    }

    /**
     * Get the url for the redirection.
     *
     * @return string A url or an empty string.
     */
    public function getLogoutRedirection()
    {
        return $this->configuration->getLogoutRedirectionUrl();
    }

    /**
     * Get the setting for direct access to patient record.
     *
     * @return boolean
     */
    public function getPatientRecordDirectAccess()
    {
        return $this->configuration->getPatientRecordDirectAccess();
    }

    /**
     * Return true if patients can get direct access to more than their last patient record.
     *
     * @return boolean
     */
    public function getLastPatientsRecordOnlyMssg()
    {
        return $this->configuration->getLastPatientsRecordOnlyMssg();
    }

    /**
     * Return the path of the video tuto about creating an account.
     *
     * @return boolean
     */
    public function getVideoTutorialRegister()
    {
        return $this->configuration->getVideoTutorialRegister();
    }
}