<?php
namespace Agfa\HpaBundle\Services;

class Cache
{

    const KEY_SEARCH_FORM_VARIABLES = 'key_search_form_variables';

    public static function getCacheKey($key, array $params)
    {
        $cacheKey = null;

        switch ($key) {
            case self::KEY_SEARCH_FORM_VARIABLES:
                $personId = $params['person_id'];

                $cacheKey = sprintf('%s_%d', self::KEY_SEARCH_FORM_VARIABLES, $personId);
                break;
        }

        return $cacheKey;
    }
}