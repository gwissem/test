<?php
namespace Agfa\HpaBundle\Services\Piwik;

use PiwikTracker;
use Agfa\HpaBundle\Entity\RechercheDemande;

class SearchForm
{

    public function __construct($piwikHost, $piwikSiteId)
    {
        PiwikTracker::$URL = 'https://' . $piwikHost;
        $this->tracker = new PiwikTracker($piwikSiteId);
    }

    public function compteRendusForm($criterias)
    {
        if (! empty($criterias['prescripteur'])) {
            $this->tracker->doTrackEvent('Recherche comptes rendus', 'Champs de recherche', 'Médecin prescripteur');
        }

        if (! empty($criterias['laboratoire'])) {
            $this->tracker->doTrackEvent('Recherche comptes rendus', 'Champs de recherche', 'Laboratoire');
        }

        if (! empty($criterias['examen'])) {
            $this->tracker->doTrackEvent('Recherche comptes rendus', 'Champs de recherche', 'Examen');
        }

        if (! empty($criterias['element'])) {
            $this->tracker->doTrackEvent('Recherche comptes rendus', 'Champs de recherche', 'Element');
        }

        $datePrelevementDebut = $criterias['datePrelevementDebut'] . ' 00:00:00';
        $datePrelevementDebut = \DateTime::createFromFormat('d/m/Y H:i:s', $datePrelevementDebut);

        if (! empty($criterias['datePrelevementDebut']) && $datePrelevementDebut != new \DateTime(RechercheDemande::RECHERCHE_DATE_PRELEVEMENT_DEBUT . ' 00:00:00')) {
            $this->tracker->doTrackEvent('Recherche comptes rendus', 'Champs de recherche', 'Début période');
        }

        $datePrelevementFin= $criterias['datePrelevementFin'] . ' 00:00:00';
        $datePrelevementFin = \DateTime::createFromFormat('d/m/Y H:i:s', $datePrelevementFin);

        if (! empty($criterias['datePrelevementFin']) && $datePrelevementFin != new \DateTime(RechercheDemande::RECHERCHE_DATE_PRELEVEMENT_FIN . ' 23:59:59')) {
            $this->tracker->doTrackEvent('Recherche comptes rendus', 'Champs de recherche', 'Fin période');
        }
    }
}


