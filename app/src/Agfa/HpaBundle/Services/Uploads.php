<?php

namespace Agfa\HpaBundle\Services;

use Doctrine\Bundle\DoctrineBundle\Registry as Doctrine;
use Symfony\Bridge\Monolog\Logger;

class Uploads
{
    public function __construct (Doctrine $doctrine, Logger $logger)
    {
        $this->em = $doctrine->getManager();
        $this->logger = $logger;
    }

    public function getTotalFilesizeForPersonne($personne)
    {
        /* @var $qb \Doctrine\ORM\QueryBuilder */
        $qb = $this->em->createQueryBuilder();
        $qb->select('SUM(u.filesize)')
        ->from('AgfaUploadBundle:Upload', 'u')
        ->where('u.personne = ?1');
        $q = $qb->setParameter('1', $personne)->getQuery();
        $result = $q->getOneOrNullResult();
        if($result) {
            return array_shift($result);
        } else {
            return 0;
        }
    }

    public function getHumanReadableSize($filesize)
    {
        if(!$filesize) {
            return '-';
        }

        // les unites, version française
        $unites = ['','Ko','Mo','Go','To'];

        // la taille min est 1 Ko, sauf si on a 0
        $octets = max(1024, $filesize);

        // le log1000 de la taille du fichier nous donne la puissance de 1000 qui correspond
        // on ne doit pas dépasser la taille du tableau $unites
        $grandeur = min(floor(log($octets, 1024)), count($unites) - 1);

        $unite = $unites[$grandeur];

        // formatage : on va afficher 2 chiffres significatifs
        return sprintf("%g %s", sprintf("%.2e", $octets / pow(1024, $grandeur)), $unite);
    }

    public function getIcon($mimetype)
    {
        // pour FontAwesome, les icones possibles sont :
        // file, -text, -pdf, -word, -excel, -powerpoint, -image, -archive, -audio, -video, -code

        // par défault, on n'a pas de type particulier, donc ce sera file
        $icon = null;

        $mimetype = explode('/', $mimetype);

        switch($mimetype[0]) {

            case 'text':
                $icon = 'text';
                break;

            case 'image':
                $icon = 'image';
                break;

            case 'audio':
                $icon = 'audio';
                break;

            case 'video':
                $icon = 'video';
                break;

            case 'application':
                switch($mimetype[1]) {

                    case 'pdf':
                        $icon = 'pdf';
                        break;

                    case 'msword':
                        $icon = 'word';
                        break;

                    case 'vnd.ms-excel':
                        $icon = 'excel';
                        break;

                    case 'vnd.ms-powerpoint':
                        $icon = 'powerpoint';
                        break;

                    case 'zip':
                        $icon = 'archive';
                        break;

                }
                break;

        }

        if($icon) {
            return "file-$icon-o";
        } else {
            return "file-o";
        }
    }
}