<?php

namespace Agfa\HpaBundle\Entity;

/**
 * Personne
 */
class Personne
{
    public function  __toString()
    {
        return $this->getNomComplet();
    }

    public function getAge()
    {
        return $this->dateNaissance->diff(new \DateTime())->format('%y');
    }

    public function getNomComplet()
    {
        $fullName = mb_convert_case($this->getPrenom(), MB_CASE_TITLE, "UTF-8") . ' ' . mb_strtoupper($this->getNom(), "UTF-8");

        return $fullName;
    }

    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $nom;

    /**
     * @var string
     */
    private $prenom;

    /**
     * @var \DateTime
     */
    private $dateNaissance;

    /**
     * @var string
     */
    private $sexe;

    /**
     * @var \datetime
     */
    private $createdAt;

    /**
     * @var \datetime
     */
    private $updatedAt;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $uploads;

    /**
     * @var \Agfa\UserBundle\Entity\User
     */
    private $user;

    /**
     * @var \Agfa\UserBundle\Entity\User
     */
    private $owner;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->demandes = new \Doctrine\Common\Collections\ArrayCollection();
        $this->uploads = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nom
     *
     * @param string $nom
     * @return Personne
     */
    public function setNom($nom)
    {
        $this->nom = mb_strtoupper($nom, "UTF-8");

        return $this;
    }

    /**
     * Get nom
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set prenom
     *
     * @param string $prenom
     * @return Personne
     */
    public function setPrenom($prenom)
    {
        $this->prenom = mb_convert_case($prenom, MB_CASE_TITLE, "UTF-8");

        return $this;
    }

    /**
     * Get prenom
     *
     * @return string
     */
    public function getPrenom()
    {
        return $this->prenom;
    }

    /**
     * Set dateNaissance
     *
     * @param \DateTime $dateNaissance
     * @return Personne
     */
    public function setDateNaissance($dateNaissance)
    {
        $this->dateNaissance = $dateNaissance;

        return $this;
    }

    /**
     * Get dateNaissance
     *
     * @return \DateTime
     */
    public function getDateNaissance()
    {
        return $this->dateNaissance;
    }

    /**
     * Set sexe
     *
     * @param string $sexe
     * @return Personne
     */
    public function setSexe($sexe)
    {
        $this->sexe = mb_convert_case($sexe, MB_CASE_LOWER, "UTF-8");

        return $this;
    }

    /**
     * Get sexe
     *
     * @return string
     */
    public function getSexe()
    {
        return $this->sexe;
    }

    /**
     * Add uploads
     *
     * @param \Agfa\HpaBundle\Entity\Upload $uploads
     * @return Personne
     */
    public function addUpload(\Agfa\HpaBundle\Entity\Upload $uploads)
    {
        $this->uploads[] = $uploads;

        return $this;
    }

    /**
     * Remove uploads
     *
     * @param \Agfa\HpaBundle\Entity\Upload $uploads
     */
    public function removeUpload(\Agfa\HpaBundle\Entity\Upload $uploads)
    {
        $this->uploads->removeElement($uploads);
    }

    /**
     * Get uploads
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getUploads()
    {
        return $this->uploads;
    }

    /**
     * Set user
     *
     * @param \Agfa\UserBundle\Entity\User $user
     * @return Personne
     */
    public function setUser(\Agfa\UserBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \Agfa\UserBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Personne
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return Personne
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $patients;


    /**
     * Add patient
     *
     * @param \Agfa\HpaBundle\Entity\Patient $patient
     *
     * @return Personne
     */
    public function addPatient(\Agfa\HpaBundle\Entity\Patient $patient)
    {
        $this->patients[] = $patient;

        return $this;
    }

    /**
     * Remove patient
     *
     * @param \Agfa\HpaBundle\Entity\Patient $patient
     */
    public function removePatient(\Agfa\HpaBundle\Entity\Patient $patient)
    {
        $this->patients->removeElement($patient);
    }

    /**
     * Get patients
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPatients()
    {
        return $this->patients;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $personnePatients;


    /**
     * Set owner
     *
     * @param \Agfa\UserBundle\Entity\User $owner
     *
     * @return Personne
     */
    public function setOwner(\Agfa\UserBundle\Entity\User $owner = null)
    {
        $this->owner = $owner;

        return $this;
    }

    /**
     * Get owner
     *
     * @return \Agfa\UserBundle\Entity\User
     */
    public function getOwner()
    {
        return $this->owner;
    }

    /**
     * Add personnePatient
     *
     * @param \Agfa\HpaBundle\Entity\PersonnePatient $personnePatient
     *
     * @return Personne
     */
    public function addPersonnePatient(\Agfa\HpaBundle\Entity\PersonnePatient $personnePatient)
    {
        $this->personnePatients[] = $personnePatient;

        return $this;
    }

    /**
     * Remove personnePatient
     *
     * @param \Agfa\HpaBundle\Entity\PersonnePatient $personnePatient
     */
    public function removePersonnePatient(\Agfa\HpaBundle\Entity\PersonnePatient $personnePatient)
    {
        $this->personnePatients->removeElement($personnePatient);
    }

    /**
     * Get personnePatients
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPersonnePatients()
    {
        return $this->personnePatients;
    }
}
