<?php
namespace Agfa\HpaBundle\Entity;

use Agfa\HpaBundle\Library\MaintenanceFr;

/**
 * Etablissement
 */
class Etablissement
{

    function getTelephoneFormate()
    {
        return preg_replace('/(..)/', '$1 ', $this->telephone);
    }

    /**
     *
     * @var \Agfa\HpaBundle\Entity\Paiement
     */
    private $paiement;

    /**
     *
     * @var integer
     */
    private $id;

    /**
     *
     * @var string
     */
    private $nom;

    /**
     *
     * @var string
     *
     */
    private $libelle;

    /**
     *
     * @var string
     */
    private $adresse;

    /**
     *
     * @var string
     */
    private $codePostal;

    /**
     *
     * @var string
     */
    private $ville;

    /**
     *
     * @var string
     */
    private $telephone;

    /**
     *
     * @var string
     */
    private $bsrvrCodeLabo;

    /**
     *
     * @var string
     */
    private $bsrvrCodeEntiteJuridique;

    /**
     *
     * @var string
     *
     */
    private $bsrvrRefEtablissement;

    /**
     *
     * @var string
     */
    private $hxlCodeStructure;

    /**
     *
     * @var \Doctrine\Common\Collections\Collection
     */
    private $demandes;

    /**
     *
     * @var \Agfa\HpaBundle\Entity\Etablissement
     */
    private $children;

    /**
     *
     * @var \Doctrine\Common\Collections\Collection
     */
    private $parent;

    /**
     *
     * @var \DateTime
     */
    private $createdAt;

    /**
     *
     * @var \DateTime
     */
    private $updatedAt;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->demandes = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function __toString()
    {
        return sprintf('%s, %s', $this->getNom(), $this->getVille());
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nom
     *
     * @param string $nom
     * @return Etablissement
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set adresse
     *
     * @param string $adresse
     * @return Etablissement
     */
    public function setAdresse($adresse)
    {
        $this->adresse = $adresse;

        return $this;
    }

    /**
     * Get adresse
     *
     * @return string
     */
    public function getAdresse()
    {
        return $this->adresse;
    }

    /**
     * Set codePostal
     *
     * @param string $codePostal
     * @return Etablissement
     */
    public function setCodePostal($codePostal)
    {
        $this->codePostal = $codePostal;

        return $this;
    }

    /**
     * Get codePostal
     *
     * @return string
     */
    public function getCodePostal()
    {
        return $this->codePostal;
    }

    /**
     * Set ville
     *
     * @param string $ville
     * @return Etablissement
     */
    public function setVille($ville)
    {
        $this->ville = mb_convert_case($ville, MB_CASE_TITLE, "UTF-8");

        return $this;
    }

    /**
     * Get ville
     *
     * @return string
     */
    public function getVille()
    {
        return $this->ville;
    }

    /**
     * Set telephone
     *
     * @param string $telephone
     * @return Etablissement
     */
    public function setTelephone($telephone)
    {
        $this->telephone = MaintenanceFr::maintainPhoneNumber($telephone);

        return $this;
    }

    /**
     * Get telephone
     *
     * @return string
     */
    public function getTelephone()
    {
        return $this->telephone;
    }

    /**
     * Add demandes
     *
     * @param \Agfa\HpaBundle\Entity\Demande $demandes
     * @return Etablissement
     */
    public function addDemande(\Agfa\HpaBundle\Entity\Demande $demandes)
    {
        $this->demandes[] = $demandes;

        return $this;
    }

    /**
     * Remove demandes
     *
     * @param \Agfa\HpaBundle\Entity\Demande $demandes
     */
    public function removeDemande(\Agfa\HpaBundle\Entity\Demande $demandes)
    {
        $this->demandes->removeElement($demandes);
    }

    /**
     * Get demandes
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDemandes()
    {
        return $this->demandes;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Etablissement
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return Etablissement
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set bsrvrCodeLabo
     *
     * @param string $bsrvrCodeLabo
     *
     * @return Etablissement
     */
    public function setBsrvrCodeLabo($bsrvrCodeLabo)
    {
        $this->bsrvrCodeLabo = $bsrvrCodeLabo;

        return $this;
    }

    /**
     * Get bsrvrCodeLabo
     *
     * @return string
     */
    public function getBsrvrCodeLabo()
    {
        return $this->bsrvrCodeLabo;
    }

    /**
     * Set hxlCodeStructure
     *
     * @param string $hxlCodeStructure
     *
     * @return Etablissement
     */
    public function setHxlCodeStructure($hxlCodeStructure)
    {
        $this->hxlCodeStructure = $hxlCodeStructure;

        return $this;
    }

    /**
     * Get hxlCodeStructure
     *
     * @return string
     */
    public function getHxlCodeStructure()
    {
        return $this->hxlCodeStructure;
    }

    /**
     * Set libelle
     *
     * @param string $libelle
     *
     * @return Etablissement
     */
    public function setLibelle($libelle)
    {
        $this->libelle = $libelle;

        return $this;
    }

    /**
     * Get libelle
     *
     * @return string
     */
    public function getLibelle()
    {
        return $this->libelle;
    }

    /**
     * Add child
     *
     * @param \Agfa\HpaBundle\Entity\Etablissement $child
     *
     * @return Etablissement
     */
    public function addChild(\Agfa\HpaBundle\Entity\Etablissement $child)
    {
        $this->children[] = $child;

        return $this;
    }

    /**
     * Remove child
     *
     * @param \Agfa\HpaBundle\Entity\Etablissement $child
     */
    public function removeChild(\Agfa\HpaBundle\Entity\Etablissement $child)
    {
        $this->children->removeElement($child);
    }

    /**
     * Get children
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getChildren()
    {
        return $this->children;
    }

    /**
     * Set parent
     *
     * @param \Agfa\HpaBundle\Entity\Etablissement $parent
     *
     * @return Etablissement
     */
    public function setParent(\Agfa\HpaBundle\Entity\Etablissement $parent = null)
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * Get parent
     *
     * @return \Agfa\HpaBundle\Entity\Etablissement
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * Set bsrvrCodeEntiteJuridique
     *
     * @param string $bsrvrCodeEntiteJuridique
     *
     * @return Etablissement
     */
    public function setBsrvrCodeEntiteJuridique($bsrvrCodeEntiteJuridique)
    {
        $this->bsrvrCodeEntiteJuridique = $bsrvrCodeEntiteJuridique;

        return $this;
    }

    /**
     * Get bsrvrCodeEntiteJuridique
     *
     * @return string
     */
    public function getBsrvrCodeEntiteJuridique()
    {
        return $this->bsrvrCodeEntiteJuridique;
    }

    /**
     * Set paiement
     *
     * @param \Agfa\HpaBundle\Entity\Paiement $paiement
     *
     * @return Etablissement
     */
    public function setPaiement(\Agfa\HpaBundle\Entity\Paiement $paiement = null)
    {
        $this->paiement = $paiement;

        return $this;
    }

    /**
     * Get paiement
     *
     * @return \Agfa\HpaBundle\Entity\Paiement
     */
    public function getPaiement()
    {
        return $this->paiement;
    }

    /**
     * Set bsrvrRefEtablissement
     *
     * @param string $bsrvrRefEtablissement
     *
     * @return Etablissement
     */
    public function setBsrvrRefEtablissement($bsrvrRefEtablissement)
    {
        $this->bsrvrRefEtablissement = $bsrvrRefEtablissement;

        return $this;
    }

    /**
     * Get bsrvrRefEtablissement
     *
     * @return string
     */
    public function getBsrvrRefEtablissement()
    {
        return $this->bsrvrRefEtablissement;
    }
}
