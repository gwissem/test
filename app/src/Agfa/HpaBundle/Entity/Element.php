<?php

namespace Agfa\HpaBundle\Entity;

/**
 * Element
 */
class Element
{
    public function getClassName()
    {
        return (new \ReflectionClass($this))->getShortName();
    }

    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $code;

    /**
     * @var string
     */
    private $libelle;

    /**
     * @var string
     */
    private $codeLoinc;

    /**
     * @var string
     */
    private $libelleLoinc;

    /**
     * @var \Agfa\HpaBundle\Entity\Examen
     */
    private $examen;

    /**
     * @var \Agfa\HpaBundle\Entity\Resultat
     */
    private $resultats;

    public function __toString(){
        return $this->getLibelle();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set code
     *
     * @param string $code
     * @return Element
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set libelle
     *
     * @param string $libelle
     * @return Element
     */
    public function setLibelle($libelle)
    {
        $this->libelle = $libelle;

        return $this;
    }

    /**
     * Get libelle
     *
     * @return string
     */
    public function getLibelle()
    {
        return $this->libelle;
    }

    /**
     * Set codeLoinc
     *
     * @param string $codeLoinc
     * @return Element
     */
    public function setCodeLoinc($codeLoinc)
    {
        $this->codeLoinc = $codeLoinc;

        return $this;
    }

    /**
     * Get codeLoinc
     *
     * @return string
     */
    public function getCodeLoinc()
    {
        return $this->codeLoinc;
    }

    /**
     * Set libelleLoinc
     *
     * @param string $libelleLoinc
     * @return Element
     */
    public function setLibelleLoinc($libelleLoinc)
    {
        $this->libelleLoinc = $libelleLoinc;

        return $this;
    }

    /**
     * Get libelleLoinc
     *
     * @return string
     */
    public function getLibelleLoinc()
    {
        return $this->libelleLoinc;
    }

    /**
     * Set examen
     *
     * @param \Agfa\HpaBundle\Entity\Examen $examen
     * @return Element
     */
    public function setExamen(\Agfa\HpaBundle\Entity\Examen $examen = null)
    {
        $this->examen = $examen;

        return $this;
    }

    /**
     * Get examen
     *
     * @return \Agfa\HpaBundle\Entity\Examen
     */
    public function getExamen()
    {
        return $this->examen;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->resultats = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add resultat
     *
     * @param \Agfa\HpaBundle\Entity\Resultat $resultat
     *
     * @return Element
     */
    public function addResultat(\Agfa\HpaBundle\Entity\Resultat $resultat)
    {
        $this->resultats[] = $resultat;

        return $this;
    }

    /**
     * Remove resultat
     *
     * @param \Agfa\HpaBundle\Entity\Resultat $resultat
     */
    public function removeResultat(\Agfa\HpaBundle\Entity\Resultat $resultat)
    {
        $this->resultats->removeElement($resultat);
    }

    /**
     * Get resultats
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getResultats()
    {
        return $this->resultats;
    }
}
