<?php

namespace Agfa\HpaBundle\Entity;

/**
 * Resultat
 */
class Resultat
{

    /**
     * @var \Agfa\HpaBundle\Entity\Demande
     */
    private $demande;

    /**
     * @var \Agfa\HpaBundle\Entity\Element
     */
    private $element;

    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $valeurAlpha;

    /**
     * @var float
     */
    private $valeurNum;

    /**
     * @var float
     */
    private $normInf;

    /**
     * @var float
     */
    private $normSup;

    /**
     * @var string
     */
    private $unite;

    public function __toString(){
        return (string) $this->getValeurNum();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set valeurAlpha
     *
     * @param string $valeurAlpha
     * @return Resultat
     */
    public function setValeurAlpha($valeurAlpha)
    {
        $this->valeurAlpha = $valeurAlpha;

        return $this;
    }

    /**
     * Get valeurAlpha
     *
     * @return string
     */
    public function getValeurAlpha()
    {
        return $this->valeurAlpha;
    }

    /**
     * Set valeurNum
     *
     * @param float $valeurNum
     * @return Resultat
     */
    public function setValeurNum($valeurNum)
    {
        $this->valeurNum = $valeurNum;

        return $this;
    }

    /**
     * Get valeurNum
     *
     * @return float
     */
    public function getValeurNum()
    {
        return $this->valeurNum;
    }

    /**
     * Set normInf
     *
     * @param float $normInf
     * @return Resultat
     */
    public function setNormInf($normInf)
    {
        $this->normInf = $normInf;

        return $this;
    }

    /**
     * Get normInf
     *
     * @return float
     */
    public function getNormInf()
    {
        return $this->normInf;
    }

    /**
     * Set normSup
     *
     * @param float $normSup
     * @return Resultat
     */
    public function setNormSup($normSup)
    {
        $this->normSup = $normSup;

        return $this;
    }

    /**
     * Get normSup
     *
     * @return float
     */
    public function getNormSup()
    {
        return $this->normSup;
    }

    /**
     * Set unite
     *
     * @param string $unite
     * @return Resultat
     */
    public function setUnite($unite)
    {
        $this->unite = $unite;

        return $this;
    }

    /**
     * Get unite
     *
     * @return string
     */
    public function getUnite()
    {
        return $this->unite;
    }

    /**
     * Set element
     *
     * @param \Agfa\HpaBundle\Entity\Element $element
     * @return Resultat
     */
    public function setElement(\Agfa\HpaBundle\Entity\Element $element = null)
    {
        $this->element = $element;

        return $this;
    }

    /**
     * Get element
     *
     * @return \Agfa\HpaBundle\Entity\Element
     */
    public function getElement()
    {
        return $this->element;
    }

    /**
     * Set demande
     *
     * @param \Agfa\HpaBundle\Entity\Demande $demande
     *
     * @return Resultat
     */
    public function setDemande(\Agfa\HpaBundle\Entity\Demande $demande = null)
    {
        $this->demande = $demande;

        return $this;
    }

    /**
     * Get demande
     *
     * @return \Agfa\HpaBundle\Entity\Demande
     */
    public function getDemande()
    {
        return $this->demande;
    }
}
