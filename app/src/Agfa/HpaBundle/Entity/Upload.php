<?php

namespace Agfa\HpaBundle\Entity;

/**
 * Upload
 */
class Upload
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $filename;

    /**
     * @var string
     */
    private $description;

    /**
     * @var string
     */
    private $mimetype;

    /**
     * @var \DateTime
     */
    private $createdAt;

    /**
     * @var integer
     */
    private $filesize;

    /**
     * @var string
     */
    private $content;

    /**
     * @var boolean
     */
    private $isAttachment;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set filename
     *
     * @param string $filename
     * @return Upload
     */
    public function setFilename($filename)
    {
        $this->filename = $filename;

        return $this;
    }

    /**
     * Get filename
     *
     * @return string
     */
    public function getFilename()
    {
        return $this->filename;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Upload
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set mimetype
     *
     * @param string $mimetype
     * @return Upload
     */
    public function setMimetype($mimetype)
    {
        $this->mimetype = $mimetype;

        return $this;
    }

    /**
     * Get mimetype
     *
     * @return string
     */
    public function getMimetype()
    {
        return $this->mimetype;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Upload
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set filesize
     *
     * @param integer $filesize
     * @return Upload
     */
    public function setFilesize($filesize)
    {
        $this->filesize = $filesize;

        return $this;
    }

    /**
     * Get filesize
     *
     * @return integer
     */
    public function getFilesize()
    {
        return $this->filesize;
    }

    /**
     * Set content
     *
     * @param string $content
     * @return Upload
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set isAttachment
     *
     * @param boolean $isAttachment
     * @return Upload
     */
    public function setIsAttachment($isAttachment)
    {
        $this->isAttachment = $isAttachment;

        return $this;
    }

    /**
     * Get isAttachment
     *
     * @return boolean
     */
    public function getIsAttachment()
    {
        return $this->isAttachment;
    }

    /**
     * @var \Agfa\HpaBundle\Entity\Demande
     */
    private $demande;


    /**
     * Set demande
     *
     * @param \Agfa\HpaBundle\Entity\Demande $demande
     * @return Upload
     */
    public function setDemande(\Agfa\HpaBundle\Entity\Demande $demande = null)
    {
        $this->demande = $demande;

        return $this;
    }

    /**
     * Get demande
     *
     * @return \Agfa\HpaBundle\Entity\Demande
     */
    public function getDemande()
    {
        return $this->demande;
    }
    /**
     * @var \Agfa\HpaBundle\Entity\Personne
     */
    private $personne;


    /**
     * Set personne
     *
     * @param \Agfa\HpaBundle\Entity\Personne $personne
     * @return Upload
     */
    public function setPersonne(\Agfa\HpaBundle\Entity\Personne $personne = null)
    {
        $this->personne = $personne;

        return $this;
    }

    /**
     * Get personne
     *
     * @return \Agfa\HpaBundle\Entity\Personne
     */
    public function getPersonne()
    {
        return $this->personne;
    }
}
