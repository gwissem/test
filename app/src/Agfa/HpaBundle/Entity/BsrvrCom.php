<?php
namespace Agfa\HpaBundle\Entity;

/**
 * Contains all actions waiting to be updated to BioServeur.
 */
class BsrvrCom
{

    /**
     * Demande was read. Waiting to be updated to BioServeur.
     * @var string
     */
    const ACTION_UPDATE_READING = 'update_read';

    /**
     * Demande was paid. Waiting to be updated to BioServeur.
     * @var string
     */
    const ACTION_UPDATE_PAYMENT = 'update_payment';

    /**
     * Patient or whole account was deleted. Enrollment with HPA has to be canceled on BioServeur side.
     * @var string
     */
    const ACTION_UPDATE_UNENROLL = 'update_unenroll';

    const MAX_NUMBER_ATTEMPS = 3;

    /**
     *
     * @var \Agfa\HpaBundle\Entity\Demande
     */
    private $demande;

    /**
     *
     * @var integer
     */
    private $id;

    /**
     *
     * @var string
     */
    private $action;

    /**
     *
     * @var string
     */
    private $data;

    /**
     *
     * @var integer
     */
    private $attempt = 0;

    /**
     *
     * @var string
     */
    private $responseStatus;

    /**
     *
     * @var \DateTime
     */
    private $createdAt;

    /**
     *
     * @var \DateTime
     */
    private $updatedAt;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set action
     *
     * @param string $action
     *
     * @return BsrvrCom
     */
    public function setAction($action)
    {
        $this->action = $action;

        return $this;
    }

    /**
     * Get action
     *
     * @return string
     */
    public function getAction()
    {
        return $this->action;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return BsrvrCom
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set demande
     *
     * @param \Agfa\HpaBundle\Entity\Demande $demande
     *
     * @return BsrvrCom
     */
    public function setDemande(\Agfa\HpaBundle\Entity\Demande $demande = null)
    {
        $this->demande = $demande;

        return $this;
    }

    /**
     * Get demande
     *
     * @return \Agfa\HpaBundle\Entity\Demande
     */
    public function getDemande()
    {
        return $this->demande;
    }

    /**
     * Set attempt
     *
     * @param integer $attempt
     *
     * @return BsrvrCom
     */
    public function setAttempt($attempt)
    {
        $this->attempt = $attempt;

        return $this;
    }

    /**
     * Get attempt
     *
     * @return integer
     */
    public function getAttempt()
    {
        return $this->attempt;
    }

    /**
     * Set responseStatus
     *
     * @param string $responseStatus
     *
     * @return BsrvrCom
     */
    public function setResponseStatus($responseStatus)
    {
        $this->responseStatus = $responseStatus;

        return $this;
    }

    /**
     * Get responseStatus
     *
     * @return string
     */
    public function getResponseStatus()
    {
        return $this->responseStatus;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return BsrvrCom
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set data
     *
     * @param array $data
     *
     * @return BsrvrCom
     */
    public function setData($data)
    {
        $this->data = $data;

        return $this;
    }

    /**
     * Get data
     *
     * @return array
     */
    public function getData()
    {
        return $this->data;
    }
}
