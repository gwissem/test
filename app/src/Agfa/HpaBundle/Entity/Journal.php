<?php
namespace Agfa\HpaBundle\Entity;

/**
 * Journal
 */
class Journal
{

    const EVENT_USER_LOGIN = 0;

    const EVENT_USER_LOGOUT = 1;

    const EVENT_USER_LOCKED_ACCOUNT= 2;

    const EVENT_ENROLEMENT_SUCCESS = 10;

    const EVENT_ENROLEMENT_FAILURE = 11;

    const EVENT_ENROLEMENT_BAD_IDENTIFIERS = 12;

    const EVENT_ENROLEMENT_BAD_PORTAL = 13;

    const EVENT_CR_CREATION = 20;

    const EVENT_CR_DOWNLOAD = 21;

    const EVENT_CR_SUPPRESSION = 22;

    const EVENT_CR_REMPLACEMENT = 23;

    const EVENT_CR_REMPLACEMENT_PARTIEL = 24;

    const EVENT_PATIENT_CREATION = 30;

    const EVENT_PATIENT_EDITION = 31;

    const EVENT_PATIENT_SUPPRESSION = 32;

    const EVENT_PERSONNE_CREATION = 40;

    const EVENT_PERSONNE_MODIFICATION = 41;

    const EVENT_PERSONNE_SUPPRESSION = 42;

    const EVENT_UPLOAD_CREATION = 50;

    const EVENT_UPLOAD_MODIFICATION = 51;

    const EVENT_UPLOAD_SUPPRESSION = 52;

    const EVENT_BSRVRCOM_CR = 60;

    // When a request of paiement is made
    const EVENT_PAYMENT_REQUEST = 70;

    // When a response of paiement is received
    const EVENT_PAYMENT_RESPONSE = 71;

    // Payment data updated by Bioserveur or an admin
    const EVENT_PAYMENT_UPDATED = 72;

    const EVENT_LEVEL_SUCCESS = 'SUCCESS';

    const EVENT_LEVEL_INFO = 'INFO';

    const EVENT_LEVEL_WARNING = 'WARNING';

    const EVENT_LEVEL_DANGER = 'DANGER';

    /**
     *
     * @var integer
     */
    private $id;

    /**
     *
     * @var integer
     */
    private $type;

    /**
     *
     * @var string
     */
    private $level;

    /**
     *
     * @var integer
     */
    private $entityId;

    /**
     * @Assert\Ip
     *
     * @var integer
     *
     */
    private $ipAddress;

    /**
     *
     * @var \DateTime
     */
    private $createdAt;

    /**
     *
     * @var string
     */
    private $shortDescription;

    /**
     *
     * @var string
     */
    private $longDescription;

    /**
     *
     * @var \Agfa\UserBundle\Entity\User
     */
    private $user;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set type
     *
     * @param integer $type
     * @return Journal
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return integer
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set entityId
     *
     * @param integer $entityId
     * @return Journal
     */
    public function setEntityId($entityId)
    {
        $this->entityId = $entityId;

        return $this;
    }

    /**
     * Get entityId
     *
     * @return integer
     */
    public function getEntityId()
    {
        return $this->entityId;
    }

    /**
     * Set user
     *
     * @param \Agfa\UserBundle\Entity\User $user
     * @return Journal
     */
    public function setUser(\Agfa\UserBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \Agfa\UserBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set ipAddress
     *
     * @param integer $ipAddress
     *
     * @return Journal
     */
    public function setIpAddress($ipAddress)
    {
        $this->ipAddress = $ipAddress;

        return $this;
    }

    /**
     * Get ipAddress
     *
     * @return integer
     */
    public function getIpAddress()
    {
        return $this->ipAddress;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Journal
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set level
     *
     * @param string $level
     *
     * @return Journal
     */
    public function setLevel($level)
    {
        $this->level = $level;

        return $this;
    }

    /**
     * Get level
     *
     * @return string
     */
    public function getLevel()
    {
        return $this->level;
    }

    /**
     * Set shortDescription
     *
     * @param string $shortDescription
     *
     * @return Journal
     */
    public function setShortDescription($shortDescription)
    {
        $this->shortDescription = $shortDescription;

        return $this;
    }

    /**
     * Get shortDescription
     *
     * @return string
     */
    public function getShortDescription()
    {
        return $this->shortDescription;
    }

    /**
     * Set longDescription
     *
     * @param array $longDescription
     *
     * @return Journal
     */
    public function setLongDescription($longDescription)
    {
        $this->longDescription = $longDescription;

        return $this;
    }

    /**
     * Get longDescription
     *
     * @return array
     */
    public function getLongDescription()
    {
        return $this->longDescription;
    }
}
