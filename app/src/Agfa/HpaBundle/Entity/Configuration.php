<?php
namespace Agfa\HpaBundle\Entity;

/**
 * Stores all configurations for the app (website).
 */
class Configuration
{

    /**
     * Weither the app's logo has to be aligned on right or in the middle of the page.
     * @var string
     */
    const DEFAULT_LOGO_ALIGNMENT = "center";

    /**
     * Length of the password used for enrollment.
     * @var integer
     */
    const DEFAULT_LONGUEUR_MDP_ENROLEMENT = 8;

    /**
     *
     * @var integer
     */
    private $id;

    /**
     *
     * @var string
     */
    private $logoAlignement = self::DEFAULT_LOGO_ALIGNMENT;

    /**
     *
     * @var integer
     */
    private $longueurMdpEnrolement = self::DEFAULT_LONGUEUR_MDP_ENROLEMENT;

    /**
     *
     * @var string
     */
    private $themeName;

    /**
     *
     * @var string
     */
    private $logoutRedirectionUrl;

    /**
     *
     * @var boolean
     */
    private $patientRecordDirectAccess = TRUE;


    /**
     *
     * @var boolean
     */
    private $lastPatientsRecordOnlyMssg = FALSE;

    /**
     *
     * @var string
     */
    private $videoTutorialRegister;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set themeName
     *
     * @param string $themeName
     * @return Configuration
     */
    public function setThemeName($themeName)
    {
        $this->themeName = $themeName;

        return $this;
    }

    /**
     * Get themeName
     *
     * @return string
     */
    public function getThemeName()
    {
        return $this->themeName;
    }

    /**
     * Set logoAlignement
     *
     * @param string $logoAlignement
     * @return Configuration
     */
    public function setLogoAlignement($logoAlignement)
    {
        $this->logoAlignement = $logoAlignement;

        return $this;
    }

    /**
     * Get logoAlignement
     *
     * @return string
     */
    public function getLogoAlignement()
    {
        return $this->logoAlignement;
    }

    /**
     * Set logoutRedirectionUrl
     *
     * @param string $logoutRedirectionUrl
     *
     * @return Configuration
     */
    public function setLogoutRedirectionUrl($logoutRedirectionUrl)
    {
        $this->logoutRedirectionUrl = $logoutRedirectionUrl;

        return $this;
    }

    /**
     * Get logoutRedirectionUrl
     *
     * @return string
     */
    public function getLogoutRedirectionUrl()
    {
        return $this->logoutRedirectionUrl;
    }

    /**
     * Set patientRecordDirectAccess
     *
     * @param boolean $patientRecordDirectAccess
     *
     * @return Configuration
     */
    public function setPatientRecordDirectAccess($patientRecordDirectAccess)
    {
        $this->patientRecordDirectAccess = $patientRecordDirectAccess;

        return $this;
    }

    /**
     * Get patientRecordDirectAccess
     *
     * @return boolean
     */
    public function getPatientRecordDirectAccess()
    {
        return $this->patientRecordDirectAccess;
    }

    /**
     * Set lastPatientsRecordOnlyMssg
     *
     * @param boolean $lastPatientsRecordOnlyMssg
     *
     * @return Configuration
     */
    public function setLastPatientsRecordOnlyMssg($lastPatientsRecordOnlyMssg)
    {
        $this->lastPatientsRecordOnlyMssg = $lastPatientsRecordOnlyMssg;

        return $this;
    }

    /**
     * Get lastPatientsRecordOnlyMssg
     *
     * @return boolean
     */
    public function getLastPatientsRecordOnlyMssg()
    {
        return $this->lastPatientsRecordOnlyMssg;
    }

    /**
     * Set videoTutorialRegister
     *
     * @param string $videoTutorialRegister
     *
     * @return Configuration
     */
    public function setVideoTutorialRegister($videoTutorialRegister)
    {
        $this->videoTutorialRegister = $videoTutorialRegister;

        return $this;
    }

    /**
     * Get videoTutorialRegister
     *
     * @return string
     */
    public function getVideoTutorialRegister()
    {
        return $this->videoTutorialRegister;
    }
}
