<?php

namespace Agfa\HpaBundle\Entity;

/**
 * Examen
 */
class Examen
{
    public function getClassName()
    {
        return (new \ReflectionClass($this))->getShortName();
    }

    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $code;

    /**
     * @var string
     */
    private $libelle;

    /**
     * @var string
     */
    private $codeLoinc;

    /**
     * @var string
     */
    private $libelleLoinc;

    /**
     * @var string
     */
    private $elements;

    /**
     * @var \Agfa\HpaBundle\Entity\Chapitre
     */
    private $chapitre;

    public function __toString(){
        return $this->getLibelle();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set code
     *
     * @param string $code
     * @return Examen
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set libelle
     *
     * @param string $libelle
     * @return Examen
     */
    public function setLibelle($libelle)
    {
        $this->libelle = $libelle;

        return $this;
    }

    /**
     * Get libelle
     *
     * @return string
     */
    public function getLibelle()
    {
        return $this->libelle;
    }

    /**
     * Set codeLoinc
     *
     * @param string $codeLoinc
     * @return Examen
     */
    public function setCodeLoinc($codeLoinc)
    {
        $this->codeLoinc = $codeLoinc;

        return $this;
    }

    /**
     * Get codeLoinc
     *
     * @return string
     */
    public function getCodeLoinc()
    {
        return $this->codeLoinc;
    }

    /**
     * Set libelleLoinc
     *
     * @param string $libelleLoinc
     * @return Examen
     */
    public function setLibelleLoinc($libelleLoinc)
    {
        $this->libelleLoinc = $libelleLoinc;

        return $this;
    }

    /**
     * Get libelleLoinc
     *
     * @return string
     */
    public function getLibelleLoinc()
    {
        return $this->libelleLoinc;
    }

    /**
     * Set chapitre
     *
     * @param \Agfa\HpaBundle\Entity\Chapitre $chapitre
     * @return Examen
     */
    public function setChapitre(\Agfa\HpaBundle\Entity\Chapitre $chapitre = null)
    {
        $this->chapitre = $chapitre;

        return $this;
    }

    /**
     * Get chapitre
     *
     * @return \Agfa\HpaBundle\Entity\Chapitre
     */
    public function getChapitre()
    {
        return $this->chapitre;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->elements = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add element
     *
     * @param \Agfa\HpaBundle\Entity\Element $element
     *
     * @return Examen
     */
    public function addElement(\Agfa\HpaBundle\Entity\Element $element)
    {
        $this->elements[] = $element;

        return $this;
    }

    /**
     * Remove element
     *
     * @param \Agfa\HpaBundle\Entity\Element $element
     */
    public function removeElement(\Agfa\HpaBundle\Entity\Element $element)
    {
        $this->elements->removeElement($element);
    }

    /**
     * Get elements
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getElements()
    {
        return $this->elements;
    }
}
