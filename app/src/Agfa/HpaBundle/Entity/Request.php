<?php
namespace Agfa\HpaBundle\Entity;

use Agfa\MoneticoPaiementBundle\Entity\Request as BaseRequest;

/**
 * Request
 */
class Request extends BaseRequest
{

    /**
     *
     * @var \Agfa\HpaBundle\Entity\Demande
     */
    private $demandes;

    /**
     * Constructor
     */
    public function __construct()
    {
        parent::__construct();
        $this->demandes = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add demande
     *
     * @param \Agfa\HpaBundle\Entity\Demande $demande
     *
     * @return Request
     */
    public function addDemande(\Agfa\HpaBundle\Entity\Demande $demande)
    {
        $this->demandes[] = $demande;

        return $this;
    }

    /**
     * Remove demande
     *
     * @param \Agfa\HpaBundle\Entity\Demande $demande
     */
    public function removeDemande(\Agfa\HpaBundle\Entity\Demande $demande)
    {
        $this->demandes->removeElement($demande);
    }

    /**
     * Get demandes
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDemandes()
    {
        return $this->demandes;
    }
}
