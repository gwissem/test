<?php
namespace Agfa\HpaBundle\Entity;

use Symfony\Component\Validator\Constraints as assert;

/**
 * Selection d'une liste de demandes
 */
class RechercheDemande
{

    const RECHERCHE_DATE_PRELEVEMENT_DEBUT = 'LAST MONTH';

    const RECHERCHE_DATE_PRELEVEMENT_FIN = 'NOW';

    /**
     *
     * @var string
     */
    private $prescripteur;

    /**
     *
     * @var string
     */
    private $examen;

    /**
     *
     * @var string
     */
    private $element;

    /**
     *
     * @var string
     */
    private $laboratoire;

    /**
     *
     * @var string
     * @Assert\NotBlank
     */
    private $datePrelevementDebut;

    /**
     *
     * @var string
     * @Assert\NotBlank
     */
    private $datePrelevementFin;

    public function __construct()
    {
        // WARNING These settings are also set in the repository DemandeRepository.
        $datePrelevementDebut = new \DateTime(self::RECHERCHE_DATE_PRELEVEMENT_DEBUT);
        $datePrelevementFin = new \DateTime(self::RECHERCHE_DATE_PRELEVEMENT_FIN);

        $this->setDatePrelevementDebut($datePrelevementDebut);
        $this->setDatePrelevementFin($datePrelevementFin);
    }

    public function getPrescripteur()
    {
        return $this->prescripteur;
    }

    public function setPrescripteur($prescripteur)
    {
        $this->prescripteur = $prescripteur;
        return $this;
    }

    public function getExamen()
    {
        return $this->examen;
    }

    public function setExamen($examen)
    {
        $this->examen = $examen;
        return $this;
    }

    public function getElement()
    {
        return $this->element;
    }

    public function setElement($element)
    {
        $this->element = $element;
        return $this;
    }

    public function getLaboratoire()
    {
        return $this->laboratoire;
    }

    public function setLaboratoire($laboratoire)
    {
        $this->laboratoire = $laboratoire;
        return $this;
    }

    public function getDatePrelevementDebut()
    {
        return $this->datePrelevementDebut;
    }

    public function setDatePrelevementDebut($datePrelevementDebut)
    {
        $this->datePrelevementDebut = $datePrelevementDebut;
        return $this;
    }

    public function getDatePrelevementFin()
    {
        return $this->datePrelevementFin;
    }

    public function setDatePrelevementFin($datePrelevementFin)
    {
        $this->datePrelevementFin = $datePrelevementFin;
        return $this;
    }
}