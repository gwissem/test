<?php

namespace Agfa\HpaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Setting
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Agfa\HpaBundle\Repository\SettingRepository")
 */
class Setting
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $themeName;

    /**
     * @var string
     */
    private $siteLogoAlign;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set themeName
     *
     * @param string $themeName
     * @return Setting
     */
    public function setThemeName($themeName)
    {
        $this->themeName = $themeName;

        return $this;
    }

    /**
     * Get themeName
     *
     * @return string 
     */
    public function getThemeName()
    {
        return $this->themeName;
    }

    /**
     * Set siteLogoAlign
     *
     * @param string $siteLogoAlign
     * @return Setting
     */
    public function setSiteLogoAlign($siteLogoAlign)
    {
        $this->siteLogoAlign = $siteLogoAlign;

        return $this;
    }

    /**
     * Get siteLogoAlign
     *
     * @return string 
     */
    public function getSiteLogoAlign()
    {
        return $this->siteLogoAlign;
    }
}
