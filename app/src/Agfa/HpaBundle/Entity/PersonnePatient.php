<?php

namespace Agfa\HpaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * PersonnePatient
 *
 * @ORM\Table(name="hpa_personne_patient")
 * @ORM\Entity
 */
class PersonnePatient
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \Agfa\HpaBundle\Entity\Personne
     */
    private $personne;

    /**
     * @var \Agfa\HpaBundle\Entity\Patient
     */
    private $patient;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime")
     */
    private $updatedAt;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return PersonnePatient
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return PersonnePatient
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set personne
     *
     * @param \Agfa\HpaBundle\Entity\Personne $personne
     *
     * @return PersonnePatient
     */
    public function setPersonne(\Agfa\HpaBundle\Entity\Personne $personne = null)
    {
        $this->personne = $personne;

        return $this;
    }

    /**
     * Get personne
     *
     * @return \Agfa\HpaBundle\Entity\Personne
     */
    public function getPersonne()
    {
        return $this->personne;
    }

    /**
     * Set patient
     *
     * @param \Agfa\HpaBundle\Entity\Patient $patient
     *
     * @return PersonnePatient
     */
    public function setPatient(\Agfa\HpaBundle\Entity\Patient $patient = null)
    {
        $this->patient = $patient;

        return $this;
    }

    /**
     * Get patient
     *
     * @return \Agfa\HpaBundle\Entity\Patient
     */
    public function getPatient()
    {
        return $this->patient;
    }
}
