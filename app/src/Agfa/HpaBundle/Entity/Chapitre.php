<?php

namespace Agfa\HpaBundle\Entity;

/**
 * Chapitre
 */
class Chapitre
{
    public function getClassName()
    {
        return (new \ReflectionClass($this))->getShortName();
    }

    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $code;

    /**
     * @var string
     */
    private $libelle;

    /**
     * @var string
     */
    private $codeLoinc;

    /**
     * @var string
     */
    private $libelleLoinc;

    /**
     * @var \Agfa\HpaBundle\Entity\Etablissement
     */
    private $etablissement;

    /**
     * @var \Agfa\HpaBundle\Entity\Examen
     */
    private $examens;

    public function __toString(){
        return $this->getLibelle();
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->examens = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set code
     *
     * @param string $code
     * @return Chapitre
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set libelle
     *
     * @param string $libelle
     * @return Chapitre
     */
    public function setLibelle($libelle)
    {
        $this->libelle = $libelle;

        return $this;
    }

    /**
     * Get libelle
     *
     * @return string
     */
    public function getLibelle()
    {
        return $this->libelle;
    }

    /**
     * Set codeLoinc
     *
     * @param string $codeLoinc
     * @return Chapitre
     */
    public function setCodeLoinc($codeLoinc)
    {
        $this->codeLoinc = $codeLoinc;

        return $this;
    }

    /**
     * Get codeLoinc
     *
     * @return string
     */
    public function getCodeLoinc()
    {
        return $this->codeLoinc;
    }

    /**
     * Set libelleLoinc
     *
     * @param string $libelleLoinc
     * @return Chapitre
     */
    public function setLibelleLoinc($libelleLoinc)
    {
        $this->libelleLoinc = $libelleLoinc;

        return $this;
    }

    /**
     * Get libelleLoinc
     *
     * @return string
     */
    public function getLibelleLoinc()
    {
        return $this->libelleLoinc;
    }

    /**
     * Add examen
     *
     * @param \Agfa\HpaBundle\Entity\Examen $examen
     *
     * @return Chapitre
     */
    public function addExamen(\Agfa\HpaBundle\Entity\Examen $examen)
    {
        $this->examens[] = $examen;

        return $this;
    }

    /**
     * Remove examen
     *
     * @param \Agfa\HpaBundle\Entity\Examen $examen
     */
    public function removeExamen(\Agfa\HpaBundle\Entity\Examen $examen)
    {
        $this->examens->removeElement($examen);
    }

    /**
     * Get examens
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getExamens()
    {
        return $this->examens;
    }

    /**
     * Set etablissement
     *
     * @param \Agfa\HpaBundle\Entity\Etablissement $etablissement
     *
     * @return Chapitre
     */
    public function setEtablissement(\Agfa\HpaBundle\Entity\Etablissement $etablissement = null)
    {
        $this->etablissement = $etablissement;

        return $this;
    }

    /**
     * Get etablissement
     *
     * @return \Agfa\HpaBundle\Entity\Etablissement
     */
    public function getEtablissement()
    {
        return $this->etablissement;
    }
}
