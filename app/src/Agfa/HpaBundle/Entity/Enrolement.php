<?php

namespace Agfa\HpaBundle\Entity;


/**
 * Enrolement
 */
class Enrolement
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $login;

    /**
     * @var string
     */
    private $password;

    /**
     * @var \DateTime
     */
    private $date_envoi;

    /**
     * @var \DateTime
     */
    private $date_reception;

    /**
     * @var string
     */
    private $statut;

    /**
     * @var \Agfa\UserBundle\Entity\User
     */
    private $user;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
    	return $this->id;
    }

    /**
     * Set login
     *
     * @param string $login
     * @return Enrolement
     */
    public function setLogin($login)
    {
    	$this->login = $login;

    	return $this;
    }

    /**
     * Get login
     *
     * @return string
     */
    public function getLogin()
    {
    	return $this->login;
    }

    /**
     * Set password
     *
     * @param string $password
     * @return Enrolement
     */
    public function setPassword($password)
    {
    	$this->password = $password;

    	return $this;
    }

    /**
     * Get password
     *
     * @return string
     */
    public function getPassword()
    {
    	return $this->password;
    }

    /**
     * Set date_envoi
     *
     * @param \DateTime $dateEnvoi
     * @return Enrolement
     */
    public function setDateEnvoi($dateEnvoi)
    {
    	$this->date_envoi = $dateEnvoi;

    	return $this;
    }

    /**
     * Get date_envoi
     *
     * @return \DateTime
     */
    public function getDateEnvoi()
    {
    	return $this->date_envoi;
    }

    /**
     * Set date_reception
     *
     * @param \DateTime $dateReception
     * @return Enrolement
     */
    public function setDateReception($dateReception)
    {
    	$this->date_reception = $dateReception;

    	return $this;
    }

    /**
     * Get date_reception
     *
     * @return \DateTime
     */
    public function getDateReception()
    {
    	return $this->date_reception;
    }

    /**
     * Set statut
     *
     * @param string $statut
     * @return Enrolement
     */
    public function setStatut($statut)
    {
    	$this->statut = $statut;

    	return $this;
    }

    /**
     * Get statut
     *
     * @return string
     */
    public function getStatut()
    {
    	return $this->statut;
    }

    /**
     * Set user
     *
     * @param \Agfa\UserBundle\Entity\User $user
     * @return Enrolement
     */
    public function setUser(\Agfa\UserBundle\Entity\User $user = null)
    {
    	$this->user = $user;

    	return $this;
    }

    /**
     * Get user
     *
     * @return \Agfa\UserBundle\Entity\User
     */
    public function getUser()
    {
    	return $this->user;
    }

}
