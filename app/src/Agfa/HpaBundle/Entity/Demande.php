<?php

namespace Agfa\HpaBundle\Entity;

/**
 * Demande
 */
class Demande
{
    const PAID_NEW = 'not_paid';
    const PAID_ONLINE = 'online';
    const PAID_LABORATORY = 'labo';
    const PAID_ADMIN = 'admin';
    const PAID_UNKNOWN = 'unknown';

    /**
     * @var integer
     */
    private $id;

    /**
     * @var \DateTime
     */
    private $dateAccueil;

    /**
     * @var \DateTime
     */
    private $datePrelevement;

    /**
     * @var string
     */
    private $bsrvrConstantLogin;

    /**
     * @var string
     */
    private $nomPatient;

    /**
     * @var string
     */
    private $prenomPatient;

    /**
     * @var \DateTime
     */
    private $dateNaissancePatient;

    /**
     * @var string
     */
    private $sexePatient;

    /**
     * @var string
     */
    private $inscPatient;

    /**
     * @var string
     */
    private $code;

    /**
     * @var string
     */
    private $hash;

    /**
     * @var string
     */
    private $editcr;

    /**
     * @var string
     */
    private $compteRendu;

    /**
     * @var boolean
     */
    private $isNew = TRUE;

    /**
     * @var boolean
     */
    private $isUpdated = FALSE;

    /**
     * @var string
     */
    private $nomPrescripteur;

    /**
     * @var string
     */
    private $nodem;

    /**
     * @var string
     */
    private $nodemx;

    /**
     * @var \Agfa\HpaBundle\Entity\Resultat
     */
    private $resultats;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $bsrvrComs;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $uploads;

    /**
     * @var \Agfa\HpaBundle\Entity\Patient
     */
    private $patient;

    /**
     * @var float
     */
    private $paieSolde = 0;

    /**
     * @var string
     */
    private $paieSource = self::PAID_NEW;

    /**
     *
     * @var string
     */
    private $affichageCr = FALSE;

    /**
     * @var \DateTime
     */
    private $createdAt;

    /**
     * @var \DateTime
     */
    private $updatedAt;

    /**
     * @var \Agfa\HpaBundle\Entity\Etablissement
     */
    private $etablissement;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $requests;

    public function getNomComplet()
    {
        $fullName = mb_convert_case($this->getPrenomPatient(), MB_CASE_TITLE, "UTF-8") . ' ' . mb_strtoupper($this->getNomPatient(), "UTF-8");

        return $fullName;
    }

    /**
     * Get examens
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getExamens()
    {
        $examens = [];
        foreach($this->resultats as $resultat) {
            $examen = $resultat->getElement()->getExamen();
            $examens[$examen->getId()] = $examen;
        }
        return array_values($examens);
    }

    public function  __toString()
    {
        return $this->getNomComplet();
    }

    /**
     * Constructor
     */
    public function __construct()
    {
    	$this->bsrvrComs = new \Doctrine\Common\Collections\ArrayCollection();
    	$this->uploads = new \Doctrine\Common\Collections\ArrayCollection();
    	$this->resultats = new \Doctrine\Common\Collections\ArrayCollection();
    	$this->requests = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
    	return $this->id;
    }

    /**
     * Set dateAccueil
     *
     * @param \DateTime $dateAccueil
     * @return Demande
     */
    public function setDateAccueil($dateAccueil)
    {
    	$this->dateAccueil = $dateAccueil;

    	return $this;
    }

    /**
     * Get dateAccueil
     *
     * @return \DateTime
     */
    public function getDateAccueil()
    {
    	return $this->dateAccueil;
    }

    /**
     * Set datePrelevement
     *
     * @param \DateTime $datePrelevement
     * @return Demande
     */
    public function setDatePrelevement($datePrelevement)
    {
    	$this->datePrelevement = $datePrelevement;

    	return $this;
    }

    /**
     * Get datePrelevement
     *
     * @return \DateTime
     */
    public function getDatePrelevement()
    {
    	return $this->datePrelevement;
    }

    /**
     * Set bsrvrConstantLogin
     *
     * @param string $bsrvrConstantLogin
     * @return Demande
     */
    public function setBsrvrConstantLogin($bsrvrConstantLogin)
    {
    	$this->bsrvrConstantLogin = $bsrvrConstantLogin;

    	return $this;
    }

    /**
     * Get bsrvrConstantLogin
     *
     * @return string
     */
    public function getBsrvrConstantLogin()
    {
    	return $this->bsrvrConstantLogin;
    }

    /**
     * Set nomPatient
     *
     * @param string $nomPatient
     * @return Demande
     */
    public function setNomPatient($nomPatient)
    {
    	$this->nomPatient = mb_strtoupper($nomPatient, "UTF-8");

    	return $this;
    }

    /**
     * Get nomPatient
     *
     * @return string
     */
    public function getNomPatient()
    {
    	return $this->nomPatient;
    }

    /**
     * Set prenomPatient
     *
     * @param string $prenomPatient
     * @return Demande
     */
    public function setPrenomPatient($prenomPatient)
    {
    	$this->prenomPatient = mb_convert_case($prenomPatient, MB_CASE_TITLE, "UTF-8");

    	return $this;
    }

    /**
     * Get prenomPatient
     *
     * @return string
     */
    public function getPrenomPatient()
    {
    	return $this->prenomPatient;
    }

    /**
     * Set dateNaissancePatient
     *
     * @param \DateTime $dateNaissancePatient
     * @return Demande
     */
    public function setDateNaissancePatient($dateNaissancePatient)
    {
    	$this->dateNaissancePatient = $dateNaissancePatient;

    	return $this;
    }

    /**
     * Get dateNaissancePatient
     *
     * @return \DateTime
     */
    public function getDateNaissancePatient()
    {
    	return $this->dateNaissancePatient;
    }

    /**
     * Set sexePatient
     *
     * @param string $sexePatient
     * @return Demande
     */
    public function setSexePatient($sexePatient)
    {
        $this->sexePatient = mb_convert_case($sexePatient, MB_CASE_LOWER, "UTF-8");

    	return $this;
    }

    /**
     * Get sexePatient
     *
     * @return string
     */
    public function getSexePatient()
    {
    	return $this->sexePatient;
    }

    /**
     * Set inscPatient
     *
     * @param string $inscPatient
     * @return Demande
     */
    public function setInscPatient($inscPatient)
    {
    	$this->inscPatient = $inscPatient;

    	return $this;
    }

    /**
     * Get inscPatient
     *
     * @return string
     */
    public function getInscPatient()
    {
    	return $this->inscPatient;
    }

    /**
     * Set code
     *
     * @param string $code
     * @return Demande
     */
    public function setCode($code)
    {
    	$this->code = $code;

    	return $this;
    }

    /**
     * Get code
     *
     * @return string
     */
    public function getCode()
    {
    	return $this->code;
    }

    /**
     * Set hash
     *
     * @param string $hash
     * @return Demande
     */
    public function setHash($hash)
    {
    	$this->hash = $hash;

    	return $this;
    }

    /**
     * Get hash
     *
     * @return string
     */
    public function getHash()
    {
    	return $this->hash;
    }

    /**
     * Set editcr
     *
     * @param string $editcr
     * @return Demande
     */
    public function setEditcr($editcr)
    {
    	$this->editcr = $editcr;

    	return $this;
    }

    /**
     * Get editcr
     *
     * @return string
     */
    public function getEditcr()
    {
    	return $this->editcr;
    }

    /**
     * Set compteRendu
     *
     * @param string $compteRendu
     * @return Demande
     */
    public function setCompteRendu($compteRendu)
    {
    	$this->compteRendu = $compteRendu;

    	return $this;
    }

    /**
     * Get compteRendu
     *
     * @return string
     */
    public function getCompteRendu()
    {
    	return $this->compteRendu;
    }

    /**
     * Set nomPrescripteur
     *
     * @param string $nomPrescripteur
     * @return Demande
     */
    public function setNomPrescripteur($nomPrescripteur)
    {
    	$this->nomPrescripteur = $nomPrescripteur;

    	return $this;
    }

    /**
     * Get nomPrescripteur
     *
     * @return string
     */
    public function getNomPrescripteur()
    {
    	return $this->nomPrescripteur;
    }

    /**
     * Add uploads
     *
     * @param \Agfa\HpaBundle\Entity\Upload $uploads
     * @return Demande
     */
    public function addUpload(\Agfa\HpaBundle\Entity\Upload $uploads)
    {
    	$this->uploads[] = $uploads;

    	return $this;
    }

    /**
     * Remove uploads
     *
     * @param \Agfa\HpaBundle\Entity\Upload $uploads
     */
    public function removeUpload(\Agfa\HpaBundle\Entity\Upload $uploads)
    {
    	$this->uploads->removeElement($uploads);
    }

    /**
     * Get uploads
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getUploads()
    {
    	return $this->uploads;
    }

    /**
     * Set patient
     *
     * @param \Agfa\HpaBundle\Entity\Patient $patient
     * @return Demande
     */
    public function setPatient(\Agfa\HpaBundle\Entity\Patient $patient = null)
    {
    	$this->patient = $patient;

    	return $this;
    }

    /**
     * Get patient
     *
     * @return \Agfa\HpaBundle\Entity\Patient
     */
    public function getPatient()
    {
    	return $this->patient;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Demande
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return Demande
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set nodem
     *
     * @param string $nodem
     *
     * @return Demande
     */
    public function setNodem($nodem)
    {
        $this->nodem = $nodem;

        return $this;
    }

    /**
     * Get nodem
     *
     * @return string
     */
    public function getNodem()
    {
        return $this->nodem;
    }

    /**
     * Set nodemx
     *
     * @param string $nodemx
     *
     * @return Demande
     */
    public function setNodemx($nodemx)
    {
        $this->nodemx = $nodemx;

        return $this;
    }

    /**
     * Get nodemx
     *
     * @return string
     */
    public function getNodemx()
    {
        return $this->nodemx;
    }

    /**
     * Set etablissement
     *
     * @param \Agfa\HpaBundle\Entity\Etablissement $etablissement
     *
     * @return Demande
     */
    public function setEtablissement(\Agfa\HpaBundle\Entity\Etablissement $etablissement = null)
    {
        $this->etablissement = $etablissement;

        return $this;
    }

    /**
     * Get etablissement
     *
     * @return \Agfa\HpaBundle\Entity\Etablissement
     */
    public function getEtablissement()
    {
        return $this->etablissement;
    }

    /**
     * Set paieSolde
     *
     * @param string $paieSolde
     *
     * @return Demande
     */
    public function setPaieSolde($paieSolde)
    {
        $this->paieSolde = $paieSolde;

        return $this;
    }

    /**
     * Get paieSolde
     *
     * @return string
     */
    public function getPaieSolde()
    {
        return $this->paieSolde;
    }

    /**
     * Add request
     *
     * @param \Agfa\HpaBundle\Entity\Request $request
     *
     * @return Demande
     */
    public function addRequest(\Agfa\HpaBundle\Entity\Request $request)
    {
        $this->requests[] = $request;

        return $this;
    }

    /**
     * Remove request
     *
     * @param \Agfa\HpaBundle\Entity\Request $request
     */
    public function removeRequest(\Agfa\HpaBundle\Entity\Request $request)
    {
        $this->requests->removeElement($request);
    }

    /**
     * Get requests
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getRequests()
    {
        return $this->requests;
    }

    /**
     * Set paieSource
     *
     * @param string $paieSource
     *
     * @return Demande
     */
    public function setPaieSource($paieSource)
    {
        $this->paieSource = $paieSource;

        return $this;
    }

    /**
     * Get paieSource
     *
     * @return string
     */
    public function getPaieSource()
    {
        return $this->paieSource;
    }

    /**
     * Add bsrvrCom
     *
     * @param \Agfa\HpaBundle\Entity\BsrvrCom $bsrvrCom
     *
     * @return Demande
     */
    public function addBsrvrCom(\Agfa\HpaBundle\Entity\BsrvrCom $bsrvrCom)
    {
        $this->bsrvrComs[] = $bsrvrCom;

        return $this;
    }

    /**
     * Remove bsrvrCom
     *
     * @param \Agfa\HpaBundle\Entity\BsrvrCom $bsrvrCom
     */
    public function removeBsrvrCom(\Agfa\HpaBundle\Entity\BsrvrCom $bsrvrCom)
    {
        $this->bsrvrComs->removeElement($bsrvrCom);
    }

    /**
     * Get bsrvrComs
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getBsrvrComs()
    {
        return $this->bsrvrComs;
    }

    /**
     * Add resultat
     *
     * @param \Agfa\HpaBundle\Entity\Resultat $resultat
     *
     * @return Demande
     */
    public function addResultat(\Agfa\HpaBundle\Entity\Resultat $resultat)
    {
        $this->resultats[] = $resultat;

        return $this;
    }

    /**
     * Remove resultat
     *
     * @param \Agfa\HpaBundle\Entity\Resultat $resultat
     */
    public function removeResultat(\Agfa\HpaBundle\Entity\Resultat $resultat)
    {
        $this->resultats->removeElement($resultat);
    }

    /**
     * Get resultats
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getResultats()
    {
        return $this->resultats;
    }

    /**
     * Set affichageCr
     *
     * @param boolean $affichageCr
     *
     * @return Demande
     */
    public function setAffichageCr($affichageCr)
    {
        $this->affichageCr = $affichageCr;

        return $this;
    }

    /**
     * Get affichageCr
     *
     * @return boolean
     */
    public function getAffichageCr()
    {
        return $this->affichageCr;
    }

    /**
     * Set isNew
     *
     * @param boolean $isNew
     *
     * @return Demande
     */
    public function setIsNew($isNew)
    {
        $this->isNew = $isNew;

        return $this;
    }

    /**
     * Get isNew
     *
     * @return boolean
     */
    public function getIsNew()
    {
        return $this->isNew;
    }

    /**
     * Set isUpdated
     *
     * @param boolean $isUpdated
     *
     * @return Demande
     */
    public function setIsUpdated($isUpdated)
    {
        $this->isUpdated = $isUpdated;

        return $this;
    }

    /**
     * Get isUpdated
     *
     * @return boolean
     */
    public function getIsUpdated()
    {
        return $this->isUpdated;
    }
}
