<?php

namespace Agfa\HpaBundle\Entity;

use Agfa\HpaBundle\Library\MaintenanceFr;

/**
 * Paiement
 */
class Paiement
{

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $etablissements;

    /**
     * @var int
     */
    private $id;

    /**
     *
     * @var string
     */
    private $tpe;

    /**
     *
     * @var string
     */
    private $cle;

    /**
     *
     * @var string
     */
    private $societe;

    /**
     *
     * @var string
     */
    private $nom;

    /**
     *
     * @var string
     */
    private $prenom;

    /**
     *
     * @var string
     */
    private $email;

    /**
     *
     * @var string
     */
    private $telephone;

    /**
     *
     * @var string
     */
    private $affichageCr = FALSE;

    /**
     *
     * @var string
     */
    private $actif = FALSE;

    /**
     *
     * @var \DateTime
     */
    private $createdAt;

    /**
     *
     * @var \DateTime
     */
    private $updatedAt;


    /**
     * Constructor
     */
    public function __construct()
    {
        $this->etablissements = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set tpe
     *
     * @param string $tpe
     *
     * @return Paiement
     */
    public function setTpe($tpe)
    {
        $this->tpe = $tpe;

        return $this;
    }

    /**
     * Get tpe
     *
     * @return string
     */
    public function getTpe()
    {
        return $this->tpe;
    }

    /**
     * Set cle
     *
     * @param string $cle
     *
     * @return Paiement
     */
    public function setCle($cle)
    {
        $this->cle = $cle;

        return $this;
    }

    /**
     * Get cle
     *
     * @return string
     */
    public function getCle()
    {
        return $this->cle;
    }

    /**
     * Set societe
     *
     * @param string $societe
     *
     * @return Paiement
     */
    public function setSociete($societe)
    {
        $this->societe = $societe;

        return $this;
    }

    /**
     * Get societe
     *
     * @return string
     */
    public function getSociete()
    {
        return $this->societe;
    }

    /**
     * Set nom
     *
     * @param string $nom
     *
     * @return Paiement
     */
    public function setNom($nom)
    {
        $this->nom = mb_convert_case($nom, MB_CASE_UPPER, "UTF-8");

        return $this;
    }

    /**
     * Get nom
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set prenom
     *
     * @param string $prenom
     *
     * @return Paiement
     */
    public function setPrenom($prenom)
    {
        $this->prenom = mb_convert_case($prenom, MB_CASE_TITLE, "UTF-8");

        return $this;
    }

    /**
     * Get prenom
     *
     * @return string
     */
    public function getPrenom()
    {
        return $this->prenom;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return Paiement
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set telephone
     *
     * @param string $telephone
     *
     * @return Paiement
     */
    public function setTelephone($telephone)
    {
        $this->telephone = MaintenanceFr::maintainPhoneNumber($telephone);

        return $this;
    }

    /**
     * Get telephone
     *
     * @return string
     */
    public function getTelephone()
    {
        return $this->telephone;
    }

    /**
     * Set affichageCr
     *
     * @param boolean $affichageCr
     *
     * @return Paiement
     */
    public function setAffichageCr($affichageCr)
    {
        $this->affichageCr = $affichageCr;

        return $this;
    }

    /**
     * Get affichageCr
     *
     * @return boolean
     */
    public function getAffichageCr()
    {
        return $this->affichageCr;
    }

    /**
     * Set actif
     *
     * @param boolean $actif
     *
     * @return Paiement
     */
    public function setActif($actif)
    {
        $this->actif = $actif;

        return $this;
    }

    /**
     * Get actif
     *
     * @return boolean
     */
    public function getActif()
    {
        return $this->actif;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Paiement
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return Paiement
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Add etablissement
     *
     * @param \Agfa\HpaBundle\Entity\Etablissement $etablissement
     *
     * @return Paiement
     */
    public function addEtablissement(\Agfa\HpaBundle\Entity\Etablissement $etablissement)
    {
        $this->etablissements[] = $etablissement;

        return $this;
    }

    /**
     * Remove etablissement
     *
     * @param \Agfa\HpaBundle\Entity\Etablissement $etablissement
     */
    public function removeEtablissement(\Agfa\HpaBundle\Entity\Etablissement $etablissement)
    {
        $this->etablissements->removeElement($etablissement);
    }

    /**
     * Get etablissements
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getEtablissements()
    {
        return $this->etablissements;
    }
}
