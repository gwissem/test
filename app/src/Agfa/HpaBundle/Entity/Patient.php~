<?php
namespace Agfa\HpaBundle\Entity;

/**
 * Patient
 */
class Patient
{

    /**
     *
     * @var \Doctrine\Common\Collections\Collection
     */
    private $patientPersonnes;

    /**
     *
     * @var integer
     */
    private $id;

    /**
     *
     * @var string
     */
    private $nom;

    /**
     *
     * @var string
     */
    private $prenom;

    /**
     *
     * @var \DateTime
     */
    private $dateNaissance;

    /**
     *
     * @var string
     */
    private $sexe;

    /**
     *
     * @var string
     */
    private $insc;

    /**
     *
     * @var string
     */
    private $bsrvrNopat;

    /**
     *
     * @var \Doctrine\Common\Collections\Collection
     */
    private $demandes;

    /**
     *
     * @var \Agfa\UserBundle\Entity\User
     */
    private $user;

    /**
     *
     * @var \Agfa\HpaBundle\Entity\Etablissement
     */
    private $etablissement;

    /**
     *
     * @var \DateTime
     */
    private $createdAt;

    /**
     *
     * @var \DateTime
     */
    private $updatedAt;

    public function getNomComplet()
    {
        $fullName = mb_convert_case($this->getPrenom(), MB_CASE_TITLE, "UTF-8") . ' ' . mb_strtoupper($this->getNom(), "UTF-8");

        return $fullName;
    }

    public function __toString()
    {
        return $this->getNomComplet();
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->demandes = new \Doctrine\Common\Collections\ArrayCollection();
        $this->patientPersonnes = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nom
     *
     * @param string $nom
     * @return Patient
     */
    public function setNom($nom)
    {
        $this->nom = mb_strtoupper($nom, "UTF-8");

        return $this;
    }

    /**
     * Get nom
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set prenom
     *
     * @param string $prenom
     * @return Patient
     */
    public function setPrenom($prenom)
    {
        $this->prenom = mb_convert_case($prenom, MB_CASE_TITLE, "UTF-8");

        return $this;
    }

    /**
     * Get prenom
     *
     * @return string
     */
    public function getPrenom()
    {
        return $this->prenom;
    }

    /**
     * Set dateNaissance
     *
     * @param \DateTime $dateNaissance
     * @return Patient
     */
    public function setDateNaissance($dateNaissance)
    {
        $this->dateNaissance = $dateNaissance;

        return $this;
    }

    /**
     * Get dateNaissance
     *
     * @return \DateTime
     */
    public function getDateNaissance()
    {
        return $this->dateNaissance;
    }

    /**
     * Set sexe
     *
     * @param string $sexe
     * @return Patient
     */
    public function setSexe($sexe)
    {
        $this->sexe = mb_convert_case($sexe, MB_CASE_LOWER, "UTF-8");

        return $this;
    }

    /**
     * Get sexe
     *
     * @return string
     */
    public function getSexe()
    {
        return $this->sexe;
    }

    /**
     * Set insc
     *
     * @param string $insc
     * @return Patient
     */
    public function setInsc($insc)
    {
        $this->insc = $insc;

        return $this;
    }

    /**
     * Get insc
     *
     * @return string
     */
    public function getInsc()
    {
        return $this->insc;
    }

    /**
     * Add demandes
     *
     * @param \Agfa\HpaBundle\Entity\Demande $demandes
     * @return Patient
     */
    public function addDemande(\Agfa\HpaBundle\Entity\Demande $demandes)
    {
        $this->demandes[] = $demandes;

        return $this;
    }

    /**
     * Remove demandes
     *
     * @param \Agfa\HpaBundle\Entity\Demande $demandes
     */
    public function removeDemande(\Agfa\HpaBundle\Entity\Demande $demandes)
    {
        $this->demandes->removeElement($demandes);
    }

    /**
     * Get demandes
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDemandes()
    {
        return $this->demandes;
    }

    /**
     * Set user
     *
     * @param \Agfa\UserBundle\Entity\User $user
     * @return Patient
     */
    public function setUser(\Agfa\UserBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \Agfa\UserBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set bsrvrNopat
     *
     * @param string $bsrvrNopat
     * @return Patient
     */
    public function setBsrvrNopat($bsrvrNopat)
    {
        $this->bsrvrNopat = $bsrvrNopat;

        return $this;
    }

    /**
     * Get bsrvrNopat
     *
     * @return string
     */
    public function getBsrvrNopat()
    {
        return $this->bsrvrNopat;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Patient
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return Patient
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Add patientPersonne
     *
     * @param \Agfa\HpaBundle\Entity\PersonnePatient $patientPersonne
     *
     * @return Patient
     */
    public function addPatientPersonne(\Agfa\HpaBundle\Entity\PersonnePatient $patientPersonne)
    {
        $this->patientPersonnes[] = $patientPersonne;

        return $this;
    }

    /**
     * Remove patientPersonne
     *
     * @param \Agfa\HpaBundle\Entity\PersonnePatient $patientPersonne
     */
    public function removePatientPersonne(\Agfa\HpaBundle\Entity\PersonnePatient $patientPersonne)
    {
        $this->patientPersonnes->removeElement($patientPersonne);
    }

    /**
     * Get patientPersonnes
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPatientPersonnes()
    {
        return $this->patientPersonnes;
    }

    /**
     * Set etablissement
     *
     * @param \Agfa\HpaBundle\Entity\Etablissement $etablissement
     *
     * @return Patient
     */
    public function setEtablissement(\Agfa\HpaBundle\Entity\Etablissement $etablissement = null)
    {
        $this->etablissement = $etablissement;

        return $this;
    }

    /**
     * Get etablissement
     *
     * @return \Agfa\HpaBundle\Entity\Etablissement
     */
    public function getEtablissement()
    {
        return $this->etablissement;
    }
}
