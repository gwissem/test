<?php
namespace Agfa\HpaBundle\Library;

class MaintenanceFr
{

    /**
     * Maintenance for all data contained in db
     */
    public static function maintainAll($string)
    {
        $string = trim($string);
        $string = mb_convert_case($string, MB_CASE_LOWER, "UTF-8");

        return $string;
    }

    /**
     * Anything else than digits is removed
     * Must be 10 digits long.
     * Add leading 0 is it is 9's long.
     * Must start with 0
     * Second digit must be between 1 and 9
     * If it is 13 digits long and starts with 0033, convert to 0 (France's extension)
     * If it is 11 digits long and starts with 33, convert to 0 (France's extension)
     *
     * @param
     *            french phone number $phone
     * @return $result
     */
    public static function maintainPhoneNumber($phone)
    {
        $phone = self::maintainAll($phone);
        $number = preg_replace('#(\D)*#', '', $phone); // Strip all non numeric characters

        if (strlen($number) == 9) {
            $number = "0" . $number; // Check if number is french: leading 0 and 10 number long
            return $number;
        } elseif (strlen($number) == 13 && substr($number, 0, 4) == '0033') {
            $number = preg_replace('#^0033([1-9][0-9]{8})$#', '0$1', $number); // Rewrite France's phone extension to local phone number
            return $number;
        } elseif (strlen($number) == 11 && substr($number, 0, 2) == '33') {
            $number = preg_replace('#^33([1-9][0-9]{8})$#', '0$1', $number); // Rewrite France's phone extension with no leading 0 to local phone number
            return $number;
        }

        return $number;
    }

//     /**
//      * FIXME Does not work when float is 2.00
//      * Remove all dots and commas and add a coma to second decimal place
//      * @param string $decimal
//      * @return number
//      */
//     public static function maintainDecimal($decimal){
//         $decimal = "$decimal";
//         $result = preg_replace('/(\ |\,|\.)/', "", $decimal) / 100;

//         // clean phone number or "null"
//         return $result;
//     }


}