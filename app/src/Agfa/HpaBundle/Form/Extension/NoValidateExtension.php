<?php
namespace Agfa\HpaBundle\Form\Extension;

use Symfony\Component\Form\AbstractTypeExtension;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\OptionsResolver\OptionsResolver;


class NoValidateExtension extends AbstractTypeExtension
{

    public function buildView(FormView $view, FormInterface $form, array $options)
    {
// dump('test');
        die('test');
        $view->vars['attr'] = array_merge($view->vars['attr'], [
            'novalidate' => 'novalidate',
        ]);

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        dump('retest');
        // makes it legal for FileType fields to have an image_property option
        $resolver->setDefined(array('novalidate'));
    }

    public function getExtendedType()
    {
        return FormType::class;
    }
}

