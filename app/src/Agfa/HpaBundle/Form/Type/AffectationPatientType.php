<?php
namespace Agfa\HpaBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AffectationPatientType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $traitChoices = $options['trait_choices'];

        $user = $traitChoices['user'];
        $patient = $traitChoices['patient'];

        $builder->add('personne', EntityType::class, [
            'label' => 'Personne',
            'expanded' => TRUE,
            'multiple' => FALSE,
            'required' => FALSE,
            'placeholder' => $patient->getNomComplet() . ' (une nouvelle personne va être créée)',
            'class' => 'AgfaHpaBundle:Personne',
            'query_builder' => function (EntityRepository $repo) use (&$user) {
                return $repo->createQueryBuilder('pe')
                    ->where('pe.user = :user')
                    ->setParameter('user', $user);
            }
        ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'trait_choices' => null,
        ));
    }
}