<?php
namespace Agfa\HpaBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Agfa\HpaBundle\Manager\DemandeManager;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Cache\Adapter\AdapterInterface;
use Agfa\HpaBundle\Services\Cache;

class RechercheDemandeType extends AbstractType
{
    const CACHED_KEY_SEARCH_FORM = 'SEARCH_FORM';

    public function __construct(TokenStorage $tokenStorage, DemandeManager $demandeMgr, AdapterInterface $cacheApp) // , $liste_chapitres, $liste_labos, $liste_mois_debut, $liste_mois_fin)
    {
        $this->user = $tokenStorage->getToken()->getUser();
        $this->demandeMgr = $demandeMgr;
        $this->cacheApp = $cacheApp;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $this->traitChoices = $options['trait_choices'];

        $this->personne = $this->traitChoices['personne'];

        $allExamens = array();
        $allPrescripteurs = array();
        $allLabos = array();

        // Set cache parameters
        $cacheKey = Cache::getCacheKey(Cache::KEY_SEARCH_FORM_VARIABLES, array('person_id' => $this->personne->getId()));
        $cachedLists = $this->cacheApp->getItem($cacheKey);

        // Retrieve details that are used in the forms if not in cache, use cache otherwise.
        if (! $cachedLists->isHit()) {
            $demandes = $this->demandeMgr->getDemandesResultats($this->user, $this->personne);

            foreach ($demandes as $demande) {
                $allPrescripteurs[$demande->getNomPrescripteur()] = $demande->getNomPrescripteur();
                $allLabos[$demande->getEtablissement()->getNom()] = $demande->getEtablissement()->getId();

                foreach($demande->getResultats() as $resultat){
                    $allExamens[$resultat->getElement()->getExamen()->getLibelle()] = $resultat->getElement()->getExamen()->getId();
                }
            }

            ksort($allExamens);
            ksort($allPrescripteurs);
            ksort($allLabos);

            $lists = array(
                'examens' => $allExamens,
                'gp' => $allPrescripteurs,
                'labs' => $allLabos
            );

            // Set result in cache
            $cachedLists->set($lists);
            $this->cacheApp->save($cachedLists);

        }else{
            $lists = $cachedLists->get();
            $allExamens = $lists['examens'];
            $allPrescripteurs = $lists['gp'];
            $allLabos = $lists['labs'];
        }

        $builder->add('examen', ChoiceType::class, array(
            'label' => "Examen",
            'required' => false,
            'choices' => $allExamens,
            'placeholder' => 'Tous'
        ))
            ->add('laboratoire', ChoiceType::class, array(
            'label' => "Laboratoire",
            'required' => FALSE,
            'choices' => $allLabos,
            'placeholder' => 'Tous'
        ))
            ->add('prescripteur', ChoiceType::class, array(
            'label' => "Médecin prescripteur",
            'required' => FALSE,
            'choices' => $allPrescripteurs,
            'placeholder' => 'Tous'
        ))
        ->add('datePrelevementDebut', DateType::class, array(
            'label' => 'Examen réalisé entre le',
            'widget' => 'single_text',
            'required' => FALSE,
            'years' => range(date('Y'), date('Y') - 5),
            'format' => 'dd/MM/yyyy'
        ))
        ->add('datePrelevementFin', DateType::class, array(
            'label' => 'et le',
            'widget' => 'single_text',
            'required' => FALSE,
            'years' => range(date('Y'), date('Y') - 5),
            'format' => 'dd/MM/yyyy'
        ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'trait_choices' => null
        ));
    }
}