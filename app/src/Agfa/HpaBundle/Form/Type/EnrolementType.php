<?php
namespace Agfa\HpaBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bundle\FrameworkBundle\Translation\Translator;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Agfa\HpaBundle\Form\DataTransformer\BsrvrPasswordTransformer;
use Symfony\Component\Validator\Constraints as Assert;

class EnrolementType extends AbstractType
{

    private $translator;

    private $transformer;

    public function __construct(Translator $translator, BsrvrPasswordTransformer $transformer)
    {
        $this->translator = $translator;
        $this->transformer = $transformer;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        // ajout d'un fake password pour empecher l'autofill de Chrome :(
        $builder->add('fake', PasswordType::class, array(
            'label' => false,
            'mapped' => false,
            'required' => false,
            'attr' => array(
                'class' => 'hidden'
            )
        ))->add('login', TextType::class, array(
            'label' => 'Identifiant',
            'constraints' => new Assert\Regex(array('pattern' => '/^(\d{4}[a-z0-9]\d{9,11})$|^((19|20)\d{2})(0?[1-9]|1[012])(0[1-9]|[123][0-9])([0-9]*5)$|^[0-9]{9}\-[0-9][a-z][0-9]{10,12}$|^([0-9]{4}[a-z][0-9]{5}[a-z0-9][0-9]{4})$|^([0-9]{9}\-[0-9][a-z][0-9]{5}[a-z0-9][0-9]{4})$|^([a-z][0-9]{10})$|^(test)$/i', 'message' => 'L\'identifiant indiqué n\'a pas le bon format. Merci d\'indiquer l\'identifiant fourni par votre laboratoire. Merci d\'indiquer l\'identifiant fourni par votre laboratoire. Exemple : 4693B7040300001')),
            'attr' => array(
                'autocomplete' => 'new-password',
                'placeholder' => $this->translator->trans('agfa.hpa_bundle.enrolment.form.placeholder.id')
            )
        ));

        // Password has to be a date formated as YYYYMMDD
        $builder->add('password', TextType::class, array(
            'label' => 'Votre date de naissance',
            'attr' => array(
                'data-inputmask-inputformat' => 'dd/mm/yyyy',
                'data-inputmask-alias' => 'date',
                'class' => 'type-switch',
                'placeholder' => $this->translator->trans('agfa.hpa_bundle.enrolment.form.placeholder.psswd'),
                'autocomplete' => 'new-password'
            )
        ));

        $builder->get('password')->addModelTransformer($this->transformer);
    }

//     public function configureOptions(OptionsResolver $resolver)
//     {
//         $resolver
//             ->setDefaults(array(
//             'data_class' => Enrolement::class
//         ));
//     }
}