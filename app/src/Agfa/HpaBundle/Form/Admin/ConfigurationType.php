<?php
namespace Agfa\HpaBundle\Form\Admin;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\UrlType;

class ConfigurationType extends AbstractType
{

    /**
     *
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('logoAlignement', ChoiceType::class, array(
            'label' => 'agfa.hpa_bundle.configuration.form.label.logo_alignment',
            'expanded' => TRUE,
            'choices' => array(
                'agfa.admin_bundle.configuration.form.label.center' => 'center',
                'agfa.admin_bundle.configuration.form.label.left' => 'left'
            )
        ))
            ->add('logoutRedirectionUrl', UrlType::class, array(
            'label' => 'agfa.hpa_bundle.configuration.form.label.logout_redirection_url',
                'required' => true,
            'attr' => array(
                'placeholder' => 'https://...',
                'help_text' => 'agfa.hpa_bundle.form.admin.configuration.help'
            )
        ))
            ->add('videoTutorialRegister', UrlType::class, array(
            'label' => 'agfa.hpa_bundle.configuration.form.label.video_tutorial_register',
            'attr' => array(
                'placeholder' => 'https://...'
            )
        ))
            ->add('patientRecordDirectAccess', ChoiceType::class, array(
            'label' => 'agfa.hpa_bundle.configuration.form.label.patient_record_direct_access',
            'expanded' => TRUE,
            'multiple' => FALSE,
            'required' => TRUE,
            'choices' => array(
                'twig_filter.boolean.enabled' => TRUE,
                'twig_filter.boolean.disabled' => FALSE
            )
        ))
            ->add('lastPatientsRecordOnlyMssg', ChoiceType::class, array(
            'label' => 'agfa.hpa_bundle.configuration.form.label.last_patients_record_only_mssg',
            'expanded' => TRUE,
            'multiple' => FALSE,
            'required' => TRUE,
            'choices' => array(
                'twig_filter.boolean.enabled' => TRUE,
                'twig_filter.boolean.disabled' => FALSE
            )
        ));
    }

    /**
     *
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Agfa\HpaBundle\Entity\Configuration'
        ));
    }
}
