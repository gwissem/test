<?php
namespace Agfa\HpaBundle\Form\DataTransformer;

use Symfony\Component\Form\DataTransformerInterface;

class BsrvrPasswordTransformer implements DataTransformerInterface
{

    /**
     * Transform a date from 01/01/1970 to 19700101.
     *
     * {@inheritdoc}
     * @see \Symfony\Component\Form\DataTransformerInterface::transform()
     */
    public function transform($dateForBioserveur)
    {
        $date = preg_replace('/((19|20)[0-9]{2})([0-1][0-9])([0-3][0-9])/i', '$1/$2/$3', $dateForBioserveur);

        return $date;
    }

    /**
     * Transform a date from 19700101 to 01/01/1970.
     *
     * {@inheritdoc}
     * @see \Symfony\Component\Form\DataTransformerInterface::reverseTransform()
     */
    public function reverseTransform($dateForHumans)
    {
        $date = preg_replace('/([0-3][0-9])\/([0-1][0-9])\/((19|20)[0-9]{2})/i', '$3$2$1', $dateForHumans);
        return $date;
    }
}
