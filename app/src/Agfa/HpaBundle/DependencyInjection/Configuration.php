<?php
namespace Agfa\HpaBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html#cookbook-bundles-extension-config-class}
 */
class Configuration implements ConfigurationInterface
{

    /**
     *
     * {@inheritdoc}
     *
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('agfa_hpa');

        // Here you should define the parameters that are allowed to
        // configure your bundle. See the documentation linked above for
        // more information on that topic.
        $rootNode
            ->children()
                ->arrayNode('bioserveur_env')
                    ->children()
                        ->booleanNode('debug')->defaultFALSE()->end()
                        ->scalarNode('host')->end()
                        ->scalarNode('uri_enrolement')->end()
                        ->scalarNode('uri_notification_reading')->end()
                        ->scalarNode('uri_notification_payment')->end()
                        ->scalarNode('uri_notification_unenrollment')->end()
                        ->arrayNode('ips')
                            ->prototype('scalar')->end()
                        ->end()
                    ->end()
                ->end()
                ->arrayNode('bioserveur')
                    ->children()
                        ->scalarNode('pass_hexapat')->end()
                        ->scalarNode('pass_bioserveur')->end()
                    ->end()
                ->end()
                ->arrayNode('admin_allowed_ips')
                    ->children()
                        ->arrayNode('dev')->prototype('scalar')->end()->end()
                        ->arrayNode('biocentre')->prototype('scalar')->end()->end()
                    ->end()
                ->end()
            ->end();

        return $treeBuilder;
    }
}
