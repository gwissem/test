<?php
namespace Agfa\HpaBundle\DataFixtures\ORM\Dev;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\Persistence\ObjectManager;
use Agfa\UserBundle\Entity\User;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;

class UserData extends AbstractFixture implements OrderedFixtureInterface
{

    public function load(ObjectManager $manager)
    {
        // Admin
        $user = new User();
        $user->setEmail('charles.becker@agfa.com')
            ->addRole('ROLE_ADMIN')
            ->setPlainPassword('Test2016')
            ->setEnabled(true);

        $manager->persist($user);
        $this->addReference('userAdm', $user);

        $manager->flush();
    }

    public function getOrder()
    {
        return 500;
    }
}