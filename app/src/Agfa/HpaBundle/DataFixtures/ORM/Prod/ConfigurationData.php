<?php
namespace Agfa\HpaBundle\DataFixtures\ORM\Prod;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\Persistence\ObjectManager;
use Agfa\HpaBundle\Entity\Configuration;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;

class ConfigurationData extends AbstractFixture implements OrderedFixtureInterface
{

    const THEME_DEV = 'dev';

    const THEME_PREPROD = 'preprod';

    const THEME_DEMO = 'demo';

    const THEME_MONLABO = 'monlabo';

    const THEME_REUNILAB = 'reunilab';

    const THEME_BIOCENTRE = 'biocentre';

    const THEME_BIONORMA = 'bionorma';

    const THEME_UNILABS = 'unilabs';

    const THEME_LABAZUR = 'labazur';

    const THEME_GENBIO = 'genbio';

    const THEME_ORIADE = 'oriade';

    const THEME_SBLBIO = 'sbl-bio';

    const THEME_BIOARDAISNE = 'bioardaisne';

    const THEME_LECHESNAY = 'lechesnay';

    const THEME_BIOLAB = 'biolab';

    const THEME_AXBIOOCEAN = 'axbioocean';

    public function load(ObjectManager $manager)
    {
        // Agfa
        $configuration0 = new Configuration();
        $configuration0->setThemeName(self::THEME_DEV)->setLogoAlignement('center')->setVideoTutorialRegister('https://www.youtube.com/embed/knHiro_yKlM?rel=0&amp;showinfo=0');
        $manager->persist($configuration0);

        $configuration0bis = new Configuration();
        $configuration0bis->setThemeName(self::THEME_PREPROD)->setLogoAlignement('center')->setVideoTutorialRegister('https://www.youtube.com/embed/knHiro_yKlM?rel=0&amp;showinfo=0');
        $manager->persist($configuration0bis);

        $configuration0bisbis = new Configuration();
        $configuration0bisbis->setThemeName(self::THEME_DEMO)->setLogoAlignement('center')->setVideoTutorialRegister('https://www.youtube.com/embed/knHiro_yKlM?rel=0&amp;showinfo=0');
        $manager->persist($configuration0bisbis);

        $this->addReference('configurationLab0', $configuration0);

        // Monlabo
        $configuration1 = new Configuration();
        $configuration1->setThemeName(self::THEME_MONLABO)->setLogoAlignement('center')->setVideoTutorialRegister('https://www.youtube.com/embed/knHiro_yKlM?rel=0&amp;showinfo=0');

        $manager->persist($configuration1);
        $this->addReference('configurationLab1', $configuration1);

        // Reunilab
        $configuration2 = new Configuration();
        $configuration2->setThemeName(self::THEME_REUNILAB)->setLogoAlignement('center')->setVideoTutorialRegister('https://www.youtube.com/embed/knHiro_yKlM?rel=0&amp;showinfo=0');

        $manager->persist($configuration2);
        $this->addReference('configurationLab2', $configuration2);

        // Biocentre
        $configuration3 = new Configuration();
        $configuration3->setThemeName(self::THEME_BIOCENTRE)->setLogoAlignement('center')->setVideoTutorialRegister('https://www.youtube.com/embed/knHiro_yKlM?rel=0&amp;showinfo=0');
        $manager->persist($configuration3);

        // Bionorma
        $configuration4 = new Configuration();
        $configuration4->setThemeName(self::THEME_BIONORMA)->setLogoAlignement('center')->setVideoTutorialRegister('https://www.youtube.com/embed/knHiro_yKlM?rel=0&amp;showinfo=0');
        $manager->persist($configuration4);

        // Unilabs
        $configuration5 = new Configuration();
        $configuration5->setThemeName(self::THEME_UNILABS)->setLogoAlignement('left')->setVideoTutorialRegister('https://www.youtube.com/embed/knHiro_yKlM?rel=0&amp;showinfo=0');

        $manager->persist($configuration5);
        $this->addReference('configurationLab5', $configuration5);

        // Bioaccess
        $configuration6 = new Configuration();
        $configuration6->setThemeName(self::THEME_LABAZUR)->setLogoAlignement('center')->setVideoTutorialRegister('https://www.youtube.com/embed/knHiro_yKlM?rel=0&amp;showinfo=0');
        $manager->persist($configuration6);
        $this->addReference('configurationLab6', $configuration6);

        // Genbio
        $configuration7 = new Configuration();
        $configuration7->setThemeName(self::THEME_GENBIO)->setLogoAlignement('center')->setVideoTutorialRegister('https://www.youtube.com/embed/knHiro_yKlM?rel=0&amp;showinfo=0');

        $manager->persist($configuration7);
        $this->addReference('configurationLab7', $configuration7);

        // Oriade
        $configuration8 = new Configuration();
        $configuration8->setThemeName(self::THEME_ORIADE)->setLogoAlignement('center')->setVideoTutorialRegister('https://www.youtube.com/embed/knHiro_yKlM?rel=0&amp;showinfo=0');
        $manager->persist($configuration8);

        // SBL Bio
        $configuration9 = new Configuration();
        $configuration9->setThemeName(self::THEME_SBLBIO)->setLogoAlignement('center')->setVideoTutorialRegister('https://www.youtube.com/embed/knHiro_yKlM?rel=0&amp;showinfo=0');
        $manager->persist($configuration9);

        // Bioardaisne
        $configuration10 = new Configuration();
        $configuration10->setThemeName(self::THEME_BIOARDAISNE)->setLogoAlignement('center')->setVideoTutorialRegister('https://www.youtube.com/embed/knHiro_yKlM?rel=0&amp;showinfo=0');
        $manager->persist($configuration10);

        // LeChesnay
        $configuration11 = new Configuration();
        $configuration11->setThemeName(self::THEME_LECHESNAY)->setLogoAlignement('center')->setVideoTutorialRegister('https://www.youtube.com/embed/knHiro_yKlM?rel=0&amp;showinfo=0');
        $manager->persist($configuration11);

        // Biolab
        $configuration12 = new Configuration();
        $configuration12->setThemeName(self::THEME_BIOLAB)->setLogoAlignement('center')->setVideoTutorialRegister('https://www.youtube.com/embed/knHiro_yKlM?rel=0&amp;showinfo=0');
        $manager->persist($configuration12);

        // Axbioocean
        $configuration13 = new Configuration();
        $configuration13->setThemeName(self::THEME_AXBIOOCEAN)->setLogoAlignement('center')->setVideoTutorialRegister('https://www.youtube.com/embed/knHiro_yKlM?rel=0&amp;showinfo=0');
        $manager->persist($configuration13);

        $manager->flush();
    }

    public function getOrder()
    {
        return 100;
    }
}