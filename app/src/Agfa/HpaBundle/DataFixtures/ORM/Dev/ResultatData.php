<?php
namespace Agfa\HpaBundle\DataFixtures\ORM\Dev;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Agfa\HpaBundle\Entity\Resultat;

class ResultatData extends AbstractFixture implements OrderedFixtureInterface
{

    public function load(ObjectManager $manager)
    {
        $resultats = array(
            array(
                'element' => $this->getReference('element01'),
                'valeur_alpha' => 'De_01-Ch_01-Ex_01-El_01-Re-01',
                'demande' => $this->getReference('demande01'),
                'reference_name' => 'resultat01'
            ),
            array(
                'element' => $this->getReference('element02'),
                'valeur_alpha' => 'De_01-Ch_01-Ex_01-El_01-Re-02',
                'demande' => $this->getReference('demande01'),
                'reference_name' => 'resultat02'
            ),
            array(
                'element' => $this->getReference('element04'),
                'valeur_alpha' => 'De_03-Ch_03-Ex_04-El_04-Re-03',
                'demande' => $this->getReference('demande03'),
                'reference_name' => 'resultat03'
            )
        );

        foreach ($resultats as $data) {
            $resultat = new Resultat();
            $resultat->setElement($data['element'])
                ->setDemande($data['demande'])
                ->setValeurAlpha($data['valeur_alpha']);
            $manager->persist($resultat);
            $this->addReference($data['reference_name'], $resultat);
        }

        for ($i = 5; $i < 30; $i ++) {
            for ($y = 1; $y <= 300; $y ++) {
                $resultat = new Resultat();
                $resultat->setElement($this->getReference("element$i"))
                    ->setDemande($this->getReference('Demande' . $y))
                    ->setValeurAlpha("Resultat $i");

                $manager->persist($resultat);
            }
        }

        $manager->flush();
    }

    public function getOrder()
    {
        return 535;
    }
}