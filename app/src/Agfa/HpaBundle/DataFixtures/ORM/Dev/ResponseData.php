<?php
namespace Agfa\HpaBundle\DataFixtures\ORM\Dev;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Agfa\MoneticoPaiementBundle\Entity\Response;

class ResponseData extends AbstractFixture implements OrderedFixtureInterface
{

    public function load(ObjectManager $manager)
    {
        $responses = array(
            array(
                'ip_client' => '127.0.0.1',
                'code_retour' => 'payetest',
                'montant' => 124,
                'currency' => 'EUR',
                'date_response' => new \DateTime('NOW'),
                'reference' => 'TST000000001',
                'texte_libre' => 'Nothing to say',
                'cvx' => 'oui',
                'vld' => '1219',
                'brand' => 'vi',
                'status3ds' => '4',
                'origine_tr' => 'FRA',
                'reference_name' => 'response01'
            ),
            array(
                'ip_client' => '127.0.0.1',
                'code_retour' => 'payetest',
                'montant' => 124,
                'currency' => 'EUR',
                'date_response' => new \DateTime('NOW'),
                'reference' => 'TST000000002',
                'texte_libre' => 'Nothing to say',
                'cvx' => 'oui',
                'vld' => '1219',
                'brand' => 'vi',
                'status3ds' => '4',
                'origine_tr' => 'FRA',
                'reference_name' => 'response02'
            )
        );

        foreach ($responses as $data) {
            $response = new Response();
            $response->setIpClient($data['ip_client'])
                ->setCodeRetour($data['code_retour'])
                ->setMontant($data['montant'])
                ->setCurrency($data['currency'])
                ->setDate($data['date_response'])
                ->setReference($data['reference'])
                ->setTexteLibre($data['texte_libre'])
                ->setCvx($data['cvx'])
                ->setVld($data['vld'])
                ->setBrand($data['brand'])
                ->setStatus3ds($data['status3ds'])
                ->setOrigineTr($data['origine_tr']);

            $manager->persist($response);
            $this->addReference($data['reference_name'], $response);
        }

        $manager->flush();
    }

    public function getOrder()
    {
        return 501;
    }
}