<?php
namespace Agfa\HpaBundle\DataFixtures\ORM\Dev;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Agfa\HpaBundle\Entity\Patient;

class PatientData extends AbstractFixture implements OrderedFixtureInterface
{

    public function load(ObjectManager $manager)
    {
        $patients = array(
            array(
                'nom' => 'Hanks',
                'prenom' => 'Tom',
                'sexe' => 'h',
                'date_naissance' => new \DateTime('9 july 1956'),
                'insc' => '123',
                'sexe' => 'h',
                'bsrvr_nopat' => 1,
                'user_id' => $this->getReference('userAdm'),
                'etablissement' => $this->getReference('etab01'),
                'reference_name' => 'patient01'
            ),array(
                'nom' => 'Ford',
                'prenom' => 'Henry',
                'sexe' => 'h',
                'date_naissance' => new \DateTime('30 july 1863'),
                'insc' => '456',
                'sexe' => 'h',
                'bsrvr_nopat' => 'B6022600002',
                'user_id' => $this->getReference('userAdm'),
                'etablissement' => $this->getReference('etab01'),
                'reference_name' => 'patient03'
            ),array(
                'nom' => 'Test',
                'prenom' => 'Virginie',
                'sexe' => 'f',
                'date_naissance' => new \DateTime('14 january 1978'),
                'insc' => '789',
                'sexe' => 'h',
                'bsrvr_nopat' => 'B6022600001',
                'user_id' => $this->getReference('userAdm'),
                'etablissement' => $this->getReference('etab01'),
                'reference_name' => 'patient02'
            )
        );


        foreach ($patients as $data) {
            $patient = new Patient();
            $patient->setNom($data['nom'])
                ->setPrenom($data['prenom'])
                ->setDateNaissance($data['date_naissance'])
                ->setSexe($data['sexe'])
                ->setInsc($data['insc'])
                ->setBsrvrNopat($data['bsrvr_nopat'])
                ->setUser($data['user_id'])
                ->setEtablissement($data['etablissement']);

            $manager->persist($patient);
            $this->addReference($data['reference_name'], $patient);
        }

        $manager->flush();
    }

    public function getOrder()
    {
        return 510;
    }
}