<?php
namespace Agfa\HpaBundle\DataFixtures\ORM\Dev;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Agfa\HpaBundle\Entity\Element;

class ElementData extends AbstractFixture implements OrderedFixtureInterface
{

    public function load(ObjectManager $manager)
    {
        $elements = array(
            array(
                'examen' => $this->getReference('examen01'),
                'code' => '00002MP',
                'libelle' => 'De_01-Ch_01-Ex_01-El_01',
                'reference_name' => 'element01'
            ),
            array(
                // 'examen' => $this->getReference('examen01'),
                'examen' => $this->getReference('examen02'),
                'code' => '00002RAI',
                'libelle' => 'De_01-Ch_01-Ex_01-El_02',
                'reference_name' => 'element02'
            ),
            array(
                'examen' => $this->getReference('examen01'),
                'code' => '00002CI',
                'libelle' => 'De_01-Ch_01-Ex_01-El_03',
                'reference_name' => 'element03'
            ),
            array(
                'examen' => $this->getReference('examen04'),
                'code' => '00002CI',
                'libelle' => 'De_03-Ch_03-Ex_04-El_04',
                'reference_name' => 'element04'
            )
        );

        foreach ($elements as $data) {
            $element = new Element();
            $element->setExamen($data['examen'])
                ->setCode($data['code'])
                ->setLibelle($data['libelle']);

            $manager->persist($element);
            $this->addReference($data['reference_name'], $element);
        }

        for ($i = 5; $i < 30; $i ++) {
            $element = new Element();
            $element->setExamen($this->getReference("examen$i"))
                ->setCode("EL000$i")
                ->setLibelle("Element $i");

            $manager->persist($element);
            $this->addReference("element$i", $element);
        }

        $manager->flush();
    }

    public function getOrder()
    {
        return 525;
    }
}