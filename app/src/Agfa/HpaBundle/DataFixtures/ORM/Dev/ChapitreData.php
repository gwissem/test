<?php
namespace Agfa\HpaBundle\DataFixtures\ORM\Dev;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Agfa\HpaBundle\Entity\Chapitre;

class ChapitreData extends AbstractFixture implements OrderedFixtureInterface
{

    public function load(ObjectManager $manager)
    {
        $chapitres = array(
            array(
                'etablissement' => $this->getReference('etab02'),
                'code' => '0002BIOCH',
                'libelle' => 'De_01-Ch_01',
                'reference_name' => 'chapitre01'
            ),
            array(
                'etablissement' => $this->getReference('etab02'),
                'code' => '0002VALID',
                'libelle' => 'De_01-Ch_02',
                'reference_name' => 'chapitre02'
            ),
            array(
                'etablissement' => $this->getReference('etab02'),
                'code' => '0002HEMAT',
                'libelle' => 'De_03-Ch_03',
                'reference_name' => 'chapitre03'
            )
        );

        foreach ($chapitres as $data) {
            $chapitre = new Chapitre();
            $chapitre->setEtablissement($data['etablissement'])
                ->setCode($data['code'])
                ->setLibelle($data['libelle']);

            $manager->persist($chapitre);
            $this->addReference($data['reference_name'], $chapitre);
        }

        $manager->flush();
    }

    public function getOrder()
    {
        return 515;
    }
}