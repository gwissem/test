<?php
namespace Agfa\HpaBundle\DataFixtures\ORM\Dev;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Agfa\HpaBundle\Entity\BsrvrCom;

class BsrvrComData extends AbstractFixture implements OrderedFixtureInterface
{

    public function load(ObjectManager $manager)
    {
        $responses = array(
            array(
                'demande_id' => $this->getReference('demande01'),
                'action' => BsrvrCom::ACTION_UPDATE_PAYMENT,
                'reference_name' => 'bsrvr01'
            ),
            array(
                'demande_id' => $this->getReference('demande02'),
                'action' => BsrvrCom::ACTION_UPDATE_PAYMENT,
                'reference_name' => 'bsrvr03'
            ),
            array(
                'demande_id' => $this->getReference('demande01'),
                'action' => BsrvrCom::ACTION_UPDATE_READING,
                'reference_name' => 'bsrvr02'
            )
        );

        foreach ($responses as $data) {
            $bsrvrCom = new BsrvrCom();
            $bsrvrCom->setDemande($data['demande_id'])->setAction($data['action']);

            $manager->persist($bsrvrCom);
            $this->addReference($data['reference_name'], $bsrvrCom);
        }

        $manager->flush();
    }

    public function getOrder()
    {
        return 570;
    }
}