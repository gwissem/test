<?php
namespace Agfa\HpaBundle\DataFixtures\ORM\Dev;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\Persistence\ObjectManager;
use Agfa\UserBundle\Entity\User;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;

class UserData extends AbstractFixture implements OrderedFixtureInterface
{

    public function load(ObjectManager $manager)
    {
        // Admin
        $user = new User();
        $user->setEmail('beccha2+adm@gmail.com')
            ->addRole('ROLE_ADMIN')
            ->setPlainPassword('test')
            ->setEnabled(true);

        $manager->persist($user);
        $this->addReference('userAdm', $user);

        // Commerce
        $user = new User();
        $user->setEmail('beccha2+com@gmail.com')
            ->addRole('ROLE_SUPPORT')
            ->addRole('ROLE_DEBUG')
            ->setPlainPassword('test')
            ->setEnabled(true);

        $manager->persist($user);
        $this->addReference('userCom', $user);

        // Labo
        $user = new User();
        $user->setEmail('beccha2+lab1@gmail.com')
            ->addRole('ROLE_LABO')
            ->setPlainPassword('test')
            ->setEnabled(true);

        $manager->persist($user);
        $this->addReference('userLab1', $user);

        $user = new User();
        $user->setEmail('beccha2+lab2@gmail.com')
            ->addRole('ROLE_LABO')
            ->setPlainPassword('test')
            ->setEnabled(true);

        $manager->persist($user);
        $this->addReference('userLab2', $user);

        // Patient
        $user = new User();
        $user->setEmail('beccha2+pat@gmail.com')
            ->setPlainPassword('test')
            ->setEnabled(true);

        $manager->persist($user);
        $this->addReference('userPat', $user);

        // Garbage
        for ($i = 0; $i < 50; $i ++) {
            $user = new User();
            $user->setEmail("beccha2+garbage$i@gmail.com")
                ->setPlainPassword('test')
                ->setEnabled(true);

            $manager->persist($user);
        }

        $manager->flush();
    }

    public function getOrder()
    {
        return 500;
    }
}