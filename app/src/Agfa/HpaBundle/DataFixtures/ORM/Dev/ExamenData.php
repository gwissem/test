<?php
namespace Agfa\HpaBundle\DataFixtures\ORM\Dev;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Agfa\HpaBundle\Entity\Examen;

class ExamenData extends AbstractFixture implements OrderedFixtureInterface
{

    public function load(ObjectManager $manager)
    {
        $examens = array(
            array(
                'chapitre' => $this->getReference('chapitre01'),
                'code' => '00002CRP',
                'libelle' => 'De_01-Ch_01-Ex_01',
                'reference_name' => 'examen01'
            ),
            array(
                'chapitre' => $this->getReference('chapitre01'),
                'code' => '00002/BLOQ',
                'libelle' => 'De_01-Ch_01-Ex_02',
                'reference_name' => 'examen02'
            ),
            array(
                'chapitre' => $this->getReference('chapitre02'),
                'code' => '00002RAI',
                'libelle' => "De_01-Ch_02-Ex_03",
                'reference_name' => 'examen03'
            ),
            array(
                'chapitre' => $this->getReference('chapitre03'),
                'code' => '00002RAI',
                'libelle' => "De_03-Ch_03-Ex_04",
                'reference_name' => 'examen04'
            )
        );

        foreach ($examens as $data) {
            $examen = new Examen();
            $examen->setChapitre($data['chapitre'])
                ->setCode($data['code'])
                ->setLibelle($data['libelle']);

            $manager->persist($examen);
            $this->addReference($data['reference_name'], $examen);
        }

        // Add more exams
        for ($i = 5; $i < 30; $i ++) {
            $examen = new Examen();
            $examen->setChapitre($this->getReference('chapitre03'))
                ->setCode("EX000$i")
                ->setLibelle("Examen $i");

            $manager->persist($examen);
            $this->addReference("examen$i", $examen);
        }

        $manager->flush();
    }

    public function getOrder()
    {
        return 520;
    }
}