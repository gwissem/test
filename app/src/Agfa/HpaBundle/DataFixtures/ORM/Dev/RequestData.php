<?php
namespace Agfa\HpaBundle\DataFixtures\ORM\Dev;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Agfa\HpaBundle\Entity\Request;

class RequestData extends AbstractFixture implements OrderedFixtureInterface
{

    public function load(ObjectManager $manager)
    {
        $requests = array(
            array(
                'tpe' => '7777777',
                'montant' => 0,
                'reference' => 'TST000000001',
                'texte_libre' => 'Just a test',
                'response_id' => $this->getReference('response01'),
                'reference_name' => 'request01'
            ),
            array(
                'tpe' => '7777777',
                'montant' => 10,
                'reference' => 'TST000000002',
                'texte_libre' => 'Just a test',
                'response_id' => $this->getReference('response02'),
                'reference_name' => 'request02'
            )
        );

        foreach ($requests as $data) {
            $request = new Request();
            $request->setTpe($data['tpe'])
                ->setMontant($data['montant'])
                ->setReference($data['reference'])
                ->setTexteLibre($data['texte_libre'])
                ->setResponse($data['response_id']);

                $manager->persist($request);
                $this->addReference($data['reference_name'], $request);
        }

        $manager->flush();
    }

    public function getOrder()
    {
        return 502;
    }
}