<?php
namespace Agfa\HpaBundle\DataFixtures\ORM\Dev;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Agfa\HpaBundle\Entity\PersonnePatient;

class PersonnePatientData extends AbstractFixture implements OrderedFixtureInterface
{

    public function load(ObjectManager $manager)
    {
        $personnesPatients = array(
            array(
                'patient_id' => $this->getReference('patient01'),
                'personne_id' => $this->getReference('tomHanks')
            ),array(
                'patient_id' => $this->getReference('patient02'),
                'personne_id' => $this->getReference('virginietest')
            )
        );

        foreach ($personnesPatients as $data) {
            $perPat = new PersonnePatient();
            $perPat->setPatient($data['patient_id'])
                ->setPersonne($data['personne_id']);

            $manager->persist($perPat);
        }

        $manager->flush();
    }

    public function getOrder()
    {
        return 511;
    }
}