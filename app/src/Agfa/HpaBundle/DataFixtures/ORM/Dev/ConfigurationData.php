<?php
namespace Agfa\HpaBundle\DataFixtures\ORM\Dev;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\Persistence\ObjectManager;
use Agfa\HpaBundle\Entity\Configuration;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Agfa\HpaBundle\DataFixtures\ORM\Prod\ConfigurationData as ConfigurationDataProd;

class ConfigurationData extends AbstractFixture implements OrderedFixtureInterface
{

    public function load(ObjectManager $manager)
    {
        // Agfa
        $configuration0 = new Configuration();
        $configuration0->setThemeName(ConfigurationDataProd::THEME_DEV)->setLogoAlignement('center')->setVideoTutorialRegister('https://www.youtube.com/embed/knHiro_yKlM?rel=0&amp;showinfo=0');
        $manager->persist($configuration0);

        $configuration0bis = new Configuration();
        $configuration0bis->setThemeName(ConfigurationDataProd::THEME_PREPROD)->setLogoAlignement('center')->setVideoTutorialRegister('https://www.youtube.com/embed/knHiro_yKlM?rel=0&amp;showinfo=0');
        $manager->persist($configuration0bis);

        $configuration0bisbis = new Configuration();
        $configuration0bisbis->setThemeName(ConfigurationDataProd::THEME_DEMO)->setLogoAlignement('center')->setVideoTutorialRegister('https://www.youtube.com/embed/knHiro_yKlM?rel=0&amp;showinfo=0');
        $manager->persist($configuration0bisbis);

        $this->addReference('configurationLab0', $configuration0);

        // Monlabo
        $configuration1 = new Configuration();
        $configuration1->setThemeName(ConfigurationDataProd::THEME_MONLABO)->setLogoAlignement('center')->setVideoTutorialRegister('https://www.youtube.com/embed/knHiro_yKlM?rel=0&amp;showinfo=0');

        $manager->persist($configuration1);
        $this->addReference('configurationLab1', $configuration1);

        // Reunilab
        $configuration2 = new Configuration();
        $configuration2->setThemeName(ConfigurationDataProd::THEME_REUNILAB)->setLogoAlignement('center')->setVideoTutorialRegister('https://www.youtube.com/embed/knHiro_yKlM?rel=0&amp;showinfo=0');

        $manager->persist($configuration2);
        $this->addReference('configurationLab2', $configuration2);

        // Biocentre
        $configuration3 = new Configuration();
        $configuration3->setThemeName(ConfigurationDataProd::THEME_BIOCENTRE)->setLogoAlignement('center')->setVideoTutorialRegister('https://www.youtube.com/embed/knHiro_yKlM?rel=0&amp;showinfo=0');
        $manager->persist($configuration3);

        // Bionorma
        $configuration4 = new Configuration();
        $configuration4->setThemeName(ConfigurationDataProd::THEME_BIONORMA)->setLogoAlignement('center')->setVideoTutorialRegister('https://www.youtube.com/embed/knHiro_yKlM?rel=0&amp;showinfo=0');
        $manager->persist($configuration4);

        // Unilabs
        $configuration5 = new Configuration();
        $configuration5->setThemeName(ConfigurationDataProd::THEME_UNILABS)->setLogoAlignement('left')->setVideoTutorialRegister('https://www.youtube.com/embed/knHiro_yKlM?rel=0&amp;showinfo=0');

        $manager->persist($configuration5);
        $this->addReference('configurationLab5', $configuration5);

        // Bioaccess
        $configuration6 = new Configuration();
        $configuration6->setThemeName(ConfigurationDataProd::THEME_LABAZUR)->setLogoAlignement('center')->setVideoTutorialRegister('https://www.youtube.com/embed/knHiro_yKlM?rel=0&amp;showinfo=0');
        $manager->persist($configuration6);
        $this->addReference('configurationLab6', $configuration6);

        // Genbio
        $configuration7 = new Configuration();
        $configuration7->setThemeName(ConfigurationDataProd::THEME_GENBIO)->setLogoAlignement('center')->setVideoTutorialRegister('https://www.youtube.com/embed/knHiro_yKlM?rel=0&amp;showinfo=0');

        $manager->persist($configuration7);
        $this->addReference('configurationLab7', $configuration7);

        // Oriade
        $configuration8 = new Configuration();
        $configuration8->setThemeName(ConfigurationDataProd::THEME_ORIADE)->setLogoAlignement('center')->setVideoTutorialRegister('https://www.youtube.com/embed/knHiro_yKlM?rel=0&amp;showinfo=0');
        $manager->persist($configuration8);

        // SBL Bio
        $configuration9 = new Configuration();
        $configuration9->setThemeName(ConfigurationDataProd::THEME_SBLBIO)->setLogoAlignement('center')->setVideoTutorialRegister('https://www.youtube.com/embed/knHiro_yKlM?rel=0&amp;showinfo=0');
        $manager->persist($configuration9);

        // Bioardaisne
        $configuration10 = new Configuration();
        $configuration10->setThemeName(ConfigurationDataProd::THEME_BIOARDAISNE)->setLogoAlignement('center')->setVideoTutorialRegister('https://www.youtube.com/embed/knHiro_yKlM?rel=0&amp;showinfo=0');
        $manager->persist($configuration10);

        // LeChesnay
        $configuration11 = new Configuration();
        $configuration11->setThemeName(ConfigurationDataProd::THEME_LECHESNAY)->setLogoAlignement('center')->setVideoTutorialRegister('https://www.youtube.com/embed/knHiro_yKlM?rel=0&amp;showinfo=0');
        $manager->persist($configuration11);

        // Biolab
        $configuration12 = new Configuration();
        $configuration12->setThemeName(ConfigurationDataProd::THEME_BIOLAB)->setLogoAlignement('center')->setVideoTutorialRegister('https://www.youtube.com/embed/knHiro_yKlM?rel=0&amp;showinfo=0');
        $manager->persist($configuration12);

        // Axbioocean
        $configuration13 = new Configuration();
        $configuration13->setThemeName(ConfigurationDataProd::THEME_AXBIOOCEAN)->setLogoAlignement('center')->setVideoTutorialRegister('https://www.youtube.com/embed/knHiro_yKlM?rel=0&amp;showinfo=0');
        $manager->persist($configuration13);

        $manager->flush();
    }

    public function getOrder()
    {
        return 100;
    }
}