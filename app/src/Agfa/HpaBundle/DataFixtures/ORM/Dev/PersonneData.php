<?php
namespace Agfa\HpaBundle\DataFixtures\ORM\Dev;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\Persistence\ObjectManager;
use Agfa\HpaBundle\Entity\Personne;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;

class PersonneData extends AbstractFixture implements OrderedFixtureInterface
{

    public function load(ObjectManager $manager)
    {
        // ADMIN
        $personnes = array(
            array(
                'nom' => 'Hanks',
                'prenom' => 'Tom',
                'sexe' => 'h',
                'dateNaissance' => new \DateTime('9 july 1956'),
                'reference_name' => 'tomHanks'
            ),
//             array(
//                 'nom' => 'Ryan',
//                 'prenom' => 'Meg',
//                 'sexe' => 'f',
//                 'dateNaissance' => new \DateTime('19 november 1961'),
//                 'reference_name' => 'megRyan'
//             ),
//             array(
//                 'nom' => 'Diaz',
//                 'prenom' => 'Cameron',
//                 'sexe' => 'h',
//                 'dateNaissance' => new \DateTime('30 august 1972'),
//                 'reference_name' => 'cameronDiaz'
//             ),
//             array(
//                 'nom' => 'DiCaprio',
//                 'prenom' => 'Leonardo',
//                 'sexe' => 'h',
//                 'dateNaissance' => new \DateTime('11 november 1974'),
//                 'reference_name' => 'leonardoDiCaprio'
//             ),
//             array(
//                 'nom' => 'Law',
//                 'prenom' => 'Jude',
//                 'sexe' => 'h',
//                 'dateNaissance' => new \DateTime('29 december 1972'),
//                 'reference_name' => 'judeLaw'
    //             ),
            array(
                'nom' => 'Ford',
                'prenom' => 'Henry',
                'sexe' => 'h',
                'dateNaissance' => new \DateTime('30 july 1863'),
                'reference_name' => 'henryford'
            ),
            array(
                'nom' => 'Test',
                'prenom' => 'Virginie',
                'sexe' => 'f',
                'dateNaissance' => new \DateTime('14 january 1978 16:48:46'),
                'reference_name' => 'virginietest'
            )
        );

        // Add for admin
        foreach ($personnes as $data) {
            $personne = new Personne();
            $personne->setNom($data['nom'])
                ->setPrenom($data['prenom'])
                ->setDateNaissance($data['dateNaissance'])
                ->setSexe($data['sexe'])
                ->setUser($this->getReference('userAdm'));

            $manager->persist($personne);
            $this->addReference($data['reference_name'], $personne);
        }

        // LAB1
        $personnes = array(
            array(
                'nom' => 'Hitchcock',
                'prenom' => 'Alfred',
                'sex' => 'h',
                'dateNaissance' => new \DateTime('13 august 1899')
            )
        );

        // Add for admin
        foreach ($personnes as $data) {
            $personne = new Personne();
            $personne->setNom($data['nom'])
                ->setPrenom($data['prenom'])
                ->setDateNaissance($data['dateNaissance'])
                ->setSexe($data['sex'])
                ->setUser($this->getReference('userLab1'));

            $manager->persist($personne);
        }

        $manager->flush();
    }

    public function getOrder()
    {
        return 505;
    }
}