<?php
namespace Agfa\HpaBundle\DataFixtures\ORM\Dev;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Agfa\HpaBundle\Entity\Demande;

class DemandeData extends AbstractFixture implements OrderedFixtureInterface
{

    public function load(ObjectManager $manager)
    {
        $demandes = array(
            array(
                'request' => $this->getReference('request01'),
                'patient' => $this->getReference('patient01'),
                'etablissement' => $this->getReference('etab02'),
                'date_accueil' => new \DateTime("2017-01-01 12:00:00"),
                'date_prelevement' => new \DateTime("2017-01-01 12:00:00"),
                'bsrvr_constant_login' => 'abc123',
                'nom_patient' => 'Hanks',
                'prenom_patient' => 'Tom',
                'code' => '12345',
                'hash' => 'abcde',
                'editcr' => 'test',
                'compte_rendu' => 'test',
                'nodem' => 'Dem_01',
                'nodemx' => 'Dem_01x',
                'nom_prescripteur' => 'Dr Jekyll',
                'reference_name' => 'demande01'
            ),
            array(
                'request' => $this->getReference('request02'),
                'patient' => $this->getReference('patient01'),
                'etablissement' => $this->getReference('etab02'),
                'date_accueil' => new \DateTime("2017-09-01 12:00:00"),
                'date_prelevement' => new \DateTime("2017-09-01 12:00:00"),
                'bsrvr_constant_login' => 'abc123',
                'nom_patient' => 'Hanks',
                'prenom_patient' => 'Tom',
                'code' => '12345',
                'hash' => 'abcde',
                'editcr' => 'test',
                'compte_rendu' => 'test',
                'nodem' => 'Dem_02',
                'nodemx' => 'Dem_02x',
                'nom_prescripteur' => 'Dr Jekyll',
                'reference_name' => 'demande02'
            ),
            array(
                'patient' => $this->getReference('patient01'),
                'etablissement' => $this->getReference('etab03'),
                'date_accueil' => new \DateTime("2017-06-07 12:00:00"),
                'date_prelevement' => new \DateTime("2017-06-07 12:00:00"),
                'bsrvr_constant_login' => 'abc123',
                'nom_patient' => 'Hanks',
                'prenom_patient' => 'Tom',
                'code' => '12345',
                'hash' => 'abcde',
                'editcr' => 'test',
                'compte_rendu' => 'test',
                'nodem' => 'Dem_04',
                'nodemx' => 'Dem_04x',
                'nom_prescripteur' => 'Dr Jekyll',
                'reference_name' => 'demande03'
            )
        );

        foreach ($demandes as $data) {
            $demande = new Demande();
            $demande->setPatient($data['patient'])
                ->setEtablissement($data['etablissement'])
                ->setDateAccueil($data['date_accueil'])
                ->setDatePrelevement($data['date_prelevement'])
                ->setBsrvrConstantLogin($data['bsrvr_constant_login'])
                ->setNomPatient($data['nom_patient'])
                ->setPrenomPatient($data['prenom_patient'])
                ->setCode($data['code'])
                ->setHash($data['hash'])
                ->setEditcr($data['editcr'])
                ->setCompteRendu($data['compte_rendu'])
                ->setNomPatient($data['nom_patient'])
                ->setNodem($data['nodem'])
                ->setNodemx($data['nodemx'])
                ->setNomPrescripteur($data['nom_prescripteur']);

            if (isset($data['request'])) {
                $demande->addRequest($data['request']);
            }

            $manager->persist($demande);
            $this->addReference($data['reference_name'], $demande);
        }

        for ($i = 1; $i <= 300; $i ++) {

            $demande = new Demande();
            $demande->setPatient($this->getReference('patient01'))
                ->setEtablissement($this->getReference('etab03'))
                ->setDateAccueil(new \DateTime("2017-06-07 12:00:00"))
                ->setDatePrelevement(new \DateTime("2017-06-07 12:00:00"))
                ->setBsrvrConstantLogin($data['bsrvr_constant_login'])
                ->setNomPatient('Hanks')
                ->setPrenomPatient('Tom')
                ->setCode(99999)
                ->setHash('hash')
                ->setEditcr('test')
                ->setCompteRendu('test')
                ->setNodem("B1234567$i")
                ->setNodemx("B12345678$i"."x")
                ->setNomPrescripteur('M. Hyde');

            $manager->persist($demande);
            $this->addReference('Demande' . $i, $demande);
        }

        $manager->flush();
    }

    public function getOrder()
    {
        return 511;
    }
}