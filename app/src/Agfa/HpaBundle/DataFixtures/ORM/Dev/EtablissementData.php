<?php
namespace Agfa\HpaBundle\DataFixtures\ORM\Dev;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Agfa\HpaBundle\Entity\Etablissement;

/**
 * Add test data to table Etablissement
 */
class EtablissementData extends AbstractFixture implements OrderedFixtureInterface
{
    /**
     * {@inheritDoc}
     * @see \Doctrine\Common\DataFixtures\FixtureInterface::load()
     */
    public function load(ObjectManager $manager)
    {
        $etablissement = array(
            array(
                'nom' => 'Toulbio',
                'adresse' => '1 rue des carmes',
                'code_postal' => '31000',
                'ville' => 'Toulouse',
                'hxl_code_structure' => '11',
                'telephone' => '05.23.45.67.89',
                'bsrvr_code_labo' => 1234,
                'bsrvr_ref_etablissement' => '111111111',
                'libelle' => 'Entité number 1',
                'parent' => null,
                'reference_name' => 'etab01'
            ),
            array(
                'nom' => 'Montabio',
                'adresse' => '1 rue de Toulouse',
                'code_postal' => '31000',
                'ville' => 'Toulouse',
                'hxl_code_structure' => '22',
                'telephone' => '05.23.45.67.88',
                'bsrvr_code_labo' => 1234,
                'bsrvr_ref_etablissement' => '222222222',
                'libelle' => 'Labo number 1',
                'parent' => 'etab01',
                'reference_name' => 'etab02'
            ),
            array(
                'nom' => 'TestXml01',
                'adresse' => '1 rue de Paris',
                'code_postal' => '32124',
                'ville' => 'Orlean',
                'hxl_code_structure' => '1',
                'telephone' => '02.52.12.18.46',
                'bsrvr_code_labo' => 2345,
                'bsrvr_ref_etablissement' => '333333333',
                'libelle' => 'EntJuri02',
                'parent' => null,
                'reference_name' => 'etab03'
            ),
            array(
                'nom' => 'OldLab01',
                'adresse' => '1 rue de Paris',
                'code_postal' => '32124',
                'ville' => 'Orlean',
                'hxl_code_structure' => '1',
                'telephone' => '02.52.12.18.46',
                'bsrvr_code_labo' => 1234,
                'bsrvr_ref_etablissement' => '999999999',
                'libelle' => 'OldLab01',
                'parent' => null,
                'reference_name' => 'oldLab1'
            ),
            array(
                'nom' => 'OldLab02',
                'adresse' => '1 rue de Paris',
                'code_postal' => '32124',
                'ville' => 'Orlean',
                'hxl_code_structure' => '1',
                'telephone' => '02.52.12.18.46',
                'bsrvr_code_labo' => 1234,
                'bsrvr_ref_etablissement' => '999999999',
                'libelle' => 'OldLab02',
                'parent' => null,
                'reference_name' => 'OldLab02'
            ),
            array(
                'nom' => 'OldLab03',
                'adresse' => '1 rue de Paris',
                'code_postal' => '32124',
                'ville' => 'Orlean',
                'hxl_code_structure' => '1',
                'telephone' => '02.52.12.18.46',
                'bsrvr_code_labo' => 1234,
                'bsrvr_ref_etablissement' => '999999999',
                'libelle' => 'OldLab03',
                'parent' => null,
                'reference_name' => 'OldLab03'
            )
        );

        foreach ($etablissement as $data) {
            $etablissementEntity= new Etablissement();
            $etablissementEntity->setNom($data['nom'])
                ->setAdresse($data['adresse'])
                ->setCodePostal($data['code_postal'])
                ->setVille($data['ville'])
                ->setHxlCodeStructure($data['hxl_code_structure'])
                ->setBsrvrCodeLabo($data['bsrvr_code_labo'])
                ->setTelephone($data['telephone'])
                ->setBsrvrRefEtablissement($data['bsrvr_ref_etablissement'])
                ->setLibelle($data['libelle'])
            ;

                if($data['parent']){
                    $etablissementEntity->setParent($this->getReference($data['parent']));
                }

                $manager->persist($etablissementEntity);
                $this->addReference($data['reference_name'], $etablissementEntity);
        }

        $manager->flush();
    }

    /**
     * {@inheritDoc}
     * @see \Doctrine\Common\DataFixtures\OrderedFixtureInterface::getOrder()
     */
    public function getOrder()
    {
        return 110;
    }
}