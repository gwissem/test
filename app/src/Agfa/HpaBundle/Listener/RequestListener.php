<?php
namespace Agfa\HpaBundle\Listener;

use Symfony\Component\HttpKernel\HttpKernelInterface;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;

/**
 * Launch on each request to check and update data.
 */
class RequestListener
{

    protected $session;

    protected $router;

    protected $maxIdleTime;

    public function __construct(SessionInterface $session, RouterInterface $router, AuthorizationChecker $authorizationChecker, $maxIdleTime = 0)
    {
        $this->session = $session;
        $this->router = $router;
        $this->authorizationChecker = $authorizationChecker;
        $this->maxIdleTime = $maxIdleTime;
    }

    /**
     * Update the last_login user field to keep user logged as long as they are active on the site.
     *
     * @param GetResponseEvent $event
     */
    public function onKernelRequest(GetResponseEvent $event)
    {
        if (! $event->isMasterRequest()) {
            return;
        }

        if ($this->maxIdleTime > 0) {
            $this->session->start();

            $lapse = time() - $this->session->getMetadataBag()->getLastUsed();

            if ($lapse > $this->maxIdleTime && $this->authorizationChecker->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
                // Logout the user
                $event->setResponse(new RedirectResponse($this->router->generate('fos_user_security_logout')));
            }
        }
    }
}