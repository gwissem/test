<?php
namespace Agfa\HpaBundle\Listener;

use Symfony\Component\HttpKernel\Event\FilterResponseEvent;

/**
 * Executed on every request.
 */
class ResponseListener
{
    /**
     * Denies access to pages within frames not hosted on the main hosting.
     * @param FilterResponseEvent $event
     */
    public function onKernelResponse(FilterResponseEvent $event)
    {
        // Empèche le contenu du site d'être intégré dans des iframes
        $event->getResponse()->headers->set('x-frame-options', 'deny');

        return;
    }
}