<?php
namespace Agfa\HpaBundle\Listener\Entity;

use Agfa\HpaBundle\Entity\Configuration;
use Doctrine\Common\Persistence\Event\LifecycleEventArgs;
use Agfa\HpaBundle\Manager\ConfigurationManager;

class ConfigurationListener
{

    public function postPersist(LifecycleEventArgs $args)
    {
        $entity = $args->getObject();

        if (! $entity instanceof Configuration) {
            return;
        }

        // Delete cache
        $em = $args->getObjectManager();
        $cacheMgr = $em->getConfiguration()->getResultCacheImpl();

        $cacheMgr->delete(ConfigurationManager::CACHE_KEY_GETCONFIGURATION);
    }

    public function postUpdate(LifecycleEventArgs $args)
    {
        $entity = $args->getObject();

        if (! $entity instanceof Configuration) {
            return;
        }

        // Delete cache
        $em = $args->getObjectManager();
        $cacheMgr = $em->getConfiguration()->getResultCacheImpl();

        $cacheMgr->delete(ConfigurationManager::CACHE_KEY_GETCONFIGURATION);
    }

    public function postRemove(LifecycleEventArgs $args)
    {
        $entity = $args->getObject();

        if (! $entity instanceof Configuration) {
            return;
        }

        // Delete cache
        $em = $args->getObjectManager();
        $cacheMgr = $em->getConfiguration()->getResultCacheImpl();

        $cacheMgr->delete(ConfigurationManager::CACHE_KEY_GETCONFIGURATION);
    }
}
