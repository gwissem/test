<?php
namespace Agfa\HpaBundle\Listener\Entity;

use Agfa\HpaBundle\Entity\Demande;
use Doctrine\Common\Persistence\Event\LifecycleEventArgs;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Agfa\HpaBundle\Services\Cache;

use Symfony\Component\Cache\Adapter\AdapterInterface;
use Agfa\UserBundle\Entity\User;

class DemandeListener
{

    public function __construct(TokenStorage $tokenStorage, AdapterInterface $cacheApp)
    {
        $this->tokenStorage = $tokenStorage;
        $this->cacheApp = $cacheApp;
    }

    public function postPersist(LifecycleEventArgs $args)
    {
        $entity = $args->getObject();

        if (! $entity instanceof Demande) {
            return;
        }

        // Get user and make unique id
        if(!$this->tokenStorage->getToken()){
            return;
        }

        $user = $this->tokenStorage->getToken()->getUser();

        // Get cache ids
        if($user instanceof User && $user->getCurrentPersonne()){
            $cacheKey = Cache::getCacheKey(Cache::KEY_SEARCH_FORM_VARIABLES, array('person_id' => $user->getCurrentPersonne()->getId()));

            // ...to remove the entry in the cache
            $this->cacheApp->deleteItem($cacheKey);
        }
    }

    public function postUpdate(LifecycleEventArgs $args)
    {
        $entity = $args->getObject();

        if (! $entity instanceof Demande) {
            return;
        }

        // Get user and make unique id
        if(!$this->tokenStorage->getToken()){
            return;
        }

        $user = $this->tokenStorage->getToken()->getUser();

        // Get cache ids
        if($user instanceof User && $user->getCurrentPersonne()){
            $cacheKey = Cache::getCacheKey(Cache::KEY_SEARCH_FORM_VARIABLES, array('person_id' => $user->getCurrentPersonne()->getId()));

            // ...to remove the entry in the cache
            $this->cacheApp->deleteItem($cacheKey);
        }
    }

    public function postRemove(LifecycleEventArgs $args)
    {
        $entity = $args->getObject();

        if (! $entity instanceof Demande) {
            return;
        }

        // Get user and make unique id
        if(!$this->tokenStorage->getToken()){
            return;
        }

        $user = $this->tokenStorage->getToken()->getUser();

        // Get cache ids
        if($user instanceof User && $user->getCurrentPersonne()){
            $cacheKey = Cache::getCacheKey(Cache::KEY_SEARCH_FORM_VARIABLES, array('person_id' => $user->getCurrentPersonne()->getId()));

            // ...to remove the entry in the cache
            $this->cacheApp->deleteItem($cacheKey);
        }
    }
}
