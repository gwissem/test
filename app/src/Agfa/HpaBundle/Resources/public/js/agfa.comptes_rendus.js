agfa('ComptesRendus', function () {
	var oComptesRendus = this;
	
	/** 
	 * ici on initialise ce qu'il faut pour les autres méthodes
	 * le DOM n'est pas chargé
	 */ 
	oComptesRendus.init = function () {
	}
	
	/**
	 * ici on continue à initialiser
	 * le DOM est chargé
	 */		
	oComptesRendus.ready = function () {
		$('body').scrollspy({
			target: '#scrollspy',
			offset: 62
		});
		$('#scrollspy').affix({
			offset: {
				top: $('#scrollspy').offset().top - 62
			}
	    }).css('width', $('#col-form-enrolement').width());
		
		$('#scrollspy').on('activate.bs.scrollspy', function () {
			var $active = $(this).find('li.active');
			var $collapse = $active.closest('.collapse');
			if(! $collapse.hasClass('in')) {
				$collapse.collapse('show');
			}
		});
		
		var jump = $('.init-focus').attr('href');
		if(jump) {
			location.hash = jump;
		}
	}
	
});
