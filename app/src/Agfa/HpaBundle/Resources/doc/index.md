Sommaire
========

    * [Release note](ReleaseNote/index.md)
    * Les roles(app/roles.md)
    * LAPI Bioserveur-HPA(Echanges-HP-BS v1.2.md)
    

Modification avant la mise en production
========================================

app/config/config.yml - Les lignes suivantes doivent être décommentées pour le passage de l'appli sur un serveur utilisant une base de données Oracle.

	services:
	    my.listener:
	        class: Doctrine\DBAL\Event\Listeners\OracleSessionInit
	        tags:
	            - { name: doctrine.event_listener, event: postConnect }
	        arguments:
	            - { NLS_SORT: "FRENCH_M_AI", NLS_COMP: "LINGUISTIC" }
	    twig.extension.intl:
	        class: Twig_Extensions_Extension_Intl
	        tags:
	            - { name: twig.extension }