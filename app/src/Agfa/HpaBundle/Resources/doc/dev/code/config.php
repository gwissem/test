<?php

use Sami\Sami;
use Sami\Parser\Filter\TrueFilter;
use Sami\RemoteRepository\GitHubRemoteRepository;
use Sami\Version\GitVersionCollection;
use Symfony\Component\Finder\Finder;

/*
 * This will look for all PHP files in the directory and in folders
 */
$iterator = Finder::create()
    ->files()
    ->name('*.php')
    ->exclude('Resources')
    ->exclude('Tests')
    ->exclude('Scheb')
    ->in($dir = '/home/charles/workspace/hexapat-reloaded/app/src');



// Add versions
$versions = GitVersionCollection::create($dir)
->add('1.0.2-docker', '1.0.2')
->add('1.1-docker', '2.0.0');

/*
 * This will set up some options in the documentation
 */
$sami = new Sami($iterator, array(
    'title' => 'HPA', // This will be the name of the docs
    'versions' => $versions,
    'build_dir' => __DIR__ . '/build/sf3/%version%', // This will be the folder the docs will be put in
    'cache_dir' => __DIR__ . '/cache/sf3/%version%', // This will be generated by sami, but you can .gitignore it
    'default_opened_level' => 2,
));

/*
 * Include this section if you want sami to document
 * private and protected functions/properties
 */
$sami['filter'] = function () {
    return new TrueFilter();
};

return $sami;
