﻿é# Flux d'échange entre Hexapat et Bioserveur

## Principes de communication

Tous les échanges se font en méthode POST, avec une requête au format JSON et donnent lieu à une réponse au format JSON.

Les requêtes contiennent des informations d'authentification dans une propriété `authentification` à la racine.

Les réponses contiennent toujours les propriétés suivantes à la racine:

* `statut`: une chaîne qui indique le statut de la réponse:
    * `OK` si la requête est réussie
    * `BAD_REQUEST` si la requête est malformée
    * `UNAUTHORIZED` si les clés d'authentification sont refusées
    * `UNKNOWN_ERROR` en cas d'erreur inconnue
    * autres valeurs en fonction de la requête
* `message_erreur`: une chaîne qui donne plus d'informations sur le statut.

### Réenvois

Lorsqu'une requête n'obtient pas de réponse, elle doit être retentée, jusqu'à un maximum de 3 réenvois pour la même requête.

## Principes d'authentification

### Authentification de Hexapat par Bioserveur

Hexapat envoie dans chaque requête une propriété `authentification` à la racine JSON, dont la valeur est :

    {
        "passphrase": "..."
    }

Cette **passphrase Hexapat** est définie par Bioserveur et stockée dans le paramétrage de Hexapat. Elle est unique à chaque instance de Hexapat.

### Authentification de Bioserveur par Hexapat

Bioserveur envoie dans chaque requête une propriété `authentification` à la racine JSON, dont la valeur est :

    {
        "passphrase": "..."
    }

Cette **passphrase Bioserveur** est définie par Hexapat et stockée dans le paramétrage de Bioserveur. Elle est unique à chaque instance de Hexapat.

L'adresse IP de Bioserveur est comparée à une whitelist des IP autorisées à communiquer avec Hexapat.

### Erreur d'authentification

Lorsque les clés d'authentification sont refusées, le serveur doit répondre avec `"statut": "UNAUTHORIZED"`.

## Description d'un compte rendu

Lorsqu'un compte rendu est transmis, il doit correspondre à la structure JSON suivante:

    {
        "compte_rendu": {
            "contenu": {
                "format": "editcr"/"hprim"/"pdf",
                "base64": true/false,
                "data": "..."
            },
            "numero_demande": "...",
            "identifiant_structure": "...",
            "identifiant_patient": "...",
            "solde_patient": 1.23,
            "source_paiement": "...",
            "entite_juridique": {
                "finess": "...",
                "code_ej": "...",
                "code_postal": "...",
                "raison_sociale": "...",
                "adresse": "...",
                "libelle": "..."
            },
            "laboratoire": {
                "finess": "...",
                "nom": "...",
                "paiement": {
                    "paiement_affichage_cr": true/false,
                    "paiement_actif": true/false,
                    "tpe": "...",
                    "cle": "...",
                    "societe": "...",
                    "nom": "...",
                    "prenom": "...",
                    "email": "...",
                    "telephone": "..."
                }
            }
        }
    }

La propriété `contenu` est optionnelle.

* `data` vaut le contenu brut du compte rendu

* `format` décrit le format du compte rendu
    * `editcr` : format editCR de Hexalis
    * `hprim` : format standardisé HPRIM
    * `pdf` : compte rendu au format PDF sans données structurées

* `base64` vaut `true` si la valeur de `data` a été encodée en base64

* `solde_patient` Nombre entier ou décimale au format anglo-saxon. Exemple : 10 ou 10.45
* `source_paiement` peut valoir 'not_paid' pour les comptes rendus non payés ou 'labo' pour les comptes rendus payés au laboratoire

* `entite_juridique`
    * `finess` : le code finess de l'entité juridique (9 caractères)
    * `code_ej` : le code Bioserveur représentant un groupe de laboratoire partageant un finess juridique avec un autre groupe de laboratoires
    * `code_postal`, `raison_sociale`, `adresse` : les données propres à l'entité juridique
    * `libelle` : le libellé donné à l'entité juridique

* `laboratoire`
    * `finess` : le code finess de l'entité juridique (9 caractères)
    * `nom` : le nom du laboratoire

* `paiement`
    * `paiement_affichage_cr` : true si le compte rendu est disponible pour le patient avant que ce dernier n'ai payé, false sinon
    * `paiement_actif` : le paiement est-il actif ou pas
    * `societe`, `tpe` et `cle` : données de paiement Monetico
    * `nom`, `prénom`, `email` et `téléphone` : données relative à l'entité recevant le paiement

## Format des timestamps

Les timestamps sont au format strftime `%Y%m%d%H%M%S` (14 chiffres).

# Description des flux d'échange - Bioserveur vers HPA

## Enrolement

### Objectif

Hexapat envoie une clé CR pour enrôler le patient correspondant et obtenir le CR en réponse

### URL

#### Pré-production

    https://securetest.bioserveur.com/bioserveur-access/wshxp_enrol.pl

#### Production

    https://secure.bioserveur.com/bioserveur-access/wshxp_enrol.pl

### Requête

    {
        "authentification": {
            "passphrase": "..."
        },
        "patient_login": "...",
        "patient_password": "...",
        "creer_enrolement": true/false
    }

### Traitement

Si la clé CR est correcte et si le labo peut envoyer à cet Hexapat, il enregistre l'enrolement du patient (si `creer_enrolement` vaut `true`), puis renvoie le dernier CR du patient.

### Réponse

    {
        "statut": "...",
        "message_erreur": "...",
        "message_patient": "...",
        "compte_rendu": {
            "identifiant_structure": "...",
            "identifiant_patient": "...",
            "numero_demande": "...",
            "solde_patient": 1.23,
            "source_paiement": "...",
            "contenu": "...",
             "entite_juridique": {
                "finess": "...",
                "code_ej": "...",
                "code_postal": "...",
                "raison_sociale": "...",
                "adresse": "...",
                "libelle": "..."
            },
            "laboratoire": {
                "finess": "...",
                "nom": "...",
                "paiement": {
                    "paiement_affichage_cr": true/false,
                    "paiement_actif": true/false,
                    "tpe": "...",
                    "cle": "...",
                    "societe": "...",
                    "nom": "...",
                    "prenom": "...",
                    "email": "...",
                    "telephone": "..."
                }
            }
        }
    }

Codes de statut spécifiques :

* `MAUVAIS_IDENTIFIANTS` si le couple login/mdp du patient est invalide
* `LABO_INTERDIT` si ce laboratoire ne peut pas envoyer sur cet Hexapat
* `DEJA_PUBLIE` si ce CR a déjà été publié par Bioserveur et n'est plus disponible

Si le statut est `OK`, la réponse contient une propriété `compte_rendu` qui contient le compte rendu avec son contenu (voir §3).
Si le statut est `MAUVAIS_IDENTIFIANTS`, la réponse définit le champ `message_patient` qui est affiché au patient.
Le statut `DEJA_PUBLIE` indique que Bioserveur est dans l'impossibilité de livrer le fichier EditCR de la demande.
Ce cas peut survenir pour une seconde demande d'enrôlement d'un dossier ou pour une demande d'enrôlement d'une structure non configurée HPA.

## Publication

### Objectif

Bioserveur envoie un nouveau CR pour un patient enrôlé. Si Hexapat indique que l'enrolement n'est plus valable en réponse, Bioserveur doit le supprimer.

### URL

`/biologie/api/publication_compte_rendu`

### Requête

    {
        "authentification": {
            "passphrase": "..."
        },
        "compte_rendu": {
            "identifiant_structure": "...",
            "identifiant_patient": "...",
            "numero_demande": "...",
            "solde_patient": 1.23,
            "source_paiement": "...",
            "contenu": "...",
            "entite_juridique": {
                "finess": "...",
                "code_ej": "...",
                "code_postal": "...",
                "raison_sociale": "...",
                "adresse": "...",
                "libelle": "..."
            },
            "laboratoire": {
                "finess": "...",
                "nom": "...",
                "paiement": {
                    "paiement_affichage_cr": true/false,
                    "paiement_actif": true/false,
                    "tpe": "...",
                    "cle": "...",
                    "societe": "...",
                    "nom": "...",
                    "prenom": "...",
                    "email": "...",
                    "telephone": "..."
                }
            }
        }
    }

### Traitement

Hexapat recherche le patient concerné dans sa base. Si le patient existe et a un enrôlement valide, le CR est ajouté.

### Réponse

    {
        "statut": "...",
        "message_erreur": "...",
        "supprimer_enrolement": true/false
    }

Codes de statut spécifiques :

* `LABO_INCONNU` si le labo n'existe pas dans la base
* `PATIENT_INCONNU` si le patient n'existe pas dans la base
* `UTILISATEUR_DESACTIVE` si l'utilisateur est désactivé
* `QUOTA_ATTEINT` si le quota de stockage de l'utilisateur est atteint

La réponse contient une propriété booléenne `supprimer_enrolement`. Si elle vaut `true`, Bioserveur doit supprimer l'enrolement qui a déclenché la publication.

## Notification de suppression

### Objectif

Bioserveur notifie Hexapat qu'un CR a été supprimé. Hexapat doit totalement supprimer les données correspondantes de son côté.

### URL

`/biologie/api/notification_suppression`

### Requête

    {
        "authentification": {
            "passphrase": "..."
        },
        "notification_suppression": {
            "timestamp": "...",
            "compte_rendu": {
                "identifiant_structure": "...",
                "identifiant_patient": "...",
                "numero_demande": "..."
            }
        }
    }

### Traitement

Hexapat supprime la demande et son CR et les résultats rattachés à la demande. Il enregistre une trace de l'action dans son journal.

### Réponse

    {
        "statut": "...",
        "message_erreur": "..."
    }

Codes de statut spécifiques :

* `LABO_INCONNU` si le labo n'existe pas dans la base
* `PATIENT_INCONNU` si le patient n'existe pas dans la base



# Description des flux d'échange - HPA vers Bioserveur

## Notification de lectures

### Objectif

Hexapat notifie Bioserveur qu'un utilisateur a lu une liste de comptes rendus. Une seule notification est envoyée pour chaque document, la première fois que le compte rendu est lu. Sachant que si un compte rendu est mis à jour par Bioserveur, il revient à l'état *Non lu*.

### URL

#### Pré-production

    https://securetest.bioserveur.com/bioserveur-access/wshxp_rdnotif.pl

#### Production

    https://secure.bioserveur.com/bioserveur-access/wshxp_rdnotif.pl

### Requête

    {
        "authentification": {
            "passphrase": "..."
        },
        "notification_lectures": [
            {
                "timestamp": "...",
                "compte_rendu": {
                    "identifiant_structure": "...",
                    "identifiant_patient": "...",
                    "numero_demande": "..."
                }
            },
            ...
        ]
    }

### Traitement

Bioserveur enregistre les notifications de lecture, en ignorant celles qui concernent des comptes rendus épurés.

### Réponse

    {
        "statut": "...",
        "message_erreur": "..."
    }

## Mise à jour d'un paiement

### Objectif

Hexapat informe Bioserveur des paiements effectués sur hexapat. Cette mise à jour comprend le solde du dossier, la date du paiement et le numéro de la transaction.

### URL

#### Pré-production

    https://securetest.bioserveur.com/bioserveur-access/wshxp_paynotif.pl

#### Production

    https://secure.bioserveur.com/bioserveur-access/wshxp_paynotif.pl

### Requête

    {
        "authentification": {
            "passphrase": "..."
        },
        "notification_paiement": [
            {
                "timestamp": "...",
                "compte_rendu": {
                    "identifiant_structure": "...",
                    "identifiant_patient": "...",
                    "numero_demande": "...",
                    "solde": "...",
                    "numeros_transactions": [
                        "...",
                    ]
                }
            }
	]
    }

### Traitement

Bioserveur enregistre le paiement en ligne du dossier, le numéro de la transaction associé et le nouveau solde.

### Réponse

    {
        "statut": "...",
        "message_erreur": "...",
        "ignore_nodemx": ["...", "..."]
    }

Codes de statut spécifiques :

* `LABO_INCONNU` si le labo n'existe pas dans la base
* `PATIENT_INCONNU` si le patient n'existe pas dans la base
* `DEMANDE_INCONNU` si la demande n'existe pas dans la base


## Historique des révisions

    - 1.0  01/06/2015 Version initiale
    - 1.1  09/06/2016 Ajout code de retour DEJA_PUBLIE pour l'API de l'enrôlement
    - 1.2  21/06/2017 Ajout de l'entité juridique pour les retours de Bioserveur 'Enrolement' et 'Publication'
    - 1.3  14/09/2017 Ajout des données relatives au paiement en ligne. Suppression de la partie 'Envoi de SMS'
    - 1.3a 19/09/2017 Révisions suite à relecture
