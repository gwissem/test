Release note - Version 2.2.0, oct. 2018
======================================

   * MAJ de Symfony de la version Sf3.3 à Sf3.4
   * Thèmes pour les pages d'erreurs
   * Message d'erreur indiquant au patient sur quel site il peut créer son compte (même chose que pour un accès direct)
   * MAJ de l'adresse email sur Bioserveur au changement de l'adresse email par l'utilisateur.
   * Optimisation des requètes SLQ
   * Mise en place d'une queue pour les notifications de désenrôlement à BioServeur
   * Ajout d'un regex pour une validation plus forte des adresses emails dans les formulaires.
