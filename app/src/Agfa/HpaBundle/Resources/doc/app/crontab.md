@daily . /opt/hexapatdev/VARENV_HEXAPAT; /opt/hexapatdev/logicielstiers/bin/php -q /opt/hexapatdev/hexapat/htdocs/bin/console hpa:prune:devices > /dev/null 2>&1
@daily . /opt/hexapatdev/VARENV_HEXAPAT; /opt/hexapatdev/logicielstiers/bin/php -q /opt/hexapatdev/hexapat/htdocs/bin/console hpa:prune:users 3 > /dev/null 2>&1
@daily . /opt/hexapatdev/VARENV_HEXAPAT; /opt/hexapatdev/logicielstiers/bin/php -q /opt/hexapatdev/hexapat/htdocs/bin/console hpa:piwik:db-size > /dev/null 2>&1

*/5 9-18 * * 1-5 . /opt/hexapatdev/VARENV_HEXAPAT; /opt/hexapatdev/logicielstiers/bin/php -q /opt/hexapatdev/hexapat/htdocs/bin/console hpa:bsrvr:update-payments > /dev/null 2>&1
*/5 9-18 * * 1-5 . /opt/hexapatdev/VARENV_HEXAPAT; /opt/hexapatdev/logicielstiers/bin/php -q /opt/hexapatdev/hexapat/htdocs/bin/console hpa:bsrvr:update-reading > /dev/null 2>&1

