Roles
=====

  * ROLE_ADMIN : admin system, a accès à tout
  * ROLE_DEBUG : accès aux information de déboguage
  * ROLE SUPPORT : accès aux outils d'administration du système
  * ROLE_LABO : pas de besoins pour l'instant
  * ROLE_PATIENT : compte d'un utilisateur authentifié