# API

phpunit -c phpunit.xml

## Enrolement / réception et mise à jour d'un compte-rendu

    - Une entrée dans Paiement est créée si aucune entrée existante n'est trouvée via le code TPE.
    - Si une entrée dans Paiement existe, cette dernière est mise à jour
    - Les données concernant les laboratoires, chapitres, examens, elements et resultats ne sont jamais mis à jour mais sont créées à la première réception d'un compte-rendu.
    
## Mise à jour de l'entité juridique
    - Les données concernant une entité juridique sont toujours mises à jours.
    - Seul les données suivantes sont mises à jour concernant un laboratoire: affichage CR et le paiement lié.
    - Un laboratoire ne peut pas être retiré d'une entité juridique. Si c'est le cas, HPA ne le prend pas en compte.
    - Un laboratoire peut être déplacé d'une entité juridique vers une qutre sur Bioserveur et cette mise à jour sera prise en compte par HPA créant des soucis de cohérences avec les anciens compte rendus patient.

    
# Générale

## Contrôle d'intégrité des données
    - Toutes les demandes doivent avoir un laboratoire lié
