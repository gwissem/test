<?php
namespace Agfa\HpaBundle\Twig;

use Agfa\HpaBundle\Entity\Demande;
use Agfa\UserBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Translation\Translator;

class AppExtension extends \Twig_Extension
{
//     public function __construct(Translator $translator){
//         $this->translator = $translator;
//     }

    public function getFilters()
    {
        return array(
            new \Twig_SimpleFilter('boolean', array($this,'booleanFilter')),
            new \Twig_SimpleFilter('formatPaieSource', array($this,'formatPaieSourceFilter')),
            new \Twig_SimpleFilter('formatPhoneNumberFr', array($this,'formatPhoneNumberFrFilter')),
            new \Twig_SimpleFilter('getDebugInfo', array($this,'getDebugInfoFilter')),
            new \Twig_SimpleFilter('hasAdvancedRights', array($this,'hasAdvancedRightsFilter')),
        );
    }

    public function booleanFilter($value)
    {
        if ($value == true) {
            return "twig_filter.boolean.enabled";
        } else {
            return "twig_filter.boolean.disabled";
        }
    }

    public function formatPaieSourceFilter($value){
        switch($value){
            case Demande::PAID_ADMIN:
                return 'par l\'opérateur';
                break;
            case Demande::PAID_LABORATORY:
                return 'au laboratoire';
                break;
            case Demande::PAID_ONLINE:
                return 'par l\'opérateur';
                break;
            case Demande::PAID_UNKNOWN:
                return 'inconnu';
                break;
            default:
                return 'non payé';
        }
    }

    public function formatPhoneNumberFrFilter($phoneNumber){
        if(strlen($phoneNumber) == 10){
            $chunks = str_split($phoneNumber, 2);
            $phoneNumber = implode('.', $chunks);
        }

        return $phoneNumber;
    }

    public function getDebugInfoFilter(Demande $demande){
        $pieces = array();

        if ($demande->getNodemx()) $pieces[] = "Nodemx : " . $demande->getNodemx();
        if ($demande->getNodem()) $pieces[] = "Nodem : " . $demande->getNodem() . ' (' . $demande->getId() . ')';
        $pieces[] = "Nopat : " . $demande->getPatient()->getBsrvrNopat() . ' (' . $demande->getPatient()->getId() . ')';
        $pieces[] = "Code labo : " . $demande->getEtablissement()->getBsrvrCodeLabo() . ' (' . $demande->getEtablissement()->getId() . ')';
        if($demande->getEtablissement()->getParent()) $pieces[] = "Entité juridique : " . $demande->getEtablissement()->getParent()->getId();
        if($demande->getEtablissement()->getParent()) $pieces[] = "Finess EJ : " . $demande->getEtablissement()->getParent()->getBsrvrRefEtablissement();
        if ($demande->getEtablissement()->getBsrvrRefEtablissement()) $pieces[] = "Finess labo : " . $demande->getEtablissement()->getBsrvrRefEtablissement();

        $debugInfo = implode("\n", $pieces);

        return $debugInfo;

    }

    public function hasAdvancedRightsFilter($roles){
        $mainRole = '';

        switch ($roles){
            case in_array(User::ROLE_SUPER_ADMIN, $roles):
            case in_array(User::ROLE_ADMIN, $roles):
            case in_array(User::ROLE_SUPPORT, $roles):
            case in_array(User::ROLE_LABO, $roles):
                $mainRole = '<span class="text-danger" data-toggle="tooltip" title="Compte avec accès aux fonctionnalités de support."><i class="fa fa-support"></i></span>';
                break;
        }

        return $mainRole;
    }

    public function getName()
    {
        return 'app_extension';
    }
}

