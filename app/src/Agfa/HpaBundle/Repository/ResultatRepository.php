<?php
namespace Agfa\HpaBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Agfa\HpaBundle\Entity\Personne;
use Agfa\UserBundle\Entity\User;

/**
 * Resultat repository
 */
class ResultatRepository extends EntityRepository
{

    /**
     * Used in :
     * - Agfa\HpaBundle\Form\Type\RechercheDemandeType
     *
     * @param Personne $personne
     * @return \Doctrine\ORM\QueryBuilder
     */
    public function getResultats(User $user, Personne $personne)
    {
        $query = $this->createQueryBuilder('r')
            ->join('r.element', 'el')->addSelect('el')
            ->join('el.examen', 'ex')->addSelect('ex')
//             ->join('ex.chapitre', 'ch')
            ->join('r.demande', 'de')
            ->join('de.patient', 'pa')
            ->join('pa.personne', 'pe')
            ->where('pe = :personne')
//             ->andWhere('ch.libelle IS NOT NULL')
            ->andWhere('pa.user = :user')
            ->setParameter('user', $user)
            ->setParameter('personne', $personne)
            ->orderBy('ex.libelle', 'asc');

        return $query;
    }
}