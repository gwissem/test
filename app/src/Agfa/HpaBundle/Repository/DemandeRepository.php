<?php
namespace Agfa\HpaBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Agfa\UserBundle\Entity\User;
use Agfa\HpaBundle\Entity\Personne;
use Agfa\HpaBundle\Entity\Patient;
use Agfa\MoneticoPaiementBundle\Entity\Response;
use Agfa\HpaBundle\Entity\BsrvrCom;

/**
 * Get data from hpa_demande and tables that are linked.
 */
class DemandeRepository extends EntityRepository
{
    /**
     * Result of searching for a demande using the admin part.
     */
    public function getDemandesAnonymously($nodemx, $laboCode = null)
    {
        $fields = array(
            'PARTIAL de.{id, dateAccueil, datePrelevement, bsrvrConstantLogin, nomPatient, prenomPatient, dateNaissancePatient, sexePatient, inscPatient, code, hash, isNew, isUpdated, nomPrescripteur, nodem, nodemx, paieSolde, paieSource, affichageCr, createdAt, updatedAt}',
            'pa', 'us', 'et'
        );

        $query = $this->createQueryBuilder('de')
            ->select($fields)
            ->join('de.patient', 'pa')
            ->join('pa.user', 'us')
            ->join('de.etablissement', 'et')
            ->where('de.nodemx = :nodemx')
            ->setParameter('nodemx', $nodemx);

        if ($laboCode) {
            $query->andWhere('et.bsrvrCodeLabo = :laboCode')->setParameter('laboCode', $laboCode);
        }

        return $query;
    }

    /**
     * Used in the admin to show related demandes.
     */
    public function getAllDemandes($user)
    {
        $fields = array(
            'PARTIAL de.{id, dateAccueil, datePrelevement, bsrvrConstantLogin, nomPatient, prenomPatient, dateNaissancePatient, sexePatient, inscPatient, code, hash, isNew, isUpdated, nomPrescripteur, nodem, nodemx, paieSolde, paieSource, affichageCr, createdAt, updatedAt}',
            'pa', 'us', 'et'
        );

        $query = $this->createQueryBuilder('de')
            ->select($fields)
            ->join('de.patient', 'pa')
            ->join('pa.user', 'us')
            ->join('de.etablissement', 'et')
            ->where('us = :user')
            ->setParameter('user', $user)
            ->orderBy('de.dateAccueil', 'desc');

        return $query;
    }

    /**
     * Get a list of demandes for a given user and personne.
     * Can be limited in number of results.
     *
     * @param User $user
     * @param Personne $personne
     * @param integer $limit
     * @return \Doctrine\ORM\QueryBuilder
     */
    public function getDemandes(User $user, Personne $personne, $limit = NULL)
    {
        $fields = array(
            'PARTIAL de.{id, dateAccueil, datePrelevement, bsrvrConstantLogin, nomPatient, prenomPatient, dateNaissancePatient, sexePatient, inscPatient, code, hash, isNew, isUpdated, nomPrescripteur, nodem, nodemx, paieSolde, paieSource, affichageCr, createdAt, updatedAt}',
            'pa', 'pe', 'etla', 'pp', 'paie', 'etej'
        );

        $query = $this->createQueryBuilder('de')
            ->select($fields)
            ->join('de.patient', 'pa')
            ->join('pa.patientPersonnes', 'pp')
            ->join('pp.personne', 'pe')
            ->join('pe.user', 'us')
            ->join('de.etablissement', 'etla')
            ->leftJoin('etla.parent', 'etej') // All labs should be linked to EJ but we never know...
            ->leftJoin('etla.paiement', 'paie')
            ->where('pp.personne = :personne')
            ->andWhere('us = :user')
            ->orderBy('de.isNew', 'DESC')
            ->addOrderBy('MONTH(de.dateAccueil)', 'ASC')
            ->addOrderBy('de.dateAccueil', 'DESC')
            ->setParameter('personne', $personne)
            ->setParameter('user', $user);

        if ($limit) {
            $query->setMaxResults($limit);
        }

        return $query;
    }

    /**
     * Used in :
     * - DemandeController:DeleteAction : used to check if a patient still has some demandes before eventually removing it
     *
     * @param Patient $patient
     * @return \Doctrine\ORM\QueryBuilder
     */
    public function getPatientDemandes(Patient $patient)
    {
        $fields = array(
            'PARTIAL de.{id}',
        );

        $query = $this->createQueryBuilder('de')
            ->select($fields)
            ->join('de.patient', 'pa')
            ->where('pa = :patient')
            ->setParameter('patient', $patient);

        return $query;
    }

    /**
     * Used to display filtered demandes on the user homepage
     *
     * @param User $user
     * @param Personne $personne
     * @param integer $year
     * @param integer $month
     * @return \Doctrine\ORM\QueryBuilder
     */
    public function getDemandesByMonth(User $user, Personne $personne, $year, $month)
    {
        $date = new \DateTime("$year-$month-01");
        $dateStart = $date;
        $dateEnd = $date->format('Y-m-t') . " 23:59:59";

        $query = $this->getDemandes($user, $personne)
            ->andWhere('de.dateAccueil BETWEEN :dateStart and :dateEnd')
            ->setParameter('dateStart', $dateStart)
            ->setParameter('dateEnd', $dateEnd);

        return $query;
    }

    /**
     * Used in admin to get one demande.
     *
     * @param User $user
     * @param integer $demandeId
     * @return \Doctrine\ORM\QueryBuilder
     */
    public function getDemande(User $user, $demandeId)
    {
        // We need to get the CR as it is also used to download the file.
        // Joints are also necessary to check that payment has been done, ...
        $query = $this->createQueryBuilder('de')
            ->addSelect('ex')
            ->addSelect('ch')
            ->addSelect('el')
            ->addSelect('re')
            ->addSelect('de')
            ->addSelect('pa')
            ->addSelect('pp')
            ->addSelect('pe')
            ->addSelect('et')
            ->leftJoin('de.resultats', 're')
            ->leftJoin('re.element', 'el')
            ->leftJoin('el.examen', 'ex')
            ->leftJoin('ex.chapitre', 'ch')
            ->join('de.patient', 'pa')
            ->join('pa.patientPersonnes', 'pp')
            ->join('pp.personne', 'pe')
            ->join('pe.user', 'us')
            ->join('de.etablissement', 'et')
            ->leftJoin('et.paiement', 'pai')
            ->where('de.id = :demandeId')
            ->andWhere('us = :user')
            ->setParameter('demandeId', $demandeId)
            ->setParameter('user', $user);

        return $query;
    }

    /**
     * Get Demand details that the user is alowed to access (that has been paid when the payment module is enabled).
     *
     * @param User $user
     * @param integer $demandeId
     * @return \Doctrine\ORM\QueryBuilder
     */
    public function getDemandeToDownload(User $user, $demandeId)
    {
        $query = $this->getDemande($user, $demandeId)->andWhere(
            '(de.paieSolde = 0 OR
                    de.isNew = FALSE OR
                    pai.actif = FALSE OR
                    pai.actif IS NULL OR
                    (pai.actif = TRUE AND de.paieSolde > 0 AND (pai.affichageCr = TRUE OR de.affichageCr = TRUE )))');

        return $query;
    }

    /**
     * Used to find out if a compte rendu has already been integrated by an other user.
     * Used in:
     * - EnrolementController
     *
     * @param User $user
     * @param integer $demandeId
     * @return \Doctrine\ORM\QueryBuilder
     */
    public function getDemandeOnOtherUserAccount(User $user, $demandeId)
    {
        $query = $this->createQueryBuilder('de')
            ->join('de.patient', 'pa')
            ->join('pa.patientPersonnes', 'pp')
            ->join('pp.personne', 'pe')
            ->join('pe.user', 'us')
            ->where('de.id = :demandeId')
            ->andWhere('us <> :user')
            ->setParameter('demandeId', $demandeId)
            ->setParameter('user', $user);

        return $query;
    }

    /**
     * Used in
     * - AgfaHpaBundle:Demande:recherche
     */
    public function getDemandesResultats(User $user, Personne $personne)
    {
        $fields = array(
            'PARTIAL de.{id, dateAccueil, nomPatient, prenomPatient, isNew, nomPrescripteur, paieSolde, affichageCr, updatedAt}',
            'PARTIAL el.{id}',
            'PARTIAL re.{id}',
            'PARTIAL ex.{id, libelle}',
            'PARTIAL pa.{id, nom, prenom}',
            'PARTIAL et.{id, nom, adresse, telephone, ville}',
        );

        $query = $this->createQueryBuilder('de')
            ->select($fields)
            ->leftJoin('de.resultats', 're')
            ->leftJoin('re.element', 'el')
            ->leftJoin('el.examen', 'ex')
            ->join('de.patient', 'pa')
            ->join('pa.patientPersonnes', 'pp')
            ->join('pp.personne', 'pe')
            ->join('pe.user', 'us')
            ->leftJoin('de.etablissement', 'et')
            ->where('pe = :personne')
            ->andWhere('us = :user')
            ->orderBy('de.isNew', 'DESC')
            ->addOrderBy('de.dateAccueil', 'DESC')
            ->setParameter('user', $user)
            ->setParameter('personne', $personne)
        ;

        return $query;
    }

    /**
     * Get demande for "rapprochement" with a personne if it exists
     */
    public function getDemandeForRapprochement(User $user, $demandeId)
    {
        $query = $this->createQueryBuilder('de')
            ->join('de.patient', 'pa')
            ->join('pa.patientPersonnes', 'pp')
            ->join('pp.personne', 'pe')
            ->join('pe.user', 'peus')
            ->join('pa.user', 'paus')
            ->where('peus = :user')
            ->andWhere('paus = :user')
            ->andWhere('de.id = :demandeId')
            ->setParameter('user', $user)
            ->setParameter('demandeId', $demandeId);

        return $query;
    }

    /**
     *
     * @param User $user
     * @param Personne $personne
     * @param array $criterias
     * @return \Doctrine\ORM\QueryBuilder
     */
    public function searchDemandes(User $user, Personne $personne, array $criterias)
    {
        $query = $this->getDemandesResultats($user, $personne);

        if ($nomPrescripteur = $criterias['prescripteur']) {
            $query->andWhere('de.nomPrescripteur LIKE :nomPrescripteur')->setParameter('nomPrescripteur',
                $nomPrescripteur);
        }

        if ($laboratoireId = $criterias['laboratoire']) {
            $query->andWhere('et.id = :laboratoireId')->setParameter('laboratoireId', $laboratoireId);
        }

        if ($examenId = $criterias['examen']) {
            $query->andWhere('ex.id = :examenId')->setParameter('examenId', $examenId);
        }

        // Filter using date
        $datePrelevementDebut = $criterias['datePrelevementDebut'] . ' 00:00:00';

        $datePrelevementDebut = \DateTime::createFromFormat('d/m/Y H:i:s', $datePrelevementDebut);
        $datePrelevementFin = $criterias['datePrelevementFin'] . ' 23:59:59';
        $datePrelevementFin = \DateTime::createFromFormat('d/m/Y H:i:s', $datePrelevementFin);

        $query->andWhere('de.datePrelevement BETWEEN :datePrelevementDebut AND :datePrelevementFin')
            ->setParameter('datePrelevementDebut', $datePrelevementDebut)
            ->setParameter('datePrelevementFin', $datePrelevementFin);

        return $query;
    }

    /**
     * Below for Monetico Paiement
     */

    /**
     * Get a demande that has not been paid yet and can be paid online.
     * Used in
     * - Payment:attente
     *
     * @param User $user
     * @param integer $demandeId
     * @return \Doctrine\ORM\QueryBuilder
     */
    public function getUnpaidDemande(User $user, $demandeId)
    {
        $fields = array(
            'PARTIAL de.{id, dateAccueil, datePrelevement, bsrvrConstantLogin, nomPatient, prenomPatient, dateNaissancePatient, sexePatient, inscPatient, code, hash, isNew, isUpdated, nomPrescripteur, nodem, nodemx, paieSolde, paieSource, affichageCr, createdAt, updatedAt}',
            'PARTIAL pa.{id, nom, prenom}',
            'PARTIAL us.{id}',
            'PARTIAL et.{id}',
            'PARTIAL pai.{id, tpe}',
            'PARTIAL pp.{id}',
            'PARTIAL et.{id, nom}'
        );

        $query = $this->createQueryBuilder('de')
            ->select($fields)
            ->join('de.patient', 'pa')
            ->join('pa.user', 'us')
            ->join('de.etablissement', 'et')
            ->leftJoin('et.paiement', 'pai')
            ->leftJoin('pa.patientPersonnes', 'pp')
            ->where('us = :user')
            ->andWhere('de.paieSolde > 0')
            ->andWhere('pai.actif = TRUE')
            ->andWhere('de.id = :demandeId')
            ->setParameter('user', $user)
            ->setParameter('demandeId', $demandeId);

        return $query;
    }

    /**
     * Get all demandes that have not been paid yet and can be paid online.
     * Used in
     * - Payment:attente
     *
     * @param User $user
     * @return \Doctrine\ORM\QueryBuilder
     */
    public function getUnpaidDemandes(User $user)
    {
        $fields = array(
            'PARTIAL de.{id, dateAccueil, datePrelevement, bsrvrConstantLogin, nomPatient, prenomPatient, dateNaissancePatient, sexePatient, inscPatient, code, hash, isNew, isUpdated, nomPrescripteur, nodem, nodemx, paieSolde, paieSource, affichageCr, createdAt, updatedAt}',
            'et'
        );

        $query = $this->createQueryBuilder('de')
            ->select($fields)
            ->join('de.patient', 'pa')
            ->join('pa.user', 'us')
            ->join('de.etablissement', 'et')
            ->leftJoin('et.paiement', 'pai')
            ->leftJoin('pa.patientPersonnes', 'pp')
            ->where('us = :user')
            ->andWhere('de.paieSolde > 0')
            ->andWhere('pai.actif = TRUE')
            ->setParameter('user', $user);

        return $query;
    }

    /**
     * Get a list of paid demandes.
     * Used in
     * - Payment:history
     *
     * @param User $user
     * @return \Doctrine\ORM\QueryBuilder
     */
    public function getPaidDemandes(User $user)
    {
        $fields = array(
            'PARTIAL de.{id, dateAccueil, datePrelevement, bsrvrConstantLogin, nomPatient, prenomPatient, dateNaissancePatient, sexePatient, inscPatient, code, hash, isNew, isUpdated, nomPrescripteur, nodem, nodemx, paieSolde, paieSource, affichageCr, createdAt, updatedAt}',
            'et', 'pa', 'res', 'req'
        );

        $query = $this->createQueryBuilder('de')
            ->select($fields)
            ->join('de.patient', 'pa')
            ->join('pa.user', 'us')
            ->join('de.etablissement', 'et')
            ->join('de.requests', 'req')
            ->join('req.response', 'res')
            ->where('us = :user')
            ->andWhere("res.codeRetour = :paiement OR res.codeRetour = :payetest")
            ->setParameter('user', $user)
            ->setParameter('paiement', Response::CODE_RETOUR_PAIEMENT)
            ->setParameter('payetest', Response::CODE_RETOUR_PAYETEST)
            ->orderBy('res.createdAt', 'desc');

        return $query;
    }

    /**
     * Find a demande based on the reference ID provided by Monetico Paiement.
     * Used in:
     * - Paiement:history
     *
     * @param string $moneticoReference
     * @return \Doctrine\ORM\QueryBuilder
     */
    public function getDemandeByReference($moneticoReference)
    {
        $query = $this->createQueryBuilder('de')
            ->join('de.requests', 're')
            ->where('re.reference = :reference')
            ->setParameter('reference', $moneticoReference);

        return $query;
    }

    /**
     * Find a demande when we do not know any details from HPA.
     * Used :
     * - to remove a demande when bioserveur asks: ApiController:deleteCr
     *
     * @param string $bsrvrConstLogin
     * @param int $bsrvrCodeLabo
     * @return \Doctrine\ORM\QueryBuilder
     */
    public function findDemande($bsrvrConstLogin, $nodemx)
    {
        $fields = array(
            'PARTIAL de.{id}',
            'PARTIAL pa.{id}',
            'PARTIAL us.{id, email}'
        );

        $query = $this->createQueryBuilder('de')
            ->select($fields)
//             ->join('de.etablissement', 'et')
            ->join('de.patient', 'pa')
            ->join('pa.user', 'us')
            ->where('de.bsrvrConstantLogin = :bsrvrConstLogin')
            ->andWhere('de.nodemx = :nodemx')
            ->setParameter('bsrvrConstLogin', $bsrvrConstLogin)
            ->setParameter('nodemx', $nodemx);

        return $query;
    }

    /**
     * Used by
     * - admin/DemandeController: admin panel to get a list of demandes for a given user
     *
     * @param User $user
     * @return \Doctrine\ORM\QueryBuilder
     */
    public function getUserDemandes(User $user)
    {
        $fields = array(
            'PARTIAL de.{id, dateAccueil, datePrelevement, bsrvrConstantLogin, nomPatient, prenomPatient, dateNaissancePatient, sexePatient, inscPatient, code, hash, isNew, isUpdated, nomPrescripteur, nodem, nodemx, paieSolde, paieSource, affichageCr, createdAt, updatedAt}',
            'et', 'pa', 'res', 'req'
        );

        $query = $this->createQueryBuilder('de')
            ->select($fields)
            ->join('de.patient', 'pa')
            ->join('pa.user', 'us')
            ->join('de.etablissement', 'et')
            ->addSelect('et')
            ->where('us = :user')
            ->setParameter('user', $user);

        return $query;
    }

    /**
     * Get all demandes that are waiting to be updated on Bioserveur side.
     *
     * @return \Doctrine\ORM\QueryBuilder
     */
    public function getDemandsForBsrvrUpdateReading($limit=50)
    {
        $fields = array(
            'PARTIAL de.{id, dateAccueil, datePrelevement, bsrvrConstantLogin, nomPatient, prenomPatient, dateNaissancePatient, sexePatient, inscPatient, code, hash, isNew, isUpdated, nomPrescripteur, nodem, nodemx, paieSolde, paieSource, affichageCr, createdAt, updatedAt}',
            'bc'
        );

        $query = $this->createQueryBuilder('de')
            ->select($fields)
            ->join('de.bsrvrComs', 'bc')
            ->where('bc.action = :action')
            ->andWhere('bc.attempt < :nbAttempts')
            ->setParameter('action', BsrvrCom::ACTION_UPDATE_READING)
            ->setParameter('nbAttempts', BsrvrCom::MAX_NUMBER_ATTEMPS);

            if(!is_null($limit)){
                $query->setMaxResults($limit);
            }

        return $query;
    }

    /**
     *
     * @return \Doctrine\ORM\QueryBuilder
     */
    public function getDemandsForBsrvrUpdatePayment($limit=50)
    {
        $fields = array(
            'PARTIAL de.{id, dateAccueil, datePrelevement, bsrvrConstantLogin, nomPatient, prenomPatient, dateNaissancePatient, sexePatient, inscPatient, code, hash, isNew, isUpdated, nomPrescripteur, nodem, nodemx, paieSolde, paieSource, affichageCr, createdAt, updatedAt}',
            'req', 'bc'
        );

        $query = $this->createQueryBuilder('de')
            ->select($fields)
            ->join('de.bsrvrComs', 'bc')
            ->join('de.requests', 'req')
            ->join('req.response', 'res')
            ->where('bc.action = :action')
            ->andWhere('bc.attempt < :nbAttempts')
            ->setParameter('action', BsrvrCom::ACTION_UPDATE_PAYMENT)
            ->setParameter('nbAttempts', BsrvrCom::MAX_NUMBER_ATTEMPS);

            if(!is_null($limit)){
                $query->setMaxResults($limit);
            }

        return $query;
    }
}