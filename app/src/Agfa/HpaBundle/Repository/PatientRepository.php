<?php
namespace Agfa\HpaBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Agfa\UserBundle\Entity\User;

class PatientRepository extends EntityRepository
{

    /**
     * Used in:
     * - PatientControlle:assigneAction
     *
     * @param User $user
     * @param integer $patientId
     * @return \Doctrine\ORM\QueryBuilder
     */
    public function getPatient(User $user, $patientId)
    {
        $query = $this->createQueryBuilder('pa')
            ->join('pa.user', 'us')
            ->where('us = :user')
            ->andWhere('pa.id = :patientId')
            ->setParameter('user', $user)
            ->setParameter('patientId', $patientId);

        return $query;
    }

    function getPatientByInsc(User $user, $patientInsc)
    {
        $query = $this->createQueryBuilder('pa')
            ->join('pa.patientPersonnes', 'pp')
            ->join('pp.personne', 'pe')
            ->join('pe.user', 'peus')
            ->where('pe.user = :user')
            ->andWhere('pa.insc = :patientInsc')
            ->setParameter('user', $user)
            ->setParameter('patientInsc', $patientInsc);

        return $query;
    }

    /**
     * Renvoie un patient du compte qui n'a pas de personne de rattachement
     * Used in :
     * - AgfaUserBundle:EventListener/interactiveLoginListener
     * - PatientController:reassignPatient
     *
     * @param User $user
     * @return \Doctrine\ORM\QueryBuilder
     */
    public function findPatientSansPersonne(User $user)
    {
        $patients = $this->createQueryBuilder('pa')
            ->leftJoin('pa.patientPersonnes', 'pp')
            ->where('pa.user = :user')
            ->andWhere('pp.id IS NULL')
            ->setParameter('user', $user);

        return $patients;
    }

    public function findPatientFromPersonalDetails(User $user, $nom, $prenom, $dateNaissance)
    {
        // Nom, prenom and dateNaissance have to be the details from the personne as the ones from the patient can be modified by the user
        $query = $this->createQueryBuilder('pa')
            ->join('pa.patientPersonnes', 'pp')
            ->join('pp.personne', 'pe')
            ->join('pe.user', 'peus')
            ->where('pe.user = :user')
            ->andWhere('pe.nom = :nom')
            ->andWhere('pe.prenom = :prenom')
            ->andWhere('pe.dateNaissance = :dateNaissance')
            ->setParameter('user', $user)
            ->setParameter('nom', $nom)
            ->setParameter('prenom', $prenom)
            ->setParameter('dateNaissance', $dateNaissance->setTime(0,0,0));

        return $query;
    }
}