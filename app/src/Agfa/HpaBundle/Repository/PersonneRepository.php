<?php
namespace Agfa\HpaBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Agfa\UserBundle\Entity\User;

/**
 * PersonneRepository
 */
class PersonneRepository extends EntityRepository
{

    public function getPersonnes(User $user)
    {
        $query = $this->createQueryBuilder('pe')
            ->join('pe.user', 'us')
            ->where('us = :user')
            ->orderBy('pe.nom', 'asc')
            ->addOrderBy('pe.prenom', 'asc')
            ->setParameter('user', $user);

        return $query;
    }

    public function getPersonne(User $user, $personneId)
    {
        $fields = array(
            'PARTIAL pe.{id, nom, prenom}',
            'PARTIAL us.{id}'
        );

        $query = $this->createQueryBuilder('pe')
            ->select($fields)
            ->join('pe.user', 'us')
            ->where('us = :user')
            ->andWhere('pe.id = :personneId')
            ->setParameter('user', $user)
            ->setParameter('personneId', $personneId);

        return $query;
    }

    /**
     * Get data for the menu
     *
     * @param User $user
     * @return \Doctrine\ORM\QueryBuilder
     */
    public function getMenuPersonnesData(User $user)
    {
        $fields = array(
            'PARTIAL de.{id, isNew}',
            'PARTIAL pe.{id, nom, prenom}',
            'PARTIAL pa.{id}',
            'PARTIAL pp.{id}',
            'PARTIAL ow.{id}'
        );

        $query = $this->getPersonnes($user)
            ->select($fields)
            ->leftJoin('pe.owner', 'ow')
            ->leftJoin('pe.personnePatients', 'pp')
            ->leftJoin('pp.patient', 'pa')
            ->leftJoin('pa.demandes', 'de');

        return $query;
    }

    /**
     * Used on page personne_index (that lists the personne attached to the user)
     *
     * @param User $user
     * @return \Doctrine\ORM\QueryBuilder
     */
    public function getDemandes(User $user)
    {
        $query = $this->createQueryBuilder('pe')
            ->addSelect('pa')
            ->addSelect('pp')
            ->addSelect('de')
            ->addSelect('ow')
            ->addSelect('de')
            ->addSelect('et')
            ->join('pe.user', 'us')
            ->leftJoin('pe.owner', 'ow')
            ->leftJoin('pe.personnePatients', 'pp')
            ->leftJoin('pp.patient', 'pa')
            ->leftJoin('pa.demandes', 'de')
            ->leftJoin('de.etablissement', 'et')
            ->where('us = :user')
            ->setParameter('user', $user)
            ->orderBy('pe.nom')
            ->addOrderBy('pe.prenom');

        return $query;
    }
}