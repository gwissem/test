<?php
namespace Agfa\HpaBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Agfa\UserBundle\Entity\User;

/**
 * Request repository
 */
class RequestRepository extends EntityRepository
{

    /**
     * Get a Monetico request belonging to the specified user (usualy the currently authenticated user)
     *
     * @param User $user
     * @param integer $requestId
     * @return \Doctrine\ORM\QueryBuilder
     */
    public function getUserRequest(User $user, $requestId)
    {
        $query = $this->createQueryBuilder('re')
            ->join('re.demandes', 'de')
            ->join('de.patient', 'pa')
            ->join('pa.user', 'us')
            ->where('re.id = :requestId')
            ->setParameter('requestId', $requestId);

        return $query;
    }
}