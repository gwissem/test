<?php
namespace Agfa\HpaBundle\Repository;

use Agfa\HpaBundle\Entity\Etablissement;
use Agfa\HpaBundle\Entity\Paiement;

/**
 * Etablissement Repository
 */
class EtablissementRepository extends \Doctrine\ORM\EntityRepository
{

    /**
     * Find a Etablissement using its ioServeur etablissement reference number.
     *
     * @param string $bsrvrRefEtablissement
     * @return \Doctrine\ORM\QueryBuilder
     */
    public function getEtablissement($bsrvrRefEtablissement)
    {
        $query = $this->createQueryBuilder('et')
            ->where('et.bsrvrRefEtablissement = :bsrvrRefEtablissement')
            ->setParameter('bsrvrRefEtablissement', $bsrvrRefEtablissement);

        return $query;
    }

    /**
     * Update/add data of/to a Etablissement entity.
     *
     * @param Etablissement $etablissement
     * @param array $etablissementDetails
     * @return \Agfa\HpaBundle\Entity\Etablissement
     */
    public function updateEtablissement(Etablissement $etablissement, array $etablissementDetails)
    {
        // Data userd is finess for Entité Juridique and login for Laboratoires.
        $bsrvrRefEtablissement = isset($etablissementDetails['finess']) ? $etablissementDetails['finess'] : $etablissementDetails['login'];

        $etablissement->setBsrvrRefEtablissement($bsrvrRefEtablissement)
            ->setCodePostal($etablissementDetails['code_postal'])
            ->setBsrvrCodeEntiteJuridique($etablissementDetails['code_ej'])
            ->setNom($etablissementDetails['raison_sociale'])
            ->setAdresse($etablissementDetails['adresse'])
            ->setLibelle($etablissementDetails['libelle']);

        return $etablissement;
    }

//     /**
//      * Reset payment link of Etablissement entry to null.
//      *
//      * @param Paiement $payment
//      * @return \Doctrine\DBAL\Query\QueryBuilder QueryBuilder
//      */
//     public function unlinkEtablissementPaiement(Paiement $payment)
//     {
//         $query = $this->createQueryBuilder('et')
//             ->update('Agfa\HpaBundle\Entity\Etablissement', 'et')
//             ->set('et.paiement', '?1')
//             ->where('et.paiement = ?2')
//             ->setParameter(1, NULL)
//             ->setParameter(2, $payment->getId())
//             ->getQuery();

//         return $query->execute();
//     }

//     public function unlinkLabsFromEntiteJuridique(Etablissement $entiteJuridique)
//     {
//         $query = $this->createQueryBuilder('et')
//             ->update('Agfa\HpaBundle\Entity\Etablissement', 'et')
//             ->set('et.parent', '?1')
//             ->where('et.parent = ?2')
//             ->setParameter(1, NULL)
//             ->setParameter(2, $entiteJuridique)
//             ->getQuery();

//         return $query->execute();
//     }
}
