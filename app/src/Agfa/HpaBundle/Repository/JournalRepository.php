<?php
namespace Agfa\HpaBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Agfa\UserBundle\Entity\User;
use Agfa\HpaBundle\Entity\Journal;
use Doctrine\ORM\QueryBuilder;

class JournalRepository extends EntityRepository
{

    public function getUserEntries(User $user)
    {
        $query = $this->createQueryBuilder('jo')
            ->join('jo.user', 'us')
            ->where('jo.user = :user')
            ->orderBy('jo.createdAt', 'DESC')
            ->addOrderBy('jo.id', 'DESC')
            ->setParameter('user', $user);

        return $query;
    }

    public function getUserEntry(User $user, Journal $journal)
    {
        $query = $this->createQueryBuilder('jo')
            ->join('jo.user', 'us')
            ->where('jo.user = :user')
            ->andWhere('jo = :journal')
            ->setParameter('journal', $journal)
            ->setParameter('user', $user);

        return $query;
    }

    /**
     * Get the list of successfull authentication for a given user.
     *
     * @param User $user
     * @return \Doctrine\ORM\QueryBuilder
     */
    public function getUserAuthentication(User $user)
    {
        $query = $this->createQueryBuilder('jo')
            ->join('jo.user', 'us')
            ->where('us = :user')
            ->andWhere('jo.type = :eventType')
            ->andWhere('jo.level = :eventLevel')
            ->setParameter('user', $user)
            ->setParameter('eventType', Journal::EVENT_USER_LOGIN)
            ->setParameter('eventLevel', Journal::EVENT_LEVEL_SUCCESS)
            ->orderBy('jo.createdAt', 'desc')
            ->setMaxResults(10);

        return $query;
    }

    /**
     * Used by getAllButLastEntries in order to keep the x most recent entries.
     *
     * @param User $user
     * @param integer $nbEntriesToKeep
     * @return \Doctrine\ORM\QueryBuilder
     */
    public function getLastEntries(User $user, $nbEntriesToKeep, $nbMonthToKeep)
    {
        // Date max to keep the entries
        $date = new \DateTime($nbMonthToKeep . ' month ago');

        // Minus 1 for the entry that has just been created by the user who is logging in
        $nbEntriesToKeep = $nbEntriesToKeep > 1 ? $nbEntriesToKeep - 1 : 0;

        $query = $this->createQueryBuilder('njo')
            ->select('njo.id')
            ->where('njo.user = :user')
            ->andWhere('njo.createdAt > :date')
            ->setParameter('user', $user)
            ->setParameter('date', $date)
            ->setMaxResults($nbEntriesToKeep)
            ->orderBy('njo.createdAt', 'desc');

        return $query;
    }

    /**
     * Get entries to remove from the journal (all but getLastEntries)
     *
     * @param User $user
     * @param array $lastEntries
     * @return \Doctrine\ORM\QueryBuilder
     */
    public function getAllButLastEntries(User $user, $lastEntries)
    {
        $qb = $this->createQueryBuilder('jo');

        $query = $qb->where('jo.user = :user')->setParameter('user', $user);

        // Removes all if settings is 0. However, even if we clear entries, they will still be written during the user's session.
        // So if setting is 0, you will immediately get the entry of the user connection.
        if (count($lastEntries) > 0) {
            $qb->andWhere($qb->expr()
                ->notIn('jo.id', ':lastEntries'))
                ->setParameter('lastEntries', $lastEntries);
        }

        return $query;
    }
}