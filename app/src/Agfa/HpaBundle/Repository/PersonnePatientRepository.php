<?php
namespace Agfa\HpaBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Agfa\UserBundle\Entity\User;

class PersonnePatientRepository extends EntityRepository
{

    public function getPersonnePatient(User $user, $personneId, $patientId)
    {
        $query = $this->createQueryBuilder('pp')
            ->addSelect('pa')
            ->addSelect('pe')
            ->addSelect('ow')
            ->join('pp.personne', 'pe')
            ->join('pp.patient', 'pa')
            ->leftJoin('pe.owner', 'ow')
            ->where('pa.user = :user')
            ->andWhere('pe.user = :user')
            ->andWhere('pa.id = :patientId')
            ->andWhere('pe.id = :personneId')
            ->setParameter('user', $user)
            ->setParameter('personneId', $personneId)
            ->setParameter('patientId', $patientId);

        return $query;
    }

    /**
     * Find out if a patient is still being used by its owner or shared with other users efore deleting it.
     * Will be useful to check if a personne is being used before deleting it (has patient and thus, reports)
     * FIXME Has to be implemented in PersonneController:delete (is in PatientController but disabled for now)
     *
     * @param User $user
     * @param integer $patientId
     */
    public function getPersonneIsUsed(User $user, $personneId)
    {
        $query = $this->createQueryBuilder('pp')
            ->join('pp.personne', 'pe')
            ->where('pe.user = :user')
            ->andWhere('pe.id = :personneId')
            ->setParameter('user', $user)
            ->setParameter('personneId', $personneId);

        return $query;
    }

    /**
     * Find out if a patient is already assign to a personne and thus, has an owner
     * Used in :
     * - PatientController:assignAction
     * - PersonneController:DeleteAction
     *
     * @param integer $patientId
     * @return \Doctrine\ORM\QueryBuilder
     */
    public function getPatientIsUsed($patientId)
    {
        $query = $this->createQueryBuilder('pp')
            ->where('pp.patient = :patientId')
            ->setParameter('patientId', $patientId);

        return $query;
    }
}