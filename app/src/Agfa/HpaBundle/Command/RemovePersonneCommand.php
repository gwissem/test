<?php
namespace Agfa\HpaBundle\Command;

use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;

/**
 * Removes a personne and all its related data.
 */
class RemovePersonneCommand extends ContainerAwareCommand
{

    /**
     * Settings for the command.
     *
     * {@inheritdoc}
     * @see \Symfony\Component\Console\Command\Command::configure()
     */
    protected function configure()
    {
        $this->setName('hpa:remove:personne')
            ->setDescription('Supprime une personne, ses patients et ses comptes rendus')
            ->addArgument('personne_id', InputArgument::REQUIRED, 'Id du user');
    }

    /**
     * Execute the command.
     *
     * {@inheritdoc}
     * @see \Symfony\Component\Console\Command\Command::execute()
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {

        /* @var $em \Doctrine\ORM\EntityManager */
        $em = $this->getContainer()
            ->get('doctrine')
            ->getManager();

        $personne_id = $input->getArgument('personne_id');
        $personne = $em->getRepository('AgfaHpaBundle:Personne')->find($personne_id);
        if (! $personne) {
            $output->writeln("<error>Impossible de trouver personne [$personne_id].</error>");
            return;
        }

        $em->remove($personne);
        $em->flush();

        $output->writeln(("<info>Done</info>"));
    }
}
