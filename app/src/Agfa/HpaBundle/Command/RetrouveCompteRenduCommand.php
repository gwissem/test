<?php

namespace Agfa\HpaBundle\Command;

use Symfony\Component\Console\Input\InputArgument;
// use Symfony\Component\Console\Input\InputInterface;
// use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;

class RetrouveCompteRenduCommand extends ContainerAwareCommand
{

    protected function configure()
    {
        $this
        ->setName('hpa:retrieve:cr')
        ->setDescription('Reproduit un compte rendu stocké dans la base')
        ->addArgument(
            'demande_id',
            InputArgument::REQUIRED,
            'Id du compte rendu'
        )
        ;
    }

    // FIXME Where does $user_id come from ? Check and fix.
//     protected function execute(InputInterface $input, OutputInterface $output)
//     {

//     	/* @var $em \Doctrine\ORM\EntityManager */
//     	$em = $this->getContainer()->get('doctrine')->getManager();

//     	$demande_id = $input->getArgument('demande_id');
//     	$demande = $em->getRepository('AgfaHpaBundle:Demande')->find($demande_id);
//     	if(!$demande) {
//     		$output->writeln("<error>Impossible de trouver la demande [$user_id].</error>");
//     		return;
//     	}

//     	$editCR = simplexml_load_string(gzuncompress(stream_get_contents($demande->getEditcr())));
//     	$compteRenduPdf = stream_get_contents($demande->getCompteRendu());

//     	$editCR->documentCompteRendu = base64_encode($compteRenduPdf);

//     	$output->writeln($editCR->asXML());
//     }
}