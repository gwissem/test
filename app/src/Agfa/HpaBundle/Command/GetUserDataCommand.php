<?php

namespace Agfa\HpaBundle\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
// use Symfony\Component\Console\Input\InputInterface;
// use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;

class GetUserDataCommand extends ContainerAwareCommand
{

    protected function configure()
    {
        $this
        ->setName('hpa:export:user-data')
        ->setDescription("Récupère toutes les données d'un compte")
        ->addArgument(
            'user_id',
            InputArgument::REQUIRED,
            "Id de l'utilisateur"
        )
        ;
    }

    // TODO Not finished
//     protected function execute(InputInterface $input, OutputInterface $output)
//     {

//         /* @var $em \Doctrine\ORM\EntityManager */
//         $em = $this->getContainer()->get('doctrine')->getManager();

//         $userId = $input->getArgument('user_id');


//         $data = $em->getRepository('AgfaUserBundle:User')->find($userId);

//         $query2 = $em->getRepository('AgfaUserBundle:User')->createQueryBuilder('u')
//             ->where('u.id = :userId')
//             ->setParameter('userId', $userId)
//             ->getQuery();


//             $user = clone $query2;
//             $em->persist($user);
//             $sql = $user->getSql();

// //         $result = "
// //             insert into hpa_user (
// //             id,
// //             username,
// //             username_canonical,
// //             email,
// //             email_canonical,
// //             enabled,
// //             salt,
// //             password,
// //             last_login,
// //             locked,
// //             expired,
// //             expires_at,
// //             confirmation_token,
// //             password_requested_at,
// //             roles,
// //             credentials_expired,
// //             credentials_expires_at,
// //             date_creation,
// //             auth_code,
// //             trusted
// //             ) values(
// //             ".
// // //             $query->getId() .",".
// // //             $query->getUsername() .",".
// // //             $query->getUsernameCanonical() .",".
// // //             $query->getemail() .",".
// // //             $query->getEmailCanonical() .",".
// // //             $query->isEnabled() .",".
// // //             $query->getSalt() .",".
// // //             $query->getPassword() .",".
// // //             $query->getLastLogin()->format('Y-M-d H:i:s') .",".
// // //             $query->isLocked() .",".
// // //             $query->isExpired() .",".
// //             "null,".
// //             $query->getConfirmationToken() .",".
// //             $query->getPasswordRequestedAt() .",".
// // //             $query->getRoles() .",".
// //             $query->isCredentialsExpired() .",".
// //             "null,".
// // //             $query->getCreatedAt()->format('Y-M-d H:i:s') .",".
// // //             $query->getAuthCode() .",".
// //             $query->getTrusted() .
// //             ");";


// //         if(!$query) {
// //             $output->writeln("<error>Impossible de trouver l'utilisateur [$userId].</error>");
// //             return;
// //         }



//         $output->writeln($sql);
//     }
}