Ce qui fonctionne
=================

    - DatabaseConfigCommand (hpa:dbconf)
    - PruneUsersCommand (hpa:prune:users)
    - PruneDevicesCommand (hpa:prune:devices) # NOT USED AS OF 27/06/2018 - TOO GREADY - REPLACED BY LOGIN LISTENER
    - BsrvrUpdateDemandeIsRead
    - BsrvrUpdateDemandePayment
    - UpdateUserEmailCommand (hpa:user:update-email)

A terminer
----------

    - GetUserDataCommand (hpa:export:user-data)
    - RetrouverCompteRenduCommande


Cron à mettre en place
----------------------

// Toutes les minutes
php bin/console hpa:bsrvr:update-reading
php bin/console hpa:bsrvr:update-payments

// Une fois par jour, la nuit
php bin/console hpa:piwik:db-size
php bin/console hpa:prune:devices
php bin/console hpa:prune:users 7

# A développer
PrunePaymentsCommand (hpa:prune:payment) Efface toutes les données de paiements qui ne sont plus reliées à un laboratoire.
A voir mais la nécessité n'est pas avérée au moment ou ce développement à commencé.

