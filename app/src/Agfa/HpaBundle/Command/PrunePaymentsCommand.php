<?php
namespace Agfa\HpaBundle\Command;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;

/**
 * Clears Paiement entries that are not linked to an Etablissement anymore.
 * TODO Dev was stopped because it requires a subquery and it is going to be heavy to use on large database. Consider necessity before finishing this code.
 */
class PrunePaymentsCommand extends ContainerAwareCommand
{

    /**
     * Settings for the command.
     *
     * {@inheritdoc}
     * @see \Symfony\Component\Console\Command\Command::configure()
     */
    protected function configure()
    {
        $this->setName('hpa:prune:payments')->setDescription("Supprime les entrées dans la table paiement qui ne sont plus liés à aucun laboratoire.");
    }

    /**
     * Execute the command.
     *
     * {@inheritdoc}
     * @see \Symfony\Component\Console\Command\Command::execute()
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $count = 0;

        /* @var $em \Doctrine\ORM\EntityManager */
        $em = $this->getContainer()
            ->get('doctrine')
            ->getManager();
        $logger = $this->getContainer()->get('monolog.logger.command');
        $paiementMgr = $this->getContainer()->get('agfa_hpa.paiement_manager');

        $orphans = $paiementMgr->getOrphanPaiements();

        foreach ($orphans as $orphan) {
            $em->remove($orphan);
        }

        $em->flush();
        $output->writeln("$count entrée dans Paiement ont été épurées.");

        // Log result
        $logger->info("hpa:prune:payments - $count entrée dans Paiement ont été épurées.");
    }
}