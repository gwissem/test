<?php
namespace Agfa\HpaBundle\Command;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;

/**
 * For migration from v1 to v2 in case the system was in prod before the migration was updated with all finesses.
 * DO NOT USE ANYMORE (it won't hurt but there is no need)
 * If a labo is not linked to a EJ, it is recreated. This script will take the data linnked to the labo and transfer it
 * to the new labo (the one linked to the ej).
 */
class FixOldEtablissementCommand extends ContainerAwareCommand
{

    /**
     * Settings for the command.
     *
     * {@inheritdoc}
     * @see \Symfony\Component\Console\Command\Command::configure()
     */
    protected function configure()
    {
        $this->setName('hpa:fix:etablissement')->setDescription('Reconstruit les données liées à l\'ancien établissement avec les nouveaux');
    }

    /**
     * Execute the command.
     *
     * {@inheritdoc}
     * @see \Symfony\Component\Console\Command\Command::execute()
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {

        /* @var $em \Doctrine\ORM\EntityManager */
        $em = $this->getContainer()
            ->get('doctrine')
            ->getManager();

        $logger = $this->getContainer()->get('monolog.logger.command');

        $etablissementRepository = $em->getRepository('AgfaHpaBundle:Etablissement');
        $demandeRepository = $em->getRepository('AgfaHpaBundle:Demande');
        $chapitreRepository = $em->getRepository('AgfaHpaBundle:Chapitre');
        $patientRepository = $em->getRepository('AgfaHpaBundle:Patient');
        $paiementRepository = $em->getRepository('AgfaHpaBundle:Paiement');

        $paiements = $paiementRepository->createQueryBuilder('pai')
            ->getQuery()
            ->getResult();
        if (count($paiements) > 0) {
            $output->writeln("Cannot proceed becase there are some data in hpa_paiement.");
            $logger->info("hpa:fix:etablissement - Cannot proceed because there are some data in hpa_paiement.");
            return;
        }

        // Get old labo entries
        $oldLabos = $etablissementRepository->createQueryBuilder('et')
            ->where('et.parent IS NULL')
            ->andWhere('et.bsrvrRefEtablissement = :defaultRef')
            ->setParameter('defaultRef', "999999999")
            ->getQuery()
            ->getResult();

        // Get new labo entries
        $newLabos = $etablissementRepository->createQueryBuilder('et')
            ->where('et.parent IS NOT NULL')
            ->andWhere('et.bsrvrRefEtablissement != :defaultRef')
            ->setParameter('defaultRef', "999999999")
            ->getQuery()
            ->getResult();

        // Make an array of entries to update
        $replacements = array();
        foreach ($oldLabos as $oldLabo) {
            foreach ($newLabos as $newLabo) {
                if ($oldLabo->getBsrvrCodeLabo() == $newLabo->getBsrvrCodeLabo()) {
                    $replacements[] = array(
                        'bsrvrCodeLabo' => $oldLabo->getBsrvrCodeLabo(),
                        'oldLaboName' => $oldLabo->getNom(),
                        'newLaboName' => $newLabo->getNom(),
                        'oldLaboId' => $oldLabo->getId(),
                        'newLaboId' => $newLabo->getId()
                    );
                }
            }
        }

        $output->writeln('Replacements:');

        // Update chapitres
        foreach ($replacements as $replacement) {
            $oldLaboName = $replacement['oldLaboName'];
            $newLaboName = $replacement['newLaboName'];
            $oldLaboId = $replacement['oldLaboId'];
            $newLaboId = $replacement['newLaboId'];

            $output->writeln("$oldLaboName ($oldLaboId) replaced with $newLaboName ($newLaboId)");
            $logger->info("hpa:fix:etablissement - $oldLaboName ($oldLaboId) replaced with $newLaboName ($newLaboId)");

            $chapitreRepository->createQueryBuilder('ch')
                ->update()
                ->set('ch.etablissement', ':newLaboId')
                ->where('ch.etablissement = :oldLaboId')
                ->setParameter('newLaboId', $newLaboId)
                ->setParameter('oldLaboId', $oldLaboId)
                ->getQuery()
                ->execute();

            // Update demandes
            $demandeRepository->createQueryBuilder('de')
                ->update()
                ->set('de.etablissement', ':newLaboId')
                ->where('de.etablissement = :oldLaboId')
                ->setParameter('newLaboId', $newLaboId)
                ->setParameter('oldLaboId', $oldLaboId)
                ->getQuery()
                ->execute();

            // Update patients
            $patientRepository->createQueryBuilder('pa')
                ->update()
                ->set('pa.etablissement', ':newLaboId')
                ->where('pa.etablissement = :oldLaboId')
                ->setParameter('newLaboId', $newLaboId)
                ->setParameter('oldLaboId', $oldLaboId)
                ->getQuery()
                ->execute();
        }

        // If the etablissement is not used anymore by a chapitre, demande or patient, delete it
        foreach ($oldLabos as $oldLabo) {
            $oldLaboName = $oldLabo->getNom();
            $oldLaboId = $oldLabo->getId();

            $noPatient = false;
            $noDemande = false;
            $noChapitre = false;

            $patients = $patientRepository->createQueryBuilder('pa')
                ->where('pa.etablissement = :oldLaboId')
                ->setParameter('oldLaboId', $oldLaboId)
                ->getQuery()
                ->getResult();
            if (count($patients) == 0) {
                $noPatient = true;
            }

            $demandes = $demandeRepository->createQueryBuilder('de')
                ->where('de.etablissement = :oldLaboId')
                ->setParameter('oldLaboId', $oldLaboId)
                ->getQuery()
                ->getResult();
            if (count($demandes) == 0) {
                $noDemande = true;
            }

            $chapitres = $chapitreRepository->createQueryBuilder('ch')
                ->where('ch.etablissement = :oldLaboId')
                ->setParameter('oldLaboId', $oldLaboId)
                ->getQuery()
                ->getResult();
            if (count($chapitres) == 0) {
                $noChapitre = true;
            }

            if (true === $noPatient && true === $noDemande && true === $noChapitre) {
                $etablissementRepository->createQueryBuilder('et')
                    ->delete()
                    ->where('et.id = :oldLaboId')
                    ->setParameter('oldLaboId', $oldLabo)
                    ->getQuery()
                    ->execute();

                $output->writeln("Lab $oldLaboName ($oldLaboId) deleted");

                $logger->info("hpa:fix:etablissement - Lab $oldLaboName ($oldLaboId) deleted");
            }
        }

        return;
    }
}
