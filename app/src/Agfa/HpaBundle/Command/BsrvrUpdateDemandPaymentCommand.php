<?php
namespace Agfa\HpaBundle\Command;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Agfa\HpaBundle\Services\CommunicationBioserveur;

/**
 * Sends informations to BioServeur to update payment status of CRs.
 */
class BsrvrUpdateDemandPaymentCommand extends ContainerAwareCommand
{

    /**
     * Settings for the command.
     * {@inheritDoc}
     * @see \Symfony\Component\Console\Command\Command::configure()
     */
    protected function configure()
    {
        $this->setName('hpa:bsrvr:update-payments')->setDescription('Envoi les mises à jour concernant les paiements des demandes sur Bioserveur.');
    }

    /**
     * Execute the command.
     * {@inheritDoc}
     * @see \Symfony\Component\Console\Command\Command::execute()
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {

        /* @var $em \Doctrine\ORM\EntityManager */
        $em = $this->getContainer()
            ->get('doctrine')
            ->getManager();
        $logger = $this->getContainer()->get('monolog.logger.command');

        $demandes = $this->getContainer()
            ->get('agfa_hpa.demande_manager')
            ->getDemandsForBsrvrUpdatePayment();

        if(count($demandes) > 0){
            $output->writeln(count($demandes) . " comptes rendus ont des mises à jour concernant leur paiement.");

            try {
                $response = $this->getContainer()
                    ->get('communication_bioserveur')
                    ->updatePaiement($demandes);

                if($response['status'] == CommunicationBioserveur::STATUS_OK){
                    $logger->info("hpa:bsrvr:update-payment - Update success: Bioserveur has been told of the payment of " . count($demandes) . " 'demandes'.");
                }

            } catch (\Exception $e) {
                $logger->critical("hpa:bsrvr:update-payments - Error on Bioserveur update [" . $e->getMessage() . "]");
                throw new \Exception("Envoi de la MAJ en echec : les demandes restent dans la file d'attente [" . $e->getMessage() . "]");
            }

            $output->writeln($response['message']);

        }else{
            $logger->info("hpa:bsrvr:update-payments - Nothing to update.");
            $output->writeln("Aucune demande à mettre à jour concernant les paiements.");
        }

        return;
    }
}