<?php
namespace Agfa\HpaBundle\Command;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;

/**
 * Sends informations to BioServeur about all demands that have been read.
 */
class BsrvrUnenrollmentCommand extends ContainerAwareCommand
{

    /**
     * Settings for the command.
     *
     * {@inheritdoc}
     * @see \Symfony\Component\Console\Command\Command::configure()
     */
    protected function configure()
    {
        $this->setName('hpa:bsrvr:unenrollment')->setDescription('Notifie BioServeur que le patient n\'est plus enrôlé.');
    }

    /**
     * Execute the command.
     *
     * {@inheritdoc}
     * @see \Symfony\Component\Console\Command\Command::execute()
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {

        /* @var $em \Doctrine\ORM\EntityManager */
        $em = $this->getContainer()
            ->get('doctrine')
            ->getManager();

        $logger = $this->getContainer()->get('monolog.logger.command');

        if ($unenrollment = $this->getContainer()
            ->get('agfa_hpa.bsrvr_com_manager')
            ->getUnenrollment()) {

            $bsrvrRefEtablissement = $unenrollment->getData()['bsrvr_ref_etablissement'];

            $output->writeln("" . count($unenrollment) . " notifications to be sent to Bioserveur.");

            try {
                $response = $this->getContainer()
                    ->get('communication_bioserveur')
                    ->notificationDesenrolement($unenrollment);
            } catch (\Exception $e) {
                $logger->critical("hpa:bsrvr:unenrollment - Error on Bioserveur update [" . $e->getMessage() . "]");
                throw new \Exception("hpa:bsrvr:unenrollment - Envoi de la MAJ en echec : les mises à jours restent dans la file d'attente [" . $e->getMessage() . "]");
            }

            if ($response !== FALSE) {
                $output->writeln(sprintf("Update success: Bioserveur has been told about the unenrollment of the patients with constant login: %s", $bsrvrRefEtablissement));
                $logger->info(sprintf("hpa:bsrvr:unenrollment - Update success: Bioserveur has been told of the patients with constant login: %s", $bsrvrRefEtablissement));

                $em->remove($unenrollment);

                $em->flush();
            }
        } else {
            $logger->info("hpa:bsrvr:unenrollment - Nothing to update.");
            $output->writeln("Nothing to update.");
        }

        return;
    }
}
