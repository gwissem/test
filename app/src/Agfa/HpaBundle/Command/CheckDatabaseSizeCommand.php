<?php
namespace Agfa\HpaBundle\Command;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Doctrine\ORM\Query;
use PiwikTracker;

/**
 * Send database size to Piwik for tracking its evolution.
 */
class CheckDatabaseSizeCommand extends ContainerAwareCommand
{

    protected function configure()
    {
        $this->setName('hpa:piwik:db-size')->setDescription('Récupère le volume occupé sur le serveur par les données de la base de données.');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $logger = $this->getContainer()->get('monolog.logger.command');
        $dbSize = $this->getBddData();

        PiwikTracker::$URL = 'https://' . $this->getContainer()->getParameter('host_piwik');
        $siteId = $this->getContainer()->getParameter('piwik_site_id');
        $tracker = new PiwikTracker($siteId);
        $tracker->doTrackEvent('CRON', 'Taille de la bdd', 'Taille bdd en MB', $dbSize);

        $output->writeln("<info>Volume utilisé sur la base de données : $dbSize MB</info>");
        $logger->info("hpa:piwik:db-size - Taille de la bdd envoyé à Piwik : $dbSize MB");
    }

    private function getBddData()
    {
        /* @var $dbh \Doctrine\DBAL\Connection */
        $dbh = $this->getContainer()
            ->get('doctrine')
            ->getConnection();
        $dbDriver = $this->getContainer()->getParameter('database_driver');

        if ($dbDriver == 'pdo_mysql') {
            // "DB Size in MB"
            $sql = trim('SELECT table_schema "DB Name",
                    Round(Sum(data_length + index_length) / 1024 / 1024, 1)
                    FROM   information_schema.tables
                    GROUP  BY table_schema;');
        } else {
            $sql = "select b.tablespace_name, tbs_size - a.free_space from  (select tablespace_name, round(sum(bytes)/1024/1024 ,2) as free_space from dba_free_space group by tablespace_name) ";
            $sql .= "a, (select tablespace_name, sum(bytes)/1024/1024 as tbs_size from dba_data_files group by tablespace_name) b where a.tablespace_name(+)=b.tablespace_name and ";
            $sql .= "a.tablespace_name in (select default_tablespace from user_users)";
        }

        $stmt = $dbh->prepare($sql);
        $stmt->execute();
        $donnees = $stmt->fetch(Query::HYDRATE_SCALAR);
        return $donnees[1];
    }
}
