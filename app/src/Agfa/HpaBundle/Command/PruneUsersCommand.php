<?php
namespace Agfa\HpaBundle\Command;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;

/**
 * Delete users account that have not been confirmed after the defined period 'nb_days_before_remove'.
 *
 * @author charles
 *
 */
class PruneUsersCommand extends ContainerAwareCommand
{

    /**
     * Settings for the command.
     *
     * {@inheritdoc}
     * @see \Symfony\Component\Console\Command\Command::configure()
     */
    protected function configure()
    {
        $this->setName('hpa:prune:users')
            ->setDescription("Supprime les comptes inactifs vieux de plus de X jours")
            ->addArgument('nb_days_before_remove', InputArgument::REQUIRED, 'Nombre de jour depuis la création du compte');
    }

    /**
     * Execute the command.
     *
     * {@inheritdoc}
     * @see \Symfony\Component\Console\Command\Command::execute()
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $logger = $this->getContainer()->get('monolog.logger.command');
        $nbDaysBeforeRemove = $input->getArgument('nb_days_before_remove');

        /* @var $em \Doctrine\ORM\EntityManager */
        $em = $this->getContainer()
            ->get('doctrine')
            ->getManager();

        $limit = new \DateTime();
        $limit->sub(new \DateInterval('P' . $nbDaysBeforeRemove . 'D'));

        $qb = $em->createQueryBuilder();
        $qb->select('u')
            ->from('AgfaUserBundle:User', 'u')
            ->where('u.createdAt < ?1')
            ->andWhere('u.createdAt = u.updatedAt')
            ->andWhere('u.enabled = false')
            ->setParameter(1, $limit)
            ->setMaxResults(500);
        $results = $qb->getQuery()->getResult();

        foreach ($results as $result) {
            $em->remove($result);
            $logger->info("hpa:prune:users - Comptes inactifs supprimé [" . $result->getEmail() . "]");
        }
        $em->flush();

        $output->writeln(count($results) . " comptes epures.");
    }
}