<?php
namespace Agfa\HpaBundle\Command;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\DependencyInjection\Exception\EnvNotFoundException;
use Doctrine\ORM\EntityNotFoundException;

/**
 * Update email of a user.
 * Used when a user loses access to its email adress.
 * TODO: send confirmation link for customer to confirm its new account.
 */
class UpdateUserEmailCommand extends ContainerAwareCommand
{

    /**
     * Settings for the command.
     *
     * {@inheritdoc}
     * @see \Symfony\Component\Console\Command\Command::configure()
     */
    protected function configure()
    {
        $this->setName('hpa:user:update-email')
            ->setDescription("Changer l'adresse email d'un utilisateur et envoi un lien de confirmation sur la nouvelle adrese email.")
            ->addArgument('email_current', InputArgument::REQUIRED, 'Email actuel de l\'utilisateur')
            ->addArgument('email_new', InputArgument::REQUIRED, 'Nouvel email de l\'utilisateur');
    }

    /**
     * Execute the command.
     *
     * {@inheritdoc}
     * @see \Symfony\Component\Console\Command\Command::execute()
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $emailCurrent = $input->getArgument('email_current');
        $emailNew = $input->getArgument('email_new');

        /* @var $em \Doctrine\ORM\EntityManager */
        $container = $this->getContainer();
        $em = $container->get('doctrine')->getManager();
        $logger = $container->get('monolog.logger.command');
        $userMgr = $container->get('agfa_user.user_manager');
        $mailer = $container->get('fos_user.mailer');
        $tokenGenerator = $container->get('fos_user.util.token_generator');

        // Udpate the email
        if (! $user = $userMgr->getRepository()->findOneBy(array(
            'email' => $emailCurrent
        ))) {
            throw new EntityNotFoundException("No entry was found for  $emailCurrent");
        }

        $user->setEmail($emailNew);

        // Send confirmation email to user
        $user->setEnabled(false);
        if (null === $user->getConfirmationToken()) {
            $user->setConfirmationToken($tokenGenerator->generateToken());
        }

        $mailer->sendConfirmationEmailMessage($user);

        $em->flush();

        $output->writeln("Le compte '$emailCurrent' est mis à jour avec pour email '$emailNew'. Un email de confirmation à été envoyé à l'utilisateur.");

        // Log result
        $logger->info("hpa:user:update-email - Account with email '$emailCurrent' is updated with: '$emailNew'.");
    }
}