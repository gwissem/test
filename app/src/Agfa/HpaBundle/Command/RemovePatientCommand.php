<?php
namespace Agfa\HpaBundle\Command;

use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;

/**
 * Removes a patient from the database and all its related data.
 */
class RemovePatientCommand extends ContainerAwareCommand
{

    /**
     * Settings for the command.
     *
     * {@inheritdoc}
     * @see \Symfony\Component\Console\Command\Command::configure()
     */
    protected function configure()
    {
        $this->setName('hpa:remove:patient')
            ->setDescription('Supprime un patient et ses comptes rendus')
            ->addArgument('patient_id', InputArgument::REQUIRED, 'Id du user');
    }

    /**
     * Execute the command.
     *
     * {@inheritdoc}
     * @see \Symfony\Component\Console\Command\Command::execute()
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /* @var $em \Doctrine\ORM\EntityManager */
        $em = $this->getContainer()
            ->get('doctrine')
            ->getManager();

        $patient_id = $input->getArgument('patient_id');
        $patient = $em->getRepository('AgfaHpaBundle:Patient')->find($patient_id);
        if (! $patient) {
            $output->writeln("<error>Impossible de trouver patient [$patient_id].</error>");
            return;
        }

        $em->remove($patient);
        $em->flush();

        $output->writeln(("<info>Done</info>"));
    }
}
