<?php
namespace Agfa\HpaBundle\Command;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;

/**
 * Clears the authorized devices that have their date expired.
 */
class PruneDevicesCommand extends ContainerAwareCommand
{

    /**
     * Settings for the command.
     *
     * {@inheritdoc}
     * @see \Symfony\Component\Console\Command\Command::configure()
     */
    protected function configure()
    {
        $this->setName('hpa:prune:devices')->setDescription("Supprime les autorisations d'appareils qui ont expiré");
    }

    /**
     * Execute the command.
     *
     * {@inheritdoc}
     * @see \Symfony\Component\Console\Command\Command::execute()
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /* @var $em \Doctrine\ORM\EntityManager */
        $em = $this->getContainer()
            ->get('doctrine')
            ->getManager();
        $logger = $this->getContainer()->get('monolog.logger.command');

        $now = new \DateTime();

        $count = 0;

        /* @var $user \Agfa\UserBundle\Entity\User */
        $list_user = $em->getRepository('AgfaUserBundle:User')->findAll();
        foreach ($list_user as $user) {
            $trustedComputers = $user->getTrustedComputers();
            foreach ($trustedComputers as $token => $trustedComputer) {
                $validUntil = new \DateTime($trustedComputer['valid-until']);
                if ($validUntil < $now) {
                    $user->removeTrustedComputer($token);
                    $count ++;
                }
            }
        }

        $em->flush();
        $output->writeln("$count autorisations épurées.");

        // Log result
        $logger->info("hpa:prune:devices - $count autorisations épurées.");
    }
}