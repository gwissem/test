<?php
namespace Agfa\HpaBundle\Command;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Agfa\HpaBundle\Entity\Etablissement;

/**
 * Migration fix: only use to update the Etablissement of an existing database
 * for a migration from v1.0.2 to v2.2.
 * A temporary table with the data that will be used to update the existing table is required.
 * TODO: send confirmation link for customer to confirm its new account.
 */
class UpdateEtablissementsCommand extends ContainerAwareCommand
{

    /**
     * Settings for the command.
     *
     * {@inheritdoc}
     * @see \Symfony\Component\Console\Command\Command::configure()
     */
    protected function configure()
    {
        $this->setName('hpa:user:update-etablissements')->setDescription("Mise à jour des etablissement à partir d'une table temporaire.");
    }

    /**
     * Execute the command.
     *
     * {@inheritdoc}
     * @see \Symfony\Component\Console\Command\Command::execute()
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $container = $this->getContainer();
        $em = $container->get('doctrine')->getManager();
        $logger = $container->get('monolog.logger.command');

        /**
         *  Get and update/insert EJs
         */
        $conn = $em->getConnection();
        $sql = 'SELECT * FROM tmp_etablissement tet WHERE tet.parent IS NULL';

        $stmt = $conn->prepare($sql);
        $stmt->execute();

        $newLabs = $stmt->fetchAll();

        foreach ($newLabs as $newLab) {
            $ej = $em->getRepository('AgfaHpaBundle:Etablissement')->findOneBy(array(
                'bsrvrRefEtablissement' => $newLab['FINESS']
            ));

            if (!$ej) {
                $ej = new Etablissement();
            }

            $ej->setNom($newLab['NAME'])
//                 ->setLibelle($newLab[''])
                ->setAdresse($newLab['ADDRESS'])
                ->setCodePostal($newLab['PC'])
                ->setVille($newLab['CITY'])
                ->setTelephone($newLab['PHONE'])
                ->setBsrvrCodeLabo((int) $newLab['CODE'])
                ->setBsrvrCodeEntiteJuridique($newLab['NAME_EJ'])
//                 ->setHxlCodeStructure($newLab[''])
                ->setBsrvrRefEtablissement($newLab['FINESS']);
            $em->persist($ej);
        }
        $em->flush();

        /**
         *  Get and update/insert laboratories
         */
        $conn = $em->getConnection();
        $sql = 'SELECT * FROM tmp_etablissement tet WHERE tet.parent IS NOT NULL';

        $stmt = $conn->prepare($sql);
        $stmt->execute();

        $newLabs = $stmt->fetchAll();

        foreach($newLabs as $newLab){
            $labo = $em->getRepository('AgfaHpaBundle:Etablissement')->findOneBy(array(
                'bsrvrRefEtablissement' => $newLab['FINESS']
            ));

            $ej =  $em->getRepository('AgfaHpaBundle:Etablissement')->findOneBy(array(
                'bsrvrRefEtablissement' => $newLab['PARENT']
            ));

            if (!$labo) {
                $labo = new Etablissement();
            }

            $labo->setNom($newLab['NAME'])
            ->setAdresse($newLab['ADDRESS'])
            ->setCodePostal($newLab['PC'])
            ->setVille($newLab['CITY'])
            ->setTelephone($newLab['PHONE'])
            ->setBsrvrCodeLabo((int) $newLab['CODE'])
            ->setBsrvrCodeEntiteJuridique($newLab['NAME_EJ'])
            ->setBsrvrRefEtablissement($newLab['FINESS'])
            ->setParent($ej);
            $em->persist($labo);
        }

        $em->flush();


        $output->writeln("Les données constituants les entités juridiques ainsi que les laboratoires ont bien été mises à jour.");

        // Log result
        $logger->info("hpa:user:update-etablissement - Labs and EJs have been successfully updated.");
    }
}