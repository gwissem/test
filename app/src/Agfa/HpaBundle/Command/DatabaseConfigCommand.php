<?php

namespace Agfa\HpaBundle\Command;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;

class DatabaseConfigCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
        ->setName('hpa:dbconf')
        ->setDescription('Dump des parametres de connexion à la base')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln(sprintf("connection string: [ %s/%s@%s ]",
                $this->getContainer()->getParameter('database_user'),
                $this->getContainer()->getParameter('database_password'),
                $this->getContainer()->getParameter('database_name')
        ));
        $output->writeln(sprintf("database server: [ %s:%s ]",
                $this->getContainer()->getParameter('database_host'),
                $this->getContainer()->getParameter('database_port')
        ));
    }
}