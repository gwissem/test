<?php
namespace Agfa\HpaBundle\Command;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Agfa\HpaBundle\Entity\Chapitre;

/**
 * Transfer chapitre and patient from v1 that where linked to labo to the labo's EJ (for migration from v1 to v2)
 */
class FixLabToEjCommand extends ContainerAwareCommand
{

    /**
     * Settings for the command.
     *
     * {@inheritdoc}
     * @see \Symfony\Component\Console\Command\Command::configure()
     */
    protected function configure()
    {
        $this->setName('hpa:fix:labs')->setDescription('Les anciennes données liés aux patient et chapites étaient des labos');
    }

    /**
     * Execute the command.
     *
     * {@inheritdoc}
     * @see \Symfony\Component\Console\Command\Command::execute()
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {

        /* @var $em \Doctrine\ORM\EntityManager */
        $em = $this->getContainer()
            ->get('doctrine')
            ->getManager();

        $logger = $this->getContainer()->get('monolog.logger.command');

        $etablissementRepository = $em->getRepository('AgfaHpaBundle:Etablissement');
        $patientRepository = $em->getRepository('AgfaHpaBundle:Patient');
        $chapitreRepository = $em->getRepository(Chapitre::class);

        $labs = $etablissementRepository->createQueryBuilder('et')
            ->join('et.parent', 'pa')
            ->getQuery()
            ->getResult();

        foreach ($labs as $lab) {
            $laboId = $lab->getId();
            $ejId = $lab->getParent()->getId();

            // Update patients data
            $result = $patientRepository->createQueryBuilder('pa')
                ->update()
                ->set('pa.etablissement', ':newLaboId')
                ->where('pa.etablissement = :oldLaboId')
                ->setParameter('newLaboId', $ejId)
                ->setParameter('oldLaboId', $laboId)
                ->getQuery()
                ->execute();

            $output->writeln("Lab $laboId => $ejId: $result patients updated");
            $logger->info("hpa:fix:labs - Lab $laboId => $ejId: $result patients updated");

            // Update chapitres data
            $result = $chapitreRepository->createQueryBuilder('ch')
                ->update()
                ->set('ch.etablissement', ':newLaboId')
                ->where('ch.etablissement = :oldLaboId')
                ->setParameter('newLaboId', $ejId)
                ->setParameter('oldLaboId', $laboId)
                ->getQuery()
                ->execute();

            $output->writeln("Lab $laboId => $ejId: $result chapitres updated");
            $logger->info("hpa:fix:labs - Lab $laboId => $ejId: $result chapitres updated");
        }

        return;
    }
}
