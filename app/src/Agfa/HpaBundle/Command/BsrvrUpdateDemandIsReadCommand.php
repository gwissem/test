<?php
namespace Agfa\HpaBundle\Command;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;

/**
 * Sends informations to BioServeur about all demands that have been read.
 */
class BsrvrUpdateDemandIsReadCommand extends ContainerAwareCommand
{

    /**
     * Settings for the command.
     *
     * {@inheritdoc}
     * @see \Symfony\Component\Console\Command\Command::configure()
     */
    protected function configure()
    {
        $this->setName('hpa:bsrvr:update-reading')->setDescription('Notifie Bioserveur des demandes qui ont été lues.')
        ->addArgument('limit', InputArgument::OPTIONAL, 'Nombre d\'entrées maximum à mettre à jour.');
    }

    /**
     * Execute the command.
     *
     * {@inheritdoc}
     * @see \Symfony\Component\Console\Command\Command::execute()
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {

        /* @var $em \Doctrine\ORM\EntityManager */
        $em = $this->getContainer()
            ->get('doctrine')
            ->getManager();
        $logger = $this->getContainer()->get('monolog.logger.command');
        $limit = $input->getArgument('limit');

        $demandes = $this->getContainer()
            ->get('agfa_hpa.demande_manager')
            ->getDemandsForBsrvrUpdateReading($limit);

        if (count($demandes) > 0) {
            $output->writeln("" . count($demandes) . " notifications to be sent to Bioserveur.");

            try {
                $response = $this->getContainer()
                    ->get('communication_bioserveur')
                    ->notificationLectures($demandes);
            } catch (\Exception $e) {
                $logger->critical("hpa:bsrvr:update-reading - Error on Bioserveur update [" . $e->getMessage() . "]");
                throw new \Exception("hpa:bsrvr:update-reading - Envoi de la MAJ en echec : les demandes lues restent dans la file d'attente [" . $e->getMessage() . "]");
            }

            if ($response !== FALSE) {
                $output->writeln("Update success: Bioserveur has been told about the reading of " . count($demandes) . " 'demandes'.");
                $logger->info("hpa:bsrvr:update-reading - Update success: Bioserveur has been told of the reading of " . count($demandes) . " 'demandes'.");

                foreach ($demandes as $demande) {
                    foreach ($demande->getBsrvrComs() as $action) {
                        $em->remove($action);
                    }
                }

                $em->flush();
            }
        } else {
            $logger->info("hpa:bsrvr:update-reading - Nothing to update.");
            $output->writeln("Nothing to update.");
        }

        return;
    }
}
