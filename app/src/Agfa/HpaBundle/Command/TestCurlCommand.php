<?php

namespace Agfa\HpaBundle\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
// use Symfony\Component\Console\Input\InputInterface;
// use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;

class TestCurlCommand extends ContainerAwareCommand
{
	protected $hostname = 'google.com';

	protected function configure()
	{
		$this
		->setName('agfa:test-curl')
		->setDescription("Teste l'envoi de requêtes HTTPS via curl")
        ->addArgument(
            'hostname',
            InputArgument::OPTIONAL,
            'Serveur a interroger',
        	$this->hostname
        )
		;
	}

	// FIXME No response ? Disabled until fixed
// 	protected function execute(InputInterface $input, OutputInterface $output)
// 	{
// 		$client = new \GuzzleHttp\Client();

// 		$hostname = $input->getArgument('hostname');
// 		if(!$hostname) $hostname = $this->hostname;

// 		$url = "https://$hostname";
// 		$output->writeln("Envoi d'une requete GET a $url");

// 		$request = $client->get($url);
// 	}
}