<?php

namespace Agfa\HpaBundle\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;

class IntegreCompteRenduCommand extends ContainerAwareCommand
{

    protected function configure()
    {
        $this
        ->setName('agfa:integre')
        ->setDescription('Intèger un compte rendu sous forme de fichier')
        ->addArgument(
            'user_id',
            InputArgument::REQUIRED,
            'Id du user'
        )
        ->addArgument(
            'file_path',
            InputArgument::REQUIRED,
            'Fichier à intégrer'
        )
        ->addOption(
        		'dry-run',
        		'd',
        		InputOption::VALUE_NONE,
        		"Don't commit to the database"
        )
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
    	/* @var $em \Doctrine\ORM\EntityManager */
    	$em = $this->getContainer()->get('doctrine')->getManager();

    	$dry_run = $input->getOption('dry-run');

    	$user_id = $input->getArgument('user_id');
    	$user = $em->getRepository('AgfaUserBundle:User')->find($user_id);
    	if(!$user) {
    		$output->writeln("<error>Impossible de trouver user [$user_id].</error>");
    		return;
    	}

    	$file_path = $input->getArgument('file_path');
    	if(!file_exists($file_path)) {
    		$output->writeln("<error>Le fichier [$file_path] n'existe pas.</error>");
    		return;
    	}
    	$file_content = file_get_contents($file_path);
    	if($file_content === FALSE) {
    		$output->writeln("<error>Impossible de lire le fichier [$file_path].</error>");
    		return;
    	}

    	$demande = $this->getContainer()->get('editcr_connector')->creerDemande($file_content, $user);

    	if(!$demande) {
    		$output->writeln("<error>Impossible de créer la demande.</error>");
    		return;
    	}

    	$rapprochement = $this->getContainer()->get('patients')->rapprocherPatient($demande);
    	if($rapprochement) {
    		$output->writeln("<info>Patient rattaché à la personne [".$demande->getPatient()->getPersonne()->getId()."].</info>");
    	}
    	else {
    		$output->writeln("<info>Patient non rattaché à une personne.</info>");
    	}

    	if(!$dry_run) {
    		$em->flush();
    		$output->writeln("<info>Creation de la demande [".$demande->getId()."] affectée au patient [".$demande->getPatient()->getId()."].</info>");
    	}
    }
}
