<?php
namespace Agfa\MoneticoPaiementBundle\Manager;

use Doctrine\ORM\EntityManager;

class RequestManager
{

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    public function getRepository()
    {
        return $this->em->getRepository('AgfaMoneticoPaiementBundle:Request');
    }

    public function getRequest($reference)
    {
        return $this->getRepository()
            ->getRequest($reference)
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function getLastRequest($referenceCode)
    {
        return $this->getRepository()
            ->getLastRequest($referenceCode)
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function getUnusedRequest($userSession, $tpe, $montant)
    {
        return $this->getRepository()
            ->getUnusedRequest($userSession, $tpe, $montant)
            ->getQuery()
            ->getOneOrNullResult();
    }
}