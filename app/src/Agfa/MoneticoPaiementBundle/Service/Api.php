<?php
namespace Agfa\MoneticoPaiementBundle\Service;

use Agfa\MoneticoPaiementBundle\Library\Request;
use Agfa\MoneticoPaiementBundle\Library\Monetico\MoneticoPaiementHmac;
use Agfa\MoneticoPaiementBundle\Library\MoneticoServer;
use Agfa\MoneticoPaiementBundle\Form\Type\RequestType;
use Symfony\Component\Form\FormFactory;
use Doctrine\ORM\EntityManager;
use Agfa\MoneticoPaiementBundle\Manager\RequestManager;
use Agfa\MoneticoPaiementBundle\Library\Response;
use Monolog\Logger;
use Symfony\Component\HttpFoundation\Session\Session;

class Api
{

    public $requestEntity;

    public function __construct(EntityManager $entityMgr, MoneticoServer $mntcServer, FormFactory $formFactory, RequestManager $requestMgr, Logger $logger, $moneticoPaiementParameters, Session $session)
    {
        $this->em = $entityMgr;
        $this->formFactory = $formFactory;
        $this->mntcServer = $mntcServer;
        $this->requestMgr = $requestMgr;
        $this->mntcParameters = $moneticoPaiementParameters['parameters'];
        $this->logger = $logger;
        $this->session = $session;
    }

    public function createRequestForm(Request $request)
    {
        // Record request in db
        $this->requestEntity = $this->createRequestEntry($request);

        // Add reference_code to form
        $request->setReference($this->requestEntity->getReference());

        // Add currency to montant as it is needed in the form
        $request->setMontant($request->getMontant() . $request->getCurrency());

        // Make mac hash tag
        $mac = $this->getMac($request);

        // Add mac hash tag to form
        $request->setMac($mac);

        // Build form
        $requestForm = $this->formFactory->create(RequestType::class, $request, array(
            'method' => 'POST',
            'action' => $this->mntcServer->getUrlPaiement(),
            'csrf_protection' => false
        ));

        return $requestForm;
    }

    public function getRequest()
    {
        return $this->requestEntity;
    }

    private function createRequestEntry(Request $request)
    {
        $userSession = $this->session->getId();
        $tpe = $request->getTpe();
        $montant = $request->getMontant();

        // Only create a request entry if no entry already exists for the same session, tpe and amount.
        if (! $requestEntity = $this->requestMgr->getUnusedRequest($userSession, $tpe, $montant)) {
            $requestEntity = new \Agfa\MoneticoPaiementBundle\Entity\Request();

            $referenceCode = $this->generateReferenceCode();

            $requestEntity->setUserSession($userSession)
                ->setTpe($tpe)
                ->setMontant($montant)
                ->setReference($referenceCode)
                ->setTexteLibre($request->getTexteLibre());

            $this->em->persist($requestEntity);
            $this->em->flush();
        }

        return $requestEntity;
    }

    /**
     * Create a reference_code to differenciate online payments from other payments (manual,...)
     *
     * @return string
     */
    private function generateReferenceCode()
    {
        $referenceCode = mb_strtoupper($this->mntcParameters['reference_code']);
        $reference = 0;

        // Get last entry in the db. Ref = 0 if there is no entry yet in the table.
        if ($request = $this->requestMgr->getLastRequest($referenceCode)) {
            $reference = $request->getReference();
        }

        // Strip the reference code
        $refDigits = (int) preg_replace('/(\D+)?(\d+)/i', '${2}', $reference);
        ;

        // Increment the latest code by one
        $nextRequestId = $refDigits ? $refDigits + 1 : 1;

        // Re-add 0s if and where needed
        $nbZerosToAdd = \Agfa\MoneticoPaiementBundle\Entity\Request::REFERENCE_MAX_LENGTH - (strlen($nextRequestId) + strlen($referenceCode));

        // Number of caracteres allowed in reference field is REFERENCE_MAX_LENGTH
        $referenceCode = mb_strtoupper($referenceCode) . str_repeat('0', $nbZerosToAdd) . $nextRequestId;

        return (string) $referenceCode;
    }

    /**
     * Create the hash MAC based on the code provided by Monetico.
     *
     * @param Request $request
     * @return string
     */
    private function getMac(Request $request)
    {
        $content = array();

        // Do not change the order of the following fields
        $content = array(
            1 => $request->getTpe(),
            2 => $request->getDate()->format('d/m/Y:H:i:s'),
            3 => $request->getMontant(),
            4 => $request->getReference(),
            5 => $request->getTexteLibre(),
            6 => $request->getVersion(),
            7 => $request->getLgue(),
            8 => $request->getSociete(),
            9 => $request->getEmail(),
            10 => $request->getNbrEch(),
            11 => $request->getDateech1(),
            12 => $request->getMontantech1(),
            13 => $request->getDateech2(),
            14 => $request->getMontantech2(),
            15 => $request->getDateech3(),
            16 => $request->getMontantech3(),
            17 => $request->getDateech4(),
            18 => $request->getMontantech4(),
            19 => $request->getOptions()
        );

        ksort($content);

        $content = implode('*', $content);

        // Make MAC key
        $moneticoPaiementHmac = new MoneticoPaiementHmac($request);
        $hmac = $moneticoPaiementHmac->computeHmac($content);

        return $hmac;
    }

    /**
     * Parse data received from Monetico as a response from the payment
     *
     * @param Response $response
     * @param array $responseArray
     * @return \Agfa\MoneticoPaiementBundle\Library\Response
     */
    public function createResponse(Response $response, $responseArray, $cle)
    {
        // Debug only : to test a response with GET parameters instead of POST. Second part in PaiementController
        // \parse_str($responseArray, $responseArray);

        // Example response
        // http://hexapat-reloaded.loc/app_dev.php/paiement/retour-ok/?TPE=1234567&date=05%2f12%2f2006%5fa%5f11%3a55%3a23&montant=62%2e75EUR&reference=ABERTYP00145&MAC=e4359a2c18d86cf2e4b0e646016c202e89947b04&texte-libre=LeTexteLibre&code-retour=paiement&cvx=oui&vld=1208&brand=VI&status3ds=1&numauto=010101&originecb=FRA&bincb=010101&hpancb=74E94B03C22D786E0F2C2CADBFC1C00B004B7C45&ipclient=127%2e0%2e0%2e1&originetr=FRA&veres=Y&pares=Y
        // http://hexapat-reloaded.loc/app_dev.php/paiement/retour-ok/?TPE=1234567&date=05%2f12%2f2006%5fa%5f11%3a55%3a23&montant=62%2e75EUR&reference=134830&MAC=e4359a2c18d86cf2e4b0e646016c202e89947b04&texte-libre=LeTexteLibre&code-retour=paiement&cvx=oui&vld=1208&brand=VI&status3ds=1&numauto=010101&originecb=FRA&bincb=010101&hpancb=74E94B03C22D786E0F2C2CADBFC1C00B004B7C45&ipclient=127%2e0%2e0%2e1&originetr=FRA&veres=Y&pares=Y
        // http://hexapat-reloaded.loc/app_dev.php/paiement/response/?TPE=2114&date=05%2f12%2f2006%5fa%5f11%3a55%3a23&montant=112%2e05EUR&reference=144704&MAC=e4359a2c18d86cf2e4b0e646016c202e89947b04&texte-libre=LeTexteLibre&code-retour=paiement&cvx=oui&vld=1208&brand=VI&status3ds=1&numauto=010101&originecb=FRA&bincb=010101&hpancb=74E94B03C22D786E0F2C2CADBFC1C00B004B7C45&ipclient=127%2e0%2e0%2e1&originetr=FRA&veres=Y&pares=Y

        // Motifrefus and numauto are optional (one or the other)
        $motifrefus = isset($responseArray['motifrefus']) ? $responseArray['motifrefus'] : '';
        $numauto = isset($responseArray['numauto']) ? $responseArray['numauto'] : '';

        // Add data to Response object (not the response entity) (in service)
        $response->setCle($cle);
        $response->setTpe($responseArray['TPE']);
        $response->setDate($responseArray['date']);
        $response->setMontant($responseArray['montant']);
        $response->setReference($responseArray['reference']);
        $response->setMac($responseArray['MAC']);
        $response->setTexteLibre($responseArray['texte-libre']);
        $response->setCodeRetour($responseArray['code-retour']);
        $response->setCvx($responseArray['cvx']);
        $response->setVld($responseArray['vld']);
        $response->setBrand($responseArray['brand']);
        $response->setStatus3ds($responseArray['status3ds']);
        $response->setNumAuto($numauto);
        $response->setMotifRefus($motifrefus);
        $response->setOrigineCb($responseArray['originecb']);
        $response->setBinCb($responseArray['bincb']);
        $response->setHPanCb($responseArray['hpancb']);
        $response->setIpClient($responseArray['ipclient']);
        $response->setOrigineTr($responseArray['originetr']);
        $response->setVeres($responseArray['veres']);
        $response->setPares($responseArray['pares']);

        return $this->createResponseEntry($response);
    }

    private function createResponseEntry(Response $response)
    {
        // Find request id based on reference
        $request = $this->requestMgr->getRepository()->findOneBy(array(
            'reference' => $response->getReference()
        ));

        // If the original request was found, save the response
        if ($request) {

            // Update the response if a response already exists, create a new one otherwise
            if (! $request->getResponse()) {
                $responseEntity = new \Agfa\MoneticoPaiementBundle\Entity\Response();
            } else {
                $responseEntity = $request->getResponse();
            }

            // Check response integrity: if it does not pass the validation, return false to later indicate to the controler that the response was not ok.
            if (FALSE === $this->validateResponse($response)) {
                return false;
            }

            $parsedMontant = $this->parseMontant($response->getMontant());
            $formatedDate = \DateTime::createFromFormat('d/m/Y\_\a\_H:i:s', $response->getDate());

            $responseEntity->setMotifRefus($this->ifExists($response->getMotifRefus()))
                ->setIpclient($this->ifExists($response->getIpClient()))
                ->setCodeRetour($this->ifExists($response->getCodeRetour()))
                ->setMontant($this->ifExists($parsedMontant['montant']))
                ->setCurrency($parsedMontant['currency'])
                ->setDate($formatedDate)
                ->setReference($this->ifExists($response->getReference()))
                ->setTexteLibre($this->ifExists($response->getTexteLibre()))
                ->setCvx($this->ifExists($response->getCvx()))
                ->setVld($this->ifExists($response->getVld()))
                ->setBrand($this->ifExists($response->getBrand()))
                ->setStatus3ds($this->ifExists($response->getStatus3ds()))
                ->setOrigineTr($this->ifExists($response->getOrigineTr()))
                ->setVeres($this->ifExists($response->getVeres()))
                ->setPares($this->ifExists($response->getPares()))
                ->setFiltrageCause($this->ifExists($response->getFiltrageCause()))
                ->setFiltrageValeur($this->ifExists($response->getFiltrageValeur()))
                ->setModePaiement($this->ifExists($response->getModePaiement()));

            // If there is a problem with the transaction...
            if ($response->getMotifRefus()) {
                $responseEntity->setMotifRefus($response->getMotifRefus());
            }

            $this->em->persist($responseEntity);

            // Link to request entry
            $request->setResponse($responseEntity);
            $this->em->persist($request);

            $this->em->flush();
        }

        return $response;
    }

    /**
     * Build MAC tag and check with given MAC
     *
     * @param Response $response
     * @return boolean
     */
    private function validateResponse(Response $response)
    {
        $content = array();

        // Do not change the order of the following fields
        $content = array(
            1 => $response->getTpe(),
            2 => $response->getDate(),
            3 => $response->getMontant(),
            4 => $response->getReference(),
            5 => $response->getTexteLibre(),
            6 => $this->mntcParameters['version'],
            7 => $response->getCodeRetour(),
            8 => $response->getCvx(),
            9 => $response->getVld(),
            10 => $response->getBrand(),
            11 => $response->getStatus3ds(),
            12 => $response->getNumAuto(),
            13 => $response->getMotifRefus(),
            14 => $response->getOrigineCb(),
            15 => $response->getBinCb(),
            16 => $response->getHPanCb(),
            17 => $response->getIpClient(),
            18 => $response->getOrigineTr(),
            19 => $response->getVeres(),
            20 => $response->getPares()
        );

        ksort($content);

        $content = implode('*', $content) . '*';

        // Make MAC key
        $moneticoPaiementHmac = new MoneticoPaiementHmac($response);
        $hmac = $moneticoPaiementHmac->computeHmac($content);

        if ($hmac == strtolower($response->getMac())) {
            return TRUE;
        }

        $this->logger->critical('The data received did not pass the integrity check (MAC)');

        return FALSE;
    }

    /**
     * Separates currency from decimal
     *
     * @param string $montantToParse
     * @return array
     */
    public function parseMontant($montantToParse)
    {
        $parsedString = array();
        $parsedString['currency'] = \preg_replace('/([0-9]*(\.[0-9]{2})?)/', '', $montantToParse);
        $parsedString['montant'] = \preg_replace('/([a-zA-Z]{3})/i', '', $montantToParse);

        return $parsedString;
    }

    /**
     * Returns null if the variable is empty
     *
     * @param string $value
     * @return NULL|string
     */
    private function ifExists($value)
    {
        $value = $value ? $value : null;

        return $value;
    }
}

