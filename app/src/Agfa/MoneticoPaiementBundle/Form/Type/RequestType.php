<?php
namespace Agfa\MoneticoPaiementBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;

class RequestType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('TPE', TextType::class, array(
            'property_path' => 'tpe',
            'label' => FALSE
        ))
            ->add('date', DateTimeType::class, array(
            'widget' => 'single_text',
            'format' => 'dd/MM/yyyy:HH:mm:ss',
            'label' => FALSE
        ))
            ->add('montant', TextType::class, array(
            'label' => FALSE
        ))
            ->add('reference', TextType::class, array(
            'label' => FALSE
        ))
            ->add('texte-libre', TextType::class, array(
            'property_path' => 'texteLibre',
            'required' => FALSE,
            'label' => FALSE
        ))
            ->add('version', TextType::class, array(
            'label' => FALSE
        ))
            ->add('lgue', TextType::class, array(
            'label' => FALSE
        ))
            ->add('societe', TextType::class, array(
            'label' => FALSE
        ))
            ->add('mail', TextType::class, array(
            'property_path' => 'email',
            'label' => FALSE
        ))
            ->add('nbrech', TextType::class, array(
            'required' => FALSE,
            'label' => FALSE
        ))
            ->add('dateech1', TextType::class, array(
            'required' => FALSE,
            'label' => FALSE
        ))
            ->add('montantech1', TextType::class, array(
            'required' => FALSE,
            'label' => FALSE
        ))
            ->add('dateech2', TextType::class, array(
            'required' => FALSE,
            'label' => FALSE
        ))
            ->add('montantech2', TextType::class, array(
            'required' => FALSE,
            'label' => FALSE
        ))
            ->add('dateech3', TextType::class, array(
            'required' => FALSE,
            'label' => FALSE
        ))
            ->add('montantech3', TextType::class, array(
            'required' => FALSE,
            'label' => FALSE
        ))
            ->add('dateech4', TextType::class, array(
            'required' => FALSE,
            'label' => FALSE
        ))
            ->add('montantech4', TextType::class, array(
            'required' => FALSE,
            'label' => FALSE
        ))
            ->add('options', TextType::class, array(
            'required' => FALSE,
            'label' => FALSE
        ))
            ->add('MAC', TextType::class, array(
            'property_path' => 'mac',
            'label' => FALSE
        ))
            ->add('url_retour', TextType::class, array(
            'label' => FALSE
        ))
            ->add('url_retour_ok', TextType::class, array(
            'label' => FALSE
        ))
            ->add('url_retour_err', TextType::class, array(
            'label' => FALSE
        ));
    }

    public function getBlockPrefix()
    {
        return null;
    }
}

