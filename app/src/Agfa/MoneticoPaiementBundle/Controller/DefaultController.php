<?php
namespace Agfa\MoneticoPaiementBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Agfa\HpaBundle\Entity\Journal;

class DefaultController extends Controller
{

    /**
     * @Route("/", name="monetico_default_index")
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction()
    {
        $payment = $this->get('agfa_monetico_paiement.request_service');
        $moneticoApi = $this->get('agfa_monetico_paiement.api_service');
        $journal = $this->get('journalist');


        // FIXME Create a method to make the reference code
        $payment
            ->setMontant('10.30')
            ->setReference(date("His"));

        // Build form
        $requestForm = $moneticoApi->createRequestForm($payment);

        // The following is specific to HPA
        $paymentArray = $journal->convertPaymentRequest($payment);
        $journal->addInfo(Journal::EVENT_PAYMENT_REQUEST, NULL, $this->getUser(), 'Demande de paiement faite à Monetico', $paymentArray);

        return $this->render('AgfaMoneticoPaiementBundle:Default:index.html.twig', array(
            'requestForm' => $requestForm->createView()
        ));
    }
}

