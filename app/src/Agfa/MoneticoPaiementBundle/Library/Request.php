<?php
namespace Agfa\MoneticoPaiementBundle\Library;

use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bundle\FrameworkBundle\Routing\Router;

class Request
{
    const REQUEST_VERSION = '3.0';

    /**
     * @Assert\NotBlank()
     */
    private $version = self::REQUEST_VERSION;

    /**
     * @Assert\NotBlank()
     * @Assert\Length(min=7, max=7)
     */
    private $tpe;

    /**
     * @Assert\NotBlank()
     */
    private $cle;

    /**
     * @Assert\NotBlank()
     * @Assert\DateTime()
     */
    private $date;

    /**
     * @Assert\NotBlank()
     */
    private $montant;

    /**
     * @Assert\NotBlank()
     * @Assert\Length(max=12)
     */
    private $reference;

    /**
     * @Assert\NotBlank()
     */
    private $texteLibre;

    /**
     * @Assert\NotBlank()
     * @Assert\Email()
     */
    private $email;

    /**
     * @Assert\NotBlank()
     * @Assert\Language()
     */
    private $lgue;

    /**
     * @Assert\NotBlank()
     */
    private $societe;

    /**
     * @Assert\NotBlank()
     * @Assert\Url()
     */
    private $urlRetour;

    /**
     * @Assert\NotBlank()
     * @Assert\Url()
     */
    private $urlRetourOk;

    /**
     * @Assert\NotBlank()
     * @Assert\Url()
     */
    private $urlRetourErr;

    private $nbrEch;

    private $dateech1;

    private $montantech1;

    private $dateech2;

    private $montantech2;

    private $dateech3;

    private $montantech3;

    private $dateech4;

    private $montantech4;

    /**
     * @Assert\NotBlank()
     */
    private $mac;

    private $options;

    private $aliasCb;

    private $forceSaisieCb;

    private $threeDsDebrayable;

    /**
     * @Assert\NotBlank()
     */
    private $currency;

    public function __construct($moneticoPaiementParameters, Router $router)
    {
        $parameters = $moneticoPaiementParameters['parameters'];
        $this->router = $router;
// Only enable the following when parameters are not stored in the db
//         $this->setTpe($parameters['tpe']);
//         $this->setSociete($parameters['societe']);
//         $this->setCle($parameters['cle']);
//         $this->setEmail($parameters['email']);
        $this->setDate(new \DateTime('NOW'));
        $this->setVersion($parameters['version']);
        $this->setLgue($parameters['language']);
        $this->setCurrency($parameters['currency']);

        $this->setUrlRetour($this->toUrl($parameters['url_retour']));
        $this->setUrlRetourErr($this->toUrl($parameters['url_retour_err']));
        $this->setUrlRetourOk($this->toUrl($parameters['url_retour_ok']));
    }

    /**
     * Check if the string given is a URL
     * Use it as a Symfony route if not.
     *
     * @param string $string
     * @return string
     */
    private function toUrl($string)
    {
        if (\filter_var($string, FILTER_VALIDATE_URL)) {
            return $string;
        }

        return $this->router->generate($string, array(), Router::ABSOLUTE_URL);
    }

    public function getVersion()
    {
        return $this->version;
    }

    public function setVersion($version)
    {
        $this->version = $version;
        return $this;
    }

    public function getTpe()
    {
        return $this->tpe;
    }

    public function setTpe($tpe)
    {
        $this->tpe = $tpe;
        return $this;
    }

    public function getDate()
    {
        return $this->date;
    }

    public function setDate($date)
    {
        $this->date = $date;
        return $this;
    }

    public function getMontant()
    {
        return $this->montant;
    }

    public function setMontant($montant)
    {
        $this->montant = $montant;
        return $this;
    }

    public function getReference()
    {
        return $this->reference;
    }

    public function setReference($reference)
    {
        $this->reference = $reference;
        return $this;
    }

    public function getTexteLibre()
    {
        return $this->texteLibre;
    }

    public function setTexteLibre($texteLibre)
    {
        $this->texteLibre = $texteLibre;
        return $this;
    }

    public function getLgue()
    {
        return $this->lgue;
    }

    public function setLgue($lgue)
    {
        $this->lgue = mb_strtoupper($lgue);
        return $this;
    }

    public function getSociete()
    {
        return $this->societe;
    }

    public function setSociete($societe)
    {
        $this->societe = $societe;
        return $this;
    }

    public function getOptions()
    {
        return $this->options;
    }

    public function setOptions($options)
    {
        $this->options = $options;
        return $this;
    }

    public function getAliasCb()
    {
        return $this->aliasCb;
    }

    public function setAliasCb($aliasCb)
    {
        $this->aliasCb = $aliasCb;
        return $this;
    }

    public function getForceSaisieCb()
    {
        return $this->forceSaisieCb;
    }

    public function setForceSaisieCb($forceSaisieCb)
    {
        $this->forceSaisieCb = $forceSaisieCb;
        return $this;
    }

    public function getThreeDsDebrayable()
    {
        return $this->threeDsDebrayable;
    }

    public function setThreeDsDebrayable($threeDsDebrayable)
    {
        $this->threeDsDebrayable = $threeDsDebrayable;
        return $this;
    }

    public function getCurrency()
    {
        return mb_strtoupper($this->currency);
    }

    public function setCurrency($currency)
    {
        $this->currency = $currency;
        return $this;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function setEmail($email)
    {
        $this->email = $email;
        return $this;
    }

    public function getCle()
    {
        return $this->cle;
    }

    public function setCle($cle)
    {
        $this->cle = $cle;
        return $this;
    }

    public function getNbrEch()
    {
        return $this->nbrEch;
    }

    public function setNbrEch($nbrEch)
    {
        $this->nbrEch = $nbrEch;
        return $this;
    }

    public function getDateech1()
    {
        return $this->dateech1;
    }

    public function setDateech1($dateech1)
    {
        $this->dateech1 = $dateech1;
        return $this;
    }

    public function getMontantech1()
    {
        return $this->montantech1;
    }

    public function setMontantech1($montantech1)
    {
        $this->montantech1 = $montantech1;
        return $this;
    }

    public function getDateech2()
    {
        return $this->dateech2;
    }

    public function setDateech2($dateech2)
    {
        $this->dateech2 = $dateech2;
        return $this;
    }

    public function getMontantech2()
    {
        return $this->montantech2;
    }

    public function setMontantech2($montantech2)
    {
        $this->montantech2 = $montantech2;
        return $this;
    }

    public function getDateech3()
    {
        return $this->dateech3;
    }

    public function setDateech3($dateech3)
    {
        $this->dateech3 = $dateech3;
        return $this;
    }

    public function getMontantech3()
    {
        return $this->montantech3;
    }

    public function setMontantech3($montantech3)
    {
        $this->montantech3 = $montantech3;
        return $this;
    }

    public function getDateech4()
    {
        return $this->dateech4;
    }

    public function setDateech4($dateech4)
    {
        $this->dateech4 = $dateech4;
        return $this;
    }

    public function getMontantech4()
    {
        return $this->montantech4;
    }

    public function setMontantech4($montantech4)
    {
        $this->montantech4 = $montantech4;
        return $this;
    }

    public function getMac()
    {
        return $this->mac;
    }

    public function setMac($mac)
    {
        $this->mac = $mac;
        return $this;
    }

    public function getUrlRetour()
    {
        return $this->urlRetour;
    }

    public function setUrlRetour($urlRetour)
    {
        $this->urlRetour = $urlRetour;
        return $this;
    }

    public function getUrlRetourOk()
    {
        return $this->urlRetourOk;
    }

    public function setUrlRetourOk($urlRetourOk)
    {
        $this->urlRetourOk = $urlRetourOk;
        return $this;
    }

    public function getUrlRetourErr()
    {
        return $this->urlRetourErr;
    }

    public function setUrlRetourErr($urlRetourErr)
    {
        $this->urlRetourErr = $urlRetourErr;
        return $this;
    }
}