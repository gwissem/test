<?php
namespace Agfa\MoneticoPaiementBundle\Library;

use Symfony\Component\HttpKernel\Kernel;
use Symfony\Component\DependencyInjection\Exception\InvalidArgumentException;
use Symfony\Component\DependencyInjection\Exception\RuntimeException;

class MoneticoServer
{

    private $kernel;

    private $preprod;

    private $prod;

    private $servers;

    public function __construct(Kernel $kernel, $parameters)
    {
        $this->env = $kernel->getEnvironment();
        $this->preprod = $parameters['servers']['preprod'];
        $this->prod = $parameters['servers']['prod'];
    }

    public function getUrlPaiement()
    {
        $servers = $this->getServers();

        if ($this->checkServerIsAvailable($servers['paiement'])) {
            return $servers['paiement'];
        }

        throw new RuntimeException('No server available.');
    }

    public function getUrlCapture()
    {
        $servers = $this->getServers();

        if ($this->checkServerIsAvailable($servers['capture'])) {
            return $servers['capture'];
        }

        throw new RuntimeException('No server available.');
    }

    public function getUrlRecredit()
    {
        $servers = $this->getServers();

        if ($this->checkServerIsAvailable($servers['recredit'])) {
            return $servers['recredit'];
        }

        throw new RuntimeException('No server available.');
    }

    private function getServers()
    {
        $env = $this->env;

        if (! in_array($env, array(
            'dev',
            'prod',
            'preprod'
        ))) {
            throw new InvalidArgumentException("Environmnent $env does not exist.");
        }

        switch ($env) {
            case 'dev':
            case 'preprod':
                return $this->preprod;
                break;

            case 'prod':
                return $this->prod;
                break;
        }

        return;
    }

    /**
     * Check if a server is available
     *
     * @param string $url
     * @return boolean
     */
    private function checkServerIsAvailable($url)
    {
        // check, if a valid url is provided
        if (! filter_var($url, FILTER_VALIDATE_URL)) {
            return false;
        }

        // initialize curl
        $curlInit = \curl_init($url);
        \curl_setopt($curlInit, CURLOPT_CONNECTTIMEOUT, 10);
        \curl_setopt($curlInit, CURLOPT_HEADER, true);
        \curl_setopt($curlInit, CURLOPT_NOBODY, true);
        \curl_setopt($curlInit, CURLOPT_RETURNTRANSFER, true);

        // get answer
        $response = curl_exec($curlInit);

        \curl_close($curlInit);

        if ($response)
            return true;

        return false;
    }
}