<?php
namespace Agfa\MoneticoPaiementBundle\Library;

use Symfony\Component\Validator\Constraints as Assert;

class Response
{
    public function __construct($moneticoPaiementParameters)
    {
        $parameters = $moneticoPaiementParameters['parameters'];

        $this->setVersion($parameters['version']);
    }

    /**
     * @Assert\NotBlank()
     */
    private $mac;

    /**
     * @Assert\NotBlank()
     */
    private $date;

    /**
     * @Assert\NotBlank()
     */
    private $tpe;

    /**
     * @Assert\NotBlank()
     */
    private $montant;

    /**
     * @Assert\NotBlank()
     */
    private $reference;

    private $texteLibre;

    /**
     * @Assert\NotBlank()
     */
    private $codeRetour;

    /**
     * @Assert\NotBlank()
     */
    private $cvx;

    /**
     * @Assert\NotBlank()
     */
    private $vld;

    /**
     * @Assert\NotBlank()
     */
    private $brand;

    /**
     * @Assert\NotBlank()
     */
    private $status3ds;

    /**
     * @Assert\NotBlank()
     */
    private $numAuto;

    /**
     * @Assert\NotBlank()
     */
    private $motifRefus;

    /**
     * @Assert\NotBlank()
     */
    private $origineCb;

    /**
     * @Assert\NotBlank()
     */
    private $binCb;

    /**
     * @Assert\NotBlank()
     */
    private $hPanCb;

    /**
     * @Assert\NotBlank()
     */
    private $ipClient;

    private $origineTr;

    private $veres;

    private $pares;

    private $montantech;

    private $filtrageCause;

    private $filtrageValeur;

    private $cbEnregistree;

    private $cbMasquee;

    /**
     * @Assert\NotBlank()
     */
    private $modePaiement;

    /**
     * @Assert\NotBlank()
     */
    private $cle;

    /**
     * @Assert\NotBlank()
     */
    private $version;

    public function getMac()
    {
        return $this->mac;
    }

    public function setMac($mac)
    {
        $this->mac = $mac;
        return $this;
    }

    public function getDate()
    {
        return $this->date;
    }

    public function setDate($date)
    {
        $this->date = $date;
        return $this;
    }

    public function getTpe()
    {
        return $this->tpe;
    }

    public function setTpe($tpe)
    {
        $this->tpe = $tpe;
        return $this;
    }

    public function getMontant()
    {
        return $this->montant;
    }

    public function setMontant($montant)
    {
        $this->montant = $montant;
        return $this;
    }

    public function getReference()
    {
        return $this->reference;
    }

    public function setReference($reference)
    {
        $this->reference = $reference;
        return $this;
    }

    public function getTexteLibre()
    {
        return $this->texteLibre;
    }

    public function setTexteLibre($texteLibre)
    {
        $this->texteLibre = $texteLibre;
        return $this;
    }

    public function getCodeRetour()
    {
        return $this->codeRetour;
    }

    public function setCodeRetour($codeRetour)
    {
        $this->codeRetour = $codeRetour;
        return $this;
    }

    public function getCvx()
    {
        return $this->cvx;
    }

    public function setCvx($cvx)
    {
        $this->cvx = $cvx;
        return $this;
    }

    public function getVld()
    {
        return $this->vld;
    }

    public function setVld($vld)
    {
        $this->vld = $vld;
        return $this;
    }

    public function getBrand()
    {
        return $this->brand;
    }

    public function setBrand($brand)
    {
        $this->brand = $brand;
        return $this;
    }

    public function getStatus3ds()
    {
        return $this->status3ds;
    }

    public function setStatus3ds($status3ds)
    {
        $this->status3ds = $status3ds;
        return $this;
    }

    public function getNumAuto()
    {
        return $this->numAuto;
    }

    public function setNumAuto($numAuto)
    {
        $this->numAuto = $numAuto;
        return $this;
    }

    public function getMotifRefus()
    {
        return $this->motifRefus;
    }

    public function setMotifRefus($motifRefus)
    {
        $this->motifRefus = $motifRefus;
        return $this;
    }

    public function getOrigineCb()
    {
        return $this->origineCb;
    }

    public function setOrigineCb($origineCb)
    {
        $this->origineCb = $origineCb;
        return $this;
    }

    public function getBinCb()
    {
        return $this->binCb;
    }

    public function setBinCb($binCb)
    {
        $this->binCb = $binCb;
        return $this;
    }

    public function getHPanCb()
    {
        return $this->hPanCb;
    }

    public function setHPanCb($hPanCb)
    {
        $this->hPanCb = $hPanCb;
        return $this;
    }

    public function getIpClient()
    {
        return $this->ipClient;
    }

    public function setIpClient($ipClient)
    {
        $this->ipClient = $ipClient;
        return $this;
    }

    public function getOrigineTr()
    {
        return $this->origineTr;
    }

    public function setOrigineTr($origineTr)
    {
        $this->origineTr = $origineTr;
        return $this;
    }

    public function getVeres()
    {
        return $this->veres;
    }

    public function setVeres($veres)
    {
        $this->veres = $veres;
        return $this;
    }

    public function getPares()
    {
        return $this->pares;
    }

    public function setPares($pares)
    {
        $this->pares = $pares;
        return $this;
    }

    public function getMontantech()
    {
        return $this->montantech;
    }

    public function setMontantech($montantech)
    {
        $this->montantech = $montantech;
        return $this;
    }

    public function getFiltrageCause()
    {
        return $this->filtrageCause;
    }

    public function setFiltrageCause($filtrageCause)
    {
        $this->filtrageCause = $filtrageCause;
        return $this;
    }

    public function getFiltrageValeur()
    {
        return $this->filtrageValeur;
    }

    public function setFiltrageValeur($filtrageValeur)
    {
        $this->filtrageValeur = $filtrageValeur;
        return $this;
    }

    public function getCbEnregistree()
    {
        return $this->cbEnregistree;
    }

    public function setCbEnregistree($cbEnregistree)
    {
        $this->cbEnregistree = $cbEnregistree;
        return $this;
    }

    public function getCbMasquee()
    {
        return $this->cbMasquee;
    }

    public function setCbMasquee($cbMasquee)
    {
        $this->cbMasquee = $cbMasquee;
        return $this;
    }

    public function getModePaiement()
    {
        return $this->modePaiement;
    }

    public function setModePaiement($modePaiement)
    {
        $this->modePaiement = $modePaiement;
        return $this;
    }

    public function getCle()
    {
        return $this->cle;
    }

    public function setCle($cle)
    {
        $this->cle = $cle;
        return $this;
    }

    public function getVersion()
    {
        return $this->version;
    }

    public function setVersion($version)
    {
        $this->version = $version;
        return $this;
    }


}