<?php
namespace Agfa\MoneticoPaiementBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Response
 *
 * @ORM\Table(name="mntc_response")
 * @ORM\Entity(repositoryClass="Agfa\MoneticoPaiementBundle\Repository\ResponseRepository")
 */
class Response
{

    const CODE_RETOUR_PAIEMENT = 'paiement';

    const CODE_RETOUR_PAYETEST = 'payetest';

    const CODE_RETOUR_ANNULATION = 'Annulation';

    /**
     * @ORM\OneToOne(targetEntity="\Agfa\MoneticoPaiementBundle\Entity\Request", mappedBy="response")
     */
    private $request;

    /**
     *
     * @var int
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="motif_refus", type="string", length=255, nullable=TRUE)
     */
    private $motifRefus;

    /**
     * @ORM\Column(name="ip_client", type="string", length=25)
     */
    private $ipClient;

    /**
     * @ORM\Column(name="code_retour", type="string", length=255)
     */
    private $codeRetour;

    /**
     * @ORM\Column(name="montant", type="decimal", precision=10, scale=2)
     */
    private $montant;

    /**
     * @ORM\Column(name="currency", type="string", length=3)
     */
    private $currency;

    /**
     * NOTE 'date' is a reserved word with Oracle
     * @ORM\Column(name="date_response", type="datetime")
     */
    private $date;

    /**
     * @ORM\Column(name="reference", type="string", length=25)
     */
    private $reference;

    /**
     * @ORM\Column(name="texte_libre", type="text", nullable=TRUE)
     */
    private $texteLibre;

    /**
     * @ORM\Column(name="cvx", type="string", length=3)
     */
    private $cvx;

    /**
     * @ORM\Column(name="vld", type="integer")
     */
    private $vld;

    /**
     * @ORM\Column(name="brand", type="string", length=2)
     */
    private $brand;

    /**
     * @ORM\Column(name="status3ds", type="integer")
     */
    private $status3ds;

    /**
     * @ORM\Column(name="num_auto", type="integer", nullable=TRUE)
     */
    private $numAuto;

    /**
     * @ORM\Column(name="origine_cb", type="string", length=3, nullable=TRUE)
     */
    private $origineCb;

    /**
     * @ORM\Column(name="bin_cb", type="integer", nullable=TRUE)
     */
    private $binCb;

    /**
     * @ORM\Column(name="hpan_cb", type="string", length=255, nullable=TRUE)
     */
    private $hpanCb;

    /**
     * @ORM\Column(name="origine_tr", type="string", length=3, nullable=TRUE)
     */
    private $origineTr;

    /**
     * @ORM\Column(name="veres", type="string", length=1, nullable=TRUE)
     */
    private $veres;

    /**
     * @ORM\Column(name="pares", type="string", length=1, nullable=TRUE)
     */
    private $pares;

    /**
     * @ORM\Column(name="filtrage_cause", type="string", length=20, nullable=TRUE)
     */
    private $filtrageCause;

    /**
     * @ORM\Column(name="filtrage_valeur", type="string", length=20, nullable=TRUE)
     */
    private $filtrageValeur;

    /**
     * @ORM\Column(name="mode_paiement", type="string", length=20, nullable=TRUE)
     */
    private $modePaiement;

    /**
     *
     * @var \DateTime @ORM\Column(name="created_at", type="datetime")
     */
    private $createdAt;

    /**
     *
     * @var \DateTime @ORM\Column(name="updated_at", type="datetime")
     */
    private $updatedAt;

    public function __construct()
    {
        $this->createdAt = $this->createdAt ? $this->createdAt : new \DateTime('NOW');
        $this->updatedAt = new \DateTime('NOW');
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set motifRefus
     *
     * @param string $motifRefus
     *
     * @return Response
     */
    public function setMotifRefus($motifRefus)
    {
        $this->motifRefus = $motifRefus;

        return $this;
    }

    /**
     * Get motifRefus
     *
     * @return string
     */
    public function getMotifRefus()
    {
        return $this->motifRefus;
    }

    /**
     * Set ipClient
     *
     * @param string $ipClient
     *
     * @return Response
     */
    public function setIpClient($ipClient)
    {
        $this->ipClient = $ipClient;

        return $this;
    }

    /**
     * Get ipClient
     *
     * @return string
     */
    public function getIpClient()
    {
        return $this->ipClient;
    }

    /**
     * Set codeRetour
     *
     * @param string $codeRetour
     *
     * @return Response
     */
    public function setCodeRetour($codeRetour)
    {
        $this->codeRetour = $codeRetour;

        return $this;
    }

    /**
     * Get codeRetour
     *
     * @return string
     */
    public function getCodeRetour()
    {
        return $this->codeRetour;
    }

    /**
     * Set montant
     *
     * @param string $montant
     *
     * @return Response
     */
    public function setMontant($montant)
    {
        $this->montant = $montant;

        return $this;
    }

    /**
     * Get montant
     *
     * @return string
     */
    public function getMontant()
    {
        return $this->montant;
    }

    /**
     * Set currency
     *
     * @param string $currency
     *
     * @return Response
     */
    public function setCurrency($currency)
    {
        $this->currency = $currency;

        return $this;
    }

    /**
     * Get currency
     *
     * @return string
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return Response
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set reference
     *
     * @param string $reference
     *
     * @return Response
     */
    public function setReference($reference)
    {
        $this->reference = $reference;

        return $this;
    }

    /**
     * Get reference
     *
     * @return string
     */
    public function getReference()
    {
        return $this->reference;
    }

    /**
     * Set texteLibre
     *
     * @param string $texteLibre
     *
     * @return Response
     */
    public function setTexteLibre($texteLibre)
    {
        $this->texteLibre = $texteLibre;

        return $this;
    }

    /**
     * Get texteLibre
     *
     * @return string
     */
    public function getTexteLibre()
    {
        return $this->texteLibre;
    }

    /**
     * Set cvx
     *
     * @param boolean $cvx
     *
     * @return Response
     */
    public function setCvx($cvx)
    {
        $this->cvx = $cvx;

        return $this;
    }

    /**
     * Get cvx
     *
     * @return string
     */
    public function getCvx()
    {
        return $this->cvx;
    }

    /**
     * Set vld
     *
     * @param integer $vld
     *
     * @return Response
     */
    public function setVld($vld)
    {
        $this->vld = $vld;

        return $this;
    }

    /**
     * Get vld
     *
     * @return integer
     */
    public function getVld()
    {
        return $this->vld;
    }

    /**
     * Set brand
     *
     * @param string $brand
     *
     * @return Response
     */
    public function setBrand($brand)
    {
        $this->brand = $brand;

        return $this;
    }

    /**
     * Get brand
     *
     * @return string
     */
    public function getBrand()
    {
        return $this->brand;
    }

    /**
     * Set status3ds
     *
     * @param integer $status3ds
     *
     * @return Response
     */
    public function setStatus3ds($status3ds)
    {
        $this->status3ds = $status3ds;

        return $this;
    }

    /**
     * Get status3ds
     *
     * @return integer
     */
    public function getStatus3ds()
    {
        return $this->status3ds;
    }

    /**
     * Set numAuto
     *
     * @param integer $numAuto
     *
     * @return Response
     */
    public function setNumAuto($numAuto)
    {
        $this->numAuto = $numAuto;

        return $this;
    }

    /**
     * Get numAuto
     *
     * @return integer
     */
    public function getNumAuto()
    {
        return $this->numAuto;
    }

    /**
     * Set origineCb
     *
     * @param string $origineCb
     *
     * @return Response
     */
    public function setOrigineCb($origineCb)
    {
        $this->origineCb = $origineCb;

        return $this;
    }

    /**
     * Get origineCb
     *
     * @return string
     */
    public function getOrigineCb()
    {
        return $this->origineCb;
    }

    /**
     * Set binCb
     *
     * @param integer $binCb
     *
     * @return Response
     */
    public function setBinCb($binCb)
    {
        $this->binCb = $binCb;

        return $this;
    }

    /**
     * Get binCb
     *
     * @return integer
     */
    public function getBinCb()
    {
        return $this->binCb;
    }

    /**
     * Set hpanCb
     *
     * @param string $hpanCb
     *
     * @return Response
     */
    public function setHpanCb($hpanCb)
    {
        $this->hpanCb = $hpanCb;

        return $this;
    }

    /**
     * Get hpanCb
     *
     * @return string
     */
    public function getHpanCb()
    {
        return $this->hpanCb;
    }

    /**
     * Set origineTr
     *
     * @param string $origineTr
     *
     * @return Response
     */
    public function setOrigineTr($origineTr)
    {
        $this->origineTr = $origineTr;

        return $this;
    }

    /**
     * Get origineTr
     *
     * @return string
     */
    public function getOrigineTr()
    {
        return $this->origineTr;
    }

    /**
     * Set veres
     *
     * @param string $veres
     *
     * @return Response
     */
    public function setVeres($veres)
    {
        $this->veres = $veres;

        return $this;
    }

    /**
     * Get veres
     *
     * @return string
     */
    public function getVeres()
    {
        return $this->veres;
    }

    /**
     * Set pares
     *
     * @param string $pares
     *
     * @return Response
     */
    public function setPares($pares)
    {
        $this->pares = $pares;

        return $this;
    }

    /**
     * Get pares
     *
     * @return string
     */
    public function getPares()
    {
        return $this->pares;
    }

    /**
     * Set filtrageCause
     *
     * @param string $filtrageCause
     *
     * @return Response
     */
    public function setFiltrageCause($filtrageCause)
    {
        $this->filtrageCause = $filtrageCause;

        return $this;
    }

    /**
     * Get filtrageCause
     *
     * @return string
     */
    public function getFiltrageCause()
    {
        return $this->filtrageCause;
    }

    /**
     * Set filtrageValeur
     *
     * @param string $filtrageValeur
     *
     * @return Response
     */
    public function setFiltrageValeur($filtrageValeur)
    {
        $this->filtrageValeur = $filtrageValeur;

        return $this;
    }

    /**
     * Get filtrageValeur
     *
     * @return string
     */
    public function getFiltrageValeur()
    {
        return $this->filtrageValeur;
    }

    /**
     * Set modePaiement
     *
     * @param string $modePaiement
     *
     * @return Response
     */
    public function setModePaiement($modePaiement)
    {
        $this->modePaiement = $modePaiement;

        return $this;
    }

    /**
     * Get modePaiement
     *
     * @return string
     */
    public function getModePaiement()
    {
        return $this->modePaiement;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Response
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return Response
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set request
     *
     * @param \Agfa\MoneticoPaiementBundle\Entity\Request $request
     *
     * @return Response
     */
    public function setRequest(\Agfa\MoneticoPaiementBundle\Entity\Request $request = null)
    {
        $this->request = $request;

        return $this;
    }

    /**
     * Get request
     *
     * @return \Agfa\MoneticoPaiementBundle\Entity\Request
     */
    public function getRequest()
    {
        return $this->request;
    }
}
