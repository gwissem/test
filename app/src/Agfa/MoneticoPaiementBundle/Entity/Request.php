<?php
namespace Agfa\MoneticoPaiementBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Request
 *
 * @ORM\Table(name="mntc_request")
 * @ORM\MappedSuperclass(repositoryClass="Agfa\MoneticoPaiementBundle\Repository\RequestRepository")
 */
class Request
{

    const REFERENCE_MAX_LENGTH = 12;

    /**
     * @ORM\OneToOne(targetEntity="\Agfa\MoneticoPaiementBundle\Entity\Response", inversedBy="request", cascade={"REMOVE"})
     * @ORM\JoinColumn(name="response_id", referencedColumnName="id")
     */
    private $response;

    /**
     *
     * @var int @ORM\Column(name="id", type="integer")
     *      @ORM\Id
     *      @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="tpe", type="integer", length=7)
     */
    private $tpe;

    /**
     * @ORM\Column(name="montant", type="float", precision=10, scale=2)
     */
    private $montant;

    /**
     * @ORM\Column(name="reference", type="string", length=12, unique=TRUE)
     */
    private $reference;

    /**
     * @ORM\Column(name="texte_libre", type="text", nullable=TRUE)
     */
    private $texteLibre;

    /**
     * @ORM\Column(name="user_session", type="string", length=40, nullable=true)
     */
    private $userSession;

    /**
     *
     * @var \DateTime @ORM\Column(name="created_at", type="datetime")
     */
    private $createdAt;

    /**
     *
     * @var \DateTime @ORM\Column(name="updated_at", type="datetime")
     */
    private $updatedAt;

    public function __construct()
    {
        $this->createdAt = $this->createdAt ? $this->createdAt : new \DateTime('NOW');
        $this->updatedAt = new \DateTime('NOW');
        $this->responses = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set tpe
     *
     * @param integer $tpe
     *
     * @return Request
     */
    public function setTpe($tpe)
    {
        $this->tpe = $tpe;

        return $this;
    }

    /**
     * Get tpe
     *
     * @return integer
     */
    public function getTpe()
    {
        return $this->tpe;
    }

    /**
     * Set montant
     *
     * @param string $montant
     *
     * @return Request
     */
    public function setMontant($montant)
    {
        $this->montant = $montant;

        return $this;
    }

    /**
     * Get montant
     *
     * @return string
     */
    public function getMontant()
    {
        return $this->montant;
    }

    /**
     * Set reference
     *
     * @param string $reference
     *
     * @return Request
     */
    public function setReference($reference)
    {
        $this->reference = $reference;

        return $this;
    }

    /**
     * Get reference
     *
     * @return string
     */
    public function getReference()
    {
        return $this->reference;
    }

    /**
     * Set texteLibre
     *
     * @param string $texteLibre
     *
     * @return Request
     */
    public function setTexteLibre($texteLibre)
    {
        $this->texteLibre = $texteLibre;

        return $this;
    }

    /**
     * Get texteLibre
     *
     * @return string
     */
    public function getTexteLibre()
    {
        return $this->texteLibre;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Request
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return Request
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set response
     *
     * @param \Agfa\MoneticoPaiementBundle\Entity\Response $response
     *
     * @return Request
     */
    public function setResponse(\Agfa\MoneticoPaiementBundle\Entity\Response $response = null)
    {
        $this->response = $response;

        return $this;
    }

    /**
     * Get response
     *
     * @return \Agfa\MoneticoPaiementBundle\Entity\Response
     */
    public function getResponse()
    {
        return $this->response;
    }

    /**
     * Set userSession
     *
     * @param string $userSession
     *
     * @return Request
     */
    public function setUserSession($userSession)
    {
        $this->userSession = $userSession;

        return $this;
    }

    /**
     * Get userSession
     *
     * @return string
     */
    public function getUserSession()
    {
        return $this->userSession;
    }
}
