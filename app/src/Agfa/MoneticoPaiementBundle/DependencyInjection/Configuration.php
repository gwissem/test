<?php

namespace Agfa\MoneticoPaiementBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files.
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/configuration.html}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritdoc}
     * reference_code: used to differentiate online payments from manual payments. Keep a max of three letters for prod so to leave enough digits for ref ids.
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('agfa_monetico_paiement');

        // Here you should define the parameters that are allowed to
        // configure your bundle. See the documentation linked above for
        // more information on that topic.
        $rootNode
            ->children()
                ->arrayNode('parameters')
                    ->children()
                        ->scalarNode('tpe')->end()
                        ->scalarNode('cle')->end()
                        ->scalarNode('language')->defaultValue('fr')->cannotBeEmpty()->end()
                        ->scalarNode('currency')->defaultValue('EUR')->cannotBeEmpty()->end()
                        ->scalarNode('societe')->end()
                        ->scalarNode('email')->end()
                        ->floatNode('version')->defaultValue('3.0')->end()
                        ->booleanNode('debug')->defaultValue(FALSE)->end()
                        ->scalarNode('url_retour')->cannotBeEmpty()->end()
                        ->scalarNode('url_retour_ok')->cannotBeEmpty()->end()
                        ->scalarNode('url_retour_err')->cannotBeEmpty()->end()
                        ->scalarNode('reference_code')->defaultValue('MON')->end()
                    ->end()
                ->end()
                ->arrayNode('servers')
                    ->children()
                        ->arrayNode('preprod')
                            ->children()
                                ->scalarNode('paiement')->defaultValue('https://p.monetico-services.com/test/paiement.cgi')->end()
                                ->scalarNode('capture')->defaultValue('https://p.monetico-services.com/test/capture_paiement.cgi')->end()
                                ->scalarNode('recredit')->defaultValue('https://p.monetico-services.com/test/recredit_paiement.cgi')->end()
                            ->end()
                        ->end()
                        ->arrayNode('prod')
                            ->children()
                                ->scalarNode('paiement')->defaultValue('https://p.monetico-services.com/paiement.cgi')->end()
                                ->scalarNode('capture')->defaultValue('https://p.monetico-services.com/capture_paiement.cgi')->end()
                                ->scalarNode('recredit')->defaultValue('https://p.monetico-services.com/recredit_paiement.cgi')->end()
                            ->end()
                        ->end()
                    ->end()
                ->end()
            ->end()
        ;

        return $treeBuilder;
    }

    /**
     * {@inheritDoc}
     */
    public function getAlias()
    {
        return 'agfa_monetico_paiement';
    }
}
