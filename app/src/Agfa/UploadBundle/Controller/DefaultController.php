<?php

namespace Agfa\UploadBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Agfa\UploadBundle\Form\Type\UploadType;
use Agfa\HpaBundle\Entity\Upload;
use Agfa\HpaBundle\Entity\Journal;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;

class DefaultController extends Controller
{
    /**
     * @Route("/fichiers", name="uploads")
     * @Template("AgfaUploadBundle:Default:index")
     * @Method("GET")
     *
     * page principale des documents personnels
     */
    public function indexAction()
    {
        $form = $this->createForm(UploadType::class, new Upload(), [
                    'action' => $this->generateUrl('upload_upload')
            ]);

        return $this->render('AgfaUploadBundle:Default:uploads.html.twig', [
                'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/envoyer_fichier", name="upload_upload")
     * @Template("AgfaUploadBundle:Default:upload")
     * @Method("POST")
     *
     * Gère l'envoi de fichier par le formulaire d'upload
     */
    public function uploadAction()
    {
        $upload = new Upload();

        $form = $this->createForm(UploadType::class, $upload, [
                'action' => $this->generateUrl('upload_upload')
        ]);

        $form->handleRequest($this->getRequest());

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            /* @var $file \Symfony\Component\HttpFoundation\File\UploadedFile */
            $file = $form['file']->getData();
            $this->get('logger')->debug($file->getClientOriginalName());

            $clientOriginalName= $file->getClientOriginalName();

            $upload->setDateCreation(new \DateTime());
            $upload->setFilename($clientOriginalName);
            $upload->setIsAttachment(FALSE);
            $upload->setMimetype($file->getMimeType());
            $upload->setPersonne($this->get('contexte')->getPersonneContexte());

            $file->move($this->get('kernel')->getCacheDir(), $clientOriginalName);
            $filepath = $this->get('kernel')->getCacheDir() . DIRECTORY_SEPARATOR . $clientOriginalName;

            $upload->setFilesize(filesize($filepath));
            $upload->setContent(file_get_contents($filepath));

            $em->persist($upload);
            $this->get('journalist')->createEntry(Journal::EVENT_UPLOAD_CREATION, $upload, $upload->getPersonne()->getUser());
            $em->flush();

            return $this->redirect($this->generateUrl('uploads'));
        }

        return $this->render('AgfaUploadBundle:Default:upload_form.html.twig', [
                'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/fichier/{id}/{filename}", name="upload_download", requirements={"id":"\d+"})
     * @Template("AgfaUploadBundle:Default:download")
     * @Method("GET")
     *
     * @param unknown $id
     * @param unknown $filename
     * @param Request $request
     * @throws UnauthorizedHttpException
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function downloadAction($id, $filename, Request $request)
    {
        /* @var $user User */
        $user = $this->getUser();

        /* @var $em \Doctrine\ORM\EntityManager */
        $em = $this->getDoctrine()->getManager();

        /* @var $upload Upload */
        $upload = $em->getRepository('AgfaUploadBundle:Upload')->find($id);

        $personne = $upload->getPersonne();

        if($personne->getUser()->getId() !== $user->getId()) {
            throw new UnauthorizedHttpException("Vous n'avez pas le droit de voir ce compte rendu.");
        }

        $this->get('logger')->debug('size:'.$upload->getFilesize());

        $response = new Response();
        $response->setPrivate();
        $response->headers->set('Cache-Control', 'must-revalidate, post-check=0, pre-check=0');
        $response->headers->set('Content-Type', $upload->getMimetype());
        $response->headers->set('Content-Length', $upload->getFilesize());
        $response->setContent(stream_get_contents($upload->getContent()));
        return $response;
    }
}
