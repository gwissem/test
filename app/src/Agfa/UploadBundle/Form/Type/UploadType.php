<?php

namespace Agfa\UploadBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class UploadType extends AbstractType
{
    public function __construct()
    {
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
        ->add('description', 'text', ['label' => 'Description'])
        ->add('file', 'file', ['label' => 'Fichier', 'mapped' => FALSE]);

    }

    public function getName()
    {
        return 'upload';
    }

}