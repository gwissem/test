<?php
namespace MockApiBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Agfa\HpaBundle\Services\CommunicationBioserveur;

/**
 * Mock API to test the reception of data before developping BioServeur's side.
 *
 * @Route("/mock-api/")
 */
class DefaultController extends Controller
{

    /**
     * Bioserveur receives a demand from HPA and respond for the first time through the API (enrollment) with the following CR.
     * @Route("enrollment/{type}/")
     *
     * @param string $type
     * @return \Symfony\Component\HttpFoundation\JsonResponse JSON formated response.
     */
    public function enrollmentAction($type)
    {
        // $xmlFile = file_get_contents(__DIR__ . '/../Resources/files/editcr/CR_01201620090019694.xml');
        $xmlFile = file_get_contents(__DIR__ . '/../Resources/files/editcr/existing_patient.xml');

        // Change "contenu>formt" to pdf for direct access or editcr for access while enrolled
        switch ($type) {
            case 'enrolement':
                $result = array(
                    "compte_rendu" => array(
                        "contenu" => array(
                            "base64" => true,
                            "format" => "editcr", // Change for 'pdf' to test direct access, editcr otherwise
                            "data" => "'" . base64_encode($xmlFile) . "'"
                        ),
                        "numero_demande" => "B6022610001",
                        "identifiant_structure" => "3543-2",
                        "identifiant_patient" => "B6022600001",
                        "solde_patient" => 118.23, // Has to be a float with 2 decimal. If 10, then 10.00
                        "source_paiement" => FALSE, // Can 'not_paid' for unpaid CR, 'admin' when payment is forced by an admin, 'labo' or 'online'
                        "affichage_cr" => FALSE,
                        "entite_juridique" => array(
                            "finess" => "111111111",
                            "code_ej" => "agfa",
                            "code_postal" => "",
                            "raison_sociale" => "",
                            "adresse" => "",
                            "libelle" => "Nom de l'entité juridique"
                        ),
                        "laboratoire" => array(
                            "login" => "222222222",
                            "nom" => "Labo Truc",
                            "paiement" => array(
                                "tpe" => null,
                                "cle" => "1094E7FAA900A26D5780648357C156D5FE54799F",
                                "societe" => "agfahealthcare",
                                "nom" => "Marley",
                                "prenom" => "Bob",
                                "email" => "bob@gmail.com",
                                "telephone" => "01.23.45.67.89",
                                "paiement_affichage_cr" => FALSE,
                                "paiement_actif" => TRUE
                            )
                        )
                    ),
                    "statut" => CommunicationBioserveur::API_STATUS_OK
                    // "portail_url" => "dev.mesanalyses.fr",
                    // "message_erreur" => "That's a system error!",
                    // "message_patient" => "That's a patient error!"
                );
                break;
            case 'direct':
                $result = array(
                    "compte_rendu" => array(
                        "contenu" => array(
                            "base64" => true,
                            "format" => "pdf", // Change for 'pdf' to test direct access, editcr otherwise
                            "data" => "'" . base64_encode($xmlFile) . "'"
                        ),
                        "numero_demande" => "B6022610001",
                        "identifiant_structure" => "3543-2",
                        "identifiant_patient" => "B6022600001",
                        "solde_patient" => 118.23, // Has to be a float with 2 decimal. If 10, then 10.00
                        "source_paiement" => 'not_paid', // Can be 'not_paid', 'admin' when payment is forced by an admin, 'labo' or 'online'
                        "affichage_cr" => FALSE,
                        "entite_juridique" => array(
                            "finess" => "111111111",
                            "code_ej" => "agfa",
                            "code_postal" => "",
                            "raison_sociale" => "",
                            "adresse" => "",
                            "libelle" => "Nom de l'entité juridique"
                        ),
                        "laboratoire" => array(
                            "login" => "222222222",
                            "nom" => "Labo Truc",
                            "paiement" => array(
                                "tpe" => "0002114",
                                "cle" => "1094E7FAA900A26D5780648357C156D5FE54799F",
                                "societe" => "agfahealthcare",
                                "nom" => "Marley",
                                "prenom" => "Bob",
                                "email" => "bob@gmail.com",
                                "telephone" => "01.23.45.67.89",
                                "paiement_affichage_cr" => FALSE,
                                "paiement_actif" => FALSE
                            )
                        )
                    ),
                    "statut" => CommunicationBioserveur::API_STATUS_BAD_PORTAL,
                    "portail_url" => "dev.mesanalyses.fr",
                    "message_erreur" => "That's a system error!",
                    "message_patient" => "That's a patient error!"
                );
                break;
            case 'error':
                $result = array(
                    "compte_rendu" => array(
                        "contenu" => array(
                            "base64" => true,
                            "format" => "editcr", // Change for 'pdf' to test direct access, editcr otherwise
                            "data" => "'" . base64_encode($xmlFile) . "'"
                        ),
                        "numero_demande" => "B6022610001",
                        "identifiant_structure" => "3543-2",
                        "identifiant_patient" => "B6022600001",
                        "solde_patient" => 118.23, // Has to be a float with 2 decimal. If 10, then 10.00
                        "source_paiement" => 'not_paid', // Can be 'not_paid', 'admin' when payment is forced by an admin, 'labo' or 'online'
                        "affichage_cr" => TRUE,
                        "entite_juridique" => array(
                            "finess" => "111111111",
                            "code_ej" => "agfa",
                            "code_postal" => "",
                            "raison_sociale" => "",
                            "adresse" => "",
                            "libelle" => "Nom de l'entité juridique"
                        ),
                        "laboratoire" => array(
                            "login" => "222222222",
                            "nom" => "Labo Truc",
                            "paiement" => array(
                                "tpe" => "0002114",
                                "cle" => "1094E7FAA900A26D5780648357C156D5FE54799F",
                                "societe" => "agfahealthcare",
                                "nom" => "Marley",
                                "prenom" => "Bob",
                                "email" => "bob@gmail.com",
                                "telephone" => "01.23.45.67.89",
                                "paiement_affichage_cr" => TRUE,
                                "paiement_actif" => FALSE
                            )
                        )
                    ),
                    "statut" => CommunicationBioserveur::API_STATUS_BAD_IDENTIFIER,
                    "portail_url" => "dev.mesanalyses.fr",
                    "message_erreur" => ""
                );
                break;
        }

        header('Content-Type: application/json');
        return new JsonResponse($result);

        // return $this->render('MockApiBundle:Default:index.html.twig');
    }

    /**
     * HPA receive informations to update from BioServeur regarding a CR.
     * @Route("cr/update/partial", name="default_cr_update_partial")
     *
     * @param string $type
     * @return \Symfony\Component\HttpFoundation\JsonResponse JSON formated response.
     */
    public function partialUpdateCrAction()
    {
        $url = $this->generateUrl('api_cr_update_partial', array(), UrlGeneratorInterface::ABSOLUTE_URL);

        $request = array(
            "authentification" => array(
                "passphrase" => "test"
            ),
            "compte_rendu" => array(
                "numero_demande" => "B6022610001",
                "identifiant_structure" => "3543-2",
                "identifiant_patient" => "B6022600001",
                "affichage_cr" => true,
                "solde_patient" => 42.25, // Has to be a float with 2 decimal. If 10, then 10.00
                "source_paiement" => 'labo' // Can be false or 'not_paid' for unpaid, 'labo' or 'online'
            )
        );

        $client = new \GuzzleHttp\Client();
        $response = $client->request('POST', $url, array(
            'json' => $request,
            'exceptions' => FALSE
        ));

        $result = (string) $response->getBody();

        return new Response($result);
    }

    /**
     * Simulates the reception of a new CR from BioServeur when the patient already
     * has an account created and is known to BioServeur (was enroled before).
     *
     * @Route("cr/new", name="default_cr_new")
     *
     * @return \Symfony\Component\HttpFoundation\Response JSON formated response.
     */
    public function newCrAction()
    {
        $url = $this->generateUrl('api_publication_compte_rendu', array(), UrlGeneratorInterface::ABSOLUTE_URL);

        $xmlFile = file_get_contents(__DIR__ . '/../Resources/files/editcr/existing_patient_3.xml');

        $request = array(
            "authentification" => array(
                "passphrase" => "test"
            ),
            "compte_rendu" => array(
                "identifiant_structure" => "1234",
                "identifiant_patient" => "B6022600001",
                "numero_demande" => "xxx123",
                "affichage_cr" => TRUE,
                "contenu" => array(
                    "base64" => true,
                    "format" => "editcr",
                    "data" => "'" . base64_encode($xmlFile) . "'"
                ),
                "solde_patient" => 42.20, // Has to be a float with 2 decimal. If 10, then 10.00
                "source_paiement" => FALSE, // Can be false or 'not_paid' for unpaid, 'labo' or 'online'
                "entite_juridique" => array(
                    "finess" => "111111111",
                    "code_ej" => "agfa",
                    "code_postal" => "",
                    "raison_sociale" => "",
                    "adresse" => "",
                    "libelle" => "Nom de l'entité juridique"
                ),
                "laboratoire" => array(
                    "login" => "222222222",
                    "nom" => "Labo NewTruc",
                    "paiement" => array(
                        "tpe" => "0002114",
                        "cle" => "1094E7FAA900A26D5780648357C156D5FE54799F",
                        "societe" => "agfahealthcare",
                        "nom" => "Marley",
                        "prenom" => "Yves",
                        "email" => "bob@gmail.com",
                        "telephone" => "01.23.45.67.89",
                        "paiement_affichage_cr" => FALSE,
                        "paiement_actif" => TRUE
                    )
                )
            )
        );

        $client = new \GuzzleHttp\Client();
        $response = $client->request('POST', $url, array(
            'json' => $request,
            'exceptions' => FALSE
        ));

        $result = (string) $response->getBody();

        return new Response($result);
    }

    /**
     * BioServeur deletes a CR through the API.
     *
     * @Route("cr/delete/", name="default_cr_delete")
     *
     * @return \Symfony\Component\HttpFoundation\Response JSON formated response.
     */
    public function deleteCrAction()
    {
        $url = $this->generateUrl('api_notification_suppression', array(), UrlGeneratorInterface::ABSOLUTE_URL);

        $request = array(
            "authentification" => array(
                "passphrase" => "test"
            ),
            "notification_suppression" => array(
                "timestamp" => new \DateTime("NOW"),
                "compte_rendu" => array(
                    "identifiant_structure" => "AGFA", // The one from the editCr
                    "identifiant_patient" => "B6022600001",
                    "numero_demande" => "B6053110200"
                )
            )
        );

        $client = new \GuzzleHttp\Client();
        $response = $client->request('POST', $url, array(
            'json' => $request,
            'exceptions' => TRUE
        ));

        $result = (string) $response->getBody();

        return new Response($result);
    }

    /**
     * Received from Bioserveur
     */

    /**
     * BioServeur sends a response after HPA has let it know about all CRs that have been paid.
     *
     * @Route("cr/update-payment/", name="default_cr_update_payment")
     * @Route("cr/update-reading/", name="default_cr_update_reading")
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response JSON formated response.
     */
    public function updatePaymentAction(Request $request)
    {
        // $res = json_encode(array("statut" => "OK", "message_erreur" => ""));

        // Test with two demandes: one good, the other one has a problem
        $res = json_encode(array(
            "authentification" => array(
                "passphrase" => "test"
            ),
            "statut" => "DEMANDE_INCONNU",
            "message_erreur" => "",
            "resultat_maj_paiement" => array(
                array(
                    "nodemx" => "Dem_01x",
                    "statut" => "DEMANDE_INCONNU",
                    "message_erreur" => "..."
                ),
                array(
                    "nodemx" => "Dem_02x",
                    "statut" => "OK",
                    "message_erreur" => "..."
                )
            )
        ));

        // Test with one demande, all ok
        // $res = json_encode(array(
        // "statut" => "OK",
        // "message_erreur" => "",
        // "resultat_maj_paiement" => null
        // ));

        return new Response($res);
    }

    /**
     * @Route("update-unenrollment/", name="default_update_unenrollment")
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response JSON formated response.
     */
    public function updateUnenrollmentAction(Request $request)
    {
        $res = json_encode(array(
            "authentification" => array(
                "passphrase" => "test"
            ),
            "statut" => "PATIENT_INCONNU", // OK, PATIENT_INCONNU...
            "message_erreur" => "",
            "finess" => "123456789"
        ));

        return new Response($res);
    }

    /**
     * Bioserveur creates or updates an existing entry in Paiement and
     * link Etablissement to it if necessary.
     *
     * @Route("ej/update/", name="default_ej_update")
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function updateEntiteJuridiqueAction(Request $request)
    {
        $url = $this->generateUrl('api_ej_update', array(), UrlGeneratorInterface::ABSOLUTE_URL);
        // // ORIGINAL
        // $apiRequest = [
        // "authentification" => [
        // "passphrase" => "test"
        // ],
        // "statut" => "DEMANDE_INCONNU",
        // "message_erreur" => "",
        // "entite_juridique" => [
        // "finess" => "111111111",
        // "liste_labo_login" => [
        // 222222222,
        // 333333333
        // ],
        // "code_ej" => "agfa",
        // "code_postal" => "",
        // "raison_sociale" => "",
        // "adresse" => "",
        // "libelle" => "Nom de l'entité juridique",
        // "paiement" => [
        // "affichage_cr" => FALSE,
        // "actif" => TRUE,
        // "tpe" => "0002115",
        // "cle" => "1094E7FAA900A26D5780648357C156D5FE54799F",
        // "nom" => "MARLEY",
        // "prenom" => "Jean",
        // "telephone" => "0123456789",
        // "email" => "bob@gmail.com",
        // "societe" => "Les Griffes SAS",
        // "liste_labo_login" => [
        // 11111111,
        // 222222222
        // ]
        // ]
        // ]
        // ];

        // TEST EN COUR AVEC BUG
        $apiRequest = [
            "authentification" => [
                "passphrase" => "test"
            ],
            "statut" => "DEMANDE_INCONNU",
            "message_erreur" => "",
            "entite_juridique" => [
                "finess" => "111111111",
                "liste_labo_login" => [
                    "222222222",
                    "333333333"
                ],
                "code_ej" => "agfa",
                "code_postal" => "",
                "raison_sociale" => "",
                "adresse" => "",
                "libelle" => "Nom de l'entité juridique",
                "paiement" => [
                    "affichage_cr" => FALSE,
                    "actif" => FALSE,
                    "tpe" => "0002115",
                    "cle" => "1094E7FAA900A26D5780648357C156D5FE54799F",
                    "nom" => null,
                    "prenom" => null,
                    "telephone" => null,
                    "email" => "bob@gmail.com",
                    "societe" => "Les Griffes SAS",
                    "liste_labo_login" => [
                        "11111111",
                        "222222222"
                    ]
                ]
            ]
        ];

        $client = new \GuzzleHttp\Client();
        $response = $client->request('POST', $url, array(
            'json' => $apiRequest,
            'exceptions' => FALSE
        ));

        $result = (string) $response->getBody();

        return new Response($result);
    }
}
