<?php

namespace Scheb\TwoFactorBundle\Model;

use Symfony\Component\HttpFoundation\Request;

interface TrustedComputerInterface
{
    /**
     * Add a trusted computer token.
     *
     * @param string    $token
     * @param \DateTime $validUntil
     */
    public function addTrustedComputer($token, \DateTime $validUntil, Request $request);

    /**
     * Validate a trusted computer token.
     *
     * @param string $token
     *
     * @return bool
     */
    public function isTrustedComputer($token);
}
