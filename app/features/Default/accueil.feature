Feature: Pade d'accueil 
	Afin de pouvoir utiliser l'application
	En tant que visiteur
	Je dois être capable de créer et gérer mon compte
	
	Scenario: Je crée mon compte 
		Given: I am on the front page
		When I follow "My account" 
		Then I should be on "/account" 
		And I should see "S'authentifier"