//var CACHE_NAME = 'my-site-cache-v1';
//var urlsToCache = [ '/aide/' ];
//
//self.addEventListener('install', function(event) {
//	// Perform install steps
//	event.waitUntil(caches.open(CACHE_NAME).then(function(cache) {
//		console.log('Opened cache');
//		return cache.addAll(urlsToCache);
//	}));
//});
//
//self.addEventListener('fetch', function(event) {
//	if (event.request.method !== 'GET') {
//		/*
//		 * If we don't block the event as shown below, then the request will go
//		 * to the network as usual.
//		 */
//		console.log('WORKER: fetch event ignored.', event.request.method,
//				event.request.url);
//		return;
//	}
//
//	event.respondWith(caches.match(event.request).then(function(response) {
//		// Cache hit - return response
//		if (response) {
//			return response;
//		}
//		return fetch(event.request);
//	}));
//});