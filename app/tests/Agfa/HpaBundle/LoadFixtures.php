<?php
namespace Tests\Agfa\HpaBundle;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Doctrine\Common\DataFixtures\Loader;
use Doctrine\Common\DataFixtures\Executor\ORMExecutor;
use Doctrine\Common\DataFixtures\Purger\ORMPurger;

class LoadFixtures extends WebTestCase
{

    protected static $em;

    protected static $container;

    protected static $translator;

    protected static $router;

    public static function setUpBeforeClass()
    {
        // start the symfony kernel
        $kernel = static::createKernel();
        $kernel->boot();

        // get the DI container
        self::$container = $kernel->getContainer();

        // now we can instantiate our service (if you want a fresh one for
        // each test method, do this in setUp() instead
        self::$translator = self::$container->get('translator');
        self::$router = self::$container->get('router');

        self::$em = self::$container->get('doctrine')->getManager();

        self::regenerateSchema();
        self::loadFixtures();
    }

    /**
     * Drops current schema and creates a brand new one
     */
    private static function regenerateSchema()
    {
        $metadatas = self::$em->getMetadataFactory()->getAllMetadata();
        if (! empty($metadatas)) {
            $tool = new \Doctrine\ORM\Tools\SchemaTool(self::$em);
            $tool->dropSchema($metadatas);
            $tool->createSchema($metadatas);
        }
    }

    private static function loadFixtures()
    {
        $loader = new Loader();

        // Dev fixtures
        $loader->addFixture(new \Agfa\HpaBundle\DataFixtures\ORM\Dev\ConfigurationData());
        $loader->addFixture(new \Agfa\HpaBundle\DataFixtures\ORM\Dev\StructureData());
        $loader->addFixture(new \Agfa\HpaBundle\DataFixtures\ORM\Dev\LaboData());
        $loader->addFixture(new \Agfa\HpaBundle\DataFixtures\ORM\Dev\UserData());
        $loader->addFixture(new \Agfa\HpaBundle\DataFixtures\ORM\Dev\PersonneData());
        $loader->addFixture(new \Agfa\HpaBundle\DataFixtures\ORM\Dev\PatientData());
        $loader->addFixture(new \Agfa\HpaBundle\DataFixtures\ORM\Dev\DemandeData());
        $loader->addFixture(new \Agfa\HpaBundle\DataFixtures\ORM\Dev\ChapitreData());
        $loader->addFixture(new \Agfa\HpaBundle\DataFixtures\ORM\Dev\ExamenData());
        $loader->addFixture(new \Agfa\HpaBundle\DataFixtures\ORM\Dev\ElementData());
        $loader->addFixture(new \Agfa\HpaBundle\DataFixtures\ORM\Dev\ResultatData());

        $purger = new ORMPurger(self::$em);
        $executor = new ORMExecutor(self::$em, $purger);
        $executor->execute($loader->getFixtures());
    }
}