<?php
namespace Tests\Agfa\HpaBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Tests\Agfa\HpaBundle\LoadFixtures;
use Tests\Agfa\AuthenticateUser;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class DefaultControllerTest extends LoadFixtures
{

    const LINK_DESKTOP_LOGIN = 'link-desktop-login';

    const LINK_MOBILE_LOGIN = 'link-mobile-login';

    const LINK_LOGO_HOMEPAGE = 'link-logo-homepage';

    /**
     * Test content on the front page
     */
    public function testIndex()
    {
    // $client = static::createClient();
    // $translator = self::$translator;

    // $this->crawler = $client->request('GET', '/');

    // $this->assertEquals(200, $client->getResponse()
    // ->getStatusCode());

    // // Header
    // $desktopLinkText = $translator->trans('agfa.hpa_bundle.include.top_public.desktop.link.login_page');
    // $mobileLinkText = $translator->trans('agfa.hpa_bundle.include.top_public.mobile.link.login_page');

    // $this->assertContains($desktopLinkText, $this->crawler->filter('div.hidden-xs>a')
    // ->text());
    // $this->assertContains($mobileLinkText, $this->crawler->filter('div.visible-xs>a')
    // ->text());

    // // Content
    // $desktopLink = $this->crawler->selectLink($desktopLinkText)->link();
    // $client->click($desktopLink);

    // $this->assertEquals(200, $client->getResponse()
    // ->getStatusCode());

    // // Check that the homepage has at least the following links
    // // To log in, to create an account, direct access to results, help page and terms & conditions

    // $this->checkLink(self::LINK_DESKTOP_LOGIN, "Login link (desktop) does not exists.");
    // $this->checkLink(self::LINK_MOBILE_LOGIN, "Login link (mobile) does not exists.");
    // // $this->checkLink(self::LINK_LOGO_HOMEPAGE, "Link to homepage on logo does not exists.");

    // // $this->checkLink(self::LINK_LOGO_HOMEPAGE, "Link to homepage on logo does not exists.");

    }




    public function createOrUpdatePaymentTest(){
        $this->client = static::createClient();
        $this->client->followRedirect();






        $url = $this->generateUrl('api_set_payment', array(), UrlGeneratorInterface::ABSOLUTE_URL);

        $apiRequest = [
            "authentification" => [
                "passphrase" => "test"
            ],
            "statut" => "DEMANDE_INCONNU",
            "message_erreur" => "",
            "details_paiement" => [
                "liste_finess" => [123456],
                "nom" => "Freddy",
                "prenom" => "Krueger",
                "telephone" => "0123456789",
                "email" => "freddy@yahoo.com",
                "tpe" => "1234568",
                "cle" => "987654321",
                "societe" => "Les Griffes SAS",
                "affichage_cr" => TRUE,
                "actif" => TRUE
            ]
        ];

        $client = new \GuzzleHttp\Client();
        $response = $client->request('POST', $url, array(
            'json' => $apiRequest,
            'exceptions' => FALSE
        ));

        $result = (string) $response->getBody();

        return new Response($result);




    }


    public function testEnrollement()
    {
        $this->client = static::createClient();
        $this->client->followRedirects();

        // Use mock of communication bioserveur in order to test the API
        $communicationBioserveur = $this->createMock('Agfa\HpaBundle\Services\CommunicationBioserveur');

        static::$kernel->setKernelModifier(function ($kernel) use ($communicationBioserveur) {
            $kernel->getContainer()
                ->set('communication_bioserveur', $communicationBioserveur);
        });


        // Authenticate the user
        $crawler = $this->client->request('GET', self::$router->generate('index'));

        $authenticateUser = new AuthenticateUser($this->client, self::$router);
        $crawler = $authenticateUser->login('adm', 'test');

        $this->assertEquals(200, $this->client->getResponse()
            ->getStatusCode());


        // Select as which person we are going to use the service
        $link = $crawler->selectLink('Sélectionner')
            ->first()
            ->link();

        $crawler = $this->client->click($link);

        $this->assertEquals(200, $this->client->getResponse()
            ->getStatusCode());


        // Enrol a "compte rendu"
        $form = $crawler->filter('#enrol')->form();
        $form->setValues(array(
            "enrolement[login]" => "test",
            "enrolement[password]" => "testtest"
        ));

        $crawler = $this->client->submit($form);

        $this->assertEquals(200, $this->client->getResponse()
            ->getStatusCode());


        // Set which person is going to get the "compte rendu"
        $buttonCrawlerNode= $crawler->selectButton('Affecter');
        $form = $buttonCrawlerNode->form();

        $form['affectation_patient[personne]']->select(2);
        $crawler = $this->client->submit($form);

        $this->assertEquals(200, $this->client->getResponse()
            ->getStatusCode());

        $this->assertTrue($crawler->filter('h1:contains("Compte rendu")')->count() == 1);




//         \file_put_contents(__DIR__ . '/dump.html', $this->client->getResponse());

    }

    /**
     * Check that a link is on the page.
     * Display message if not.
     *
     * @param unknown $id
     * @param unknown $mssg
     */
    private function checkLink($id, $mssg)
    {
        $node = $this->crawler->filterXPath('//a[@id="' . $id . '"]');
        $this->assertTrue($node->count() == 1, $mssg);
    }
}
