<?php
namespace Tests\Agfa\HpaBundle\Security;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\Translation\Translator;
use Symfony\Component\Translation\Loader\YamlFileLoader;
use Doctrine\ORM\Tools\Setup;
use Tests\Agfa\HpaBundle\LoadFixtures;

/**
 */
class PublicAccessTest extends LoadFixtures
{

    private $route;

    private $client;

    private $translatorFosUser;

    private $config;

    private $random = "";

    // Fonction permettant de récupérer la liste des routes
    private function getAllRoutes()
    {
        return array(
            array(
                'message' => "Accueil",
                'route' => 'default_accueil',
                'params' => null,
                'access' => array(
                    'ROLE_LABO' => true,
                    'ROLE_PATIENT' => true,
                    'ROLE_SUPPORT' => true,
                    'ROLE_ADMIN' => true
                )
            )
        );
    }

    // Fonction permettant de récupérer la liste des roles
    private function getAllRoles()
    {
        return array(
            // array(
            // 'role' => 'DEBUG',
            // 'username' => 'can5',
            // 'password' => 'test'
            // ),
            array(
                'role' => 'ROLE_LABO',
                'username' => 'lab',
                'password' => 'test'
            ),
            array(
                'role' => 'ROLE_COMMERCE',
                'username' => 'com',
                'password' => 'test'
            ),
            array(
                'role' => 'ROLE_SUPPORT',
                'username' => 'sup',
                'password' => 'test'
            ),
            array(
                'role' => 'ROLE_ADMIN',
                'username' => 'adm',
                'password' => 'test'
            )
        );
    }

    public function setUp()
    {
        parent::setUp();

        // Create new client
        $this->client = static::createClient();

        // // Loading translation file
        // $this->translator = new Translator('fr_FR');
        // $this->translator->addLoader('yaml', new YamlFileLoader());
        // $this->translator->addResource('yaml', 'src/Novia/Bundle/Ut/AapBundle/Resources/translations/messages.fr.yml', 'fr_FR');

        // // Loading FOSUser translation file
        // $this->translatorFosUser = new Translator('fr_FR');
        // $this->translatorFosUser->addLoader('yaml', new YamlFileLoader());
        // $this->translatorFosUser->addResource('yaml', 'src/Novia/Bundle/Ut/UserBundle/Resources/translations/FOSUserBundle.fr.yml', 'fr_FR');

        // Loading routing
        $this->route = self::$container->get('router');
    }

    // Fonction permettant de comparer 2 string afin de savoir si elles sont égales ou pas
    private function comparePath($str1, $str2)
    {
        return strcmp($str1, $str2) == 0 ? true : false;
    }

    // Fonction permettant de comparer 2 nombres afin de savoir si ils sont égaux
    private function compareNumber($nbr1, $nbr2)
    {
        return $nbr1 == $nbr2 ? true : false;
    }

    // Fonction permettant de vérifier que l'on a accès à une route
    private function checkRouteValid($route, $params = NULL)
    {
        if (is_null($params)) {
            $path = $this->route->generate($route);
        } else {
            $path = $this->route->generate($route, $params);
        }

        $this->client->request('GET', $path);

        $currentUrl = $this->client->getRequest()->getRequestUri();
        $currentStatus = $this->client->getResponse()->getStatusCode();
        $currentRoute = $this->client->getRequest()->get('_route');

        echo $this->getInfos($currentStatus, $currentUrl, $currentRoute);

        $this->assertTrue($this->compareNumber($currentStatus, 200));
    }

    // Fonction permettant de vérifier que l'on n'a pas accès à une route
    private function checkRouteNotValid($route, $params = NULL)
    {
        if (is_null($params)) {
            $path = $this->route->generate($route);
        } else {
            $path = $this->route->generate($route, $params);
        }

        $this->client->request('GET', $path);

        $currentUrl = $this->client->getRequest()->getRequestUri();
        $currentStatus = $this->client->getResponse()->getStatusCode();
        $currentRoute = $this->client->getRequest()->get('_route');

        echo $this->getInfos($currentStatus);

        $this->assertFalse($this->compareNumber($currentStatus, 200));
    }

    private function getInfos($currentStatus, $currentUrl = null, $currentRoute = null)
    {
        $infos = null;

        if ($currentStatus)
            $infos .= "Code HTTP : " . $currentStatus . "\n";
        if ($currentUrl)
            $infos .= "URL : " . $currentUrl . "\n";
        if ($currentRoute)
            $infos .= "Route : " . $currentRoute . "\n";

        return $infos;
    }

    private function exec($roles)
    {
        foreach ($roles as $role_key => $role_value) {
            echo "\n\n/*****************************************************************/\n";
            echo "           Rôle : " . $role_value['role'] . "\n";
            $pathLogin = $this->route->generate('fos_user_security_login');
            $login = $this->client->request('GET', $pathLogin);
            echo "/*****************************************************************/\n";

            // On récupère le formulaire de connexion
            $form = $login->selectButton('_submit')->form();

            // On enregistre dans le formulaire les nouvelles valeurs que l'on veut testées
            $username = $role_value['username'];
            $password = $role_value['password'];

            $form->get('_username')->setValue($username);
            $form->get('_password')->setValue($password);

            // On soumets le formulaire et on suit les 2 redirections
            $this->client->submit($form);

            // $this->assertEquals(200, $this->client->getResponse()
            // ->getStatusCode(), " - Login for $username / $password not ok.");

            $this->client->followRedirect();

            // On récupère toutes les routes présentes dans le tableau
            $routes = $this->getAllRoutes();

            // On va tester toutes les routes que l'on vient de récupérer
            foreach ($routes as $route_key => $route_value) {
                // echo "\nAccès " . $role_value['access'][$role_value['role']] . " à la page : ".$route_value['message']."\n";
                if ($route_value['access'][$role_value['role']] === true) {
                    echo "\nAccès AUTORISÉ à la page : " . $route_value['message'] . "\n";
                    $this->checkRouteValid($route_value['route'], $route_value['params']);
                } else {
                    echo "\nAccès INTERDIT à la page : " . $route_value['message'] . "\n";
                    $this->checkRouteNotValid($route_value['route'], $route_value['params']);
                }
            }
        }
    }

    // On va tester les accès de tous les rôles hors administrateur
    public function testCandidateAccessSecurity()
    {
        $roles = $this->getAllRoles();

        echo "\n\n/*****************************************************************/\n";
        echo "           TESTS DE SÉCURITÉ DU CANDIDAT\n";
        echo "/*****************************************************************/\n";

        $this->exec($roles);
    }
}