<?php
namespace Tests\Agfa\HpaBundle\Services;

use Agfa\HpaBundle\Services\CommunicationBioserveur;
use Agfa\HpaBundle\Entity\Enrolement;

class CommunicationBioserveurMock extends CommunicationBioserveur
{

    protected function getDemande(Enrolement $enrolement)
    {
        // $result = array(
        // "statut" => "MAUVAIS_IDENTIFIANTS",
        // "message_erreur" => "Unknown account",
        // "message_patient" => "Votre dossier de biologie n'a pas envoter été transmis par le laboratoire ou votre indentifiant de connection est incorect. Olé!"
        // );
        $result = array(
            "compte_rendu" => array(
                "contenu" => array(
                    "base64" => false,
                    "format" => "editcr",
                    "data" => file_get_contents($this->kernel->getRootDir() . '/../tests/Agfa/HpaBundle/Resources/files/editcr/CR_01201620090019694.xml')
//                     "data" => file_get_contents($this->kernel->getRootDir() . '/../tests/Agfa/HpaBundle/Resources/files/editcr/CR_05111149480013128.xml')
                ),
                // "contenu" => array("base64" => false, "format" => "editcr", "data" => file_get_contents($this->kernel->getRootDir() . '/../tests/AppBundle/Resurces/files/editcr/CR_05111149480013128.xml')),
                "numero_demande" => "xxx123",
                "identifiant_structure" => "2000",
                "identifiant_patient" => "B123456789"
            ),
            "statut" => "OK",
            "message_erreur" => ""
        );

        return $result;
    }
}