<?php
namespace Tests\Agfa;

class AuthenticateUser
{
    public function __construct($client, $router){
        $this->client = $client;
        $this->router = $router;
    }

    // Log the user
    public function login($username, $password)
    {
        // First logout (just in case)
        $pathLogout = $this->router->generate('fos_user_security_logout');
        $this->client->request('GET', $pathLogout);

        $pathLogin = $this->router->generate('fos_user_security_login');
        $login = $this->client->request('GET', $pathLogin);

        // On récupère le formulaire de connexion
        $form = $login->selectButton('_submit')->form();

        $form->get('_username')->setValue($username);
        $form->get('_password')->setValue($password);

        // On soumets le formulaire et on suit les 2 redirections
        $crawler = $this->client->submit($form);

        return $crawler;
    }
}
