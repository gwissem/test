<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;
use Agfa\HpaBundle\DataFixtures\ORM\Prod\ConfigurationData;

/**
 * Migration - HPA v2.0
 */
class Version20180702105000 extends AbstractMigration
{

    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        /**
         * Oracle
         */
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'oracle', 'Migration can only be executed safely on \'oracle\'.');

        $this->addSql("ALTER TABLE hpa_configuration ADD video_tuto_register VARCHAR2(255) DEFAULT 'https://www.youtube.com/embed/knHiro_yKlM?rel=0&amp;showinfo=0'");
        $this->addSql("ALTER TABLE HPA_CONFIGURATION MODIFY (video_tuto_register VARCHAR2(255) DEFAULT NULL)");
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'oracle', 'Migration can only be executed safely on \'oracle\'.');
    }

}
