<?php
namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Update for unenrollment notifications.
 */
class Version20181018114300 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        /**
         * Oracle
         */
        $this->abortIf($this->connection->getDatabasePlatform()
            ->getName() !== 'oracle', 'Migration can only be executed safely on \'oracle\'.');

        $this->addSql("DROP INDEX idx_hpa_user_email");
        $this->addSql("DROP INDEX idx_hpa_user_username");
        $this->addSql("COMMENT ON COLUMN HPA_PAIEMENT.affichage_cr IS 'True if the CR can be accessed, false otherwise.'");
        $this->addSql("COMMENT ON COLUMN HPA_PAIEMENT.actif IS 'True if payment is enabled, false otherwise.'");
        $this->addSql("DROP INDEX uniq_26afa4fa6ec6d973");
        $this->addSql("ALTER TABLE HPA_BSRVR_COM ADD (data CLOB DEFAULT NULL NULL, updated_at TIMESTAMP(0) NOT NULL)");
        $this->addSql("COMMENT ON COLUMN HPA_BSRVR_COM.data IS '(DC2Type:json_array)'");
        $this->addSql("ALTER TABLE HPA_CONFIGURATION MODIFY (last_patients_record_only_mssg NUMBER(1) NOT NULL)");
        $this->addSql("COMMENT ON COLUMN HPA_DEMANDE.bsrvr_constant_login IS 'Unique identifier for a user in a specific lab.'");
    }

    /**
     *
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()
            ->getName() !== 'oracle', 'Migration can only be executed safely on \'oracle\'.');
    }
}





