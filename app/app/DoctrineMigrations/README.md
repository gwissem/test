
# Doctrine Migrations


## Commands

### Check status

    > php bin/console doctrine:migration:status

### Check that all entries prior to the migration have been updated

Prior to v1.1

    > php bin/console doctrine:query:sql "select * from migration_versions"
    
From v1.1

    > php bin/console doctrine:query:sql "select * from hpa_migration"

### Add non-existing migration entry into db
If an entry is missing in the migration table but the migration was already performed or is not necessary (nex instalation)
you can add the entry manually :

    > php bin/console doctrine:migration:version 20170123435906 --add

### Add a new theme to HPA
    > php bin/console doctrine:query:sql "INSERT INTO hpa_configuration (id, theme_name, logo_alignement, logout_redirection_url) VALUES (11, 'dev', 'center', null)"

## V1.1

### Migrate

    > php bin/console doctrine:migration:execute 20170628115437 --up

From v1.1, the migration has been renamed so add the previous entry once the database has been migrated

    > php bin/console doctrine:migration:version 20170123135906 --add

    > php bin/console doctrine:migration:migrate

Vérifier que le thème dans le fichier de config est à 'monlabo'

php bin/console doctrine:query:sql "select * from hpa_configuration"

php bin/console doctrine:query:sql "INSERT INTO hpa_configuration (id, theme_name, logo_alignement, logout_redirection_url) VALUES (11, 'preprod', 'center', null)"


## V2.x

### Trouver si il faut décommenter la V1 ou la v2 pour la migration vers la 2.x

    > php bin/console doctrine:query:sql "select CONSTRAINT_NAME from USER_CONS_COLUMNS where table_name='hpa_chapitre'"
    
Si la clé correspond à FK_D12700CF2534008B, décommenter V1 dans app/DoctrineMigrations/Version20180612103922.php
Si la clé correspond à FK_C6E4CB7C2534008B, décommenter V2 dans app/DoctrineMigrations/Version20180612103922.php

    > php bin/console doctrine:query:sql "select owner, table_name from dba_constraints where constraint_name ='FK_D12700CF2534008B'"
    > php bin/console doctrine:query:sql "select owner, table_name from dba_constraints where constraint_name ='FK_C6E4CB7C2534008B'"

### To upgrade an HPA v1..x to v2.x

Check if the migration with ref number 20170123135906 exists:
    
    > php bin/console doctrine:query:sql "select * from hpa_migration"
    
Only if it does not exist, run:
 
    > php bin/console doctrine:migration:version 20170123135906 --add
    
Then migrate:

    > php bin/console doctrine:migration:migrate
    
Check that the db is up to date:

	> php bin/console doctrine:schema:update --dump-sql
	
Update Etablissement. BE CAREFUL: a temporary table with updated data is needed for this to work.
Applying the fix thereafter will not work until Etablissements are up to date, whether with or without the update below.

    > php bin/console hpa:user:update-etablissements
	
Apply fix:

    > php bin/console hpa:fix:labs
    
### To fix the bug about etablissement with and HPA that was updated and used prior to the fix, use the following in order:

Dans l'ordre :
    > Rediriger les données anciennement créés vers les entrées labos nouvellements créés
    > Ajouter la référence établissement sur les données anciennes qui ne disposent pas d'une nouvelle entrée
    
Add newly used fields

    > php bin/console doctrine:migrations:migrate 20180702105000
////    php bin/console doctrine:migration:version 20180702105000 --add
    
Move demande, patient, chapitre from old lab (without finess) to new lab (with finess)

    > php bin/console hpa:fix:etablissement
    
Add finess to old labs

    > php bin/console doctrine:migrations:migrate 20180706151900
    
Assign patient to EJ instead of Labs

    > php bin/console hpa:fix:labs
    
    
    
### If the order was not respected, change 'oldLabos' from the delete script to 'nexLabos' but it has to be tested first.

Apply cleanup on:

     > Biolab Martinique
     > Genbio
     
Build a script to check how many labo do not have linked data. 
    

### Nouvelles installations
    > php bin/console doctrine:migration:version 20170123135906 --add
    > php bin/console doctrine:migration:version 20180612103922 --add
    > php bin/console doctrine:migration:version 20180702105000 --add
    > php bin/console doctrine:migrations:migrate 20180706151900 --add
    

    
    






    
# Explication sur les fixs

## Les établissements ont dû être corrigés - FixOldEtablissementCommand

Pour chaque nouveau CR ajouté, un nouvel établissement est créé avec le code finess. Les anciens persistent mais n'ont pas de code finess. Le problème est que les enrolement automatiques ne se faisaient plus pour les anciens CR car la recherche se fait à l'aide du code finess sur la version 2.0.

Une commande a été créée : FixOldEtablissementCommand. Cette commande n'est utile que dans le cas ou la plateforme a été installée avant la correction du code de migration. La correction du code de migration comprend une liste des codes établissement (finess tiret 2) qui sont insérés dans la base de données en fonction du code laboratoire.


## Re-liaison des chapitres et patients - FixLabToEjCommand

Dans la version 1.x de HPA, les patients et chapitres étaient précédement liés aux laboratoires.
Dans la version 2.x, ces données sont maintenant liés aux entités juridiques.

## Notes

Les nouvelles instalations ne nécessitent pas l'application des fix : tout est inclus dans les fichiers de migrations.
Les instalations existantes récement mise à jour nécessitent que le second fix : FixLabToEjCommand.
Les instalations mises à jour et pour lesquels ces fixs n'existaient pas doivent avoir le second fix (FixLabToEjCommand), la migration se terminant par 900 et le dernier fix appliqué. Ce fix doit être appliqué après que l'application aie reçu des CR afin de mettre à jour les laboratoires
en créant l'EJ qui est la leur.









    
Resultat de la prod :  
    
Replacements:
BIOCENTRE COUTANCES (23) replaced with BIOCENTRE COUTANCES (147)
BIOCENTRE CHERBOURG (25) replaced with BIOCENTRE CHERBOURG (145)
LBM DU BOCAGE (22) replaced with LBM DU BOCAGE (144)
LAM BOHR-LUCE (26) replaced with LAM BOHR-LUCE (148)
LAM BIOCENTRE SAINT LO (1) replaced with LAM BIOCENTRE SAINT LO (142)
LAM SAINT GERVAIS FALAISE (21) replaced with LAM BIOCENTRE FALAISE (146)
BIOCENTRE CARENTAN (24) replaced with BIOCENTRE CARENTAN (143)
Lab BIOCENTRE COUTANCES (23) deleted
Lab BIOCENTRE CHERBOURG (25) deleted
Lab LBM DU BOCAGE (22) deleted
Lab LAM BOHR-LUCE (26) deleted
Lab LAM BIOCENTRE SAINT LO (1) deleted
Lab LAM SAINT GERVAIS FALAISE (21) deleted
Lab BIOCENTRE CARENTAN (24) deleted


    
    