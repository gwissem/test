
In order to update


# Get the structure that has 2 or more labs linked to it

php bin/console doctrine:query:sql "select st.id, count(*) from hpa_structure st join hpa_labo et on (et.structure_id=st.id) group by st.id having count(*) > 1"


# Get the labo that are both linked to the same structure

php bin/console doctrine:query:sql "select et.*, st.bsrvr_labo_id from hpa_structure st join hpa_labo et on (et.structure_id=st.id) where st.id=33"
355 et 48


# Get some details about the old CRs and about the new ones too so to log into the users account and check that eveything works fine
php bin/console doctrine:query:sql "select de.id, us.email, pa.nom, pa.prenom from hpa_demande de join hpa_patient pa on (de.patient_id=pa.id) join hpa_user us on (pa.user_id=us.id) where labo_id = 355"

[852]=>
array(4) {
    ["ID"]=>
    string(6) "397631"
        ["EMAIL"]=>
        string(25) "henry.julie-52@hotmail.fr"
            ["NOM"]=>
            string(5) "HENRY"
                ["PRENOM"]=>
                string(5) "Julie"
}
[853]=>
array(4) {
    ["ID"]=>
    string(6) "397256"
        ["EMAIL"]=>
        string(21) "mandysarret@gmail.com"
            ["NOM"]=>
            string(6) "SARRET"
                ["PRENOM"]=>
                string(5) "Mandy"
}
[854]=>
array(4) {
    ["ID"]=>
    string(6) "397229"
        ["EMAIL"]=>
        string(21) "mandysarret@gmail.com"
            ["NOM"]=>
            string(6) "SARRET"
                ["PRENOM"]=>
                string(5) "Mandy"
}
[855]=>
array(4) {
    ["ID"]=>
    string(6) "398820"
        ["EMAIL"]=>
        string(18) "zixnor@hotmail.com"
            ["NOM"]=>
            string(9) "DANGLEANT"
                ["PRENOM"]=>
                string(4) "Joel"
}
[856]=>
array(4) {
    ["ID"]=>
    string(6) "400077"
        ["EMAIL"]=>
        string(17) "tina75013@live.fr"
            ["NOM"]=>
            string(6) "AOURAI"
                ["PRENOM"]=>
                string(4) "Tina"
}




php bin/console doctrine:query:sql "select de.id, us.email, pa.nom, pa.prenom from hpa_demande de join hpa_patient pa on (de.patient_id=pa.id) join hpa_user us on (pa.user_id=us.id) where labo_id = 48"

[556]=>
array(4) {
    ["ID"]=>
    string(5) "58579"
        ["EMAIL"]=>
        string(21) "lavaur.paul@gmail.com"
            ["NOM"]=>
            string(6) "LAVAUR"
                ["PRENOM"]=>
                string(10) "Jacqueline"
}
[557]=>
array(4) {
    ["ID"]=>
    string(5) "56204"
        ["EMAIL"]=>
        string(24) "amandineboulanger@sfr.fr"
            ["NOM"]=>
            string(9) "BOULANGER"
                ["PRENOM"]=>
                string(8) "Amandine"
}
[558]=>
array(4) {
    ["ID"]=>
    string(5) "56203"
        ["EMAIL"]=>
        string(24) "amandineboulanger@sfr.fr"
            ["NOM"]=>
            string(9) "BOULANGER"
                ["PRENOM"]=>
                string(8) "Amandine"
}
[559]=>
array(4) {
    ["ID"]=>
    string(5) "55373"
        ["EMAIL"]=>
        string(22) "bk.hurteaux@wanadoo.fr"
            ["NOM"]=>
            string(8) "HURTEAUX"
                ["PRENOM"]=>
                string(6) "Karine"
}
[560]=>
array(4) {
    ["ID"]=>
    string(5) "54721"
        ["EMAIL"]=>
        string(16) "laeken@skynet.be"
            ["NOM"]=>
            string(9) "DUFRAIGNE"
                ["PRENOM"]=>
                string(14) "Jacques-Oliver"
}
}


# Update CRs with old labo id to new labo id
php bin/console doctrine:query:sql "update hpa_demande de set de.labo_id = 355 where de.labo_id=48"
int(561)

## Delete old entry & unused  of labo
php bin/console doctrine:query:sql "delete from hpa_labo where id=48"










# Check EJ updates in progress
php bin/console doctrine:query:sql "select count(id) from hpa_etablissement where parent_id is null"

# Correct links 
php bin/console hpa:fix:labs










oot@2a51f0f073ad:/var/www/hpa# php bin/console hpa:fix:labs
Lab 21 => 847: 17037 patients updated
Lab 21 => 847: 54 chapitres updated
Lab 176 => 847: 8258 patients updated
Lab 176 => 847: 50 chapitres updated
Lab 22 => 847: 21237 patients updated
Lab 22 => 847: 51 chapitres updated
Lab 83 => 848: 196 patients updated
Lab 83 => 848: 41 chapitres updated

root@2a51f0f073ad:/var/www/hpa# php bin/console hpa:fix:labs
Lab 66 => 849: 1245 patients updated
Lab 66 => 849: 70 chapitres updated
Lab 258 => 850: 433 patients updated
Lab 258 => 850: 23 chapitres updated
Lab 49 => 851: 818 patients updated
Lab 49 => 851: 31 chapitres updated

root@2a51f0f073ad:/var/www/hpa# php bin/console hpa:fix:labs
Lab 46 => 851: 2461 patients updated
Lab 46 => 851: 37 chapitres updated
Lab 3 => 852: 1715 patients updated
Lab 3 => 852: 98 chapitres updated
Lab 81 => 853: 89 patients updated
Lab 81 => 853: 41 chapitres updated

root@2a51f0f073ad:/var/www/hpa# php bin/console hpa:fix:labs
Lab 44 => 850: 1734 patients updated
Lab 44 => 850: 26 chapitres updated
Lab 23 => 851: 1674 patients updated
Lab 23 => 851: 32 chapitres updated

root@2a51f0f073ad:/var/www/hpa# php bin/console hpa:fix:labsd
Lab 175 => 847: 3747 patients updated
Lab 175 => 847: 40 chapitres updated
Lab 43 => 850: 640 patients updated
Lab 43 => 850: 21 chapitres updated
Lab 253 => 850: 606 patients updated
Lab 253 => 850: 21 chapitres updated

root@2a51f0f073ad:/var/www/hpa# php bin/console hpa:fix:labs
Lab 2 => 852: 1224 patients updated
Lab 2 => 852: 96 chapitres updated
Lab 203 => 854: 2242 patients updated
Lab 203 => 854: 80 chapitres updated

root@2a51f0f073ad:/var/www/hpa# php bin/console hpa:fix:labs
Lab 24 => 851: 546 patients updated
Lab 24 => 851: 32 chapitres updated
Lab 88 => 855: 2332 patients updated
Lab 88 => 855: 19 chapitres updated
Lab 90 => 856: 48 patients updated
Lab 90 => 856: 39 chapitres updated

root@2a51f0f073ad:/var/www/hpa# php bin/console hpa:fix:labs
Lab 84 => 848: 75 patients updated
Lab 84 => 848: 33 chapitres updated
Lab 254 => 850: 1560 patients updated
Lab 254 => 850: 24 chapitres updated
Lab 228 => 854: 541 patients updated
Lab 228 => 854: 71 chapitres updated
Lab 91 => 856: 83 patients updated
Lab 91 => 856: 46 chapitres updated

11h07
root@2a51f0f073ad:/var/www/hpa# php bin/console hpa:fix:labs
Lab 82 => 848: 146 patients updated
Lab 82 => 848: 49 chapitres updated
Lab 173 => 849: 514 patients updated
Lab 173 => 849: 64 chapitres updated
Lab 47 => 851: 1079 patients updated
Lab 47 => 851: 34 chapitres updated
Lab 99 => 855: 714 patients updated
Lab 99 => 855: 19 chapitres updated
Lab 63 => 857: 6715 patients updated
Lab 63 => 857: 205 chapitres updated
Lab 201 => 857: 917 patients updated
Lab 201 => 857: 157 chapitres updated
Lab 197 => 857: 2044 patients updated
Lab 197 => 857: 171 chapitres updated

11h16
Lab 62 => 856: 261 patients updated
Lab 62 => 856: 54 chapitres updated

11h24
Lab 1 => 852: 506 patients updated
Lab 1 => 852: 90 chapitres updated
Lab 171 => 855: 1815 patients updated
Lab 171 => 855: 19 chapitres updated

11:34
Lab 45 => 851: 664 patients updated
Lab 45 => 851: 30 chapitres updated
Lab 355 => 851: 604 patients updated
Lab 355 => 851: 29 chapitres updated

11h54
Lab 65 => 849: 2095 patients updated
Lab 65 => 849: 78 chapitres updated
Lab 89 => 855: 1162 patients updated
Lab 89 => 855: 18 chapitres updated
Lab 196 => 857: 399 patients updated
Lab 196 => 857: 131 chapitres updated
Lab 242 => 858: 308 patients updated
Lab 242 => 858: 24 chapitres updated

13h39
Lab 177 => 847: 294 patients updated
Lab 177 => 847: 33 chapitres updated
Lab 174 => 847: 1695 patients updated
Lab 174 => 847: 40 chapitres updated
Lab 85 => 848: 49 patients updated
Lab 85 => 848: 20 chapitres updated
Lab 261 => 848: 557 patients updated
Lab 261 => 848: 69 chapitres updated
Lab 257 => 850: 902 patients updated
Lab 257 => 850: 21 chapitres updated
Lab 255 => 850: 1319 patients updated
Lab 255 => 850: 21 chapitres updated
Lab 256 => 850: 1113 patients updated
Lab 256 => 850: 24 chapitres updated
Lab 61 => 850: 301 patients updated
Lab 61 => 850: 20 chapitres updated
Lab 259 => 850: 329 patients updated
Lab 259 => 850: 19 chapitres updateded
Lab 252 => 850: 1204 patients updated
Lab 252 => 850: 24 chapitres updated
Lab 98 => 853: 135 patients updated
Lab 98 => 853: 40 chapitres updated
Lab 424 => 853: 77 patients updated
Lab 424 => 853: 39 chapitres updated
Lab 204 => 854: 3415 patients updated
Lab 204 => 854: 81 chapitres updated
Lab 226 => 854: 1206 patients updated
Lab 226 => 854: 81 chapitres updated
Lab 42 => 854: 2841 patients updated
Lab 42 => 854: 77 chapitres updated
Lab 97 => 855: 1489 patients updated
Lab 97 => 855: 17 chapitres updated
Lab 123 => 855: 1322 patients updated
Lab 123 => 855: 20 chapitres updated
Lab 324 => 856: 100 patients updated
Lab 324 => 856: 51 chapitres updated
Lab 262 => 856: 864 patients updated
Lab 262 => 856: 54 chapitres updated
Lab 64 => 857: 687 patients updated
Lab 64 => 857: 140 chapitres updated
Lab 202 => 857: 682 patients updated
Lab 202 => 857: 144 chapitres updated
Lab 244 => 857: 365 patients updated
Lab 244 => 857: 131 chapitres updated
Lab 249 => 858: 430 patients updated
Lab 249 => 858: 24 chapitres updated
Lab 86 => 859: 576 patients updated
Lab 86 => 859: 27 chapitres updated
Lab 121 => 859: 678 patients updated
Lab 121 => 859: 31 chapitres updated
Lab 87 => 859: 357 patients updated
Lab 87 => 859: 26 chapitres updated
Lab 172 => 860: 960 patients updated
Lab 172 => 860: 27 chapitres updated

13h55
Lab 266 => 848: 364 patients updated
Lab 266 => 848: 65 chapitres updated
Lab 195 => 847: 600 patients updated
Lab 195 => 847: 28 chapitres updated

14h04
Lab 404 => 853: 104 patients updated
Lab 404 => 853: 39 chapitres updated

15h03
Lab 245 => 857: 738 patients updated
Lab 245 => 857: 135 chapitres updated
Lab 329 => 856: 35 patients updated
Lab 329 => 856: 33 chapitres updated
Lab 41 => 854: 1932 patients updated
Lab 41 => 854: 75 chapitres updated
Lab 92 => 854: 1870 patients updated
Lab 92 => 854: 76 chapitres update
Lab 448 => 853: 51 patients updated
Lab 448 => 853: 40 chapitres updated
Lab 444 => 853: 74 patients updated
Lab 444 => 853: 38 chapitres updated
Lab 260 => 849: 363 patients updated
Lab 260 => 849: 62 chapitres updated

16h13
Lab 122 => 859: 693 patients updated
Lab 122 => 859: 33 chapitres updated
Lab 25 => 857: 1135 patients updated
Lab 25 => 857: 149 chapitres updated
Lab 251 => 858: 127 patients updated
Lab 251 => 858: 18 chapitres updated
Lab 243 => 854: 381 patients updated
Lab 243 => 854: 65 chapitres updated
Lab 450 => 853: 47 patients updated
Lab 450 => 853: 36 chapitres updated

16h45
Lab 250 => 858: 221 patients updated
Lab 250 => 858: 22 chapitres updated

16h56
Lab 227 => 854: 247 patients updated
Lab 227 => 854: 61 chapitres updated

25/10/2018, 17:11
Lab 330 => 848: 40 patients updated
Lab 330 => 848: 31 chapitres updated


































