
In order to update


# Get the structure that has 2 or more labs linked to it

php bin/console doctrine:query:sql "select st.id, count(*) from hpa_structure st join hpa_labo et on (et.structure_id=st.id) group by st.id having count(*) > 1"


# Get the labo that are both linked to the same structure

php bin/console doctrine:query:sql "select et.*, st.bsrvr_labo_id from hpa_structure st join hpa_e et on (et.structure_id=st.id) where st.id=81"
363 et 101

php bin/console doctrine:query:sql "select et.*, st.bsrvr_labo_id from hpa_structure st join hpa_labo et on (et.structure_id=st.id) where st.id=87"
761 et 107

php bin/console doctrine:query:sql "select et.*, st.bsrvr_labo_id from hpa_structure st join hpa_labo et on (et.structure_id=st.id) where st.id=88"
821 et 108

php bin/console doctrine:query:sql "select et.*, st.bsrvr_labo_id from hpa_structure st join hpa_labo et on (et.structure_id=st.id) where st.id=89"
601 et 109


# Get some details about the old CRs and about the new ones too so to log into the users account and check that eveything works fine
php bin/console doctrine:query:sql "select de.id, us.email, pa.nom, pa.prenom from hpa_demande de join hpa_patient pa on (de.patient_id=pa.id) join hpa_user us on (pa.user_id=us.id) where labo_id = 601"
php bin/console doctrine:query:sql "select de.id, us.email, pa.nom, pa.prenom from hpa_demande de join hpa_patient pa on (de.patient_id=pa.id) join hpa_user us on (pa.user_id=us.id) where labo_id = 101"

php bin/console doctrine:query:sql "select de.id, us.email, pa.nom, pa.prenom from hpa_demande de join hpa_patient pa on (de.patient_id=pa.id) join hpa_user us on (pa.user_id=us.id) where labo_id = 761" > debug/761.txt
php bin/console doctrine:query:sql "select de.id, us.email, pa.nom, pa.prenom from hpa_demande de join hpa_patient pa on (de.patient_id=pa.id) join hpa_user us on (pa.user_id=us.id) where labo_id = 107" > debug/107.txt

php bin/console doctrine:query:sql "select de.id, us.email, pa.nom, pa.prenom from hpa_demande de join hpa_patient pa on (de.patient_id=pa.id) join hpa_user us on (pa.user_id=us.id) where labo_id = 821" > debug/821.txt
php bin/console doctrine:query:sql "select de.id, us.email, pa.nom, pa.prenom from hpa_demande de join hpa_patient pa on (de.patient_id=pa.id) join hpa_user us on (pa.user_id=us.id) where labo_id = 108" > debug/108.txt

php bin/console doctrine:query:sql "select de.id, us.email, pa.nom, pa.prenom from hpa_demande de join hpa_patient pa on (de.patient_id=pa.id) join hpa_user us on (pa.user_id=us.id) where labo_id = 601" > debug/601.txt
php bin/console doctrine:query:sql "select de.id, us.email, pa.nom, pa.prenom from hpa_demande de join hpa_patient pa on (de.patient_id=pa.id) join hpa_user us on (pa.user_id=us.id) where labo_id = 109" > debug/109.txt



# Update CRs with old labo id to new labo id
php bin/console doctrine:query:sql "update hpa_demande de set de.labo_id = 601 where de.labo_id=101" && 
php bin/console doctrine:query:sql "update hpa_demande de set de.labo_id = 761 where de.labo_id=107" && 
php bin/console doctrine:query:sql "update hpa_demande de set de.labo_id = 821 where de.labo_id=108" && 
php bin/console doctrine:query:sql "update hpa_demande de set de.labo_id = 601 where de.labo_id=109"


## Delete old entry & unused  of labo - DO NOT PASS THIS STEP AS IT WILL BLOCK THE MIGRATION
php bin/console doctrine:query:sql "delete from hpa_labo where id=101" &&
php bin/console doctrine:query:sql "delete from hpa_labo where id=107" &&
php bin/console doctrine:query:sql "delete from hpa_labo where id=108" &&
php bin/console doctrine:query:sql "delete from hpa_labo where id=109"


# Migration
For MonLabo, comment the following lines in Version20181018114300
$this->addSql("DROP INDEX idx_hpa_user_email");
$this->addSql("DROP INDEX idx_hpa_user_username");
$this->addSql("DROP INDEX uniq_26afa4fa6ec6d973");
$this->addSql("ALTER TABLE HPA_CONFIGURATION MODIFY (last_patients_record_only_mssg NUMBER(1) NOT NULL)");








# Create a tmp table with updated Etablissement in it
ORACLE
php bin/console doctrine:query:sql "CREATE TABLE tmp_etablissement (id NUMBER(11) NOT NULL,name VARCHAR2(41) DEFAULT NULL,address VARCHAR2(63) DEFAULT NULL,pc NUMBER(5) DEFAULT NULL,city VARCHAR2(22) DEFAULT NULL,phone VARCHAR2(14) DEFAULT NULL,code VARCHAR2(4) DEFAULT NULL,name_ej VARCHAR2(14) DEFAULT NULL,finess VARCHAR2(11) DEFAULT NULL,parent VARCHAR2(9) DEFAULT NULL)"

MYSQL
php bin/console doctrine:query:sql "CREATE TABLE tmp_etablissement (id INT(11) NOT NULL,name VARCHAR(41) DEFAULT NULL,address VARCHAR(63) DEFAULT NULL,pc INT(5) DEFAULT NULL,city VARCHAR(22) DEFAULT NULL,phone VARCHAR(14) DEFAULT NULL,code VARCHAR(4) DEFAULT NULL,name_ej VARCHAR(14) DEFAULT NULL,finess VARCHAR(11) DEFAULT NULL,parent VARCHAR(9) DEFAULT NULL)"

## Check
php bin/console doctrine:query:sql "select count(*) from tmp_etablissement"

## Data
php bin/console doctrine:query:sql "INSERT INTO tmp_etablissement (id, name, address, pc, city, phone, code, name_ej, finess, parent) VALUES (1, 'CENTRE DE BIOLOGIE DU LANGUEDOC', '13 RUE DES FOSSES', 11100, 'NARBONNE', '', '', 'CBL', '110005840', NULL)"
php bin/console doctrine:query:sql "INSERT INTO tmp_etablissement (id, name, address, pc, city, phone, code, name_ej, finess, parent) VALUES (2, 'CLB BLANC', '54 boulevard  Jean Jaurès', 11000, 'CARCASSONNE', '04.68.71.74.79', '4340', 'CBL', '110005816-2', '110005840')"
php bin/console doctrine:query:sql "INSERT INTO tmp_etablissement (id, name, address, pc, city, phone, code, name_ej, finess, parent) VALUES (3, 'CLB AVICENNE', '2 Avenue du Maréchal Juin', 11000, 'CARCASSONNE', '4.68.72.79.70', '4339', 'CBL', '110005808-2', '110005840')"
php bin/console doctrine:query:sql "INSERT INTO tmp_etablissement (id, name, address, pc, city, phone, code, name_ej, finess, parent) VALUES (4, 'CBL NARBONNE', '12 avenue Pierre et Marie Curie', 11103, 'NARBONNE', '04.68.90.29.89', '4134', 'CBL', '110788940-2', '110005840')"
php bin/console doctrine:query:sql "INSERT INTO tmp_etablissement (id, name, address, pc, city, phone, code, name_ej, finess, parent) VALUES (5, 'CBL SERIGNAN', '1 RUE JOSEPH LAZARE', 34410, 'SERIGNAN', '467324055', '3955', 'CBL', '340791144-2', '110005840')"
php bin/console doctrine:query:sql "INSERT INTO tmp_etablissement (id, name, address, pc, city, phone, code, name_ej, finess, parent) VALUES (6, 'CBL BEZIERS', '17 AVENUE DE LA VOIE DOMITIENNE', 34500, 'BEZIERS', '468902989', '7415', 'CBL', '340024389-2', '110005840')"
php bin/console doctrine:query:sql "INSERT INTO tmp_etablissement (id, name, address, pc, city, phone, code, name_ej, finess, parent) VALUES (7, 'SELAS OXYLAB', '1 RUE PORTE DES CHANELLES', 48100, 'MARVEJOLS', '', '', 'OXYLAB', '480002047', NULL)"
php bin/console doctrine:query:sql "INSERT INTO tmp_etablissement (id, name, address, pc, city, phone, code, name_ej, finess, parent) VALUES (8, 'LBM OXYLAB BRASSAC', '10 B COURS JEAN MOULIN', 63570, 'BRASSAC LES MINES', '473542811', '5561', 'OXYLAB', '630011146-2', '480002047')"
php bin/console doctrine:query:sql "INSERT INTO tmp_etablissement (id, name, address, pc, city, phone, code, name_ej, finess, parent) VALUES (9, 'LBM OXYLAB SAINT FLOUR', '18 BIS COURS SPY DES TERNES', 15100, 'SAINT FLOUR', '471600484', '5563', 'OXYLAB', '150002962-2', '480002047')"
php bin/console doctrine:query:sql "INSERT INTO tmp_etablissement (id, name, address, pc, city, phone, code, name_ej, finess, parent) VALUES (10, 'LBM OXYLAB MURAT', '10 BIS AVENUE DU DR MALLET', 15300, 'MURAT', '471201792', '5564', 'OXYLAB', '150002970-2', '480002047')"
php bin/console doctrine:query:sql "INSERT INTO tmp_etablissement (id, name, address, pc, city, phone, code, name_ej, finess, parent) VALUES (11, 'LBM OXYLAB BRIOUDE', '1 RUE SAINT GENEYS', 43100, 'BRIOUDE', '471501075', '5560', 'OXYLAB', '430008037-2', '480002047')"
php bin/console doctrine:query:sql "INSERT INTO tmp_etablissement (id, name, address, pc, city, phone, code, name_ej, finess, parent) VALUES (12, 'LBM OXYLAB ST CHELY-DAPCHER', '8 PLACE DU TOURRAL', 48200, 'SAINT CHELY DAPCHER', '466312748', '4990', 'OXYLAB', '480002070-2', '480002047')"
php bin/console doctrine:query:sql "INSERT INTO tmp_etablissement (id, name, address, pc, city, phone, code, name_ej, finess, parent) VALUES (13, 'LBM OXYLAB LANGOGNE', '31 AVENUE FOCH', 48300, 'LANGOGNE', '466692302', '4991', 'OXYLAB', '480002088-2', '480002047')"
php bin/console doctrine:query:sql "INSERT INTO tmp_etablissement (id, name, address, pc, city, phone, code, name_ej, finess, parent) VALUES (14, 'LBM OXYLAB MENDE', '1 ALLEE PIENCOURT', 48000, 'MENDE', '466491774', '4989', 'OXYLAB', '480002062-2', '480002047')"
php bin/console doctrine:query:sql "INSERT INTO tmp_etablissement (id, name, address, pc, city, phone, code, name_ej, finess, parent) VALUES (15, 'LBM OXYLAB MARVEJOLS', '1 PORTE DE CHANELLES', 48100, 'MARVEJOLS', '466321476', '5059', 'OXYLAB', '480002054-2', '480002047')"
php bin/console doctrine:query:sql "INSERT INTO tmp_etablissement (id, name, address, pc, city, phone, code, name_ej, finess, parent) VALUES (16, 'LBM OXYLAB LANGEAC', '1 AVENUE DE LEUROPE', 43300, 'LANGEAC', '471771080', '5562', 'OXYLAB', '430008045-2', '480002047')"
php bin/console doctrine:query:sql "INSERT INTO tmp_etablissement (id, name, address, pc, city, phone, code, name_ej, finess, parent) VALUES (17, 'LBM BIOSITES', 'ROND POINT GAL DE GAULLE', 49240, 'AVRILLE', '', '', 'BIOSITES', '490017167', NULL)"
php bin/console doctrine:query:sql "INSERT INTO tmp_etablissement (id, name, address, pc, city, phone, code, name_ej, finess, parent) VALUES (18, 'LABM VAILLANT - CHAUDIERES', '39 rue Baudière', 49100, 'ANGERS', '02.41.88.28.60', '3815', 'BIOSITES', '490007853-2', '490017167')"
php bin/console doctrine:query:sql "INSERT INTO tmp_etablissement (id, name, address, pc, city, phone, code, name_ej, finess, parent) VALUES (19, 'LBM NEY SAINT SERGE', '8 rue de la Chalouère', 49100, 'ANGERS', '02.41.34.88.88', '3812', 'BIOSITES', '490008315-2', '490017167')"
php bin/console doctrine:query:sql "INSERT INTO tmp_etablissement (id, name, address, pc, city, phone, code, name_ej, finess, parent) VALUES (20, 'LBM GUAZZETTI', '4 Place de la Mairie', 49125, 'TIERCE', '02.41.42.09.58', '3813', 'BIOSITES', '490008885-2', '490017167')"
php bin/console doctrine:query:sql "INSERT INTO tmp_etablissement (id, name, address, pc, city, phone, code, name_ej, finess, parent) VALUES (21, 'LBM DU LION DANGERS', 'Rue Cholet', 49220, 'LE LION DANGERS', '02 41 95 61 32', '3820', 'BIOSITES', '490015443-2', '490017167')"
php bin/console doctrine:query:sql "INSERT INTO tmp_etablissement (id, name, address, pc, city, phone, code, name_ej, finess, parent) VALUES (22, 'LBM DE BEAUPREAU', '3 Boulevard du Général de Gaulle', 49600, 'BEAUPREAU', '02.41.75.28.70', '4362', 'BIOSITES', '490535275-2', '490017167')"
php bin/console doctrine:query:sql "INSERT INTO tmp_etablissement (id, name, address, pc, city, phone, code, name_ej, finess, parent) VALUES (23, 'LBM MONPROFIT', '14 Place MONPROFIT', 49000, 'ANGERS', '02 41 48 24 96', '3818', 'BIOSITES', '490535499-2', '490017167')"
php bin/console doctrine:query:sql "INSERT INTO tmp_etablissement (id, name, address, pc, city, phone, code, name_ej, finess, parent) VALUES (24, 'LBM PATTON', '37 Avenue du Général PATTON', 49000, 'ANGERS', '02 41 48 12 32', '3819', 'BIOSITES', '490535515-2', '490017167')"
php bin/console doctrine:query:sql "INSERT INTO tmp_etablissement (id, name, address, pc, city, phone, code, name_ej, finess, parent) VALUES (25, 'LBM DAVRILLE', 'Rond Point du Général De Gaulle', 49240, 'AVRILLE', '02.41.69.26.86', '3816', 'BIOSITES', '490535572-2', '490017167')"
php bin/console doctrine:query:sql "INSERT INTO tmp_etablissement (id, name, address, pc, city, phone, code, name_ej, finess, parent) VALUES (26, 'LBM MONTREUIL', '14 rue Victor Hugo', 49460, 'MONTREUIL JUIGNE', '02.41.42.30.70', '4455', 'BIOSITES', '490537602-2', '490017167')"
php bin/console doctrine:query:sql "INSERT INTO tmp_etablissement (id, name, address, pc, city, phone, code, name_ej, finess, parent) VALUES (27, 'LBM DU LAC DE MAINE', '14 Place Guy Riobe', 49000, 'ANGERS', '02.41.73.00.40', '3817', 'BIOSITES', '490541042-2', '490017167')"
php bin/console doctrine:query:sql "INSERT INTO tmp_etablissement (id, name, address, pc, city, phone, code, name_ej, finess, parent) VALUES (28, 'LBM D-LAB', '111 RUE DECOSSE ', 76200, 'DIEPPE', '', '', 'DLAB', '760033779', NULL)"
php bin/console doctrine:query:sql "INSERT INTO tmp_etablissement (id, name, address, pc, city, phone, code, name_ej, finess, parent) VALUES (29, 'LBM DIEPPE', '111 rue dEcosse', 76200, 'DIEPPE', '02 35 06 93 93', '3830', 'DLAB', '760012096-2', '760033779')"
php bin/console doctrine:query:sql "INSERT INTO tmp_etablissement (id, name, address, pc, city, phone, code, name_ej, finess, parent) VALUES (30, 'LBM SAINT AUBIN SUR SCIE', '1328 Avenue Maison Blanche', 76550, 'SAINT AUBIN SUR SCIE', '02 32 14 07 77', '3831', 'DLAB', '760027722-2', '760033779')"
php bin/console doctrine:query:sql "INSERT INTO tmp_etablissement (id, name, address, pc, city, phone, code, name_ej, finess, parent) VALUES (31, 'LBM SAINT VALERY EN CAUX', '1 AVENUE FOCH', 76460, 'SAINT VALERY EN CAUX', '235971257', '6794', 'DLAB', '760033753-2', '760033779')"
php bin/console doctrine:query:sql "INSERT INTO tmp_etablissement (id, name, address, pc, city, phone, code, name_ej, finess, parent) VALUES (32, 'LBM MONTVILLE', '41 PLACE DU MARCHE', 76710, 'MONTVILLE', '235337379', '6796', 'DLAB', '760033761-2', '760033779')"
php bin/console doctrine:query:sql "INSERT INTO tmp_etablissement (id, name, address, pc, city, phone, code, name_ej, finess, parent) VALUES (33, 'LABORATOIRE RÉGIONAL DE BIOLOGIE MÉDICALE', '15 BOULEVARD VAUBAN ', 80100, 'ABBEVILLE', '', '', 'LRBM', '800018558', NULL)"
php bin/console doctrine:query:sql "INSERT INTO tmp_etablissement (id, name, address, pc, city, phone, code, name_ej, finess, parent) VALUES (34, 'LRBM NOUVION', '62 ROUTE NATIONALE', 80860, 'NOUVION EN PONTHIEU', '03 22 23 23 61', '5144', 'LRBM', '800007478-2', '800018558')"
php bin/console doctrine:query:sql "INSERT INTO tmp_etablissement (id, name, address, pc, city, phone, code, name_ej, finess, parent) VALUES (35, 'LRBM ST VALERY', '56 RUE DE LA FERTE', 80230, 'SAINT VALERY SUR SOMME', '322608310', '5746', 'LRBM', '800007502-2', '800018558')"
php bin/console doctrine:query:sql "INSERT INTO tmp_etablissement (id, name, address, pc, city, phone, code, name_ej, finess, parent) VALUES (36, 'LABORATOIRE REGIONAL BIOLOGIE MEDICALE', '15 Boulevard vauban', 80101, 'ABBEVILLE', '322207000', '3580', 'LRBM', '800008740-2', '800018558')"
php bin/console doctrine:query:sql "INSERT INTO tmp_etablissement (id, name, address, pc, city, phone, code, name_ej, finess, parent) VALUES (37, 'SYNERGIBIO', '2 RUE REPUBLIQUE', 97100, 'BASSE TERRE', '', '', 'SYNERGIBIO', '970112280', NULL)"
php bin/console doctrine:query:sql "INSERT INTO tmp_etablissement (id, name, address, pc, city, phone, code, name_ej, finess, parent) VALUES (38, 'LBM PEAN-DULORME', 'IMMEUBLE DILIGENTI-RUE JOSE MARTY', 97110, 'POINTE A PITRE', '590912900', '5228', 'SYNERGIBIO', '970112314-2', '970112280')"
php bin/console doctrine:query:sql "INSERT INTO tmp_etablissement (id, name, address, pc, city, phone, code, name_ej, finess, parent) VALUES (39, 'LBM ESPIAND-GIRARD', 'Immeuble Alutechnologie', 97170, 'PETIT-BOURG', '590821008', '5232', 'SYNERGIBIO', '970112363-2', '970112280')"
php bin/console doctrine:query:sql "INSERT INTO tmp_etablissement (id, name, address, pc, city, phone, code, name_ej, finess, parent) VALUES (40, 'SYNERGIBIO - site JARRY-JEQUECE', 'IMMEUBLE FUTURA-VOIE VERTE N2- JARRY MOUDONG', 97122, 'BAIE MAHAULT', '590383545', '5239', 'SYNERGIBIO', '970112348-2', '970112280')"
php bin/console doctrine:query:sql "INSERT INTO tmp_etablissement (id, name, address, pc, city, phone, code, name_ej, finess, parent) VALUES (41, 'LBM ANABIO', 'Bat F rdc La kannopé', 97181, 'LES ABYMES', '590822015', '5235', 'SYNERGIBIO', '970112355-2', '970112280')"
php bin/console doctrine:query:sql "INSERT INTO tmp_etablissement (id, name, address, pc, city, phone, code, name_ej, finess, parent) VALUES (42, 'LBM OVIDE-DORVILLE (PAP)', '49 RUE ACHILLE RENE BOISNEUF', 97110, 'POINTE A PITRE', '590212929', '5236', 'SYNERGIBIO', '970112330-2', '970112280')"
php bin/console doctrine:query:sql "INSERT INTO tmp_etablissement (id, name, address, pc, city, phone, code, name_ej, finess, parent) VALUES (43, 'LBM HUC', '2 RUE DE LA REPUBLIQUE', 97100, 'BASSE TERRE', '590810824', '5231', 'SYNERGIBIO', '970112371-2', '970112280')"
php bin/console doctrine:query:sql "INSERT INTO tmp_etablissement (id, name, address, pc, city, phone, code, name_ej, finess, parent) VALUES (44, 'LBM HIPPOMENE', 'ANGLE RUES PAUL LACAVE ET ANATOLE LEGER-QUARTIER ASSAINISSEMENT', 97110, 'POINTE A PITRE', '590825305', '5225', 'SYNERGIBIO', '970112306-2', '970112280')"
php bin/console doctrine:query:sql "INSERT INTO tmp_etablissement (id, name, address, pc, city, phone, code, name_ej, finess, parent) VALUES (45, 'LBM BEAUBRUN-CASALAN', '7 RUE CHRISTOPHE COLOMB', 97100, 'BASSE TERRE', '590814660', '5227', 'SYNERGIBIO', '970112322-2', '970112280')"
php bin/console doctrine:query:sql "INSERT INTO tmp_etablissement (id, name, address, pc, city, phone, code, name_ej, finess, parent) VALUES (46, 'SEVRE BIOLOGIE', '1 rue du Soleil Levant', 85290, 'MORTAGNE SUR SEVRE', '', '', 'SEVRE BIOLOGIE', '850018540', NULL)"
php bin/console doctrine:query:sql "INSERT INTO tmp_etablissement (id, name, address, pc, city, phone, code, name_ej, finess, parent) VALUES (47, 'SEVRE BIOLOGIE MORTAGNE', '1 RUE DU SOLEIL LEVANT', 85290, 'MORTAGNE SUR SEVRE', '251651212', '6879', 'SEVRE BIOLOGIE', '850018557-2', '850018540')"
php bin/console doctrine:query:sql "INSERT INTO tmp_etablissement (id, name, address, pc, city, phone, code, name_ej, finess, parent) VALUES (48, 'SEVRE BIOLOGIE CLISSON', '7 RUE DU DR DOUSSAIN- POLE SANTE BP 59406', 44190, 'CLISSON', '240547500', '6881', 'SEVRE BIOLOGIE', '440050169-2', '850018540')"
php bin/console doctrine:query:sql "INSERT INTO tmp_etablissement (id, name, address, pc, city, phone, code, name_ej, finess, parent) VALUES (49, 'SEVRE BIOLOGIE LES HERBIERS', '41 GRANDE RUE', 85505, 'LES HERBIERS', '251910745', '6883', 'SEVRE BIOLOGIE', '850018615-2', '850018540')"
php bin/console doctrine:query:sql "INSERT INTO tmp_etablissement (id, name, address, pc, city, phone, code, name_ej, finess, parent) VALUES (50, 'SEVRE BIOLOGIE POUZAUGES', '41 RUE JOACHIM ROUAULT', 85700, 'POUZAUGES', '251919319', '6891', 'SEVRE BIOLOGIE', '850018599-2', '850018540')"
php bin/console doctrine:query:sql "INSERT INTO tmp_etablissement (id, name, address, pc, city, phone, code, name_ej, finess, parent) VALUES (51, 'LBM BIOMELIS', '5 ALLEE DES TREILLES', 49290, 'CHALONNES SUR LOIRE', '', '', 'BIOMELIS', '490018496', NULL)"
php bin/console doctrine:query:sql "INSERT INTO tmp_etablissement (id, name, address, pc, city, phone, code, name_ej, finess, parent) VALUES (52, 'LBM BIOMELIS CHOLET', '17 BOULEVARD FAIDHERBE', 49300, 'CHOLET', '241626145', '6885', 'BIOMELIS', '490018520-2', '490018496')"
php bin/console doctrine:query:sql "INSERT INTO tmp_etablissement (id, name, address, pc, city, phone, code, name_ej, finess, parent) VALUES (53, 'LBM BIOMELIS CHALONNES SUR LOIRE', '5 ALLEE DES TREILLES', 49290, 'CHALONNES SUR LOIRE', '241781208', '6889', 'BIOMELIS', '490018512-2', '490018496')"
php bin/console doctrine:query:sql "INSERT INTO tmp_etablissement (id, name, address, pc, city, phone, code, name_ej, finess, parent) VALUES (54, 'LBM BIOMELIS CHEMILLE', '61 AV DU GENERAL DE GAULLE', 49120, 'CHEMILLE', '241303021', '6887', 'BIOMELIS', '490018504-2', '490018496')"
php bin/console doctrine:query:sql "INSERT INTO tmp_etablissement (id, name, address, pc, city, phone, code, name_ej, finess, parent) VALUES (55, 'MEDILYS', '75 RUE REGARD', 39000, 'LONS LE SAUNIER', '', '', 'MEDILYS', '390006781', NULL)"
php bin/console doctrine:query:sql "INSERT INTO tmp_etablissement (id, name, address, pc, city, phone, code, name_ej, finess, parent) VALUES (56, 'LBM MEDILYS LONS REGARD', '75 RUE REGARD', 39000, 'LONS LE SAUNIER', '384244409', '7540', 'MEDILYS', '390006799-2', '390006781')"
php bin/console doctrine:query:sql "INSERT INTO tmp_etablissement (id, name, address, pc, city, phone, code, name_ej, finess, parent) VALUES (57, 'LBM MEDILYS LONS (P.T)', '1 RUE DU MOULIN', 39000, 'LONS LE SAUNIER', '384244387', '7541', 'MEDILYS', '390006864-2', '390006781')"
php bin/console doctrine:query:sql "INSERT INTO tmp_etablissement (id, name, address, pc, city, phone, code, name_ej, finess, parent) VALUES (58, 'LBM MEDILYS MOREZ', '18 Quai Jobez-Morez', 39400, 'HAUTS DE BIENNE', '384330778', '7543', 'MEDILYS', '390006872-2', '390006781')"
php bin/console doctrine:query:sql "INSERT INTO tmp_etablissement (id, name, address, pc, city, phone, code, name_ej, finess, parent) VALUES (59, 'LBM MEDILYS CHAMPAGNOLE', '50 Avenue de la République', 39300, 'CHAMPAGNOLE', '384525765', '7544', 'MEDILYS', '390006823-2', '390006781')"
php bin/console doctrine:query:sql "INSERT INTO tmp_etablissement (id, name, address, pc, city, phone, code, name_ej, finess, parent) VALUES (60, 'LBM MEDILYS POLIGNY', '11 Rue de la Faiencerie', 39800, 'POLIGNY', '384371383', '7545', 'MEDILYS', '390006807-2', '390006781')"
php bin/console doctrine:query:sql "INSERT INTO tmp_etablissement (id, name, address, pc, city, phone, code, name_ej, finess, parent) VALUES (61, 'LBM MEDILYS SAINT CLAUDE', '4 Rue Reybbert', 39200, 'SAINT CLAUDE', '384451683', '7546', 'MEDILYS', '390006815-2', '390006781')"
php bin/console doctrine:query:sql "INSERT INTO tmp_etablissement (id, name, address, pc, city, phone, code, name_ej, finess, parent) VALUES (62, 'LBM MEDILYS DOLE', '24 Rue du 21 Janvier BP 379', 39100, 'DOLE', '384720334', '7542', 'MEDILYS', '390006880-2', '390006781')"
















# Check EJ updates in progress
php bin/console doctrine:query:sql "select count(id) from hpa_etablissement where parent_id is null"

# Correct links 
php bin/console hpa:fix:labs










oot@2a51f0f073ad:/var/www/hpa# php bin/console hpa:fix:labs
Lab 21 => 847: 17037 patients updated
Lab 21 => 847: 54 chapitres updated
Lab 176 => 847: 8258 patients updated
Lab 176 => 847: 50 chapitres updated
Lab 22 => 847: 21237 patients updated
Lab 22 => 847: 51 chapitres updated
Lab 83 => 848: 196 patients updated
Lab 83 => 848: 41 chapitres updated

root@2a51f0f073ad:/var/www/hpa# php bin/console hpa:fix:labs
Lab 66 => 849: 1245 patients updated
Lab 66 => 849: 70 chapitres updated
Lab 258 => 850: 433 patients updated
Lab 258 => 850: 23 chapitres updated
Lab 49 => 851: 818 patients updated
Lab 49 => 851: 31 chapitres updated

root@2a51f0f073ad:/var/www/hpa# php bin/console hpa:fix:labs
Lab 46 => 851: 2461 patients updated
Lab 46 => 851: 37 chapitres updated
Lab 3 => 852: 1715 patients updated
Lab 3 => 852: 98 chapitres updated
Lab 81 => 853: 89 patients updated
Lab 81 => 853: 41 chapitres updated

root@2a51f0f073ad:/var/www/hpa# php bin/console hpa:fix:labs
Lab 44 => 850: 1734 patients updated
Lab 44 => 850: 26 chapitres updated
Lab 23 => 851: 1674 patients updated
Lab 23 => 851: 32 chapitres updated

root@2a51f0f073ad:/var/www/hpa# php bin/console hpa:fix:labsd
Lab 175 => 847: 3747 patients updated
Lab 175 => 847: 40 chapitres updated
Lab 43 => 850: 640 patients updated
Lab 43 => 850: 21 chapitres updated
Lab 253 => 850: 606 patients updated
Lab 253 => 850: 21 chapitres updated

root@2a51f0f073ad:/var/www/hpa# php bin/console hpa:fix:labs
Lab 2 => 852: 1224 patients updated
Lab 2 => 852: 96 chapitres updated
Lab 203 => 854: 2242 patients updated
Lab 203 => 854: 80 chapitres updated

root@2a51f0f073ad:/var/www/hpa# php bin/console hpa:fix:labs
Lab 24 => 851: 546 patients updated
Lab 24 => 851: 32 chapitres updated
Lab 88 => 855: 2332 patients updated
Lab 88 => 855: 19 chapitres updated
Lab 90 => 856: 48 patients updated
Lab 90 => 856: 39 chapitres updated

root@2a51f0f073ad:/var/www/hpa# php bin/console hpa:fix:labs
Lab 84 => 848: 75 patients updated
Lab 84 => 848: 33 chapitres updated
Lab 254 => 850: 1560 patients updated
Lab 254 => 850: 24 chapitres updated
Lab 228 => 854: 541 patients updated
Lab 228 => 854: 71 chapitres updated
Lab 91 => 856: 83 patients updated
Lab 91 => 856: 46 chapitres updated

11h07
root@2a51f0f073ad:/var/www/hpa# php bin/console hpa:fix:labs
Lab 82 => 848: 146 patients updated
Lab 82 => 848: 49 chapitres updated
Lab 173 => 849: 514 patients updated
Lab 173 => 849: 64 chapitres updated
Lab 47 => 851: 1079 patients updated
Lab 47 => 851: 34 chapitres updated
Lab 99 => 855: 714 patients updated
Lab 99 => 855: 19 chapitres updated
Lab 63 => 857: 6715 patients updated
Lab 63 => 857: 205 chapitres updated
Lab 201 => 857: 917 patients updated
Lab 201 => 857: 157 chapitres updated
Lab 197 => 857: 2044 patients updated
Lab 197 => 857: 171 chapitres updated

11h16
Lab 62 => 856: 261 patients updated
Lab 62 => 856: 54 chapitres updated

11h24
Lab 1 => 852: 506 patients updated
Lab 1 => 852: 90 chapitres updated
Lab 171 => 855: 1815 patients updated
Lab 171 => 855: 19 chapitres updated

11:34
Lab 45 => 851: 664 patients updated
Lab 45 => 851: 30 chapitres updated
Lab 355 => 851: 604 patients updated
Lab 355 => 851: 29 chapitres updated

11h54
Lab 65 => 849: 2095 patients updated
Lab 65 => 849: 78 chapitres updated
Lab 89 => 855: 1162 patients updated
Lab 89 => 855: 18 chapitres updated
Lab 196 => 857: 399 patients updated
Lab 196 => 857: 131 chapitres updated
Lab 242 => 858: 308 patients updated
Lab 242 => 858: 24 chapitres updated

13h39
Lab 177 => 847: 294 patients updated
Lab 177 => 847: 33 chapitres updated
Lab 174 => 847: 1695 patients updated
Lab 174 => 847: 40 chapitres updated
Lab 85 => 848: 49 patients updated
Lab 85 => 848: 20 chapitres updated
Lab 261 => 848: 557 patients updated
Lab 261 => 848: 69 chapitres updated
Lab 257 => 850: 902 patients updated
Lab 257 => 850: 21 chapitres updated
Lab 255 => 850: 1319 patients updated
Lab 255 => 850: 21 chapitres updated
Lab 256 => 850: 1113 patients updated
Lab 256 => 850: 24 chapitres updated
Lab 61 => 850: 301 patients updated
Lab 61 => 850: 20 chapitres updated
Lab 259 => 850: 329 patients updated
Lab 259 => 850: 19 chapitres updateded
Lab 252 => 850: 1204 patients updated
Lab 252 => 850: 24 chapitres updated
Lab 98 => 853: 135 patients updated
Lab 98 => 853: 40 chapitres updated
Lab 424 => 853: 77 patients updated
Lab 424 => 853: 39 chapitres updated
Lab 204 => 854: 3415 patients updated
Lab 204 => 854: 81 chapitres updated
Lab 226 => 854: 1206 patients updated
Lab 226 => 854: 81 chapitres updated
Lab 42 => 854: 2841 patients updated
Lab 42 => 854: 77 chapitres updated
Lab 97 => 855: 1489 patients updated
Lab 97 => 855: 17 chapitres updated
Lab 123 => 855: 1322 patients updated
Lab 123 => 855: 20 chapitres updated
Lab 324 => 856: 100 patients updated
Lab 324 => 856: 51 chapitres updated
Lab 262 => 856: 864 patients updated
Lab 262 => 856: 54 chapitres updated
Lab 64 => 857: 687 patients updated
Lab 64 => 857: 140 chapitres updated
Lab 202 => 857: 682 patients updated
Lab 202 => 857: 144 chapitres updated
Lab 244 => 857: 365 patients updated
Lab 244 => 857: 131 chapitres updated
Lab 249 => 858: 430 patients updated
Lab 249 => 858: 24 chapitres updated
Lab 86 => 859: 576 patients updated
Lab 86 => 859: 27 chapitres updated
Lab 121 => 859: 678 patients updated
Lab 121 => 859: 31 chapitres updated
Lab 87 => 859: 357 patients updated
Lab 87 => 859: 26 chapitres updated
Lab 172 => 860: 960 patients updated
Lab 172 => 860: 27 chapitres updated

13h55
Lab 266 => 848: 364 patients updated
Lab 266 => 848: 65 chapitres updated
Lab 195 => 847: 600 patients updated
Lab 195 => 847: 28 chapitres updated

14h04
Lab 404 => 853: 104 patients updated
Lab 404 => 853: 39 chapitres updated

15h03
Lab 245 => 857: 738 patients updated
Lab 245 => 857: 135 chapitres updated
Lab 329 => 856: 35 patients updated
Lab 329 => 856: 33 chapitres updated
Lab 41 => 854: 1932 patients updated
Lab 41 => 854: 75 chapitres updated
Lab 92 => 854: 1870 patients updated
Lab 92 => 854: 76 chapitres update
Lab 448 => 853: 51 patients updated
Lab 448 => 853: 40 chapitres updated
Lab 444 => 853: 74 patients updated
Lab 444 => 853: 38 chapitres updated
Lab 260 => 849: 363 patients updated
Lab 260 => 849: 62 chapitres updated

16h13
Lab 122 => 859: 693 patients updated
Lab 122 => 859: 33 chapitres updated
Lab 25 => 857: 1135 patients updated
Lab 25 => 857: 149 chapitres updated
Lab 251 => 858: 127 patients updated
Lab 251 => 858: 18 chapitres updated
Lab 243 => 854: 381 patients updated
Lab 243 => 854: 65 chapitres updated
Lab 450 => 853: 47 patients updated
Lab 450 => 853: 36 chapitres updated

16h45
Lab 250 => 858: 221 patients updated
Lab 250 => 858: 22 chapitres updated

16h56
Lab 227 => 854: 247 patients updated
Lab 227 => 854: 61 chapitres updated

25/10/2018, 17:11
Lab 330 => 848: 40 patients updated
Lab 330 => 848: 31 chapitres updated


































