<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Migration de HPA v1.0.0 vers HPA v1.0.1.
 */
class Version20170123135906 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'oracle', 'Migration can only be executed safely on \'oracle\'.');

        $this->addSql('ALTER TABLE HPA_PERSONNE MODIFY (date_naissance  DATE DEFAULT NULL)');
        $this->addSql('ALTER TABLE HPA_STRUCTURE RENAME COLUMN identifiant_bioserveur TO bsrvr_labo_id');
        $this->addSql('ALTER TABLE HPA_DEMANDE RENAME COLUMN CODE_PATIENT_BIOSERVEUR TO bsrvr_constant_login');
        $this->addSql('ALTER TABLE HPA_DEMANDE MODIFY (date_naissance_patient DATE DEFAULT NULL)');
        $this->addSql('ALTER TABLE HPA_PATIENT MODIFY (date_naissance DATE DEFAULT NULL)');
        $this->addSql('ALTER TABLE HPA_PATIENT RENAME COLUMN identifiant_bioserveur TO bsrvr_nopat');
        $this->addSql('CREATE SEQUENCE hpa_configuration_id_seq START WITH 1 MINVALUE 1 INCREMENT BY 1');
        $this->addSql('CREATE TABLE hpa_configuration (id NUMBER(10) NOT NULL, theme_name VARCHAR2(20) NOT NULL, logo_alignement VARCHAR2(6) NOT NULL, longueur_mdp_enrolement NUMBER(10) NOT NULL, PRIMARY KEY(id))');

        $this->addSql("INSERT INTO hpa_configuration (id, theme_name, logo_alignement, longueur_mdp_enrolement) VALUES (hpa_configuration_id_seq.NEXTVAL, 'agfa', 'center', 8)");
        $this->addSql("INSERT INTO hpa_configuration (id, theme_name, logo_alignement, longueur_mdp_enrolement) VALUES (hpa_configuration_id_seq.NEXTVAL, 'unilabs', 'left', 0)");
        $this->addSql("INSERT INTO hpa_configuration (id, theme_name, logo_alignement, longueur_mdp_enrolement) VALUES (hpa_configuration_id_seq.NEXTVAL, 'monlabo', 'center', 8)");
        $this->addSql("INSERT INTO hpa_configuration (id, theme_name, logo_alignement, longueur_mdp_enrolement) VALUES (hpa_configuration_id_seq.NEXTVAL, 'reunilab', 'center', 8)");
        $this->addSql("INSERT INTO hpa_configuration (id, theme_name, logo_alignement, longueur_mdp_enrolement) VALUES (hpa_configuration_id_seq.NEXTVAL, 'genbio', 'center', 8)");
        $this->addSql("INSERT INTO hpa_configuration (id, theme_name, logo_alignement, longueur_mdp_enrolement) VALUES (hpa_configuration_id_seq.NEXTVAL, 'labazur', 'center', 8)");
        $this->addSql("INSERT INTO hpa_configuration (id, theme_name, logo_alignement, longueur_mdp_enrolement) VALUES (hpa_configuration_id_seq.NEXTVAL, 'oriade', 'center', 8)");
        $this->addSql("INSERT INTO hpa_configuration (id, theme_name, logo_alignement, longueur_mdp_enrolement) VALUES (hpa_configuration_id_seq.NEXTVAL, 'sbl-bio', 'center', 8)");
        $this->addSql("INSERT INTO hpa_configuration (id, theme_name, logo_alignement, longueur_mdp_enrolement) VALUES (hpa_configuration_id_seq.NEXTVAL, 'bionorma', 'center', 8)");
        $this->addSql("INSERT INTO hpa_configuration (id, theme_name, logo_alignement, longueur_mdp_enrolement) VALUES (hpa_configuration_id_seq.NEXTVAL, 'biocentre', 'center', 8)");
        $this->addSql("INSERT INTO hpa_configuration (id, theme_name, logo_alignement, longueur_mdp_enrolement) VALUES (hpa_configuration_id_seq.NEXTVAL, 'bioardaisne', 'center', 8)");
        $this->addSql("INSERT INTO hpa_configuration (id, theme_name, logo_alignement, longueur_mdp_enrolement) VALUES (hpa_configuration_id_seq.NEXTVAL, 'lechesnay', 'center', 8)");
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'oracle', 'Migration can only be executed safely on \'oracle\'.');

        $this->addSql('ALTER TABLE hpa_personne MODIFY (DATE_NAISSANCE TIMESTAMP(0) DEFAULT NULL)');
        $this->addSql('ALTER TABLE hpa_structure RENAME COLUMN bsrvr_labo_id TO IDENTIFIANT_BIOSERVEUR');
        $this->addSql('ALTER TABLE hpa_demande RENAME COLUMN bsrvr_constant_login TO CODE_PATIENT_BIOSERVEUR');
        $this->addSql('ALTER TABLE hpa_demande MODIFY (DATE_NAISSANCE_PATIENT TIMESTAMP(0) DEFAULT NULL)');
        $this->addSql('ALTER TABLE hpa_patient MODIFY (DATE_NAISSANCE TIMESTAMP(0) DEFAULT NULL)');
        $this->addSql('ALTER TABLE hpa_patient RENAME COLUMN bsrvr_nopat TO IDENTIFIANT_BIOSERVEUR');
        $this->addSql('DROP TABLE hpa_configuration');
        $this->addSql('DROP SEQUENCE hpa_configuration_id_seq');

    }
}
