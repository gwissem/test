<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;
use Agfa\HpaBundle\DataFixtures\ORM\Prod\ConfigurationData;

/**
 * Migration de HPA v1.0.2 vers HPA v2.0
 */
class Version20180612103922 extends AbstractMigration
{

    /**
     * @param Schema $schema
     *
     * Remove V1 or V2 depending on the state of the current database
     */
    public function up(Schema $schema)
    {
        /**
         * Oracle
         */
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'oracle', 'Migration can only be executed safely on \'oracle\'.');

        $this->addSql("CREATE SEQUENCE mntc_request_id_seq START WITH 1 MINVALUE 1 INCREMENT BY 1");
        $this->addSql("CREATE SEQUENCE hpa_bsrvr_com_id_seq START WITH 1 MINVALUE 1 INCREMENT BY 1");
        $this->addSql("CREATE SEQUENCE hpa_personne_patient_id_seq START WITH 1 MINVALUE 1 INCREMENT BY 1");
        $this->addSql("CREATE SEQUENCE mntc_response_id_seq START WITH 1 MINVALUE 1 INCREMENT BY 1");

        $this->addSql('ALTER TABLE hpa_configuration ADD LOGOUT_REDIRECTION_URL VARCHAR2(255) DEFAULT NULL');

// Check which version applies:
// php bin/console doctrine:quer:sql "select owner, table_name from dba_constraints where constraints_name ='FK...'"

// V1        $this->addSql('ALTER TABLE hpa_chapitre DROP CONSTRAINT FK_D12700CF2534008B');
// V2       $this->addSql('ALTER TABLE hpa_chapitre DROP CONSTRAINT FK_C6E4CB7C2534008B');

// V1        $this->addSql('ALTER TABLE hpa_patient DROP CONSTRAINT FK_37713F112534008B');
// V2        $this->addSql('ALTER TABLE hpa_patient DROP CONSTRAINT FK_4304CFAF2534008B');

// V1         $this->addSql('ALTER TABLE hpa_labo DROP CONSTRAINT FK_418CACE92534008B');
// V2        $this->addSql('ALTER TABLE hpa_labo DROP CONSTRAINT FK_467E1FA12534008B');

// V1        $this->addSql('ALTER TABLE hpa_demande DROP CONSTRAINT FK_B3F3F5FB65FA4A');
// V2        $this->addSql('ALTER TABLE hpa_demande DROP CONSTRAINT FK_7F4ACFE1B65FA4A');

        // Rename hpa_labo to hpa_etablissement and transfert data
        $this->addSql('RENAME hpa_labo TO hpa_etablissement');
        $this->addSql('RENAME hpa_labo_id_seq TO hpa_etablissement_id_seq');

        $this->addSql('ALTER TABLE hpa_etablissement RENAME COLUMN code TO hxl_code_structure');
        $this->addSql("ALTER TABLE hpa_etablissement ADD (parent_id INT DEFAULT NULL, bsrvr_code_entite_juridique VARCHAR2(20) DEFAULT NULL NULL, bsrvr_code_labo NUMBER(10) DEFAULT NULL NULL, bsrvr_ref_etablissement VARCHAR2(20) DEFAULT 0 NOT NULL, libelle VARCHAR2(100) DEFAULT NULL NULL, created_at TIMESTAMP(0) DEFAULT '1970-01-01 00:00:00', updated_at TIMESTAMP(0) DEFAULT '1970-01-01 00:00:00' )");
        $this->addSql('ALTER TABLE HPA_ETABLISSEMENT MODIFY (adresse VARCHAR2(255) DEFAULT NULL NULL, code_postal VARCHAR2(255) DEFAULT NULL NULL, hxl_code_structure VARCHAR2(2) DEFAULT NULL NULL, nom VARCHAR2(255) DEFAULT NULL NULL, ville VARCHAR2(255) DEFAULT NULL NULL)');
        $this->addSql("UPDATE hpa_etablissement et1 SET (bsrvr_code_labo, bsrvr_ref_etablissement) = (SELECT st.bsrvr_labo_id, CASE WHEN st.bsrvr_labo_id = 4693 THEN 123456789 ELSE 999999999 END AS bsrvr_ref_etablissement FROM hpa_structure st WHERE et1.structure_id = st.id) WHERE EXISTS (SELECT 1 FROM hpa_etablissement et2 WHERE et1.id = et2.id)");
        $this->addSql('ALTER TABLE hpa_etablissement ADD CONSTRAINT FK_26AFA4FA727ACA70 FOREIGN KEY (parent_id) REFERENCES hpa_etablissement (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_26AFA4FAA4D60759 ON hpa_etablissement (libelle)');
        $this->addSql('CREATE INDEX IDX_26AFA4FA727ACA70 ON hpa_etablissement (parent_id)');

        // Create table hpa_personne_patient
        $this->addSql("CREATE TABLE hpa_personne_patient (id NUMBER(10) NOT NULL, patient_id INT DEFAULT NULL, personne_id INT DEFAULT NULL, created_at TIMESTAMP(0) DEFAULT '1970-01-01 00:00:00', updated_at TIMESTAMP(0) DEFAULT '1970-01-01 00:00:00', PRIMARY KEY(id))");
        $this->addSql('INSERT INTO hpa_personne_patient (id, patient_id, personne_id) SELECT hpa_personne_patient_id_seq.nextval, pa.id, pa.personne_id FROM hpa_patient pa');
        $this->addSql('CREATE INDEX IDX_6926D5BA6B899279 ON hpa_personne_patient (patient_id)');
        $this->addSql('CREATE INDEX IDX_6926D5BAA21BD112 ON hpa_personne_patient (personne_id)');
        $this->addSql('ALTER TABLE hpa_personne_patient ADD CONSTRAINT FK_6926D5BA6B899279 FOREIGN KEY (patient_id) REFERENCES hpa_patient (id)');
        $this->addSql('ALTER TABLE hpa_personne_patient ADD CONSTRAINT FK_6926D5BAA21BD112 FOREIGN KEY (personne_id) REFERENCES hpa_personne (id)');

        // Add missing column to hpa_chapitre and transfert data
        // Index of structure_id in hpa_patient
// V1        $this->addSql('DROP INDEX IDX_D12700CF2534008B');
// V2        $this->addSql('DROP INDEX IDX_4304CFAF2534008B');

        $this->addSql('ALTER TABLE hpa_chapitre ADD etablissement_id NUMBER(10) DEFAULT NULL');
        $this->addSql('UPDATE hpa_chapitre ch1 SET (ch1.etablissement_id) = (SELECT et.id FROM hpa_structure st JOIN hpa_etablissement et ON (et.structure_id = st.id) JOIN hpa_chapitre ch ON (ch.structure_id = st.id) WHERE ch1.id = ch.id) WHERE EXISTS (SELECT 1 FROM hpa_chapitre ch2 WHERE ch1.id = ch2.id)');
        $this->addSql('CREATE TABLE hpa_demande_request (demande_id NUMBER(10) NOT NULL, request_id NUMBER(10) NOT NULL, PRIMARY KEY(demande_id, request_id))');
        $this->addSql('CREATE INDEX IDX_1F0EDE5A80E95E18 ON hpa_demande_request (demande_id)');
        $this->addSql('CREATE INDEX IDX_1F0EDE5A427EB8A5 ON hpa_demande_request (request_id)');
        $this->addSql('CREATE TABLE mntc_request (id NUMBER(10) NOT NULL, response_id NUMBER(10) DEFAULT NULL NULL, tpe NUMBER(10) NOT NULL, montant DOUBLE PRECISION NOT NULL, reference VARCHAR2(12) NOT NULL, texte_libre CLOB DEFAULT NULL NULL, created_at TIMESTAMP(0) NOT NULL, updated_at TIMESTAMP(0) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_7F3B3FBFFBF32840 ON mntc_request (response_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_7F3B3FBFAEA34913 ON MNTC_REQUEST (reference)');
        $this->addSql('CREATE TABLE hpa_bsrvr_com (id NUMBER(10) NOT NULL, demande_id NUMBER(10) DEFAULT NULL NULL, attempt NUMBER(5) NOT NULL, response_status VARCHAR2(25) DEFAULT NULL NULL, action VARCHAR2(25) NOT NULL, created_at TIMESTAMP(0) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_DBC6F95880E95E18 ON hpa_bsrvr_com (demande_id)');
        $this->addSql('CREATE TABLE mntc_response (id NUMBER(10) NOT NULL, motif_refus VARCHAR2(255) DEFAULT NULL NULL, ip_client VARCHAR2(25) NOT NULL, code_retour VARCHAR2(255) NOT NULL, montant NUMERIC(10, 2) NOT NULL, currency VARCHAR2(3) NOT NULL, date_response TIMESTAMP(0) NOT NULL, reference VARCHAR2(25) NOT NULL, texte_libre CLOB DEFAULT NULL NULL, cvx VARCHAR2(3) NOT NULL, vld NUMBER(10) NOT NULL, brand VARCHAR2(2) NOT NULL, status3ds NUMBER(10) NOT NULL, num_auto NUMBER(10) DEFAULT NULL NULL, origine_cb VARCHAR2(3) DEFAULT NULL NULL, bin_cb NUMBER(10) DEFAULT NULL NULL, hpan_cb VARCHAR2(255) DEFAULT NULL NULL, origine_tr VARCHAR2(3) DEFAULT NULL NULL, veres VARCHAR2(1) DEFAULT NULL NULL, pares VARCHAR2(1) DEFAULT NULL NULL, filtrage_cause VARCHAR2(20) DEFAULT NULL NULL, filtrage_valeur VARCHAR2(20) DEFAULT NULL NULL, mode_paiement VARCHAR2(20) DEFAULT NULL NULL, created_at TIMESTAMP(0) NOT NULL, updated_at TIMESTAMP(0) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('ALTER TABLE hpa_demande_request ADD CONSTRAINT FK_1F0EDE5A80E95E18 FOREIGN KEY (demande_id) REFERENCES hpa_demande (id)');
        $this->addSql('ALTER TABLE hpa_demande_request ADD CONSTRAINT FK_1F0EDE5A427EB8A5 FOREIGN KEY (request_id) REFERENCES mntc_request (id)');
        $this->addSql('ALTER TABLE mntc_request ADD CONSTRAINT FK_7F3B3FBFFBF32840 FOREIGN KEY (response_id) REFERENCES mntc_response (id)');
        $this->addSql('ALTER TABLE hpa_bsrvr_com ADD CONSTRAINT FK_DBC6F95880E95E18 FOREIGN KEY (demande_id) REFERENCES hpa_demande (id)');
        $this->addSql("ALTER TABLE HPA_USER ADD (current_personne NUMBER(10) DEFAULT NULL NULL, nb_failed_login_attempt NUMBER(10) DEFAULT 0 NOT NULL, has_unassigned_patient NUMBER(1) DEFAULT 0 NOT NULL, payment_is_enabled NUMBER(1) DEFAULT 0 NOT NULL, updated_at TIMESTAMP(0) DEFAULT '1970-01-01 00:00:00')");
        $this->addSql('ALTER TABLE HPA_USER MODIFY (confirmation_token VARCHAR2(180) DEFAULT NULL, email VARCHAR2(180) DEFAULT NULL, email_canonical VARCHAR2(180) DEFAULT NULL, salt VARCHAR2(255) DEFAULT NULL NULL, username VARCHAR2(180) DEFAULT NULL, username_canonical VARCHAR2(180) DEFAULT NULL)');
        $this->addSql('ALTER TABLE HPA_USER RENAME COLUMN DATE_CREATION TO created_at');
        $this->addSql("UPDATE hpa_user SET updated_at = created_at");
        $this->addSql('ALTER TABLE HPA_USER DROP (CREDENTIALS_EXPIRE_AT, CREDENTIALS_EXPIRED, EXPIRED, EXPIRES_AT, LOCKED)');
        $this->addSql('ALTER TABLE HPA_USER ADD CONSTRAINT FK_5F7839FC92B6B3C5 FOREIGN KEY (current_personne) REFERENCES hpa_personne (id) ON DELETE SET NULL');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_5F7839FCC05FB297 ON HPA_USER (confirmation_token)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_5F7839FC92B6B3C5 ON HPA_USER (current_personne)');
        $this->addSql('ALTER TABLE HPA_CHAPITRE DROP (STRUCTURE_ID)');
        $this->addSql('ALTER TABLE HPA_CHAPITRE ADD CONSTRAINT FK_D12700CFFF631228 FOREIGN KEY (etablissement_id) REFERENCES hpa_etablissement (id)');
        $this->addSql('CREATE INDEX IDX_D12700CFFF631228 ON HPA_CHAPITRE (etablissement_id)');

        // Index of hpa_demande to labo_id
// V1        $this->addSql('DROP INDEX idx_b3f3f5fb65fa4a');
// V2        $this->addSql('DROP INDEX idx_7F4ACFE1B65FA4A');

        $this->addSql("ALTER TABLE HPA_DEMANDE ADD (nodemx VARCHAR2(50) DEFAULT '-', paie_solde DOUBLE PRECISION DEFAULT 0 NOT NULL, paie_source VARCHAR2(20) DEFAULT 'unknown' NOT NULL, created_at TIMESTAMP(0) DEFAULT '1970-01-01 00:00:00', updated_at TIMESTAMP(0) DEFAULT '1970-01-01 00:00:00')");
        $this->addSql('ALTER TABLE HPA_DEMANDE RENAME COLUMN labo_id TO etablissement_id');
        $this->addSql('ALTER TABLE HPA_DEMANDE ADD CONSTRAINT FK_B3F3F5FFF631228 FOREIGN KEY (etablissement_id) REFERENCES hpa_etablissement (id)');
        $this->addSql('CREATE INDEX IDX_B3F3F5FFF631228 ON HPA_DEMANDE (etablissement_id)');
        $this->addSql('ALTER TABLE HPA_JOURNAL RENAME COLUMN extra TO short_description');
        $this->addSql('ALTER TABLE HPA_JOURNAL RENAME COLUMN date_event TO created_at');
        $this->addSql("ALTER TABLE HPA_JOURNAL ADD (ip_address VARCHAR2(255) DEFAULT '0.0.0.0' NOT NULL, journal_level VARCHAR2(255) DEFAULT NULL NULL, long_description CLOB DEFAULT NULL NULL)");
        $this->addSql('ALTER TABLE HPA_JOURNAL MODIFY (entity_id NUMBER(10) DEFAULT NULL NULL)');

        // Index of hpa_patient to personne_id
// V1        $this->addSql('DROP INDEX idx_37713f11a21bd112');
// V2        $this->addSql('DROP INDEX idx_4304CFAFA21BD112');

        // Index of hpa_patient to structure_id
// V1        $this->addSql('DROP INDEX idx_37713f112534008b');

        $this->addSql("ALTER TABLE HPA_PATIENT ADD (etablissement_id NUMBER(10) DEFAULT NULL NULL, created_at TIMESTAMP(0) DEFAULT '1970-01-01 00:00:00', updated_at TIMESTAMP(0) DEFAULT '1970-01-01 00:00:00')");
        $this->addSql('UPDATE hpa_patient pa1 SET (pa1.etablissement_id) = (SELECT et.id FROM hpa_patient pa2 JOIN hpa_structure st ON (pa2.structure_id = st.id) JOIN hpa_etablissement et ON (st.bsrvr_labo_id = et.bsrvr_code_labo) WHERE pa1.id = pa2.id) WHERE EXISTS (SELECT 1 FROM hpa_patient pa3 WHERE pa3.id = pa1.id)');
        $this->addSql('ALTER TABLE HPA_PATIENT DROP (PERSONNE_ID, STRUCTURE_ID)');
        $this->addSql('ALTER TABLE HPA_PATIENT ADD CONSTRAINT FK_37713F11FF631228 FOREIGN KEY (etablissement_id) REFERENCES hpa_etablissement (id)');
        $this->addSql('CREATE INDEX IDX_37713F11FF631228 ON HPA_PATIENT (etablissement_id)');

//         // Index of hpa_labo to structure_id
// V1         $this->addSql('DROP INDEX idx_418cace92534008b');

        $this->addSql('ALTER TABLE HPA_ETABLISSEMENT DROP (STRUCTURE_ID)');
        $this->addSql("ALTER TABLE HPA_PERSONNE_PATIENT MODIFY (created_at TIMESTAMP(0)  DEFAULT '1970-01-01 00:00:00', updated_at TIMESTAMP(0)  DEFAULT '1970-01-01 00:00:00')");

// V1         $this->addSql('ALTER TABLE HPA_PERSONNE DROP CONSTRAINT FK_528B7905A76ED395');
// V2        $this->addSql('ALTER TABLE HPA_PERSONNE DROP CONSTRAINT FK_4548B2B6A76ED395');

        $this->addSql("ALTER TABLE HPA_PERSONNE ADD (created_at TIMESTAMP(0) DEFAULT '1970-01-01 00:00:00', updated_at TIMESTAMP(0) DEFAULT '1970-01-01 00:00:00')");
        $this->addSql('ALTER TABLE HPA_PERSONNE ADD CONSTRAINT FK_528B7905A76ED395 FOREIGN KEY (user_id) REFERENCES hpa_user (id) ON DELETE SET NULL');
        $this->addSql('ALTER TABLE HPA_RESULTAT ADD (foo CLOB DEFAULT NULL)');
        $this->addSql('UPDATE HPA_RESULTAT SET foo = valeur_alpha');
        $this->addSql('ALTER TABLE HPA_RESULTAT DROP COLUMN valeur_alpha');
        $this->addSql('ALTER TABLE HPA_RESULTAT RENAME COLUMN foo TO valeur_alpha');
        $this->addSql('ALTER TABLE HPA_UPLOAD RENAME COLUMN date_creation TO created_at');
        $this->addSql('ALTER TABLE HPA_CONFIGURATION DROP (LONGUEUR_MDP_ENROLEMENT)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_A3B0580114E48A3B ON HPA_CONFIGURATION (theme_name)');
        $this->addSql('ALTER TABLE HPA_USER MODIFY (created_at TIMESTAMP(0) DEFAULT NULL, has_unassigned_patient NUMBER(1) DEFAULT NULL, nb_failed_login_attempt NUMBER(10) DEFAULT NULL, payment_is_enabled NUMBER(1) DEFAULT NULL, updated_at TIMESTAMP(0) DEFAULT NULL)');
        $this->addSql('ALTER TABLE hpa_demande RENAME COLUMN NUMERO_DEMANDE TO nodem');
        $this->addSql("ALTER TABLE HPA_DEMANDE MODIFY (nodemx VARCHAR2(50) DEFAULT '-')");
        $this->addSql('ALTER TABLE HPA_DEMANDE MODIFY (created_at TIMESTAMP(0) NOT NULL, nodemx VARCHAR2(50) NOT NULL, paie_solde DOUBLE PRECISION DEFAULT NULL, paie_source VARCHAR2(20) DEFAULT NULL, updated_at TIMESTAMP(0) NOT NULL)');
        $this->addSql('ALTER TABLE HPA_JOURNAL MODIFY (ip_address VARCHAR2(255) DEFAULT NULL)');
        $this->addSql('ALTER TABLE HPA_JOURNAL ADD (foo CLOB DEFAULT NULL)');
        $this->addSql('UPDATE HPA_JOURNAL SET foo = long_description');
        $this->addSql('ALTER TABLE HPA_JOURNAL DROP COLUMN long_description');
        $this->addSql('ALTER TABLE HPA_JOURNAL RENAME COLUMN foo TO long_description');
        $this->addSql('ALTER TABLE HPA_PATIENT MODIFY (created_at TIMESTAMP(0) NOT NULL, updated_at TIMESTAMP(0) NOT NULL)');
        $this->addSql('ALTER TABLE HPA_ETABLISSEMENT MODIFY (created_at TIMESTAMP(0) DEFAULT NULL, updated_at TIMESTAMP(0) DEFAULT NULL)');
        $this->addSql('ALTER TABLE HPA_PERSONNE_PATIENT MODIFY (created_at TIMESTAMP(0) NOT NULL, updated_at TIMESTAMP(0) NOT NULL)');
        $this->addSql('ALTER TABLE HPA_PERSONNE MODIFY (created_at TIMESTAMP(0) NOT NULL, updated_at TIMESTAMP(0) NOT NULL)');
        $this->addSql('ALTER TABLE HPA_DEMANDE MODIFY (created_at TIMESTAMP(0) DEFAULT NULL, nodem VARCHAR2(50) DEFAULT NULL, nodemx VARCHAR2(50) DEFAULT NULL, updated_at TIMESTAMP(0) DEFAULT NULL)');
        $this->addSql('ALTER TABLE HPA_PATIENT MODIFY (created_at TIMESTAMP(0) DEFAULT NULL, updated_at TIMESTAMP(0) DEFAULT NULL)');
        $this->addSql('ALTER TABLE HPA_PERSONNE_PATIENT MODIFY (created_at TIMESTAMP(0) DEFAULT NULL, updated_at TIMESTAMP(0) DEFAULT NULL)');
        $this->addSql('ALTER TABLE HPA_PERSONNE MODIFY (created_at TIMESTAMP(0) DEFAULT NULL, updated_at TIMESTAMP(0) DEFAULT NULL)');
        $this->addSql('DROP TABLE hpa_structure');
        $this->addSql('ALTER TABLE HPA_USER MODIFY (created_at TIMESTAMP(0) NOT NULL, updated_at TIMESTAMP(0) NOT NULL)');

        $this->addSql('DROP SEQUENCE HPA_LECTURE_ID_SEQ');
        $this->addSql('DROP SEQUENCE HPA_STRUCTURE_ID_SEQ');
        $this->addSql('CREATE SEQUENCE hpa_paiement_id_seq START WITH 1 MINVALUE 1 INCREMENT BY 1');
        $this->addSql('CREATE TABLE hpa_paiement (id NUMBER(10) NOT NULL, tpe VARCHAR2(7) NOT NULL, cle VARCHAR2(255) NOT NULL, societe VARCHAR2(255) NOT NULL, nom VARCHAR2(255) DEFAULT NULL NULL, prenom VARCHAR2(255) DEFAULT NULL NULL, email VARCHAR2(255) NOT NULL, telephone VARCHAR2(20) DEFAULT NULL NULL, affichage_cr NUMBER(1) NOT NULL, actif NUMBER(1) NOT NULL, created_at TIMESTAMP(0) DEFAULT NULL NULL, updated_at TIMESTAMP(0) DEFAULT NULL NULL, PRIMARY KEY(id))');
        $this->addSql('DROP TABLE HPA_LECTURE');
        $this->addSql('ALTER TABLE HPA_DEMANDE ADD (is_updated NUMBER(1) DEFAULT 0 NOT NULL, affichage_cr NUMBER(1) DEFAULT 0 NOT NULL)');
        $this->addSql('ALTER TABLE HPA_DEMANDE MODIFY (is_updated NUMBER(1) DEFAULT NULL, affichage_cr NUMBER(1) DEFAULT NULL)');
        $this->addSql('ALTER TABLE HPA_DEMANDE RENAME COLUMN is_read TO is_new');
        // hpa_demande. is_read became is_new, and all values are now reversed.
        $this->addSql('UPDATE hpa_demande SET is_new = case when is_new=1 then 0 else 1 end');
        $this->addSql('DROP INDEX uniq_26afa4faa4d60759');
        $this->addSql('ALTER TABLE HPA_ETABLISSEMENT ADD (paiement_id NUMBER(10) DEFAULT NULL NULL)');
        $this->addSql('ALTER TABLE HPA_ETABLISSEMENT ADD CONSTRAINT FK_26AFA4FA2A4C4478 FOREIGN KEY (paiement_id) REFERENCES hpa_paiement (id)');
        $this->addSql('CREATE INDEX IDX_26AFA4FA2A4C4478 ON HPA_ETABLISSEMENT (paiement_id)');

        $this->addSql('COMMENT ON COLUMN HPA_DEMANDE.bsrvr_constant_login IS \'Is used as a unique identifier to retrieve a demande.\'');
        $this->addSql('COMMENT ON COLUMN HPA_DEMANDE.is_new IS \'Demande that has ever been read by the patient (or not).\'');
        $this->addSql('COMMENT ON COLUMN HPA_DEMANDE.is_updated IS \'Demande that has seen its content updated.\'');
        $this->addSql('ALTER TABLE HPA_ETABLISSEMENT ADD (foo_ej VARCHAR2(20) DEFAULT NULL, foo_lab NUMBER(10) DEFAULT NULL, foo_finess VARCHAR2(20) DEFAULT NULL)');
        $this->addSql('UPDATE HPA_ETABLISSEMENT SET foo_ej = bsrvr_code_entite_juridique, foo_lab = bsrvr_code_labo, foo_finess = bsrvr_ref_etablissement');
        $this->addSql('ALTER TABLE HPA_ETABLISSEMENT DROP COLUMN bsrvr_code_entite_juridique');
        $this->addSql('ALTER TABLE HPA_ETABLISSEMENT RENAME COLUMN foo_ej TO bsrvr_code_entite_juridique');
        $this->addSql('ALTER TABLE HPA_ETABLISSEMENT DROP COLUMN bsrvr_code_labo');
        $this->addSql('ALTER TABLE HPA_ETABLISSEMENT RENAME COLUMN foo_lab TO bsrvr_code_labo');
        $this->addSql('ALTER TABLE HPA_ETABLISSEMENT DROP COLUMN bsrvr_ref_etablissement');
        $this->addSql('ALTER TABLE HPA_ETABLISSEMENT RENAME COLUMN foo_finess TO bsrvr_ref_etablissement');
        $this->addSql('COMMENT ON COLUMN HPA_ETABLISSEMENT.bsrvr_code_entite_juridique IS \'Name given to the EJ upon its creation.\'');
        $this->addSql('COMMENT ON COLUMN HPA_ETABLISSEMENT.bsrvr_ref_etablissement IS \'\'\'Login\'\' field from BioServeur EJ page: FINESS or other string, with or without prefix and suffix.\'');

        $this->addSql('ALTER TABLE HPA_USER ADD (first_enrollment_details CLOB DEFAULT NULL NULL)');
        $this->addSql("COMMENT ON COLUMN HPA_USER.first_enrollment_details IS '(DC2Type:json_array)'");
        $this->addSql('ALTER TABLE HPA_CONFIGURATION ADD (patient_record_direct_access NUMBER(1) DEFAULT 1)');
        $this->addSql("COMMENT ON COLUMN HPA_DEMANDE.bsrvr_constant_login IS 'Unique identifier for a user in a specific lab'");
        $this->addSql('ALTER TABLE MNTC_REQUEST ADD (user_session VARCHAR2(40) DEFAULT NULL NULL)');

        $this->addSql('ALTER TABLE HPA_CONFIGURATION ADD (last_patients_record_only_mssg NUMBER(1) DEFAULT 0)');
        $this->addSql("UPDATE hpa_configuration SET last_patients_record_only_mssg = 1 WHERE theme_name = '" . ConfigurationData::THEME_UNILABS . "' OR  theme_name = '" . ConfigurationData::THEME_LABAZUR . "'");
        $this->addSql("COMMENT ON COLUMN HPA_DEMANDE.bsrvr_constant_login IS 'Unique identifier for a user in a specific lab.'");
        $this->addSql("ALTER TABLE HPA_ETABLISSEMENT MODIFY (bsrvr_ref_etablissement VARCHAR2(20) NOT NULL)");
        $this->addSql("ALTER TABLE HPA_CONFIGURATION MODIFY (patient_record_direct_access NUMBER(1) NOT NULL, last_patients_record_only_mssg NUMBER(1) NOT NULL)");
        $this->addSql("COMMENT ON COLUMN HPA_PAIEMENT.affichage_cr IS 'True if the CR can be accessed, false otherwise.'");
        $this->addSql("COMMENT ON COLUMN HPA_PAIEMENT.actif IS 'True if payment is enabled, false otherwise.'");
        $this->addSql("ALTER TABLE HPA_CHAPITRE MODIFY (libelle VARCHAR2(255) DEFAULT NULL NULL)");
        $this->addSql("ALTER TABLE HPA_CONFIGURATION MODIFY (patient_record_direct_access NUMBER(1) DEFAULT NULL, last_patients_record_only_mssg NUMBER(1) DEFAULT NULL)");
        $this->addSql("ALTER TABLE HPA_RESULTAT MODIFY (valeur_alpha NOT NULL)");
        $this->addSql("ALTER TABLE HPA_JOURNAL MODIFY (long_description DEFAULT NULL)");
        $this->addSql("ALTER TABLE HPA_JOURNAL MODIFY (long_description DEFAULT NULL)");

        // Cannot be set as unique because the default value when the db was updated was '999999999'. Check again later that all finesses are updated and update this field with UNIQ.
       // $this->addSql("CREATE UNIQUE INDEX UNIQ_26AFA4FA6EC6D973 ON HPA_ETABLISSEMENT (bsrvr_ref_etablissement)");

        $this->addSql("INSERT INTO hpa_configuration (id, theme_name, logo_alignement, logout_redirection_url, patient_record_direct_access, last_patients_record_only_mssg) VALUES (hpa_configuration_id_seq.NEXTVAL, 'bioocean', 'center', null, 0, 0)");

// V2        $this->addSql("ALTER INDEX uniq_588a8ab492fc23a8 RENAME TO UNIQ_5F7839FC92FC23A8");
// V2        $this->addSql("ALTER INDEX uniq_588a8ab4a0d96fbf RENAME TO UNIQ_5F7839FCA0D96FBF");
// V2        $this->addSql("ALTER INDEX idx_189e467d5c8659a RENAME TO IDX_6CEBB6C35C8659A");
// V2        $this->addSql("ALTER INDEX idx_7f4acfe16b899279 RENAME TO IDX_B3F3F5F6B899279");
// V2        $this->addSql("ALTER INDEX idx_9879ff09a76ed395 RENAME TO IDX_EC0C0FB7A76ED395");
// V2        $this->addSql("ALTER INDEX idx_d488b5d5a76ed395 RENAME TO IDX_B93C403FA76ED395");
// V2        $this->addSql("ALTER INDEX idx_4304cfafa76ed395 RENAME TO IDX_37713F11A76ED395");
// V2        $this->addSql("ALTER INDEX idx_3c69dcb01fbeef7b RENAME TO IDX_F84BF8131FBEEF7B");
// V2        $this->addSql("ALTER INDEX idx_4548b2b6a76ed395 RENAME TO IDX_528B7905A76ED395");
// V2        $this->addSql("ALTER INDEX idx_ad5d26bb1f1f2a24 RENAME TO IDX_BA9EED081F1F2A24");
// V2        $this->addSql("ALTER INDEX idx_ad5d26bb80e95e18 RENAME TO IDX_BA9EED0880E95E18");
// V2        $this->addSql("ALTER INDEX idx_7a98b543a21bd112 RENAME TO IDX_BEBA91E0A21BD112");
// V2        $this->addSql("ALTER INDEX idx_7a98b54380e95e18 RENAME TO IDX_BEBA91E080E95E18");
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'oracle', 'Migration can only be executed safely on \'oracle\'.');
    }

}





