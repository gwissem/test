<?php
namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Add indexes
 */
class Version20180924112600 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        /**
         * Oracle
         */
        $this->abortIf($this->connection->getDatabasePlatform()
            ->getName() !== 'oracle', 'Migration can only be executed safely on \'oracle\'.');

        $this->addSql("CREATE INDEX IDX_BSRVR_CONST_LOG_NDMX ON HPA_DEMANDE (bsrvr_constant_login,nodemx)");
        $this->addSql("CREATE INDEX IDX_NODEMX ON HPA_DEMANDE (nodemx)");
        $this->addSql("CREATE INDEX IDX_NOM_PRESCRIPTEUR ON HPA_DEMANDE (nom_prescripteur)");
    }

    /**
     *
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()
            ->getName() !== 'oracle', 'Migration can only be executed safely on \'oracle\'.');
    }
}





