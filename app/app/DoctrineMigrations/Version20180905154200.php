<?php
namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * HPA Version 2.2
 */
class Version20180905154200 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        /**
         * Oracle
         */
        $this->abortIf($this->connection->getDatabasePlatform()
            ->getName() !== 'oracle', 'Migration can only be executed safely on \'oracle\'.');

        $this->addSql("ALTER TABLE HPA_CONFIGURATION MODIFY (video_tuto_register VARCHAR2(255) NOT NULL)");
        $this->addSql("COMMENT ON COLUMN HPA_ETABLISSEMENT.bsrvr_code_labo IS 'The 4 digits number of the lab.'");
    }
    /**
     *
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()
            ->getName() !== 'oracle', 'Migration can only be executed safely on \'oracle\'.');
    }
}





