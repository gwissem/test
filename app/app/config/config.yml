imports:
    - { resource: parameters.yml }
    - { resource: security.yml }
    - { resource: services.yml }

parameters:
    session_max_idle_time: 900
    host_piwik: stats.mesanalyses.fr
    locale: fr
    timezone: Europe/Paris
    currency: EUR
    copyright: Agfa Healthcare. All rights reserved.
#    activation_compte: 5 # Nombre de jours où le compte peut être confirmé
    version_application: v2.2.0
    sys_admin_email: ['charles.becker@agfa.com', 'beccha2+aglogs@gmail.com'] # email pour les envois d'erreur par email à l'admin système.
    max_login_attempts: 3
    auth_code_lifetime: 5184000
    journal_nb_entries_to_keep: 200
    journal_nb_month_to_keep: 24
        
framework:
#    esi: { enabled: true }
#    fragments: { path: /_fragment }
    fragments: ~
    translator: { fallbacks: ['%locale%'] }
    secret: "%secret%"
    router:
        resource: "%kernel.root_dir%/config/routing.yml"
        strict_requirements: ~
    form:            ~
    csrf_protection:
        enabled: true
    validation:      { enable_annotations: true }
    templating:
        engines: ['twig']
    assets:
        version: "%version_application%"
    session:
        cookie_secure: true
        cookie_httponly: true
        cookie_lifetime: 86400 # has to be more than 'session_max_idle_time' to avoid loging out the user before the request listener does it.
        name: sid
        # http://symfony.com/doc/current/reference/configuration/framework.html#handler-id
#        handler_id:  session.handler.native_file
#        save_path:   "%kernel.root_dir%/../var/sessions/%kernel.environment%"
        handler_id: snc_redis.session.handler
    default_locale: "%locale%"
    serializer:
        enabled: true
        #enable_annotations: true
    http_method_override: true
    php_errors:
        log: true
    web_link:
        enabled: true
    cache:
        app: cache.adapter.redis
        default_redis_provider: "redis://%redis_host%:6379"
        pools:
            app.cache.search:
                adapter: cache.app
                default_lifetime: 15
        
# Twig Configuration
twig:
    debug:            "%kernel.debug%"
    strict_variables: "%kernel.debug%"
    globals:
        google_site_verification: FcYdP4gaG4D16cIqe63IpOn_DgMNlO7R9Q_scEYzwFI
        service_demandes: "@agfa_user.demandes_service"
        service_contexte: "@contexte"
        service_uploads: "@uploads"
        service_journalist: "@journalist"
        service_menu: "@agfa_hpa.menu_data_service"
        service_paiement: "@agfa_hpa.paiement_service"
        client_theme: "%client_theme%"
        client_brand: "%client_brand%"
        piwik_idsite: "%piwik_site_id%"
        piwik_hostname: "%host_piwik%"
        copyright: "%copyright%"
        version_application: "%version_application%"
        app_name: "%app_name%"
        config: "@agfa_hpa.configuration_service"
        locale: "%locale%"
        timezone: "%timezone%"
        currency: "%currency%"
        auth_forte: "%auth_forte%"
        max_login_attempts: "%max_login_attempts%"
        auth_code_lifetime: "%auth_code_lifetime%"
        journal_nb_entries_to_keep: "%journal_nb_entries_to_keep%"
        journal_nb_month_to_keep: "%journal_nb_month_to_keep%"
        client_theme_color: "%client_theme_color%"
    form_themes:
        - BraincraftedBootstrapBundle:Form:bootstrap.html.twig

# Doctrine Configuration
doctrine:
    dbal:
        driver:   "%database_driver%"
        host:     "%database_host%"
        port:     "%database_port%"
        dbname:   "%database_name%"
        user:     "%database_user%"
        password: "%database_password%"
        charset:  UTF8
        logging:  true
        persistent:  true

    orm:
        auto_generate_proxy_classes: false #"%kernel.debug%"
#        naming_strategy: doctrine.orm.naming_strategy.underscore
        entity_managers:
            default:
                auto_mapping: true
# Ready for docker
                metadata_cache_driver: redis
                query_cache_driver: redis
                result_cache_driver: redis
                second_level_cache:
                    enabled: true
                    log_enabled: true
                    region_cache_driver:
                        type: service
                        id: snc_second_level_cache
#                    region_lifetime: 86400
                    regions:
                        slc_configuration:
                            lifetime: 7200
                            cache_driver: 
                                type: service
                                id: snc_second_level_cache
#                        slc_demande:
#                            lifetime: 10
#                            cache_driver: 
#                                type: service
#                                id: snc_second_level_cache
# Problem with cache: delete personne that is also current_personne and go to front page: 'Personne with id {id} not found'
#                        slc_user:
#                            lifetime: 60
#                            cache_driver: 
#                                type: service
#                                id: snc_second_level_cache
                dql:
                    datetime_functions:
                        day: DoctrineExtensions\Query\Oracle\Day
                        month: DoctrineExtensions\Query\Oracle\Month
                        year: DoctrineExtensions\Query\Oracle\Year
                        to_char: DoctrineExtensions\Query\Oracle\ToChar
                        trunc: DoctrineExtensions\Query\Oracle\Trunc
                    string_functions:
                        nvl: DoctrineExtensions\Query\Oracle\Nvl
                        listagg: DoctrineExtensions\Query\Oracle\Listagg
                        to_date: DoctrineExtensions\Query\Oracle\ToDate
snc_redis:
    clients:
        default:
            type: predis
            alias: default
            dsn: redis://%redis_host%:6379
        doctrine:
            type: predis
            alias: doctrine
            dsn: redis://%redis_host%:6379
    doctrine:
        metadata_cache:
            client: doctrine
            entity_manager: default
#            document_manager: default
        query_cache:
            client: doctrine
            entity_manager: default
        result_cache:
            client: doctrine
            entity_manager: default
        second_level_cache:
            client: doctrine
            entity_manager: default
#            namespace: 'dslc:'
    session:
        client: default
        prefix: session
        ttl: 7200 # 2 hours
        locking: true
        spin_lock_wait: 150000

# Swiftmailer Configuration
swiftmailer:
    transport: "%mailer_transport%"
    host:      "%mailer_host%"
    username:  "%mailer_user%"
    password:  "%mailer_password%"
    spool:     { type: memory }
    
# SchebTwoFactorBundle Configuration
scheb_two_factor:
    email:
        enabled: "%auth_forte%"
        sender_email: "%client_email%"
        sender_name: "%client_brand%"
        digits: 4
        template: :Authentication:two_factor_authentication.html.twig   # Template used to render the authentication form
        mailer: agfa_authcode_mailer
    trusted_computer:
        enabled: true
        cookie_secure: true
        cookie_name: tc
        cookie_lifetime: 5184000 # 60 days

# FOSUserBundle Configuration
fos_user:
    db_driver: orm
    firewall_name: main
    user_class: Agfa\UserBundle\Entity\User
    change_password:
        form:
            type:               FOS\UserBundle\Form\Type\ChangePasswordFormType
            name:               fos_user_change_password_form
            validation_groups:  [MyChangePassword, Default]
    registration:
        confirmation:
            enabled:    true
            template:   FOSUserBundle:Registration:email.txt.twig
        form:
            type: Agfa\UserBundle\Form\Type\RegistrationFormType
            validation_groups:  [MyRegistration, Default]
    resetting:
        token_ttl: 86400
        email:
            template:   FOSUserBundle:Resetting:email.txt.twig
        form:
            type:               FOS\UserBundle\Form\Type\ResettingFormType
            name:               fos_user_resetting_form
            validation_groups:  [MyResetPassword, Default]
    profile:
        form:
            type: Agfa\UserBundle\Form\Type\ProfileFormType
    from_email:
        address:        "%client_email%"
        sender_name:    "%client_brand%"
            
# FOSJsRoutingBundle Configuration
fos_js_routing:
    routes_to_expose: [ '.*' ]

# DoctrineExtensions Configuration
stof_doctrine_extensions:
    orm:
        default:
            timestampable: true

# OracleSessionInit sets the connection to work with Oracle (date format...)

services:
# Enable the following for prod
#    my.listener:
#        class: Doctrine\DBAL\Event\Listeners\OracleSessionInit
#        tags:
#            - { name: doctrine.event_listener, event: postConnect }
#        arguments:
#            - { NLS_SORT: "FRENCH_M_AI", NLS_COMP: "LINGUISTIC" }

# Assetic Configuration
assetic:
    debug:          "%kernel.debug%"
    use_controller: false
    bundles:
        - AgfaUserBundle
        - SchebTwoFactorBundle
    #java: /usr/bin/java
    sass: "/usr/local/bin/sass"
    assets:
        hind-bold-ttf:
            inputs: '%kernel.root_dir%/Resources/public/fonts/Hind-Bold.ttf'
            output: 'fonts/Hind-Bold.ttf'
        hind-light-ttf:
            inputs: '%kernel.root_dir%/Resources/public/fonts/Hind-Light.ttf'
            output: 'fonts/Hind-Light.ttf'
        hind-medium-ttf:
            inputs: '%kernel.root_dir%/Resources/public/fonts/Hind-Medium.ttf'
            output: 'fonts/Hind-Medium.ttf'
        hind-regular-ttf:
            inputs: '%kernel.root_dir%/Resources/public/fonts/Hind-Regular.ttf'
            output: 'fonts/Hind-Regular.ttf'
        hind-semibold-ttf:
            inputs: '%kernel.root_dir%/Resources/public/fonts/Hind-Semibold.ttf'
            output: 'fonts/Hind-Semibold.ttf'
        globalCss: 
            inputs:
                - "%kernel.root_dir%/Resources/public/sass/styles.sass"
                - "%kernel.root_dir%/Resources/public/css/theme/%client_theme%/styles.sass"
    filters:
        cssrewrite: ~
        yui_css:
            jar: "%kernel.root_dir%/Resources/java/yuicompressor-2.4.8.jar"
        sass:
            apply_to: ".(scss|sass)$"
        uglifyjs2:
            bin: /usr/local/bin/uglifyjs
        uglifycss:
            bin: /usr/local/bin/uglifycss
            
braincrafted_bootstrap:
#    output_dir: ~
    assets_dir: "%kernel.root_dir%/../vendor/twbs/bootstrap-sass/assets"
    jquery_path: "%kernel.root_dir%/../vendor/jquery/jquery/jquery-1.11.1.js"
    css_preprocessor: sass # "less", "lessphp", "sass" or "none"
    fonts_dir: "%kernel.root_dir%/../web/fonts"
    fontawesome_dir: "%kernel.root_dir%/../vendor/bmatzner/fontawesome-bundle/Bmatzner/FontAwesomeBundle/Resources/public"
    icon_prefix: fa
    auto_configure:
        assetic: true
        twig: true
#        knp_menu: true
#        knp_paginator: true
    customize:
        variables_file: ~
#        bootstrap_output: "%kernel.root_dir%/Resources/less/bootstrap.less"
#        bootstrap_template: "BraincraftedBootstrapBundle:Bootstrap:bootstrap.less.twig"

sonata_intl:
    timezone:
        locales:
            fr: Europe/Paris
            mq: America/Martinique
            re: Indian/Reunion
        default: "%timezone%"

doctrine_migrations:
    table_name: hpa_migration
        
agfa_monetico_paiement: 
    parameters: 
        url_retour: paiement_retour
        url_retour_ok: paiement_retour_ok
        url_retour_err: paiement_retour_err
        reference_code: 'HPA' # Used to differentiate online payments from manual payments. Keep a max of three letters for prod so to leave enough digits for ref ids.
    servers: 
        preprod: ~
        prod: 
            paiement: https://p.monetico-services.com/test/paiement.cgi
            capture: https://p.monetico-services.com/test/capture_paiement.cgi
            recredit: https://p.monetico-services.com/test/recredit_paiement.cgi

