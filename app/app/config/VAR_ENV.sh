#!/bin/sh
#export EVT_HEXAPAT=PROD

# Host
#export SERVEUR_HEXAPAT="unilabs.mesanalyses.fr"

# Database
export DB_DRIVER="oci8"
export DB_SERVER="127.0.0.1"
export DB_PORT=null
export DB_USER="root"
export DB_PASSWORD="admin"
export DB_NAME="sf3_hexapat_dev"

# Symfony
export SECRET="ThisTokenIsNotSoSecretChangeIt"
export PIWIK_SITE_ID="2"
export PASS_HEXAPAT=null
export PASS_BIOSERVEUR=null
export CLIENT_THEME="dev"
export CLIENT_BRAND="Mes analyses"
export APP_NAME="demo.mesanalyses.fr"
export CLIENT_EMAIL="ne-pas-repondre@mesanalyses.fr"

###################

# Mailer
export MAILER_TRANSPORT="smtp"
export MAILER_HOST="127.0.0.1"
export MAILER_USER=null
export MAILER_PASSWORD=null

# Machine
#export HEXAPAT_HOME="/opt/hexapat"
#export PORT_HEXAPAT=443
#export NOM_UTILISATEUR=hexapat
#export NOM_GROUPE=hexapat
#export MODE_SECURISE="O"                                  
#unset CERTIFICATE
#export REDIRIGER_PORT80="O"
#export PATH="$HOME:$HEXAPAT_HOME/logicielstiers/bin:/usr/bin:/bin:/usr/sbin:/sbin:"
#export LD_LIBRARY_PATH="$HEXAPAT_HOME/logicielstiers/lib:"
#export TNS_ADMIN="$HEXAPAT_HOME/logicielstiers/etc"
#unset HTTPD_DEBUG
#export NLS_TIMESTAMP_FORMAT='YYYY-MM-DD HH24:MI:SS'

# PHP & Symfony
#export MA_PHP_TIMEZONE="Europe/Paris"
#export MA_PHP_MAX_UPLOAD_FILESIZE="10M"
#export MA_PHP_MAX_SIZE="11M"
#export PHP_INI_SCAN_DIR=$HEXAPAT_HOME/logicielstiers/etc/php/
    
# Other softwares
#export OPENSSL_CONF="$HEXAPAT_HOME/logicielstiers/etc/openssl.cnf"
#export CURL_HOME="$HEXAPAT_HOME/logicielstiers/etc/"
#export CERTIFICATE="wildcard.mesanalyses.fr"

# Set Symfony environment
#if [ "$EVT_HEXAPAT" == "DEV" ] ; then
#  SYMFONY_ENV="dev"
#else
#  SYMFONY_ENV="prod"
#fi
#export SYMFONY_ENV





# For vhost apache (dev conf)
# Database
SetEnv DB_SERVER 127.0.0.1
SetEnv DB_PORT
SetEnv DB_USER root
SetEnv DB_PASSWORD admin
SetEnv DB_NAME sf3_hexapat_dev

# Symfony
SetEnv SECRET ThisTokenIsNotSoSecretChangeIt
SetEnv PIWIK_SITE_ID 2
SetEnv PASS_HEXAPAT null
SetEnv PASS_BIOSERVEUR null
SetEnv CLIENT_THEME dev
SetEnv CLIENT_BRAND Mes analyses
SetEnv APP_NAME demo.mesanalyses.fr
SetEnv CLIENT_EMAIL ne-pas-repondre@mesanalyses.fr
SetEnv DB_DRIVER pdo_mysql

# Mailer
SetEnv MAILER_TRANSPORT smtp
SetEnv MAILER_HOST rdmail.lyon.gwi.fr
SetEnv MAILER_USER APVOO
SetEnv MAILER_PASSWORD xxx























