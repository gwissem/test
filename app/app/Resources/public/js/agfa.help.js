agfa('Help', function () {
	var oHelp = this;
	
	oHelp.messages = 
	[
	 {
		elt: 'a.navbar-brand',
		txt: "Ce lien vous ramène à l'accueil.",
		pos: "bottom"
	 },
	 {
		 elt: '#lien-comptes-rendus',
		 txt: "Ce lien vous amène à la liste complète des comptes rendus.",
		 pos: "bottom"
	 },
	 {
		elt: '#compteLabel',
		txt: "Cliquez ici pour vous déconnecter.",
		pos: "bottom"
	 },
	 {
		elt: '#lien-recherche',
		txt: "Cliquez ici pour rechercher des comptes rendus par date ou par examen.",
		pos: "bottom"
	 }
	];
	oHelp.next_message_index = 0;
	oHelp.current_element = null;
	
	/** 
	 * ici on initialise ce qu'il faut pour les autres méthodes
	 * le DOM n'est pas chargé
	 */ 
	oHelp.init = function () {
	}
	
	/**
	 * ici on continue à initialiser
	 * le DOM est chargé
	 */		
	oHelp.ready = function () {
	}
	
	/**
	 * mise en place des handlers
	 */		
	oHelp.bindEvents = function () {
		$('body').on('click', '.tooltip-inner-content .btn-suivant', oHelp.next);
		$('body').on('click', '.tooltip-inner-content .btn-quitter', oHelp.stop);
		$('body').on('keydown', oHelp.keyDown);
	}
	
	/**
	 * nettoyage des handlers
	 */
	oHelp.unbindEvents = function () {
		$('body').off('click', '.tooltip-inner-content .btn-suivant', oHelp.next);
		$('body').off('click', '.tooltip-inner-content .btn-quitter', oHelp.stop);
		$('body').off('keydown', oHelp.keyDown);
	}

	/**
	 * traitement des touches clavier
	 */
	oHelp.keyDown = function (event) {
		if(event.keyCode === 27) oHelp.stop();
	}
	
	/**
	 * l'utilisateur a demandé l'aide
	 */
	oHelp.start = function () {
		oHelp.bindEvents();
		oHelp.affiche_overlay('#000000', 0.5);
		oHelp.next_message_index = 0;
		oHelp.next();
	}
	
	/**
	 * fin de l'aide, soit à la demande de l'utilisateur, soit automatiquement
	 */
	oHelp.stop = function () {
		console.log("AGFA Help stop");
		oHelp.masque_aide_element();
		oHelp.masque_overlay();
		oHelp.unbindEvents();
	}
	
	/**
	 * prochain tooltip
	 */
	oHelp.next = function () {
		console.log("AGFA Help next index: "+oHelp.next_message_index);
		if(oHelp.next_message_index === oHelp.messages.length) {
			oHelp.stop();
			return;
		}
		oHelp.masque_aide_element();
		var message = oHelp.messages[oHelp.next_message_index++];
		if(!message || $(message.elt).length === 0) oHelp.next();
		oHelp.affiche_aide_element($(message.elt), message.txt, message.pos);
	}
	
	/**
	 * affichage du masque sombre sur la page
	 */
	oHelp.affiche_overlay = function (color, opacity) {
		oHelp.overlay = $('<div class="full-overlay"></div>')
			.appendTo('body')
			.css('backgroundColor', color)
			.animate({
				opacity: opacity
			}, 'slow');
	}
	
	/**
	 * suppression du masque sombre
	 */
	oHelp.masque_overlay = function () {
		oHelp.overlay.animate({
			opacity: 0
		}, 'fast', function () {
			oHelp.overlay.remove();
		});
	}
	
	/**
	 * affichage de l'aide sur un élément
	 */
	oHelp.affiche_aide_element = function (element, text, placement) {
		oHelp.current_element = $(element).tooltip({
			title: '<div class="tooltip-inner-content"><p>'+text+'</p><div class="btn-toolbar" role="toolbar"><button class="btn btn-primary btn-xs btn-suivant">Suivant</button><button class="btn btn-default btn-xs btn-quitter">Quitter</button></div></div>',
			html: true,
			container: 'body',
			placement: placement,
			trigger: 'manual'
		}).tooltip('show');
	}
	
	/**
	 * suppression de l'aide sur un élément
	 */
	oHelp.masque_aide_element = function (element) {
		if(!element) element = oHelp.current_element;
		delete oHelp.current_element;
		if(!element) return;
		element.tooltip('hide');
	}
});
