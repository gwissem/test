agfa('UpgradeBrowser', function () {
	var oUpgradeBrowser = this;
	oUpgradeBrowser.ready = function () {
		var msg = $('<div class="alert alert-danger text-center" style="margin-bottom:0">Veuillez <a href="https://www.microsoft.com/fr-fr/windows">mettre à jour</a> votre navigateur ou installer un navigateur plus récent (par exemple, <a href="https://www.google.com/chrome/">Chrome</a> ou <a href="https://www.mozilla.org/firefox/">Firefox</a>).</div>');
		$('body').prepend(msg);
	}
});
