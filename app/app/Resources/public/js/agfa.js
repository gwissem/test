var AGFA = {
	debug : false,
	modules : {}
};

/**
 * créé un module AGFA
 * 
 * @param module
 *            Function
 */
function agfa(nom, module) {
	var objet = AGFA.modules[nom] = new module();

	/**
	 * ici on initialise ce qu'il faut pour les autres méthodes le DOM n'est pas
	 * chargé
	 */
	if (typeof objet.init === 'function') {
		if (AGFA.debug)
			console.log('AGFA ' + nom + ' init');
		objet.init();
	}

	/**
	 * ici on continue à initialiser le DOM est chargé
	 */
	if (typeof objet.ready === 'function')
		$.when($.ready).then(function() {
			if (AGFA.debug)
				console.log('AGFA ' + nom + ' ready');
			objet.ready();
		});
}

$(function() {
	$('[data-toggle="popover"]').popover();

	$('input[type="password"]:visible, input[type="password"]#enrolement_password')
			.each(
					function(i, elt) {
						$(elt)
								.wrap('<div class="input-group"></div>')
								.after(
										'<span class="input-group-btn"><button class="btn btn-default reveal-password" type="button" data-target="'
												+ $(elt).attr('id')
												+ '"><i class="fa fa-eye"></i></button></span>');
					});

	$('body').on(
			'click',
			'button.reveal-password',
			function(event) {
				if ($(this).data('action') === 'hide') {
					$('#' + $(this).data('target')).attr('type', 'password');
					$(this).data('action', 'show').find('.fa').removeClass(
							'fa fa-eye-slash').addClass('fa fa-eye');
				} else {
					$('#' + $(this).data('target')).attr('type', 'text');
					$(this).data('action', 'hide').find('.fa').removeClass(
							'fa fa-eye').addClass('fa fa-eye-slash');
				}
			});

	$('body').on('click', '.btn-remove-trusted-computer', function(event) {
		var row = $(this).closest('tr');
		var token = $(row).data('token');
		$.ajax(Routing.generate('remove_trusted_computer'), {
			method : 'POST',
			data : {
				token : token
			},
			dataType : 'json',
			success : function(data) {
				$(row).remove();
			}
		});
	});
	
	if(navigator.cookieEnabled === false){
		$('#cookie-bar').removeClass('hide');
	}
})

// $('#search').keyup(
// function() {
// var searchField = $('#search').val();
// var myExp = new RegExp(searchField, 'i');
// $.ajax({
// url: Routing.generate('admin_user_user_json')+'?searchTerms='+searchField,
// success: function(data) {
// var output = '';
// $.each(data,
// function(key, val) {
// if ((val.email.search(myExp) != -1)) {
// output += '<tr><td>' + val.email
// + '</td></tr>';
// }
// });
// $('#update').html(output);
// },
// dataType: 'json',
// cache: false
// });
// });

//$('#search').keyup(
//		function() {
//			var searchField = $('#search').val();
//			var content = $("#update").html();
//			var myExp = new RegExp(searchField, 'i');
//			$.ajax({
//				url : Routing.generate('admin_user_list_users')
//						+ '?searchTerms=' + searchField,
//				date : content,
//				success : function(data) {
//					$('#update').html(data);
//					$("div.ajax_loader").remove();
//				},
//				dataType : 'html',
//				cache : false,
//				error : function(ts) {
//					alert(ts.responseText)
//				}
//			});
//		});

// GUI Optimisation

// Reload the window at the same position that it was before clicking on an
// action
function readCookie(name) {
	return (document.cookie.match('(^|; )' + name + '=([^;]*)') || 0)[2];
}


$('[data-toggle="tooltip"]').tooltip();


// Show or hide "Un nouveau code de sécurité vous sera transmis à votre prochaine connexion." when "se souvenir..." is either checked or not
$( "#_trusted:checked" ).on( "click", function() {
	$(this).closest(":has(p)").find('p').toggle();
});

// Set the cursor to the first form input field found on the page
// Disabled because Chrome does not support selection on email field type. Replaced by manual adding of 'autofocus'.
//$(this).find("input:visible").first().focus();
//$(function() {
//	var el = $("input:visible").get(0);
//	var elemLen = el.value.length;
//
//	el.selectionStart = elemLen;
//	el.selectionEnd = elemLen;
//	el.focus();
//});

// Rend la selection d'une personne entièrement clickable
$(".liste-selection-personne").click(function() {
	window.location = $(this).find("a").attr("href");
	return false;
});

// Empèche le coller dans les champs confirmation mot de passe
$(
		'#fos_user_change_password_form_plainPassword_second, #fos_user_registration_form_plainPassword_second, #fos_user_resetting_form_plainPassword_second')
		.bind('paste', function(e) {
			e.preventDefault();
		});

// Empèche l'autocomplete dans les formulaires.
//$('#enrolement_login').val(" ");
//$('#enrolement_login').on('click', function() {
//    this.value = '';
//});

// Click sur un object trigger le premier lien trouvé avec la class clickme
$(".clickme").click(function(e) {
	var link = $(this).find("a.clickmelink").get(0);
	link.click();
	e.StopPropagation();
});



//$(".reports-list > .panel-heading").mouseenter(function () {
//    $(".panel-collapse").fadeIn();
//});
//$(".reports-list > .panel-collapse").mouseleave(function(){
//   $(".panel-collapse").fadeOut();
//});
//$("#fos_user_registration_form_email").inputmask("99/99/9999");
//$('#fos_user_registration_form_email').val('test');








//$(document).ready(function(){
//	  //email mask
//	  $('#fos_user_registration_form_email').inputmask({
//	    mask: "*{1,20}[.*{1,20}][.*{1,20}][.*{1,20}]@*{1,20}[.*{2,6}][.*{1,2}]",
//	    greedy: false,
//	    onBeforePaste: function (pastedValue, opts) {
//	      pastedValue = pastedValue.toLowerCase();
//	      return pastedValue.replace("mailto:", "");
//	    },
//	    definitions: {
//	      '*': {
//	        validator: "[0-9A-Za-z!#$%&'*+/=?^_`{|}~\-]",
//	        cardinality: 1,
//	        casing: "lower"
//	      }
//	    }
//	  });
//	});




// Show and hide more details from reports list page
$('.contentReportPrimary .col-md-8').find('.btn').click(function(e){e.stopPropagation();});
$('.contentReportPrimary .col-md-8').click(function(e){
	e.preventDefault();
	
	$(this).find('.btn').click(function(e){e.stopPropagation();});

	// get id of report to show
	var dataId = $(this).data('id');
	
	$('.contentReportPrimary'+dataId).fadeOut("fast", function(){
	   $('.contentReportSecondary'+dataId).fadeIn("fast");
	   $('.contentReportSecondary'+dataId).find('.btn').click(function(e){e.stopPropagation();});
	});
});

$('.contentReportSecondary').click(function(e){	
	e.preventDefault();
	
	// get id of report to show
	var dataId = $(this).data('id');
	
	$('.contentReportSecondary'+dataId).fadeOut("fast", function(){
	   $('.contentReportPrimary'+dataId).fadeIn("fast");
	});
});

// For mobile devices
$('.contentReportPrimarySm .col-xs-12').find('.btn').click(function(e){e.stopPropagation();});
$('.contentReportPrimarySm .col-xs-12').click(function(e){
	e.preventDefault();
	
	$(this).find('.btn').click(function(e){e.stopPropagation();});

	// get id of report to show
	var dataId = $(this).data('id');
	
	$('.contentReportPrimarySm'+dataId +','+' .contentReportPrimarySm'+dataId+' .reports-list-left-sm').fadeOut("fast", function(){
		$('.contentReportSecondarySm'+dataId).fadeIn("fast");
		$('.contentReportSecondarySm'+dataId).find('.btn').click(function(e){e.stopPropagation();});
	});
});

$('.contentReportSecondarySm').click(function(e){	
	e.preventDefault();
	
	// get id of report to show
	var dataId = $(this).data('id');
	
	$('.contentReportSecondarySm'+dataId).fadeOut("fast", function(){
		   $('.contentReportPrimarySm'+dataId).fadeIn("fast");
		   $('.contentReportPrimarySm'+dataId+' .reports-list-left-sm').fadeIn("fast");
	});
});


// Read reports
$('.cr-read-short').find('.btn').click(function(e){e.stopPropagation();});
$('.cr-read-extended').find('.btn').click(function(e){e.stopPropagation();});

$('.cr-read-short').click(function(e){
	e.preventDefault();
	
	$(this).find('.btn').click(function(e){e.stopPropagation();});

	// get id of report to show
	var dataId = $(this).data('id');
	
	$('.cr-read-short'+dataId).fadeOut("fast", function(){
	   $('.cr-read-extended'+dataId).fadeIn("fast");
	   $('.cr-read-extended'+dataId).find('.btn').click(function(e){e.stopPropagation();});
	});
});

$('.cr-read-extended').click(function(e){	
	e.preventDefault();
	
	// get id of report to show
	var dataId = $(this).data('id');
	
	$('.cr-read-extended'+dataId).fadeOut("fast", function(){
	   $('.cr-read-short'+dataId).fadeIn("fast");
	});
});

// Clearing form fields has to be delayed to prevent Lastpass or other module to fill fields once the page is loaded.
setTimeout(function(){
	$('form').not('[name="recherche_demande"]').each(function() { 
	this.reset();
	});
}, 500);

