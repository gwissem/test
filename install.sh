#!/bin/bash

DOMAIN=$1

# Uncomment configurations in order to use Oracle db instead of MySql
sed -i '/my.listener:/s/^#//g' ./app/app/config/config.yml
sed -i '/class: Doctrine\\DBAL\\Event\\Listeners\\OracleSessionInit/s/^#//g' ./app/app/config/config.yml
sed -i '/tags:/s/^#//g' ./app/app/config/config.yml
sed -i '/- { name: doctrine.event_listener, event: postConnect }/s/^#//g' ./app/app/config/config.yml
sed -i '/arguments:/s/^#//g' ./app/app/config/config.yml
sed -i '/- { NLS_SORT: "FRENCH_M_AI", NLS_COMP: "LINGUISTIC" }/s/^#//g' ./app/app/config/config.yml

# Renaming the domain in the Docker's config files for Apache.
sed -i -e "s/demo/$DOMAIN/g" .env
sed -i -e "s/demo/$DOMAIN/g" varenv.yml

# Add IPs for admin depending on client
case "$DOMAIN" in
	biocentrelab) sed -i -e "s/\[81\.80\.93\.138\,\ 217\.109\.49\.173\,\ 127\.0\.0\.1]/[81\.80\.93\.138\,\ 217\.109\.49\.173\,\ 127\.0\.0\.1\,\ 37\.58\.198\.174]/g" app/app/config/security.yml;;
	laboratoire-bioardaisne) sed -i -e "s/\[81\.80\.93\.138\,\ 217\.109\.49\.173\,\ 127\.0\.0\.1]/[81\.80\.93\.138\,\ 217\.109\.49\.173\,\ 127\.0\.0\.1\,\ 80\.14\.25\.9]/g" app/app/config/security.yml;;
	oriade-noviale) sed -i -e "s/\[81\.80\.93\.138\,\ 217\.109\.49\.173\,\ 127\.0\.0\.1]/[81\.80\.93\.138\,\ 217\.109\.49\.173\,\ 127\.0\.0\.1\,\ 83\.206\.232\.145]/g" app/app/config/security.yml;;
	unilabs) sed -i -e "s/\[81\.80\.93\.138\,\ 217\.109\.49\.173\,\ 127\.0\.0\.1]/[81\.80\.93\.138\,\ 217\.109\.49\.173\,\ 127\.0\.0\.1\,\ 185\.3\.26\.251,\ 185\.3\.26\.253]/g" app/app/config/security.yml;;
    demo) 
    	sed -i -e "s/'https\:\/\/secure\.bioserveur\.com'\ \#//g" app/app/config/config_prod.yml
    	sed -i -e "s/\[81\.80\.93\.138\,\ 217\.109\.49\.173\,\ 127\.0\.0\.1]/[81\.80\.93\.138\,\ 217\.109\.49\.173\,\ 127\.0\.0\.1\,\ 185\.113\.160\.36]/g" app/app/config/security.yml
    	;;
	*) echo $"Usage: $0 {monlabo|reunilab|biocentrelab|bionorma|unilabs|eurofins|genbio|oriade-noviale|sblbio|bioardaisne|laboratoire-parly2|biolab-martinique|axbioocean}";;
esac

