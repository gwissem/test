FROM php:7.1.11-apache
#FROM php:7.0.27-apache

RUN apt-get update && apt-get install -y \
	libpng-dev \
    unzip \
    curl \
    ant \
    zlib1g-dev \
    libicu-dev \
    locales \
    vim \
    mailutils \
    mlocate \
    libaio1 \
    libmcrypt-dev \
    openssl

RUN docker-php-ext-configure intl

RUN docker-php-ext-enable opcache
RUN docker-php-ext-install intl gd mcrypt mbstring opcache
RUN docker-php-ext-enable opcache

RUN pecl install apcu \
  && docker-php-ext-enable apcu
  
RUN a2enmod rewrite
RUN a2enmod ssl
RUN a2enmod headers
RUN a2enmod expires
RUN a2enmod dav
RUN a2enmod dav_fs
RUN a2enmod cgid

ADD conf-available/httpd.custom.conf /etc/apache2/conf-available/httpd.custom.conf
RUN a2enconf httpd.custom

RUN service apache2 restart

# Install Composer
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer
RUN composer --version

# Set timezone
RUN rm /etc/localtime
RUN ln -s /usr/share/zoneinfo/Europe/Paris /etc/localtime
RUN "date"

#
# SEE installing node:
# https://github.com/joyent/node/wiki/Installing-Node.js-via-package-manager
#
RUN apt-get install -y nodejs
RUN apt-get install -y npm
RUN cd /usr/bin; ln -s nodejs node; cd /

#
# Installing SASS/Compass
# http://ndever.net/articles/linux/installing-sass-and-compass-ubuntu-1210-1304
#
RUN apt-get install -y ruby-full rubygems-integration
RUN gem install sass -v 3.4.25
#RUN gem install compass

# Install uglify css
RUN npm install -g uglifycss@0.0.25
RUN npm install -g uglify-js

# Install Oracle driver
ENV LD_LIBRARY_PATH /usr/local/instantclient/
ENV ORACLE_HOME /usr/local/instantclient/
ENV TNS_ADMIN /usr/local/instantclient/
ENV ORACLE_BASE /usr/local/instantclient/
ENV NLS_LANG FRENCH_FRANCE.UTF8

ADD oracle/instantclient-basic-linux.x64-11.2.0.4.0.zip /usr/local/
ADD oracle/instantclient-sdk-linux.x64-11.2.0.4.0.zip /usr/local/
RUN unzip /usr/local/instantclient-basic-linux.x64-11.2.0.4.0.zip -d /usr/local/
RUN unzip /usr/local/instantclient-sdk-linux.x64-11.2.0.4.0.zip -d /usr/local/
RUN rm /usr/local/*11.2.0.4.0.zip

# Prepare oci8
RUN ln -s /usr/local/instantclient_11_2 /usr/local/instantclient
RUN ln -s /usr/local/instantclient/libclntsh.so.11.1 /usr/local/instantclient/libclntsh.so
# Install the OCI8 PHP extension
RUN echo 'instantclient,/usr/local/instantclient' | pecl install -f oci8
RUN echo "extension=oci8.so" > /usr/local/etc/php/conf.d/oci8.ini

# Install emails
# Ajouter le serveur mail de grita dans le fichier de config symfony
RUN apt-get install -q -y ssmtp

# root is the person who gets all mail for userids < 1000
RUN rm /etc/ssmtp/ssmtp.conf
RUN echo "root=${MAIL_ROOT}" >> /etc/ssmtp/ssmtp.conf
RUN echo "mailhub=${MAIL_SERVEUR}" >> /etc/ssmtp/ssmtp.conf
RUN echo "hostname=${MAIL_HOSTNAME}" >> /etc/ssmtp/ssmtp.conf
RUN echo "UseTLS=YES" >> /etc/ssmtp/ssmtp.conf
RUN echo "UseSTARTTLS=YES" >> /etc/ssmtp/ssmtp.conf

# Set up php sendmail config
RUN echo "sendmail_path=sendmail -i -t" >> /usr/local/etc/php/conf.d/php-sendmail.ini

# Enable colors in vi
RUN sed -i '/export LS_OPTIONS/s/^#//g' /root/.bashrc
RUN sed -i '/alias ls=/s/^#//g' /root/.bashrc
RUN sed -i '/alias ll=/s/^#//g' /root/.bashrc
RUN sed -i '/alias l=/s/^#//g' /root/.bashrc

# Install language pack for dates
RUN sed -i '/fr_FR.UTF-8 UTF-8/s/^#//g' /etc/locale.gen
RUN sed -i '/fr_FR/s/^#//g' /etc/locale.gen		
RUN echo "LANG=fr_FR.UTF-8" > /etc/default/locale
RUN echo "LANG=fr_FR" > /etc/default/locale
RUN locale-gen fr_FR.UTF-8
RUN locale-gen fr_FR
RUN update-locale

# Securize ant file for prod
#RUN sed -i '/<target name="init" depends="dropDb,createSchema" \/>/d' /var/www/hpa/build.xml
#RUN sed -i '/<target name="initd" depends="dropDb,createSchema,fixd" \/>/d' /var/www/hpa/build.xml
#RUN sed -i '/<target name="initp" depends="dropDb,createSchema,fixp" \/>/d' /var/www/hpa/build.xml
#RUN sed -i '/<target name="fixd" depends="loadDevFixtures" \/>/d' /var/www/hpa/build.xml
#RUN sed -i '/<target name="regdb" depends="dropDb,createSchema" \/>/d' /var/www/hpa/build.xml
#RUN sed -i '/<target name="dropDb">/,/<\/target>/d' /var/www/hpa/build.xml
#RUN sed -i '/<target name="loadDevFixtures">/,/<\/target>/d' /var/www/hpa/build.xml

# Install crontab
#ADD cron/hpa  /etc/cron.d/hpa
#RUN chmod 0644 /etc/cron.d/hpa
#RUN crontab /etc/cron.d/hpa
#RUN service cron start

WORKDIR /var/www/hpa

